﻿using AqualiaContactWS.Helper;
using AqualiaContactWS.Model.NEO_CallejeroMunicipio;
using AqualiaContactWS.Model.NEO_DocumentacionNecesaria;
using AqualiaContactWS.Model.NEO_EntidadBancaria;
using AqualiaContactWS.Model.NEO_ListadoTiposCliente;
using AqualiaContactWS.Model.NEO_NuevoSuministro;
using AqualiaContactWS.Model.NEO_PedaniaMunicipio;
using System;
using System.Collections.Generic;
using Entidades;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;
using Documento = AqualiaContactWS.Model.NEO_DocumentacionNecesaria.documento;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Suministro" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Suministro.svc or Suministro.svc.cs at the Solution Explorer and start debugging.
    public class Suministro : ISuministro
    {
        /// <summary>
        /// STATUS: OK Abel Gago
        /// TODOO - Se pone código de idioma 3082, que es Castellano, pero vendrá es-es. La conversión se hará más adelante
        /// DESC:
        /// Este servicio devuelve el listado de clientes de una explotación de DIVERSA. Para ello se hacen las validaciones pertinentes y se conecta contra 
        /// DIVERSA a través del procedimiento fapp_selinstporpob y se recuperan los tipos a través de la función fapp_seltiposcli.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoTiposClienteResponse NEO_ListadoTiposCliente(NEO_ListadoTiposClienteRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoTiposClienteResponse resp = new NEO_ListadoTiposClienteResponse();
            var codIdioma = request.idioma == "es-es" ? "3082" : "3082";
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoTiposClienteResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_ListadoTiposClienteResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoTiposClienteResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoTiposClienteResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoTiposClienteResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoTiposClienteResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoTiposClienteResponse)respuestaWs.Usuario();
                }

                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    return (NEO_ListadoTiposClienteResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                QueryExpression q = new QueryExpression(HidrotecTipodecliente.EntityName);
                q.Criteria.Conditions.AddRange(
                    new ConditionExpression(HidrotecTipodecliente.HidrotecIdioma, ConditionOperator.Equal, request.idioma));
                q.ColumnSet.AddColumns(HidrotecTipodecliente.HidrotecIdtipodecliente, HidrotecTipodecliente.PrimaryName);
                q.NoLock = true;
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                List<tipos> clientes = new List<tipos>();
                foreach (Entity fila in res.Entities)
                {
                    tipos tCliente = new tipos();
                    tCliente.codigocliente = fila[HidrotecTipodecliente.HidrotecIdtipodecliente].ToString();
                    tCliente.texto = fila[HidrotecTipodecliente.PrimaryName].ToString();
             
                    clientes.Add(tCliente);
                }

                resp.tiposcliente = clientes;
                if (clientes.Count > 0)
                {
                    resp.codigorespuesta = "OK";
                    resp.textorespuesta = "OK";
                }
                else
                {
                    // resp.codigorespuesta = "KO";
                    // resp.textorespuesta = "KO";
                    return (NEO_ListadoTiposClienteResponse)respuestaWs.Notipocliente();
                }

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ListadoTiposClienteResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK OV - Falta Pasar datos.
        /// DESC:
        /// Este servicio devuelve el listado de documentos necesarios para realizar un alta de suministro en una explotación de DIVERSA. Para ello se hacen las validaciones pertinentes
        /// y se conecta contra DIVERSA a través del procedimiento fapp_selinstporpob y se recuperan los documentos necesarios a través de la función fapp_seldocaltasum.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_DocumentacionNecesariaResponse NEO_DocumentacionNecesaria(NEO_DocumentacionNecesariaRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_DocumentacionNecesariaResponse resp = new NEO_DocumentacionNecesariaResponse();

            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.SinConexion();
                }

                if (request == null)
                {
                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.SinParametros();
                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.CanalEntrada();
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.Idioma();
                }

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.Pais();
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.Usuario();
                }

                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.Codigomunicipio();
                }

                if (request.codigotipocliente == "" || request.codigotipocliente == null)
                {
                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.Codigotipocliente();
                }

                //request.Canalentrada = "CRM";
                //request.Codpais = "34";
                //request.Idioma = "es-es";
                //request.Codigomunicipio = "08123000100";
                ////request.Codigomunicipio = "10801-1";
                //request.Codigotipocliente = "ESP000254851";
                //request.Usuario = "05453623R";

                QueryExpression q = new QueryExpression(HidrotecParametrizacionactividades.EntityName);
                q.ColumnSet.AddColumns(HidrotecParametrizacionactividades.PrimaryName, HidrotecParametrizacionactividades.HidrotecBloqueante);
                q.Criteria.AddCondition(HidrotecParametrizacionactividades.HidrotecTipoactividadtercero, ConditionOperator.Equal, "1");
                q.NoLock = true;
                //
                //Enlace parametrizacion explotacion
                LinkEntity lePaExp = new LinkEntity(HidrotecParametrizacionactividades.EntityName, HidrotecParametrizacionexplotacion.EntityName,
                HidrotecParametrizacionactividades.HidrotecParamexplotacionId, HidrotecParametrizacionexplotacion.PrimaryKey, JoinOperator.Inner);
                lePaExp.Columns.AddColumn(HidrotecParametrizacionexplotacion.PrimaryKey);
                //lePaExp.LinkCriteria.AddCondition(HidrotecTipo.HidrotecIdtipo, ConditionOperator.Equal, "008001");
                //Enlace Explotacion Tipo punto de suministro
                LinkEntity leExp = new LinkEntity(HidrotecParametrizacionexplotacion.EntityName, BusinessUnit.EntityName,
                HidrotecParametrizacionexplotacion.HidrotecExplotacionId, BusinessUnit.PrimaryKey, JoinOperator.Inner);
                leExp.Columns.AddColumn(BusinessUnit.PrimaryKey);
                leExp.LinkCriteria.AddCondition(BusinessUnit.HidrotecEstructuraorganizativa, ConditionOperator.Equal, "6");
                //Enlace configuracion organizativa
                LinkEntity leConf = new LinkEntity(BusinessUnit.EntityName, HidrotecConfiguracionorganizativa.EntityName,
                 BusinessUnit.PrimaryKey, HidrotecConfiguracionorganizativa.HidrotecExplotacionId, JoinOperator.Inner);
                leConf.Columns.AddColumn(HidrotecConfiguracionorganizativa.PrimaryKey);
                //Municipio
                LinkEntity leMunicipio = new LinkEntity(HidrotecConfiguracionorganizativa.EntityName,HidrotecMunicipio.EntityName,
                 HidrotecConfiguracionorganizativa.HidrotecMunicipioId, HidrotecMunicipio.PrimaryKey, JoinOperator.Inner);
                leConf.Columns.AddColumn(HidrotecConfiguracionorganizativa.PrimaryKey);
                leMunicipio.LinkCriteria.AddCondition(HidrotecMunicipio.HidrotecCodigoine, ConditionOperator.Equal, request.codigomunicipio);
                //Documento
                LinkEntity leDocumento = new LinkEntity(HidrotecParametrizacionactividades.EntityName,HidrotecDocumento.EntityName,
                 HidrotecParametrizacionactividades.HidrotecDocumentoId, HidrotecDocumento.PrimaryKey, JoinOperator.Inner);
                leDocumento.Columns.AddColumn(HidrotecDocumento.HidrotecIddocumento);
                leDocumento.EntityAlias = "Documento";

                leConf.LinkEntities.Add(leMunicipio);
                leExp.LinkEntities.Add(leConf);
                lePaExp.LinkEntities.Add(leExp);
                q.LinkEntities.AddRange(lePaExp, leDocumento);
                //
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    resp.documentos = new List<documento>();
                    foreach (Entity item in res.Entities)
                    {
                        Documento documento = new Documento();
                        if (item.Contains("Documento." + HidrotecDocumento.HidrotecIddocumento))
                        {
                            documento.codigodocumento = ((AliasedValue)item.Attributes["Documento." + HidrotecDocumento.HidrotecIddocumento]).Value.ToString();


                        }
                        bool Obligatorio = false;
                        if (item.HasAttributeValue<Boolean>(HidrotecParametrizacionactividades.HidrotecBloqueante))
                        {
                            Obligatorio = item.GetAttributeValue<Boolean>(HidrotecParametrizacionactividades.HidrotecBloqueante);
                                

                        }

                        documento.obligatorio = Obligatorio;
                        if (item.HasAttributeValue<string>(HidrotecParametrizacionactividades.PrimaryName))
                        {
                            documento.titulodocumento= item.GetAttributeValue<string>(HidrotecParametrizacionactividades.PrimaryName);
                        }

                        resp.documentos.Add(documento);

                    }

                    resp.codigorespuesta = "OK";
                    resp.textorespuesta = "OK";
                }
                else
                {

                    return (NEO_DocumentacionNecesariaResponse)respuestaWs.NodocumentacionNecesaria();
                }

                

               
                return resp;
                
                //return (NEO_DocumentacionNecesariaResponse)respuestaWs.ok();
            }
            catch (Exception e)
            {
                return (NEO_DocumentacionNecesariaResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve el listado de las vias de un municipio de una explotación de DIVERSA. Para ello se hacen las validaciones pertinentes y 
        /// se conecta contra DIVERSA a través del procedimiento fapp_selinstporpob y se recuperan los documentos necesarios a través de la función fapp_selvias.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_CallejeroMunicipioResponse NEO_CallejeroMunicipio(NEO_CallejeroMunicipioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_CallejeroMunicipioResponse resp = new NEO_CallejeroMunicipioResponse();
            /*
            request.Canalentrada = "CRM";
            request.Codpais = "34";
            request.Idioma = "es-es";
            //request.Codigomunicipio = "08123000100";
            request.Codigomunicipio = "3063000100";
            request.Usuario = "11843194B";
            */
            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_CallejeroMunicipioResponse)respuestaWs.SinConexion();
                }
                
                if (request == null)
                {
                    return (NEO_CallejeroMunicipioResponse)respuestaWs.SinParametros();
                }
                
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_CallejeroMunicipioResponse)respuestaWs.CanalEntrada();
                }
                
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_CallejeroMunicipioResponse)respuestaWs.Idioma();
                }
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_CallejeroMunicipioResponse)respuestaWs.Pais();
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_CallejeroMunicipioResponse)respuestaWs.Usuario();
                }
                
                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    return (NEO_CallejeroMunicipioResponse)respuestaWs.Codigomunicipio();
                }

                string strAliasVia = "via";
                string strQueryVia = "via.";
                string strAliasTipoVia = "tipovia";
                string strQueryTipoVia = "tipovia.";

                QueryExpression q = new QueryExpression(HidrotecMunicipio.EntityName);
                q.Criteria.AddCondition(HidrotecMunicipio.HidrotecCodigoine, ConditionOperator.Equal, request.codigomunicipio);

                LinkEntity leVia = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecMunicipio.EntityName,
                    LinkFromAttributeName = HidrotecMunicipio.PrimaryKey,
                    LinkToEntityName = HidrotecVia.EntityName,
                    LinkToAttributeName = HidrotecVia.HidrotecMunicipioId,
                    JoinOperator = JoinOperator.Inner,
                    EntityAlias = strAliasVia,
                    Columns = new ColumnSet(HidrotecVia.PrimaryName,
                                            HidrotecVia.HidrotecIdvia),
                };
                LinkEntity leTipoVia = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecVia.EntityName,
                    LinkFromAttributeName = HidrotecVia.HidrotecTipoviaId,
                    LinkToEntityName = HidrotecTipovia.EntityName,
                    LinkToAttributeName = HidrotecTipovia.PrimaryKey,
                    JoinOperator = JoinOperator.Inner,
                    EntityAlias = strAliasTipoVia,
                    Columns = new ColumnSet(HidrotecTipovia.PrimaryName,
                                            HidrotecTipovia.HidrotecIdtipovia),
                };

                leVia.LinkEntities.Add(leTipoVia);
                q.LinkEntities.Add(leVia);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    resp.vias = new List<Model.NEO_CallejeroMunicipio.via>();
                    foreach (Entity item in res.Entities)
                    {
                        Model.NEO_CallejeroMunicipio.via via = new Model.NEO_CallejeroMunicipio.via();

                        via.codigovia = item.Contains(strQueryVia + HidrotecVia.HidrotecIdvia) ? (string)((AliasedValue)item[strQueryVia + HidrotecVia.HidrotecIdvia]).Value : null;
                        via.nombrevia = item.Contains(strQueryVia + HidrotecVia.PrimaryName) ? (string)((AliasedValue)item[strQueryVia + HidrotecVia.PrimaryName]).Value : null;
                        via.codigotipovia = item.Contains(strQueryTipoVia + HidrotecTipovia.HidrotecIdtipovia) ? (string)((AliasedValue)item[strQueryTipoVia + HidrotecTipovia.HidrotecIdtipovia]).Value : null;
                        via.nombretipovia = item.Contains(strQueryTipoVia + HidrotecTipovia.PrimaryName) ? (string)((AliasedValue)item[strQueryTipoVia + HidrotecTipovia.PrimaryName]).Value : null;

                        resp.vias.Add(via);
                    }
                    resp.codigorespuesta = "OK";
                    resp.textorespuesta = "OK";
                }
                else
                {
                    return (NEO_CallejeroMunicipioResponse)respuestaWs.DatosMunicipio();
                }
                return resp;
            }
            catch (Exception e)
            {
                return (NEO_CallejeroMunicipioResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve el listado de pedanías de un municipio de una explotación de DIVERSA. Para ello se hacen las validaciones pertinentes y se conecta 
        /// contra DIVERSA a través del procedimiento fapp_selinstporpob y se recuperan las pedanías a través de la función fov_seldistritosdiv.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_PedaniaMunicipioResponse NEO_PedaniaMunicipio(NEO_PedaniaMunicipioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_PedaniaMunicipioResponse resp = new NEO_PedaniaMunicipioResponse();

            /*
            request.Codigomunicipio = "08123000100";
            */

            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_PedaniaMunicipioResponse)respuestaWs.SinConexion();
                }
                
                if (request == null)
                {
                    return (NEO_PedaniaMunicipioResponse)respuestaWs.SinParametros();
                }
                
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_PedaniaMunicipioResponse)respuestaWs.CanalEntrada();
                }
                
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_PedaniaMunicipioResponse)respuestaWs.Idioma();
                }
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_PedaniaMunicipioResponse)respuestaWs.Pais();
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_PedaniaMunicipioResponse)respuestaWs.Usuario();
                }
                
                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    return (NEO_PedaniaMunicipioResponse)respuestaWs.Codigomunicipio();
                }

                string strAliasPedania = "pedania";
                string strQueryPedania = "pedania.";

                QueryExpression q = new QueryExpression(HidrotecMunicipio.EntityName);
                q.Criteria.AddCondition(HidrotecMunicipio.HidrotecCodigoine, ConditionOperator.Equal, request.codigomunicipio);

                LinkEntity lePedania = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecMunicipio.EntityName,
                    LinkFromAttributeName = HidrotecMunicipio.PrimaryKey,
                    LinkToEntityName = HidrotecPedania.EntityName,
                    LinkToAttributeName = HidrotecPedania.HidrotecMunicipioId,
                    JoinOperator = JoinOperator.Inner,
                    EntityAlias = strAliasPedania,
                    Columns = new ColumnSet(HidrotecPedania.PrimaryName, HidrotecPedania.HidrotecIdpedania),
                };

                q.LinkEntities.Add(lePedania);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {

                    resp.pedanias = new List<Model.NEO_PedaniaMunicipio.pedania>();
                    foreach (Entity item in res.Entities)
                    {
                        Model.NEO_PedaniaMunicipio.pedania pedania = new Model.NEO_PedaniaMunicipio.pedania()
                        {
                            codigopedania = item.Contains(strQueryPedania + HidrotecPedania.HidrotecIdpedania) ? (string)((AliasedValue)item[strQueryPedania + HidrotecPedania.HidrotecIdpedania]).Value : null,
                            nombrepedania = item.Contains(strQueryPedania + HidrotecPedania.PrimaryName) ? (string)((AliasedValue)item[strQueryPedania + HidrotecPedania.PrimaryName]).Value : null
                        };

                        resp.pedanias.Add(pedania);
                    }

                    resp.codigorespuesta = "OK";
                    resp.textorespuesta = "OK";
                }
                else
                {

                    return (NEO_PedaniaMunicipioResponse)respuestaWs.DatosMunicipio();
                }
                return resp;
            }
            catch (Exception e)
            {
                return (NEO_PedaniaMunicipioResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve la información de una entidad bancaria de una explotación de DIVERSA pasándole como parámetros el país, el municipio y el código del banco. 
        /// Para ello se hacen las validaciones pertinentes y se conecta contra DIVERSA a través del procedimiento fapp_selinstporpob y se recuperan la entidad a 
        /// través de la función fov_selbanco.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_EntidadBancariaResponse NEO_EntidadBancaria(NEO_EntidadBancariaRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_EntidadBancariaResponse resp = new NEO_EntidadBancariaResponse();

            /*
            request.Codigomunicipio = "08123000100";
            request.Codigoentidad = "2080";
            request.Codpais = "34";
            */

            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.SinConexion();
                }
                
                if (request == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.SinParametros();
                }
                
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.CanalEntrada();
                }
                
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.Idioma();
                }
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.Pais();
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.Usuario();
                }
                
                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.Codigomunicipio();
                }
                
              /*  if (request.numerocuenta == "" || request.numerocuenta == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.Numerocuenta();
                }*/
                
                if (request.codigoentidad == "" || request.codigoentidad == null)
                {
                    return (NEO_EntidadBancariaResponse)respuestaWs.Codigoentidad();
                }
                
                string strAliasBanco = "banco";
                string strQueryBanco = "banco.";

                QueryExpression q = new QueryExpression(HidrotecPais.EntityName);
                q.Criteria.AddCondition(HidrotecPais.HidrotecIdpais, ConditionOperator.Equal, request.codpais);

                LinkEntity leBanco = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecPais.EntityName,
                    LinkFromAttributeName = HidrotecPais.PrimaryKey,
                    LinkToEntityName = HidrotecBanco.EntityName,
                    LinkToAttributeName = HidrotecBanco.HidrotecPaisId,
                    JoinOperator = JoinOperator.Inner,
                    EntityAlias = strAliasBanco,
                    Columns = new ColumnSet(HidrotecBanco.PrimaryName),
                };
                leBanco.LinkCriteria.AddCondition(HidrotecBanco.HidrotecIdbanco, ConditionOperator.Equal, request.codigoentidad);

                q.LinkEntities.Add(leBanco);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {

                    if (res.Entities.Any())
                    {
                        resp.nombreentidad = (string)((AliasedValue)res.Entities[0][strQueryBanco + HidrotecBanco.PrimaryName]).Value;
                    }

                    resp.codigorespuesta = "OK";
                    resp.textorespuesta = "OK";
                }
                else
                {

                    return (NEO_EntidadBancariaResponse)respuestaWs.EntidadBancariaNoEncontrada();

                }
                return resp;
            }
            catch (Exception e)
            {
                return (NEO_EntidadBancariaResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: KO
        /// DESC:
        /// Este servicio inserta una petición de registro de suministro en la base de datos de CAC, como hace OV. Para ello, se hacen las validaciones pertinentes de datos y 
        /// se inserta en las tablas historicoEstadoSolicitud con el procedimiento pov_inshistsolic y en la tabla informacionSolicitudes con el procedimiento pov_insinforsolic
        /// y la tabla solicitudes con la función fov_inssolicitud.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_NuevoSuministroResponse NEO_NuevoSuministro(NEO_NuevoSuministroRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_NuevoSuministroResponse resp = new NEO_NuevoSuministroResponse();
            /*
            request.Usuario = "05453623A";
            */
            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.SinConexion();
                }

                if (request == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.SinParametros();
                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.CanalEntrada();
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Idioma();
                }
                else
                {
                    request.idioma=Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_NuevoSuministroResponse)respuestaWs.IdiomaNoCorrecto();

                    }

                }

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Pais();
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Usuario();
                }

                if (request.codigotipocliente == "" || request.codigotipocliente == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Codigotipocliente();
                }

                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Codigomunicipio();
                }

                if (request.datoscobro == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.DatosCobro();
                }

                if (request.datoscorrespondencia == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Datoscorrespondencia();
                }

                if (request.datossuministro == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Datossuministro();
                }

                if (request.datostitular == null)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Datostitular();
                }

                Entity incident = new Entity(Incident.EntityName);

                // 1. Primero comprobamos si el contacto existe. Si no existe, lo creamos.

                QueryExpression q = new QueryExpression(Contact.EntityName);
                q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                string idContactoSolicitante = "";
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.ContactoSolicitanteNoExiste();

                }
                else
                {

                    idContactoSolicitante = res.Entities[0].Id.ToString();

                }

               
                string IdContactoTitular = compararDatosContacto(request.datostitular.titularnombre, request.datostitular.titularapellido1, request.datostitular.titularapellido2, request.datostitular.titularnumerodocumento);
                if (IdContactoTitular=="")
                {
                    Entity contact = new Entity(Contact.EntityName);

                    contact[Contact.HidrotecNombre] = request.datostitular.titularnombre;
                    contact[Contact.HidrotecPrimerapellido] = request.datostitular.titularapellido1;
                    contact[Contact.HidrotecSegundoapellido] = request.datostitular.titularapellido1;
                    contact[Contact.Fax] = request.datostitular.titularfax;

                    contact[Contact.Birthdate] = Convert.ToDateTime(request.datostitular.titularfechanacimiento);

                    contact[Contact.HidrotecNumerodocumento] = request.datostitular.titularnumerodocumento;

                    contact[Contact.TelePhone1] = request.datostitular.titulartelefono1;
                    contact[Contact.TelePhone2] = request.datostitular.titulartelefono2;
                   // contact[Contact.TelePhone3] = request.datostitular.titulartelefono3;

                    q = new QueryExpression(HidrotecTipodedocumento.EntityName);
                    q.Criteria.AddCondition(HidrotecTipodedocumento.HidrotecIdtipodedocumento, ConditionOperator.Equal, request.datostitular.titulartipodocumento);

                    EntityCollection res2 = Utils.IOS.RetrieveMultiple(q);
                    if (res2.Entities.Any() == false)
                    {
                        return (NEO_NuevoSuministroResponse)respuestaWs.TipoDocumentoTitular();

                    }

                    contact[Contact.HidrotecTipodedocumentoId] = new EntityReference(HidrotecTipodedocumento.EntityName, new Guid(res2.Entities[0][HidrotecTipodedocumento.PrimaryKey].ToString()));

                    q = new QueryExpression(HidrotecPais.EntityName);
                    q.Criteria.AddCondition(HidrotecPais.HidrotecIdpais, ConditionOperator.Equal, request.datostitular.titularcodigopaisdocumento);
                    res2 = Utils.IOS.RetrieveMultiple(q);
                    if (res2.Entities.Any() == false)
                    {

                        return (NEO_NuevoSuministroResponse)respuestaWs.PaisTitular();

                    }
                    contact[Contact.HidrotecPaisidDocumento] = new EntityReference(HidrotecPais.EntityName, new Guid(res2.Entities[0][HidrotecPais.PrimaryKey].ToString()));

                    var id = Utils.IOS.Create(contact);
                    IdContactoTitular = id.ToString();
                }
                else
                {
                    IdContactoTitular = res.Entities[0].Id.ToString();

                }

                // 2. Ya existe seguro el contacto, y tenemos su GUID. procedemos a crear el expediente.

                //q = new QueryExpression(Contact.EntityName);
                //q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                //res = Utils.IOS.RetrieveMultiple(q);

                incident[Incident.CustomerId] = new EntityReference(Contact.EntityName, new Guid(idContactoSolicitante));
                if (idContactoSolicitante != IdContactoTitular)
                {
                    incident[Incident.HidrotecContactOtitularId] = new EntityReference(Contact.EntityName, new Guid(IdContactoTitular));
                }
                incident[Incident.HidrotecTelefonofijoTitular] = request.datostitular.titulartelefono1 != null ? request.datostitular.titulartelefono1 : null;
                incident[Incident.HidrotecTelefonomovilTitular] = request.datostitular.titulartelefono2 != null ? request.datostitular.titulartelefono2 : null;
                incident[Incident.HidrotecFaxTitular] = request.datostitular.titularfax != null ? request.datostitular.titularfax : null;
              

                q = new QueryExpression(HidrotecMunicipio.EntityName);
                q.Criteria.AddCondition(HidrotecMunicipio.HidrotecCodigoine, ConditionOperator.Equal, request.codigomunicipio);
                res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {

                    return (NEO_NuevoSuministroResponse)respuestaWs.DatosMunicipio();
                }
                incident[Incident.HidrotecMunicipioId] = new EntityReference(HidrotecMunicipio.EntityName, new Guid(res.Entities[0][HidrotecMunicipio.PrimaryKey].ToString()));
                incident[Incident.HidrotecMunicipioprincipalId] = new EntityReference(HidrotecMunicipio.EntityName, new Guid(res.Entities[0][HidrotecMunicipio.PrimaryKey].ToString()));
                //Se obtiene la explotacion
                q = new QueryExpression(HidrotecConfiguracionorganizativa.EntityName);
                q.ColumnSet.AddColumns(HidrotecConfiguracionorganizativa.HidrotecExplotacionId);
                q.Criteria.AddCondition(new ConditionExpression(HidrotecConfiguracionorganizativa.HidrotecMunicipioId, ConditionOperator.Equal, res.Entities[0][HidrotecMunicipio.PrimaryKey].ToString()));
                res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {

                    incident[Incident.HidrotecConfiguracionorganizativaId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName,res.Entities[0].Id);
                    if (res.Entities[0].HasAttributeValue<EntityReference>(HidrotecConfiguracionorganizativa.HidrotecExplotacionId))
                    {
                        incident[Incident.HidrotecExplotacionId] = new EntityReference(BusinessUnit.EntityName, res.Entities[0].GetAttributeValue<EntityReference>(HidrotecConfiguracionorganizativa.HidrotecExplotacionId).Id);
                    }

                }
                //
                q = new QueryExpression(HidrotecTipodecliente.EntityName);
                q.Criteria.AddCondition(HidrotecTipodecliente.HidrotecIdtipodecliente, ConditionOperator.Equal, request.codigotipocliente);
                q.Criteria.AddCondition(HidrotecTipodecliente.HidrotecIdioma, ConditionOperator.Equal, request.idioma);
                res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.Notipocliente();

                }

                incident[Incident.HidrotecTipodeclienteId] =new EntityReference(HidrotecTipodecliente.EntityName, new Guid(res.Entities[0][HidrotecTipodecliente.PrimaryKey].ToString()));
                if (request.canalentrada.ToUpper() == "APP")
                {
                    incident[Incident.HidrotecCanaldeentrada] = new OptionSetValue(0);


                }
                q = new QueryExpression(HidrotecRelacion.EntityName);
                q.ColumnSet.AddColumns(HidrotecRelacion.HidrotecIdrelacion, HidrotecRelacion.PrimaryKey);
                LinkEntity leTipo = new LinkEntity(HidrotecRelacion.EntityName, HidrotecTipo.EntityName,
                    HidrotecRelacion.PrimaryKey, HidrotecTipo.HidrotecRelacionId, JoinOperator.Inner);
                leTipo.EntityAlias = "Tipo";
                leTipo.Columns.AddColumns(HidrotecTipo.HidrotecIdtipo,HidrotecTipo.PrimaryKey);
                LinkEntity leSubtipo = new LinkEntity(HidrotecTipo.EntityName, HidrotecSubtipo.EntityName,
                    HidrotecTipo.PrimaryKey, HidrotecSubtipo.HidrotecTipoId, JoinOperator.Inner);
                leSubtipo.EntityAlias = "Subtipo";
                leSubtipo.LinkCriteria.AddCondition(new ConditionExpression(HidrotecSubtipo.HidrotecIdsubtipo,ConditionOperator.Equal, "013002001"));
                leSubtipo.Columns.AddColumns(HidrotecSubtipo.HidrotecIdsubtipo,HidrotecSubtipo.PrimaryKey);
                leTipo.LinkEntities.Add(leSubtipo);
                q.LinkEntities.Add(leTipo);
                res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    incident[Incident.HidrotecRelacionId]= new EntityReference(HidrotecRelacion.EntityName,new Guid(res.Entities[0].Id.ToString()));
                    string idTipo = ((AliasedValue)res.Entities[0].Attributes["Tipo." + HidrotecTipo.PrimaryKey]).Value.ToString();
                    incident[Incident.HidrotecTipoId] = new EntityReference(HidrotecTipo.EntityName, new Guid(idTipo));
                    string idSubTipo = ((AliasedValue)res.Entities[0].Attributes["Subtipo." + HidrotecSubtipo.PrimaryKey]).Value.ToString();
                    incident[Incident.HidrotecSubtipoId] = new EntityReference(HidrotecSubtipo.EntityName, new Guid(idSubTipo));
                }
                
                //Datos Correspondencia
                string idContactoCorrespondencia = "";
                idContactoCorrespondencia = compararDatosContacto(request.datoscorrespondencia.correspondencianombre, request.datoscorrespondencia.correspondenciaapellido1, request.datoscorrespondencia.correspondenciaapellido2, request.datoscorrespondencia.correspondencianumerodocumento);
                if (idContactoCorrespondencia == "")
                {
                    Entity contact = new Entity(Contact.EntityName);

                    contact[Contact.HidrotecNombre] = request.datoscorrespondencia.correspondencianombre;
                    contact[Contact.HidrotecPrimerapellido] = request.datoscorrespondencia.correspondenciaapellido1;
                    contact[Contact.HidrotecSegundoapellido] = request.datoscorrespondencia.correspondenciaapellido2;
                    contact[Contact.Fax] = request.datoscorrespondencia.correspondenciafax;



                    contact[Contact.HidrotecNumerodocumento] = request.datoscorrespondencia.correspondencianumerodocumento;

                    contact[Contact.TelePhone1] = request.datoscorrespondencia.correspondenciatelefono1;
                    contact[Contact.TelePhone2] = request.datoscorrespondencia.correspondenciatelefono2;

                    q = new QueryExpression(HidrotecTipodedocumento.EntityName);
                    q.Criteria.AddCondition(HidrotecTipodedocumento.HidrotecIdtipodedocumento, ConditionOperator.Equal, request.datoscorrespondencia.correspondenciacodigotipodocumento);

                    EntityCollection res2 = Utils.IOS.RetrieveMultiple(q);
                    if (res2.Entities.Any() == false)
                    {
                        return (NEO_NuevoSuministroResponse)respuestaWs.TipoDocumentoContactoCorrespondencia();

                    }

                    contact[Contact.HidrotecTipodedocumentoId] = new EntityReference(HidrotecTipodedocumento.EntityName, new Guid(res2.Entities[0][HidrotecTipodedocumento.PrimaryKey].ToString()));

                    q = new QueryExpression(HidrotecPais.EntityName);
                    q.Criteria.AddCondition(HidrotecPais.HidrotecIdpais, ConditionOperator.Equal, request.datoscorrespondencia.correspondenciacodigopaisdocumento);
                    res2 = Utils.IOS.RetrieveMultiple(q);
                    if (res2.Entities.Any() == false)
                    {

                        return (NEO_NuevoSuministroResponse)respuestaWs.PaisContactoCorrespondencia();

                    }
                    contact[Contact.HidrotecPaisidDocumento] = new EntityReference(HidrotecPais.EntityName, new Guid(res2.Entities[0][HidrotecPais.PrimaryKey].ToString()));

                    var id = Utils.IOS.Create(contact);
                    idContactoCorrespondencia = id.ToString();
                }

                if (idContactoCorrespondencia == idContactoSolicitante)
                {
                    incident[Incident.HidrotecContactOcorrespondencia] = new OptionSetValue(1);

                }
                else
                {
                    if (idContactoCorrespondencia == IdContactoTitular)
                    {

                        incident[Incident.HidrotecContactOcorrespondencia] = new OptionSetValue(1);

                    }
                    else
                    {

                        incident[Incident.HidrotecContactOcorrespondencia] = new OptionSetValue(2);
                        incident[Incident.HidrotecContactOcorrespondenciaId] = new EntityReference(Contact.EntityName,new Guid(idContactoCorrespondencia));
                    }

                }
                //Se almacena el primer telefono
                incident[Incident.HidrotecTelefonofijoCorrespondencia] = request.datoscorrespondencia.correspondenciatelefono1 != null ? request.datoscorrespondencia.correspondenciatelefono1 : null;
                incident[Incident.HidrotecTelefonomovilCorrespondencia] = request.datoscorrespondencia.correspondenciatelefono2 != null ? request.datoscorrespondencia.correspondenciatelefono2 : null;
                incident[Incident.HidrotecFaxCorrespondencia] = request.datoscorrespondencia.correspondenciafax != null ? request.datoscorrespondencia.correspondenciafax : null;
                incident[Incident.HidrotecEMailCorrespondencia]= request.datoscorrespondencia.correspondenciaemail != null ? request.datoscorrespondencia.correspondenciaemail : null;
                //Pais
                if ((request.datoscorrespondencia.correspondenciapais != "") && (request.datoscorrespondencia.correspondenciapais != null))
                {
                    Guid IdPaisCorrespondencia = Utils.takeField(Utils.IOS, HidrotecPais.EntityName, HidrotecPais.HidrotecIdpais, request.datoscorrespondencia.correspondenciapais);
                    if (IdPaisCorrespondencia != Guid.Empty)
                    {
                        incident[Incident.HidrotecPaisauxiliarId] = new EntityReference(HidrotecPais.EntityName, IdPaisCorrespondencia);
                    }
                }
                //Provincia
                if ((request.datoscorrespondencia.correspondenciacodigomunicipio != "") && (request.datoscorrespondencia.correspondenciacodigomunicipio != null))
                {
                    QueryExpression qMunicipio = new QueryExpression(HidrotecMunicipio.EntityName);
                    qMunicipio.ColumnSet.AddColumns(HidrotecMunicipio.PrimaryKey, HidrotecMunicipio.HidrotecProvinciaId);
                    qMunicipio.Criteria.AddCondition(new ConditionExpression(HidrotecMunicipio.HidrotecIdmunicipio, ConditionOperator.Equal, request.datoscorrespondencia.correspondenciacodigomunicipio));
                    EntityCollection resMunicipio = Utils.IOS.RetrieveMultiple(qMunicipio);
                    if (resMunicipio.Entities.Any())
                    {
                        if (resMunicipio.Entities[0].HasAttributeValue<EntityReference>(HidrotecMunicipio.HidrotecProvinciaId))
                        {
                            incident[Incident.HidrotecProvinciaauxiliarId] = resMunicipio.Entities[0].GetAttributeValue<EntityReference>(HidrotecMunicipio.HidrotecProvinciaId);

                        }
                        incident[Incident.HidrotecMunicipioauxiliarId] = new EntityReference(HidrotecMunicipio.EntityName, resMunicipio.Entities[0].Id);


                    }
                }
                else
                {

                    incident[Incident.HidrotecMunicipioauxiliar]= request.datoscorrespondencia.correspondenciamunicipiosincodificar != null ? request.datoscorrespondencia.correspondenciamunicipiosincodificar : null;

                }
                //Via
                if ((request.datoscorrespondencia.correspondenciavia.codigovia != "") && (request.datoscorrespondencia.correspondenciavia.codigovia != null))
                {
                    Guid idVia = Utils.takeField(Utils.IOS, HidrotecVia.EntityName, HidrotecVia.HidrotecIdvia, request.datoscorrespondencia.correspondenciavia.codigovia);
                    if (idVia != Guid.Empty)
                    {
                        incident[Incident.HidrotecViaauxiliarId] = new EntityReference(HidrotecVia.EntityName, idVia);

                    }

                }
                else
                {
                    incident[Incident.HidrotecViaauxiliar]=request.datoscorrespondencia.correspondenciavia.nombrevia != null ? request.datoscorrespondencia.correspondenciavia.nombrevia : null;


                }
                //Se rellena el numero

                incident[Incident.HidrotecNumeroauxiliar] = request.datoscorrespondencia.correspondencianumero != null ? request.datoscorrespondencia.correspondencianumero : null;

                //Se rellena el codigo postal
                incident[Incident.HidrotecCodigoPostalAuxiliar] = request.datoscorrespondencia.correspondenciacodigopostal != null ? request.datoscorrespondencia.correspondenciacodigopostal : null;
                
                //Se rellena el bloque
                incident[Incident.HidrotecBloqueauxiliar] = request.datoscorrespondencia.correspondenciabloque != null ? request.datoscorrespondencia.correspondenciabloque:null;
                //Edificio

                incident[Incident.HidrotecEdificioauxiliar] = request.datoscorrespondencia.correspondenciaedificio != null ? request.datoscorrespondencia.correspondenciaedificio : null;
                //Numero
                incident[Incident.HidrotecNumeroauxiliar] = request.datoscorrespondencia.correspondencianumero != null ? request.datoscorrespondencia.correspondencianumero : null;

                //Puerta
                incident[Incident.HidrotecPuertaauxiliar] = request.datoscorrespondencia.correspondenciapuerta != null ? request.datoscorrespondencia.correspondenciapuerta : null;
                //Escalera
                incident[Incident.HidrotecEscaleraauxiliar] = request.datoscorrespondencia.correspondenciaescalera != null ? request.datoscorrespondencia.correspondenciaescalera : null;
                //otros
                incident[Incident.HidrotecOtrosauxiliar] = request.datoscorrespondencia.correspondenciaotros != null ? request.datoscorrespondencia.correspondenciaotros : null;
                //Pedania
                if ((request.datoscorrespondencia.correspondenciacodigopedania != null) && (request.datoscorrespondencia.correspondenciacodigopedania != ""))
                {
                    Guid idPedania = Utils.takeField(Utils.IOS, HidrotecPedania.EntityName, HidrotecPedania.HidrotecIdpedania, request.datoscorrespondencia.correspondenciacodigopedania);
                    if (idPedania != Guid.Empty)
                    {
                        incident[Incident.HidrotecPedaniaauxiliarId] = new EntityReference(HidrotecPedania.EntityName, idPedania);
                    }
                }
                //Direccion compuesta
                incident[Incident.HidrotecDireccioncompuestaauxiliar]= request.datoscorrespondencia.correspondenciadirsincodificar != null ? request.datoscorrespondencia.correspondenciadirsincodificar : null;



                // Datos Cobro
                //Primero se comprueba si el id del contacto de cobro existe
                string idContactoCobro = compararDatosContacto(request.datoscobro.cobronombre, request.datoscobro.cobroapellido1, request.datoscobro.cobroapellido2, request.datoscobro.cobronumerodocumento);
                if (idContactoCobro == "")
                {
                    Entity contact = new Entity(Contact.EntityName);

                    contact[Contact.HidrotecNombre] = request.datoscobro.cobronombre;
                    contact[Contact.HidrotecPrimerapellido] = request.datoscobro.cobroapellido1;
                    contact[Contact.HidrotecSegundoapellido] = request.datoscobro.cobroapellido2;
                    contact[Contact.Fax] = request.datoscobro.cobrofax;


                    contact[Contact.HidrotecNumerodocumento] = request.datoscobro.cobronumerodocumento;

                    contact[Contact.TelePhone1] = request.datoscobro.cobrotelefono1;
                    contact[Contact.TelePhone2] = request.datoscobro.cobrotelefono2;
                    // contact[Contact.TelePhone3] = request.datostitular.titulartelefono3;

                    q = new QueryExpression(HidrotecTipodedocumento.EntityName);
                    q.Criteria.AddCondition(HidrotecTipodedocumento.HidrotecIdtipodedocumento, ConditionOperator.Equal, request.datoscobro.cobrotipodocumento);

                    EntityCollection res2 = Utils.IOS.RetrieveMultiple(q);
                    if (res2.Entities.Any() == false)
                    {
                        return (NEO_NuevoSuministroResponse)respuestaWs.TipoDocumentoContactoCobro();

                    }

                    contact[Contact.HidrotecTipodedocumentoId] = new EntityReference(HidrotecTipodedocumento.EntityName, new Guid(res2.Entities[0][HidrotecTipodedocumento.PrimaryKey].ToString()));

                    q = new QueryExpression(HidrotecPais.EntityName);
                    q.Criteria.AddCondition(HidrotecPais.HidrotecIdpais, ConditionOperator.Equal, request.datoscobro.cobrocodigopaisdocumento);
                    res2 = Utils.IOS.RetrieveMultiple(q);
                    if (res2.Entities.Any() == false)
                    {

                        return (NEO_NuevoSuministroResponse)respuestaWs.PaisContactoCobro();

                    }
                    contact[Contact.HidrotecPaisidDocumento] = new EntityReference(HidrotecPais.EntityName, new Guid(res2.Entities[0][HidrotecPais.PrimaryKey].ToString()));

                    var id = Utils.IOS.Create(contact);
                    idContactoCobro = id.ToString();
                }
                if (idContactoCobro == idContactoSolicitante)
                {
                    incident[Incident.HidrotecContactOcobro] = new OptionSetValue(1);
                }
                else
                {
                    if (IdContactoTitular == idContactoCobro)
                    {
                        incident[Incident.HidrotecContactOcobro] = new OptionSetValue(2);


                    }
                    else
                    {
                        if (idContactoCorrespondencia == idContactoCobro)
                        {
                            incident[Incident.HidrotecContactOcobro] = new OptionSetValue(2);
                        }
                        else
                        {
                            incident[Incident.HidrotecContactOcobro] = new OptionSetValue(4);
                            incident[Incident.HidrotecContactOcobroId] = new EntityReference(Contact.EntityName, new Guid(idContactoCobro));
                        }

                    }
                }

                
                incident[Incident.HidrotecTelefonofijoCobro] = request.datoscobro.cobrotelefono1;
                incident[Incident.HidrotecTelefonomovilCobro] = request.datoscobro.cobrotelefono2;
                incident[Incident.HidrotecEMailCobro] = null;
                incident[Incident.HidrotecFaxCobro] = request.datoscobro.cobrofax;


                incident[Incident.HidrotecImportEmaximo] = new Money(request.datoscobro.importemaximopago);
                incident[Incident.HidrotecFechalimite] = Convert.ToDateTime(request.datoscobro.fechalimitepago);

                q = new QueryExpression(HidrotecBanco.EntityName);
                q.Criteria.AddCondition(HidrotecBanco.HidrotecIdbanco, ConditionOperator.Equal, request.datoscobro.cobrobanco);
                q.ColumnSet.AddColumns(HidrotecBanco.PrimaryKey, HidrotecBanco.HidrotecPaisId);
                res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {
                    return (NEO_NuevoSuministroResponse)respuestaWs.BancoNoEncontrado();


                }
                incident[Incident.HidrotecDomiciliado] = true;
                incident[Incident.HidrotecBancoId] = new EntityReference(HidrotecBanco.EntityName, new Guid(res.Entities[0][HidrotecBanco.PrimaryKey].ToString()));
                incident[Incident.HidrotecDigitocontrolccc] = request.datoscobro.cobrodc;
                incident[Incident.HidrotecCodigobanco] = request.datoscobro.cobrobanco;
                incident[Incident.HidrotecCodigosucursal] = request.datoscobro.cobrosucursal;
                incident[Incident.HidrotecNumerodecuenta] = request.datoscobro.cobrocuenta;
                Guid IdPais = Utils.takeField(Utils.IOS, HidrotecPais.EntityName, HidrotecPais.HidrotecLetraiban, "ES");
                incident[Incident.HidrotecPaisibanId] = new EntityReference(HidrotecPais.EntityName, IdPais);
                incident[Incident.HidrotecIban] = calcularIban(request.datoscobro.cobrobanco + request.datoscobro.cobrosucursal + request.datoscobro.cobrodc + request.datoscobro.cobrocuenta);
                incident[Incident.HidrotecDigitocontroliban] = calcularIban(request.datoscobro.cobrobanco + request.datoscobro.cobrosucursal + request.datoscobro.cobrodc + request.datoscobro.cobrocuenta).Substring(2, 2);

                //Datos suministro
                if (idContactoSolicitante == IdContactoTitular)
                {

                    incident[Incident.HidrotecTitularsolicitante] = true;
                }
                else
                {
                    incident[Incident.HidrotecTitularsolicitante] = false;

                }
                if ((request.datossuministro.suministrovia.codigovia != null) && (request.datossuministro.suministrovia.codigovia != ""))
                {
                    QueryExpression qViaSuministro = new QueryExpression(HidrotecVia.EntityName);
                    qViaSuministro.Criteria.AddCondition(new ConditionExpression(HidrotecVia.HidrotecIdvia,ConditionOperator.Equal, request.datossuministro.suministrovia.codigovia));
                    qViaSuministro.ColumnSet.AddColumns(HidrotecVia.PrimaryKey,HidrotecVia.HidrotecMunicipioId);
                    LinkEntity leMunicipio = new LinkEntity(HidrotecVia.EntityName, HidrotecMunicipio.EntityName,
                        HidrotecVia.HidrotecMunicipioId, HidrotecMunicipio.PrimaryKey, JoinOperator.Inner);
                    leMunicipio.EntityAlias="municipio";
                    leMunicipio.Columns.AddColumn(HidrotecMunicipio.HidrotecProvinciaId);
                    qViaSuministro.LinkEntities.Add(leMunicipio);
                    EntityCollection resViaSuministro = Utils.IOS.RetrieveMultiple(qViaSuministro);
                    if (resViaSuministro.Entities.Any())
                    {
                        incident[Incident.HidrotecViasuministroId] = new EntityReference(HidrotecVia.EntityName, resViaSuministro.Entities[0].Id);
                        if (resViaSuministro.Entities[0].HasAttributeValue<EntityReference>(HidrotecVia.HidrotecMunicipioId))
                        {

                            incident[Incident.HidrotecMunicipiosuministroId] = resViaSuministro.Entities[0].GetAttributeValue<EntityReference>(HidrotecVia.HidrotecMunicipioId);

                        }
                        if (resViaSuministro.Entities[0].Contains("municipio." + HidrotecMunicipio.HidrotecProvinciaId))
                        {
                            //mirar en las pruebas
                            incident[Incident.HidrotecProvinciasuministroId] =  (((AliasedValue)resViaSuministro.Entities[0].Attributes["municipio." + HidrotecMunicipio.HidrotecProvinciaId]).Value);

                        }
                    }
                    




                }
                //Numero direccion de suministro

                incident[Incident.HidrotecNumerosuministro]= request.datossuministro.suministronumero != null ? request.datossuministro.suministronumero : null;
                //Codigo Postal Direccion de suministro

                incident[Incident.HidrotecCodigoPostalSuministro] = request.datossuministro.suministrocodigopostal != null ? request.datossuministro.suministrocodigopostal : null;
                //Bloque direccion de suministro
                incident[Incident.HidrotecBloquesuministro] = request.datossuministro.suministrobloque!= null ? request.datossuministro.suministrobloque: null;
                //Edificio
                incident[Incident.HidrotecEdificiosuministro] = request.datossuministro.suministroedificio != null ? request.datossuministro.suministroedificio : null;
                //Piso
                incident[Incident.HidrotecPisosuministro] = request.datossuministro.suministropiso != null ? request.datossuministro.suministropiso : null;
                //Puerta
                incident[Incident.HidrotecPuertasuministro] = request.datossuministro.suministropuerta != null ? request.datossuministro.suministropuerta : null;
                //Escaleras
                incident[Incident.HidrotecEscalerasuministro] = request.datossuministro.suministroescalera != null ? request.datossuministro.suministroescalera : null;
                //Otros
                incident[Incident.HidrotecOtrossuministro] = request.datossuministro.suministrootros != null ? request.datossuministro.suministrootros : null;
                //Pedania
                if ((request.datossuministro.suministrocodigopedania != "") && (request.datossuministro.suministrocodigopedania != ""))
                {
                    Guid IdPedaniaSuministro = Utils.takeField(Utils.IOS, HidrotecPedania.EntityName, HidrotecPedania.HidrotecIdpedania, request.datossuministro.suministrocodigopedania);
                    if (IdPedaniaSuministro != Guid.Empty)
                    {

                        incident[Incident.HidrotecPedaniasuministroId] = new EntityReference(HidrotecPedania.EntityName,IdPedaniaSuministro);

                    }
                }

                Utils.IOS.Create(incident);




                resp.Codigorespuesta = "OK";
                resp.Textorespuesta = "OK";

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_NuevoSuministroResponse)respuestaWs.Exception(e);
            }
        }

        /* public static string calculariban(string ccc)
         {
             string res = "-1";
             string cifras = ccc + "1428" + "00";
             var resto = modulo(cifras, 97);
             res = "ES"+ cerosIzquierda((98 - resto).ToString(), 2) + ccc;
             return res;
         }
         public static string cerosIzquierda(string cifras,int  largo)
         {
             cifras += "";
             while (cifras.Length < largo) { cifras = '0' + cifras; }
             return cifras;
         }
         public static long modulo(string cifras, int divisor)
         {

             var CUENTA = 10;
             var largo = cifras.Length;
             long resto = 0;
             for (var i = 0; i < largo; i += CUENTA)
             {
                 var dividendo = resto.ToString() + "" + cifras.Substring(i, CUENTA);
                 resto = long.Parse(dividendo) % divisor ;
             }
             return resto;
         }*/

        public static string calcularIban(string ccco)
        {
            // Calculamos el IBAN
            string ccc = ccco.Trim();
            if (ccc.Length != 20)
            {
                return "La CCC debe tener 20 dígitos";
            }
            // Le añadimos el codigo del pais al ccc
            ccc = ccc + "142800";

            // Troceamos el ccc en partes (26 digitos)
            string[] partesCCC = new string[5];
            partesCCC[0] = ccc.Substring(0, 5);
            partesCCC[1] = ccc.Substring(5, 5);
            partesCCC[2] = ccc.Substring(10, 5);
            partesCCC[3] = ccc.Substring(15, 5);
            partesCCC[4] = ccc.Substring(20, 6);

            int iResultado = int.Parse(partesCCC[0]) % 97;
            string resultado = iResultado.ToString();
            for (int i = 0; i < partesCCC.Length - 1; i++)
            {
                iResultado = int.Parse(resultado + partesCCC[i + 1]) % 97;
                resultado = iResultado.ToString();
            }
            // Le restamos el resultado a 98
            int iRestoIban = 98 - int.Parse(resultado);
            string restoIban = iRestoIban.ToString();
            if (restoIban.Length == 1)
                restoIban = "0" + restoIban;

            return "ES" + restoIban + ccco;
        }

        public static bool compararDatosContacto(string NombreTitular,string Apellido1Titular,string Apellido2Titular, string documentoTitular, string NombreComparado, string Apellido1Comparado, string Apellido2Comparado, string documentoComparado)
        {
            bool resultado = false;
            if (NombreTitular == NombreComparado)
            {
                if (Apellido1Titular == Apellido1Comparado)
                {
                    if (Apellido2Titular == Apellido2Comparado)
                    {
                        if (documentoTitular == documentoComparado)
                        {

                            resultado = true;

                        }

                    }


                }


            }

            return resultado;
        }
        public static string compararDatosContacto(string Nombre, string Apellido1, string Apellido2, string documento)
        {
            string resultado = "";
            QueryExpression q = new QueryExpression(Contact.EntityName);
            q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, documento);
            q.Criteria.AddCondition(Contact.HidrotecNombre, ConditionOperator.Equal, Nombre);
            q.Criteria.AddCondition(Contact.HidrotecPrimerapellido, ConditionOperator.Equal, Apellido1);
            q.Criteria.AddCondition(Contact.HidrotecSegundoapellido, ConditionOperator.Equal, Apellido2);
            EntityCollection res = Utils.IOS.RetrieveMultiple(q);
            if (res.Entities.Any())
            {
                resultado = res.Entities[0].Id.ToString();
            }
            return resultado;
        }



      }
    }
