﻿using AqualiaContactWS.Model.NEO_CallejeroMunicipio;
using AqualiaContactWS.Model.NEO_DocumentacionNecesaria;
using AqualiaContactWS.Model.NEO_EntidadBancaria;
using AqualiaContactWS.Model.NEO_ListadoTiposCliente;
using AqualiaContactWS.Model.NEO_NuevoSuministro;
using AqualiaContactWS.Model.NEO_PedaniaMunicipio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISuministro" in both code and config file together.
    [ServiceContract]
    public interface ISuministro
    {
        [OperationContract]
        NEO_ListadoTiposClienteResponse NEO_ListadoTiposCliente(NEO_ListadoTiposClienteRequest request);

        [OperationContract]
        NEO_DocumentacionNecesariaResponse NEO_DocumentacionNecesaria(NEO_DocumentacionNecesariaRequest request);

        [OperationContract]
        NEO_CallejeroMunicipioResponse NEO_CallejeroMunicipio(NEO_CallejeroMunicipioRequest request);

        [OperationContract]
        NEO_PedaniaMunicipioResponse NEO_PedaniaMunicipio(NEO_PedaniaMunicipioRequest request);

        [OperationContract]
        NEO_EntidadBancariaResponse NEO_EntidadBancaria(NEO_EntidadBancariaRequest request);

        [OperationContract]
        NEO_NuevoSuministroResponse NEO_NuevoSuministro(NEO_NuevoSuministroRequest request);

    }
}
