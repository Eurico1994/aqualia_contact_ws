﻿using WS.Usuario.Model.NEO_AltaUsuario;
using WS.Usuario.Model.NEO_AutenticationUsuario;
using WS.Usuario.Model.NEO_BajaUsuario;
using WS.Usuario.Model.NEO_CambiarClave;
using WS.Usuario.Model.NEO_CambiarPreguntaSeguridadUsuario;
using WS.Usuario.Model.NEO_ComprobarUsuario;
using WS.Usuario.Model.NEO_ListadoMotivosBaja;
using WS.Usuario.Model.NEO_PreguntaSeguridadUsuario;
using WS.Usuario.Model.NEO_RecordarClave;
using WS.Usuario.Model.NEO_TiposIdentificacionFiscal;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using AqualiaContactWS.Helper;
using Microsoft.Xrm.Sdk.Query;
using System.Linq;
using Entidades;
using System.Text.RegularExpressions;
using System.Web.Security;
using Microsoft.Crm.Sdk.Messages;
using System.Security.Cryptography;
using System.Text;

namespace WS.Usuario
{
    public class Usuario : IUsuario
    {
        private const string versionFecha = "[v190411]";

        /// <summary>
        /// STATUS: OK Abel Gago
        /// TODOO: Pdte Preguntar a Eurico cuando se debe o no crear el Usuario OV, a través de la empresa id distinto de nulo o con que condiciones exactamente.
        /// DESC:
        /// Este servicio inserta un usuario en la base de datos. Para ello, hace validaciones del documento de identidad; con la función 
        /// fov_seltipodocid, correo electrónico, existencia de usuario; con la función fapp_selusu, e identificación fiscal; con la función 
        /// fapp_comprobaridfiscal. Una vez validadas si existe el usuario se le introduce un guion seguido de un identificador, 
        /// para la posibilidad de multiusuarios y se cifra la contraseña. Lo siguiente es insertar la información del usuario en la tabla 
        /// usuarios a través del procedimiento papp_insaltausuario y en historico_acciones a través de papp_inshistaccion. 
        /// Al finalizar se envía un correo con las credenciales de registro y se devuelve el identificador por la respuesta del servicio.
        /// NOTA: El numero de contrato tiene que tener el siguiente formato: 10302-1/1-006992 con los guiones y la barra.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_AltaUsuarioResponse NEO_AltaUsuario(NEO_AltaUsuarioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_AltaUsuarioResponse resp = new NEO_AltaUsuarioResponse();

            /*
            request.numerocontrato = "10801-1/1-015742";
            request.numerodocumento = "05453623A";
            request.codigopregunta = "2";
            request.correoelectronico = "email@defaultemail.com";
            request.respuestapregunta = "RespuestaDefault";
            */

            try
            {
                Utils.InicializarVariables();

                
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", "", "SinParametros");

                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {
                   
                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }
                   


                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }
                
               
               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }
                
                if (request.codigopregunta == "" || request.codigopregunta == null)
                {
                    
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CodPregunta");
                }
                
                if (request.respuestapregunta == "" || request.respuestapregunta == null)
                {
                    
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "ResPregunta");
                }
                
                if (request.codigotipodocumento == "" || request.codigotipodocumento == null)
                {
                   
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CodTipoDocumento");
                }
            
                
                if (request.numerodocumento == "" || request.numerodocumento== null)
                {
                    // return (NEO_AltaUsuarioResponse)respuestaWs.NumDocumento();
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "NumDocumento");
                }
                
                if (request.correoelectronico == "" || request.correoelectronico == null)
                {
                    // return (NEO_AltaUsuarioResponse)respuestaWs.Correo();
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Correo");
                }
                
                if (request.numerocontrato == "" || request.numerocontrato == null || !validaFormatoContrato(request.numerocontrato)) 
                {
                    //return (NEO_AltaUsuarioResponse)respuestaWs.Contrato();
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Contrato");
                }
                

                string strAliasContactoOV = "contactoOV";
                string strQueryContactoOV = "contactoOV.";

                QueryExpression q = new QueryExpression(HidrotecContrato.EntityName);
                q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.numerocontrato);
                q.ColumnSet.AddColumns(HidrotecContrato.PrimaryName, HidrotecContrato.HidrotecTitularId);
                
                LinkEntity leContacto = new LinkEntity(HidrotecContrato.EntityName, Contact.EntityName,
                    HidrotecContrato.HidrotecTitularId, Contact.PrimaryKey, JoinOperator.Inner);
                leContacto.LinkCriteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.numerodocumento);
                //El contacto debe ser de tipo clienta
                leContacto.LinkCriteria.AddCondition(Contact.HidrotecCliente, ConditionOperator.Equal, true);
                leContacto.Columns.AddColumn(Contact.PrimaryName);
                leContacto.EntityAlias = "Contacto.";
                LinkEntity leContactoOV = new LinkEntity(Contact.EntityName, HidrotecOvUsuario.EntityName,
                    Contact.PrimaryKey, HidrotecOvUsuario.HidrotecContactOprincipalId, JoinOperator.LeftOuter);
                leContactoOV.Columns.AddColumns(HidrotecOvUsuario.PrimaryKey, HidrotecOvUsuario.PrimaryName, 
                                                HidrotecOvUsuario.HidrotecContactOprincipalId, HidrotecOvUsuario.HidrotecContactOempresaId);
                leContactoOV.EntityAlias = strAliasContactoOV;

              //  leContacto.LinkEntities.Add(leContactoOV);
                q.LinkEntities.Add(leContacto);
                
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

               
                if (res.Entities.Any())
                {
                    EntityReference ReferenciaContacto = res.Entities[0].GetAttributeValue<EntityReference>(HidrotecContrato.HidrotecTitularId);
                    //Se Comprueba si existe el usuarioOV
                    q = new QueryExpression(UsuarioOV.LogicalName);
                    LinkEntity leContac = new LinkEntity(UsuarioOV.LogicalName, Contact.EntityName,
                        UsuarioOV.ContactoPrincipal, Contact.PrimaryKey, JoinOperator.Inner);
                    leContac.LinkCriteria.AddCondition(new ConditionExpression(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.numerodocumento));
                    
                    q.LinkEntities.Add(leContac);
                    res= Utils.IOS.RetrieveMultiple(q);
                    if (res.Entities.Any())
                    {

                       
                        return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "UsuarioExiste");
                    }
                    else
                    {
                       
                        //Se crea el usuario
                        Entity contactOV = new Entity(HidrotecOvUsuario.EntityName);

                        contactOV[HidrotecOvUsuario.PrimaryName] = ReferenciaContacto.Name;
                        contactOV[HidrotecOvUsuario.HidrotecContactOempresaId] = null;
                        contactOV[HidrotecOvUsuario.HidrotecContactOprincipalId] = ReferenciaContacto;
                        contactOV[HidrotecOvUsuario.HidrotecCodigomultiusuario] = null;
                        contactOV[HidrotecOvUsuario.HidrotecEMail] = request.correoelectronico;
                        //contactOV[HidrotecOvUsuario.HidrotecPassword] = "12345";
                        // string clave= Membership.GeneratePassword(8, 2);
                        //contactOV[HidrotecOvUsuario.HidrotecPassword] = Membership.GeneratePassword(8, 2);
                        string clave= GenerarPassword();
                        contactOV[HidrotecOvUsuario.HidrotecPassword] = Encriptar(clave);

                        q = new QueryExpression(HidrotecOvPreguntasdeseguridad.EntityName);
                        q.Criteria.AddCondition(HidrotecOvPreguntasdeseguridad.HidrotecOvIdpregunta, ConditionOperator.Equal, request.codigopregunta);
                        q.Criteria.AddCondition(HidrotecOvPreguntasdeseguridad.HidrotecOvIdioma, ConditionOperator.Equal, request.idioma);
                        q.ColumnSet.AddColumn(HidrotecOvPreguntasdeseguridad.PrimaryName);
                        var res2 = Utils.IOS.RetrieveMultiple(q);
                        if (res2.Entities.Any())
                        {
                            //Si existe la pregunta se crea el contacto
                            Guid idUsuarioOV= Utils.IOS.Create(contactOV);
                            Entity respuestaOV = new Entity(HidrotecOvRespuesta.EntityName);

                            respuestaOV[HidrotecOvRespuesta.PrimaryName] = res2.Entities[0][HidrotecOvPreguntasdeseguridad.PrimaryName];
                            respuestaOV[HidrotecOvRespuesta.HidrotecOvContactOId] =new EntityReference(Contact.EntityName, idUsuarioOV);
                            respuestaOV[HidrotecOvRespuesta.HidrotecOvPreguntaId] = new EntityReference(HidrotecOvPreguntasdeseguridad.EntityName, (Guid)res2.Entities[0][HidrotecOvPreguntasdeseguridad.PrimaryKey]);
                            respuestaOV[HidrotecOvRespuesta.HidrotecOvRespuesta1] = Encriptar(request.respuestapregunta);

                            Utils.IOS.Create(respuestaOV);
                            //
                            //Se crea el expediente que refleja el alta
                            string strRolTitular = "1";
                            string strRelAPP = "008";
                            string strTipoAPP = "008001";
                            string strSubtipoAPP = "008001004";

                            string strAliasTitular = "titular";
                            string strQueryTitular = "titular.";
                            string strAliasContrato = "contract";
                            string strQueryContrato = "contract.";
                            string strAliasDirSumm = "dirSuministro";
                            string strQueryDirSumm = "dirSuministro.";
                            string strAliasConfOrg = "confOrganizativa";
                            string strQueryConfOrg = "confOrganizativa.";

                            HashSet<string> HSContratos = new HashSet<string>();
                            Guid idExp = Guid.Empty;

                            q = new QueryExpression(HidrotecRoldecontrato.EntityName);

                            leContacto = new LinkEntity(HidrotecRoldecontrato.EntityName, Contact.EntityName,
                               HidrotecRoldecontrato.HidrotecContactOId, Contact.PrimaryKey, JoinOperator.Inner);
                            leContacto.LinkCriteria.AddCondition(Contact.PrimaryKey, ConditionOperator.Equal, ReferenciaContacto.Id);
                            leContacto.EntityAlias = strAliasTitular;

                            LinkEntity leContrato = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecContrato.EntityName,
                                HidrotecRoldecontrato.HidrotecContratoId, HidrotecContrato.PrimaryKey, JoinOperator.Inner);
                            leContrato.Columns.AddColumns(HidrotecContrato.PrimaryKey, HidrotecContrato.HidrotecTitularId, HidrotecContrato.HidrotecEstado);
                            leContrato.LinkCriteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.numerocontrato);
                            leContrato.EntityAlias = strAliasContrato;

                            LinkEntity leMaestroRoles = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecMaestroderoles.EntityName,
                                HidrotecRoldecontrato.HidrotecRolId, HidrotecMaestroderoles.PrimaryKey, JoinOperator.Inner);
                            leMaestroRoles.LinkCriteria.AddCondition(HidrotecMaestroderoles.HidrotecIdrol, ConditionOperator.Equal, strRolTitular);

                            q.LinkEntities.AddRange(leContacto, leContrato, leMaestroRoles);

                            res = Utils.IOS.RetrieveMultiple(q);
                            Guid idContacto = new Guid();
                            Guid idContrato = new Guid();
                            OptionSetValue EstadoContrato = null;
                            foreach (Entity item in res.Entities)
                            {
                                idContacto = ((EntityReference)((AliasedValue)item[strQueryContrato + HidrotecContrato.HidrotecTitularId]).Value).Id;
                                idContrato = (Guid)((AliasedValue)item[strQueryContrato + HidrotecContrato.PrimaryKey]).Value;
                                EstadoContrato= (OptionSetValue)((AliasedValue)item[strQueryContrato + HidrotecContrato.HidrotecEstado]).Value;
                                if (HSContratos.Contains(idContrato.ToString()) == false)
                                {
                                    HSContratos.Add(idContrato.ToString());
                                }
                            }

                            q = new QueryExpression(HidrotecContrato.EntityName);
                            q.Criteria.AddCondition(HidrotecContrato.PrimaryKey, ConditionOperator.In, HSContratos.ToArray());

                            LinkEntity leDirSumm = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                                HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                            leDirSumm.EntityAlias = strAliasDirSumm;
                            leDirSumm.Columns.AddColumn(HidrotecDirecciondesuministro.HidrotecMunicipioId);

                            LinkEntity leConfOrg = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecConfiguracionorganizativa.EntityName,
                                HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecConfiguracionorganizativa.HidrotecMunicipioId, JoinOperator.Inner);
                            leConfOrg.EntityAlias = strAliasConfOrg;
                            leConfOrg.Columns.AddColumn(HidrotecConfiguracionorganizativa.HidrotecExplotacionId);

                            leDirSumm.LinkEntities.Add(leConfOrg);

                            q.LinkEntities.Add(leDirSumm);

                            res = Utils.IOS.RetrieveMultiple(q);
                            Dictionary<Guid, Guid> expMun = new Dictionary<Guid, Guid>();
                            foreach (Entity item in res.Entities)
                            {
                                Guid idExplotacion = ((EntityReference)((AliasedValue)item[strQueryConfOrg + HidrotecConfiguracionorganizativa.HidrotecExplotacionId]).Value).Id;
                                Guid idMunicipio = ((EntityReference)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecMunicipioId]).Value).Id;
                                if (!expMun.Keys.Contains(idExplotacion))
                                    expMun.Add(idExplotacion, idMunicipio);
                            }

                            foreach (var par in expMun)
                            {
                                List<Guid> teamExplotacionConfiguracion = Utils.getEquipoExplotacion(par.Value.ToString(), Utils.IOS);

                                Entity exp = new Entity(Incident.EntityName);
                                if (request.canalentrada.ToUpper() == "APP")
                                {
                                    exp[Incident.HidrotecCanaldeentrada] = new OptionSetValue(0);

                                }
                                exp[Incident.CustomerId] = new EntityReference(Contact.EntityName, idContacto);
                                exp[Incident.HidrotecMunicipioprincipalId] = new EntityReference(Incident.HidrotecMunicipio, par.Value);
                                exp[Incident.OwnerId] = new EntityReference(Team.EntityName, teamExplotacionConfiguracion.ElementAt(0));
                                exp[Incident.HidrotecAsignadoalequipo] = new EntityReference(Team.EntityName, teamExplotacionConfiguracion.ElementAt(0));
                                exp[Incident.HidrotecExplotacionId] = new EntityReference(BusinessUnit.EntityName, teamExplotacionConfiguracion.ElementAt(1));
                                exp[Incident.HidrotecConfiguracionorganizativaId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, teamExplotacionConfiguracion.ElementAt(2));

                                // Otras solicitudes - Otras solicitudes - Otras solicitudes
                                Guid relacion = Utils.takeField(Utils.IOS, HidrotecRelacion.EntityName, HidrotecRelacion.HidrotecIdrelacion, strRelAPP);
                                Guid tipo = Utils.takeField(Utils.IOS, HidrotecTipo.EntityName, HidrotecTipo.HidrotecIdtipo, strTipoAPP);
                                Guid subtipo = Utils.takeField(Utils.IOS, HidrotecSubtipo.EntityName, HidrotecSubtipo.HidrotecIdsubtipo, strSubtipoAPP);

                                exp[Incident.HidrotecRelacionId] = new EntityReference(HidrotecRelacion.EntityName, relacion);
                                exp[Incident.HidrotecTipoId] = new EntityReference(HidrotecTipo.EntityName, tipo);
                                exp[Incident.HidrotecSubtipoId] = new EntityReference(HidrotecSubtipo.EntityName, subtipo);
                                Guid estadoCerrado = Utils.takeField(Utils.IOS, HidrotecEstadoexpediente.EntityName, HidrotecEstadoexpediente.HidrotecIdestado, "10");
                                exp[Incident.HidrotecEstadoexpedienteId] = new EntityReference(HidrotecEstadoexpediente.EntityName, estadoCerrado);
                                exp[Incident.HidrotecCreacionautomatica] = true;

                                exp[Incident.HidrotecIdiomacontrato] = new OptionSetValue(Convert.ToInt32(request.idioma));
                                //TODOO Falta añadir idioma.
                              
                                exp[Incident.HidrotecContratoId] = new EntityReference(HidrotecContrato.EntityName, idContrato);
                                exp[Incident.HidrotecEstadocontrato] = EstadoContrato;

                               idExp = Utils.IOS.Create(exp);
                            }
                            if (idExp != Guid.Empty)
                            {
                                Utils.CrearNotaTexto(idExp, "Alta Usuario", "Se ha dado de alta un usuario asociado al contacto de este expediente.");
                                
                            }
                            //
                            //
                            string respuestaCorreo = enviarEmail(Utils.IOS, "AqualiaAPP Creación de usuario", "Se ha creado un usuario asociado al documento " + request.numerodocumento + " con la password " + clave, idContacto, idExp,true, request.correoelectronico);
                            if (respuestaCorreo != "OK")
                            {

                                throw new InvalidPluginExecutionException("Se produjo un error al enviar el correo pero se ha creado el usuario.");

                            }
                           
                            resp.textorespuesta = "00001";
                            resp.codigorespuesta = "OK";
                            resp.identificador = request.numerodocumento;
                        }
                        else
                        {
                            //return (NEO_AltaUsuarioResponse)respuestaWs.NoPreguntaSeguridad();
                            return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "NoPreguntaSeguridad");

                        }

                    }

                }
                else
                {
                    //return (NEO_AltaUsuarioResponse)respuestaWs.NoContratoContacto();
                    return (NEO_AltaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "NoContratoContacto");

                }

                return resp;
            }
            catch (Exception e)
            {
                // return (NEO_AltaUsuarioResponse)respuestaWs.Exception(e);
                return (NEO_AltaUsuarioResponse)respuestaWs.Exception(e, "99999");
            }
        }

        /// <summary>
        /// Regula mediante expresión regular el formato del número del contrato.
        /// </summary>
        /// <param name="contrato"></param>
        /// <returns></returns>
        private bool validaFormatoContrato(string contrato)
        {
            Regex pat = new Regex("[\\d]{5}[-]{1}[\\d]{1}[/]{1}[\\d]{1}[-]{1}[\\d]{6}");

            return pat.IsMatch(contrato);
        }

        /// <summary>
        /// STATUS: OK Abel Gago - Falta Password
        /// DESC:
        /// Este servicio comprueba que el usuario está registrado en la base de datos. Se hacen las validaciones pertinentes y la de 
        /// si existe el usuario a través de la función fapp_selusu. Si se valida se comprueba que la password introducida es la misma que 
        /// la de la base de datos, previamente desencriptada. Además se comprueba si el password es temporal o si ha caducado la contraseña. 
        /// Todos estos resultados se devuelven en la respuesta al igual de si el login es correcto o no.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_AutenticationUsuarioResponse NEO_AutenticationUsuario(NEO_AutenticationUsuarioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_AutenticationUsuarioResponse respuestaresponse = new NEO_AutenticationUsuarioResponse();

            /*
            request.usuario = "05453623A"
            request.clave = "?";
            */

            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    // return (NEO_AutenticationUsuarioResponse)respuestaWs.SinConexion();
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    //return (NEO_AutenticationUsuarioResponse)respuestaWs.SinParametros();
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    // return (NEO_AutenticationUsuarioResponse)respuestaWs.CanalEntrada();
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    // return (NEO_AutenticationUsuarioResponse)respuestaWs.Usuario();
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                if (request.clave == "" || request.clave == null)
                {
                    //return (NEO_AutenticationUsuarioResponse)respuestaWs.Clave();
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Clave");
                }

                QueryExpression q = new QueryExpression(Contact.EntityName);

                q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);

                LinkEntity leOvContacto = new LinkEntity(Contact.EntityName, HidrotecOvUsuario.EntityName, 
                    Contact.PrimaryKey, HidrotecOvUsuario.HidrotecContactOprincipalId, JoinOperator.Inner);
                leOvContacto.LinkCriteria.AddCondition(HidrotecOvUsuario.HidrotecPassword, ConditionOperator.Equal, Encriptar(request.clave));
                leOvContacto.Columns.AddColumn(HidrotecOvUsuario.PrimaryKey);

                q.LinkEntities.Add(leOvContacto);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                if (res.Entities.Any())
                {
                    // respuestaresponse.codigorespuesta = "OK";
                    // respuestaresponse.textorespuesta = "Autenticación correcta.";
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "AutenticacionOK");

                }
                else
                {
                    // return (NEO_AutenticationUsuarioResponse)respuestaWs.NoUSuarioClave();
                    return (NEO_AutenticationUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "NoUSuarioClave");
                }

               
                
            }
            catch (Exception e)
            {
                //return (NEO_AutenticationUsuarioResponse)respuestaWs.Exception(e);
                return (NEO_AutenticationUsuarioResponse)respuestaWs.Exception(e, "99999");
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// TODOO: Repasar
        /// DESC:
        /// Este servicio permite dar de baja un usuario de la la base de datos. Para ello, se hacen las validaciones pertinentes, la de existencia del usuario;
        /// con la función fapp_selusu. Una vez validado se da de baja el usuario con la función papp_updfechabaja y se inserta en historico_acciones a través de 
        /// papp_inshistaccion. NOTA: Se guardara el código del motivo de baja en la columna detalle de la tabla histórico_acciones.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_BajaUsuarioResponse NEO_BajaUsuario(NEO_BajaUsuarioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_BajaUsuarioResponse resp = new NEO_BajaUsuarioResponse();
            /*
            request.canalentrada = "CRM";
            request.codpais = "34";
            request.idioma = "es-es";
            request.codigomotivobaja = "1";
            request.usuario = "34823764Q";
            */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {
                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }

                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }
                List<Entity> contacts = Utils.ChequearExistenciaContacto(request.usuario);

                if (contacts == null)
                {
                    //return (NEO_BajaUsuarioResponse)respuestaWs.UsuarioBuscado();
                    return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "UsuarioBuscado");
                }

                if (request.codigomotivobaja == "" || request.codigomotivobaja == null)
                {
                   // return (NEO_BajaUsuarioResponse)respuestaWs.CodigoMotivoBaja();
                    return (NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CodigoMotivoBaja");

                }

                string strRolTitular = "1";
                string strRelAPP = "008";
                string strTipoAPP = "008001";
                string strSubtipoAPP = "008001005";

                string strAliasTitular = "titular";
                string strQueryTitular = "titular.";
                string strAliasContrato = "contract";
                string strQueryContrato = "contract.";
                string strAliasDirSumm = "dirSuministro";
                string strQueryDirSumm = "dirSuministro.";
                string strAliasConfOrg = "confOrganizativa";
                string strQueryConfOrg = "confOrganizativa.";

                // Sacamos GUID del contacto con el NIF 
                HashSet<string> HSContratos = new HashSet<string>();
                Guid idExp = Guid.Empty;

                QueryExpression q = new QueryExpression(HidrotecRoldecontrato.EntityName);

                LinkEntity leContacto = new LinkEntity(HidrotecRoldecontrato.EntityName, Contact.EntityName,
                   HidrotecRoldecontrato.HidrotecContactOId, Contact.PrimaryKey, JoinOperator.Inner);
                leContacto.LinkCriteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                leContacto.EntityAlias = strAliasTitular;

                LinkEntity leContrato = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecContrato.EntityName,
                    HidrotecRoldecontrato.HidrotecContratoId, HidrotecContrato.PrimaryKey, JoinOperator.Inner);
                leContrato.Columns.AddColumns(HidrotecContrato.PrimaryKey, HidrotecContrato.HidrotecTitularId);
                
                leContrato.EntityAlias = strAliasContrato;

                LinkEntity leMaestroRoles = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecMaestroderoles.EntityName,
                    HidrotecRoldecontrato.HidrotecRolId, HidrotecMaestroderoles.PrimaryKey, JoinOperator.Inner);
                leMaestroRoles.LinkCriteria.AddCondition(HidrotecMaestroderoles.HidrotecIdrol, ConditionOperator.Equal, strRolTitular);

                q.LinkEntities.AddRange(leContacto, leContrato, leMaestroRoles);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                Guid idContacto = new Guid();
                foreach (Entity item in res.Entities)
                {
                    idContacto = ((EntityReference)((AliasedValue)item[strQueryContrato + HidrotecContrato.HidrotecTitularId]).Value).Id;
                    var idContrato = (Guid)((AliasedValue)item[strQueryContrato + HidrotecContrato.PrimaryKey]).Value;
                    HSContratos.Add(idContrato.ToString());
                }

                q = new QueryExpression(HidrotecContrato.EntityName);
                q.Criteria.AddCondition(HidrotecContrato.PrimaryKey, ConditionOperator.In, HSContratos.ToArray());

                LinkEntity leDirSumm = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                    HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                leDirSumm.EntityAlias = strAliasDirSumm;
                leDirSumm.Columns.AddColumn(HidrotecDirecciondesuministro.HidrotecMunicipioId);

                LinkEntity leConfOrg = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecConfiguracionorganizativa.EntityName,
                    HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecConfiguracionorganizativa.HidrotecMunicipioId, JoinOperator.Inner);
                leConfOrg.EntityAlias = strAliasConfOrg;
                leConfOrg.Columns.AddColumn(HidrotecConfiguracionorganizativa.HidrotecExplotacionId);

                leDirSumm.LinkEntities.Add(leConfOrg);

                q.LinkEntities.Add(leDirSumm);

                res = Utils.IOS.RetrieveMultiple(q);
                Dictionary<Guid, Guid> expMun = new Dictionary<Guid, Guid>();
                foreach (Entity item in res.Entities)
                {
                    Guid idExplotacion = ((EntityReference)((AliasedValue)item[strQueryConfOrg + HidrotecConfiguracionorganizativa.HidrotecExplotacionId]).Value).Id;
                    Guid idMunicipio = ((EntityReference)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecMunicipioId]).Value).Id;
                    if (!expMun.Keys.Contains(idExplotacion))
                        expMun.Add(idExplotacion, idMunicipio);
                }

                Boolean ExpedienteBajaCreado = false;
                foreach (var par in expMun)
                {
                    if (ExpedienteBajaCreado == false)
                    {
                        List<Guid> teamExplotacionConfiguracion = Utils.getEquipoExplotacion(par.Value.ToString(), Utils.IOS);

                        Entity exp = new Entity(Incident.EntityName);
                        if (request.canalentrada.ToUpper() == "APP")
                        {
                            exp[Incident.HidrotecCanaldeentrada] = new OptionSetValue(0);

                        }
                        exp[Incident.CustomerId] = new EntityReference(Contact.EntityName, idContacto);
                        exp[Incident.HidrotecMunicipioprincipalId] = new EntityReference(Incident.HidrotecMunicipio, par.Value);
                        exp[Incident.OwnerId] = new EntityReference(Team.EntityName, teamExplotacionConfiguracion.ElementAt(0));
                        exp[Incident.HidrotecAsignadoalequipo] = new EntityReference(Team.EntityName, teamExplotacionConfiguracion.ElementAt(0));
                        exp[Incident.HidrotecExplotacionId] = new EntityReference(BusinessUnit.EntityName, teamExplotacionConfiguracion.ElementAt(1));
                        exp[Incident.HidrotecConfiguracionorganizativaId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, teamExplotacionConfiguracion.ElementAt(2));

                        // Otras solicitudes - Otras solicitudes - Otras solicitudes
                        Guid relacion = Utils.takeField(Utils.IOS, HidrotecRelacion.EntityName, HidrotecRelacion.HidrotecIdrelacion, strRelAPP);
                        Guid tipo = Utils.takeField(Utils.IOS, HidrotecTipo.EntityName, HidrotecTipo.HidrotecIdtipo, strTipoAPP);
                        Guid subtipo = Utils.takeField(Utils.IOS, HidrotecSubtipo.EntityName, HidrotecSubtipo.HidrotecIdsubtipo, strSubtipoAPP);

                        exp[Incident.HidrotecRelacionId] = new EntityReference(HidrotecRelacion.EntityName, relacion);
                        exp[Incident.HidrotecTipoId] = new EntityReference(HidrotecTipo.EntityName, tipo);
                        exp[Incident.HidrotecSubtipoId] = new EntityReference(HidrotecSubtipo.EntityName, subtipo);
                        Guid estadoCerrado = Utils.takeField(Utils.IOS, HidrotecEstadoexpediente.EntityName, HidrotecEstadoexpediente.HidrotecIdestado, "10");
                        exp[Incident.HidrotecEstadoexpedienteId] = new EntityReference(HidrotecEstadoexpediente.EntityName, estadoCerrado);
                        exp[Incident.HidrotecCreacionautomatica] = true;
                        Guid IdMotivoBaja = Utils.takeField(Utils.IOS, HidrotecMotivo.EntityName, HidrotecMotivo.HidrotecIdmotivo, request.codigomotivobaja);
                        if (IdMotivoBaja != (Guid.Empty))
                        {
                            exp[Incident.HidrotecMotivoId] = new EntityReference(HidrotecMotivo.EntityName, IdMotivoBaja);
                        }
                        else
                        {
                            return(NEO_BajaUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CodigoMotivoBajaNoEncontrado");
                        }
                        exp[Incident.HidrotecIdiomacontrato] = new OptionSetValue(Convert.ToInt32(request.idioma));
                        //TODOO Falta añadir idioma.

                        idExp = Utils.IOS.Create(exp);
                        if (idExp != Guid.Empty)
                        {
                            Utils.CrearNotaTexto(idExp, "Baja Usuario", "Se ha dado de baja un usuario asociado al contacto de este expediente.");
                            q = new QueryExpression(HidrotecOvUsuario.EntityName);
                            /*q.Criteria.Conditions.AddRange(
                                HidrotecOvUsuario.PrimaryKey, ConditionOperator.Equal, idContacto),
                                new ConditionExpression(HidrotecOvUsuario.HidrotecContactOempresaId, ConditionOperator.Null));
                                */
                            q.Criteria.Conditions.Add(new ConditionExpression(HidrotecOvUsuario.HidrotecContactOprincipalId, ConditionOperator.Equal, idContacto));
                            q.ColumnSet.AddColumn(HidrotecOvUsuario.PrimaryKey);

                            res = Utils.IOS.RetrieveMultiple(q);

                            if (res.Entities.Any())
                            {
                                Utils.IOS.Delete(HidrotecOvUsuario.EntityName, (Guid)res.Entities[0][HidrotecOvUsuario.PrimaryKey]);
                            }
                        }
                        ExpedienteBajaCreado = true;
                    }
                }


                // resp.codigorespuesta = "OK";
                // resp.textorespuesta = "OK";

                // return resp;
                return (NEO_BajaUsuarioResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK");
            }
            catch (Exception e)
            {
                return (NEO_BajaUsuarioResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK
        /// DESC:
        /// Este servicio modifica la password temporal suministrada por el sistema por una propia introducida por el usuario. Se hacen las validaciones pertinentes 
        /// y la de si existe el usuario a través de la función fapp_selusu y se valida la contraseña antigua. Una vez hecho esto, se actualiza la password por la 
        /// actual a través del procedimiento papp_updpassword. Al finalizar, se envía un correo con los nuevos datos de registro al usuario.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_CambiarClaveResponse NEO_CambiarClave(NEO_CambiarClaveRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_CambiarClaveResponse respuestaresponse = new NEO_CambiarClaveResponse();

            /*
            request.usuario = "05453623A";
            request.clave = "Clave";
            request.nuevaclave = "NuevaClave";
            */

            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                if (request.clave == "" || request.clave == null)
                {
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "Clave"); ;

                }

                if (request.nuevaclave == "" || request.nuevaclave == null)
                {
                   // return (NEO_CambiarClaveResponse)respuestaWs.NuevaClave();
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "NuevaClave");
                }

                string strAliasContactoOV = "contactoOV";
                string strQueryContactoOV = "contactoOV.";

                QueryExpression q = new QueryExpression(Contact.EntityName);

                q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                LinkEntity leOvContacto = new LinkEntity(Contact.EntityName, HidrotecOvUsuario.EntityName, 
                    Contact.PrimaryKey, HidrotecOvUsuario.HidrotecContactOprincipalId, JoinOperator.Inner);
                leOvContacto.LinkCriteria.Conditions.AddRange(
                    new ConditionExpression(HidrotecOvUsuario.HidrotecPassword, ConditionOperator.Equal, Encriptar(request.clave)),
                    new ConditionExpression(HidrotecOvUsuario.HidrotecContactOempresaId, ConditionOperator.Null));
                leOvContacto.EntityAlias = strAliasContactoOV;
                leOvContacto.Columns.AddColumn(HidrotecOvUsuario.PrimaryKey);

                q.LinkEntities.Add(leOvContacto);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                if (res.Entities.Any())
                {
                    string idOvUsuario = ((AliasedValue)res.Entities[0][strQueryContactoOV + HidrotecOvUsuario.PrimaryKey]).Value.ToString();

                    Entity ovContacto = new Entity(HidrotecOvUsuario.EntityName, new Guid(idOvUsuario));
                    ovContacto[HidrotecOvUsuario.HidrotecPassword] = Encriptar(request.nuevaclave);

                    Utils.IOS.Update(ovContacto);
                    q = new QueryExpression(HidrotecOvUsuario.EntityName);
                    q.Criteria.AddCondition(HidrotecOvUsuario.PrimaryKey, ConditionOperator.Equal, idOvUsuario);
                    q.ColumnSet.AddColumn(HidrotecOvUsuario.HidrotecEMail);
                    EntityCollection resUsuario = Utils.IOS.RetrieveMultiple(q);

                    string respuestaCorreo = enviarEmail(Utils.IOS, "AqualiaAPP CambiarClave", "Se ha actualizado la clave del usuario asociado al documento " + request.usuario + " con la password " + request.nuevaclave, res.Entities[0].Id,new Guid(idOvUsuario),false, resUsuario[0].GetAttributeValue<string>(HidrotecOvUsuario.HidrotecEMail));
                    if (respuestaCorreo != "OK")
                    {

                        throw new InvalidPluginExecutionException("Se produjo un error al enviar el correo pero se ha creado el usuario.");

                    }
                    //respuestaresponse.codigorespuesta = "OK";
                    //respuestaresponse.textorespuesta = "OK";
                    return (NEO_CambiarClaveResponse)respuestaWs.GetRespuesta("000001", idiomaorigen, "OK");
                }
                else
                {
                    //return (NEO_CambiarClaveResponse)respuestaWs.NoUSuarioClave();
                    return (NEO_CambiarClaveResponse)respuestaWs.GetError("99999",idiomaorigen, "NoUSuarioClave");
                }

               
                return respuestaresponse;
            }
            catch (Exception e)
            {
                return (NEO_CambiarClaveResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio permite modificar la pregunta de seguridad que tenía el usuario. Para ello, se hacen las validaciones pertinentes, la de existencia del usuario; 
        /// con la función fapp_selusu. Una vez validado se modifica la pregunat de seguridad y la respuesta  con el procedimiento papp_updpregseguridad.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_CambiarPreguntaSeguridadUsuarioResponse NEO_CambiarPreguntaSeguridadUsuario(NEO_CambiarPreguntaSeguridadUsuarioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_CambiarPreguntaSeguridadUsuarioResponse resp = new NEO_CambiarPreguntaSeguridadUsuarioResponse();

            /*
            request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
            request.usuario = "05453623A";
            request.respuesta = "?";
            */

            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {
                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }


                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.Pais();
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.respuesta == "" || request.respuesta == null)
                {
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "ResPregunta");
                }

                if (request.codigopregunta == "" || request.codigopregunta == null)
                {
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CodPregunta");
                }

                string strAliasRespuesta = "respuesta";
                string strQueryRespuesta = "respuesta.";

                QueryExpression q = new QueryExpression(HidrotecOvUsuario.EntityName);

                LinkEntity leContacto = new LinkEntity(HidrotecOvUsuario.EntityName, Contact.EntityName,
                    HidrotecOvUsuario.HidrotecContactOprincipalId, Contact.PrimaryKey, JoinOperator.Inner);
                leContacto.LinkCriteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);

                LinkEntity leRespuesta = new LinkEntity(HidrotecOvUsuario.EntityName, HidrotecOvRespuesta.EntityName,
                    HidrotecOvUsuario.PrimaryKey, HidrotecOvRespuesta.HidrotecOvContactOId, JoinOperator.Inner);
                leRespuesta.EntityAlias = strAliasRespuesta;
                leRespuesta.Columns.AddColumns(HidrotecOvRespuesta.PrimaryKey, HidrotecOvRespuesta.HidrotecOvRespuesta1);

                LinkEntity lePreguntaSeg = new LinkEntity(HidrotecOvRespuesta.EntityName, HidrotecOvPreguntasdeseguridad.EntityName,
                    HidrotecOvRespuesta.HidrotecOvPreguntaId, HidrotecOvPreguntasdeseguridad.PrimaryKey, JoinOperator.Inner);
                lePreguntaSeg.Columns.AddColumns(HidrotecOvPreguntasdeseguridad.HidrotecOvIdpregunta, HidrotecOvPreguntasdeseguridad.HidrotecOvPregunta);
                lePreguntaSeg.LinkCriteria.AddCondition(new ConditionExpression(HidrotecOvPreguntasdeseguridad.HidrotecOvIdioma,ConditionOperator.Equal,request.idioma));
                leRespuesta.LinkEntities.Add(lePreguntaSeg);
                q.LinkEntities.AddRange(leContacto, leRespuesta);

                Guid guidRespuesta = new Guid();

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                // Solo debería de ser 1 pregunta, entiendo.
                if (res.Entities.Any())
                {
                    guidRespuesta = (Guid)((AliasedValue)res.Entities[0][strQueryRespuesta + HidrotecOvRespuesta.PrimaryKey]).Value;
                }
                else
                {
                    //return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.NoUSuarioRespuesta();
                    return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CodPregunta");
                }

                q = new QueryExpression(HidrotecOvRespuesta.EntityName);
                q.Criteria.AddCondition(HidrotecOvRespuesta.PrimaryKey, ConditionOperator.Equal, guidRespuesta);
                q.ColumnSet.AddColumns(HidrotecOvRespuesta.PrimaryKey, HidrotecOvRespuesta.HidrotecOvRespuesta1);

                res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    res.Entities[0][HidrotecOvRespuesta.HidrotecOvRespuesta1] = Encriptar(request.respuesta);
                    Utils.IOS.Update(res.Entities[0]);
                }

                //resp.codigorespuesta ="00001";
                //resp.textorespuesta = "OK";

                // return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.ok();
                //return resp;
                return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK");
            }
            catch (Exception e)
            {
                return (NEO_CambiarPreguntaSeguridadUsuarioResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK
        /// Este servicio comprueba si el usuario existe en la base de datos. Se hacen las validaciones pertinentes y la de si existe el usuario a través de la función fapp_selusu. Se devuelve en la respuesta si el usuario existe o no.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ComprobarUsuarioResponse NEO_ComprobarUsuario(NEO_ComprobarUsuarioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            //string inicio = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    //return (NEO_ComprobarUsuarioResponse)respuestaWs.SinConexion();
                    return (NEO_ComprobarUsuarioResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    //return (NEO_ComprobarUsuarioResponse)respuestaWs.SinParametros();
                    return (NEO_ComprobarUsuarioResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    // return (NEO_ComprobarUsuarioResponse)respuestaWs.Idioma();
                    return (NEO_ComprobarUsuarioResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {
                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ComprobarUsuarioResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }
                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    //return (NEO_ComprobarUsuarioResponse)respuestaWs.CanalEntrada();
                    return (NEO_ComprobarUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    // return (NEO_ComprobarUsuarioResponse)respuestaWs.Pais();

                    return (NEO_ComprobarUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    //return (NEO_ComprobarUsuarioResponse)respuestaWs.Usuario();
                    return (NEO_ComprobarUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }
               

                
                //Buscamos si el usuario aparece como usuarioOV
                QueryExpression q = new QueryExpression(UsuarioOV.LogicalName);
                LinkEntity leContact = new LinkEntity(UsuarioOV.LogicalName, Contact.EntityName,
                    UsuarioOV.ContactoPrincipal, Contact.PrimaryKey, JoinOperator.Inner);
                leContact.LinkCriteria.AddCondition(new ConditionExpression(Contact.HidrotecNumerodocumento,ConditionOperator.Equal,request.usuario));
                q.LinkEntities.Add(leContact);
                q.ColumnSet.AddColumn(UsuarioOV.Nombre);
               
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    //return (NEO_ComprobarUsuarioResponse)respuestaWs.UsuarioExistente();
                    return (NEO_ComprobarUsuarioResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "UsuarioExistente");
                }
                else
                {
                    //return (NEO_ComprobarUsuarioResponse)respuestaWs.UsuarioBuscado();
                    return (NEO_ComprobarUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "UsuarioBuscado");

                }


            }
            catch (Exception e)
            {
                return (NEO_ComprobarUsuarioResponse)respuestaWs.Exception(e,"99999");
              
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio permite devolver el listado de posibles motivos de baja de un usuario. Para ello, se hacen las validaciones pertinentes. 
        /// Una vez validado se devuelve la información con la función fapp_motivosbaja.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoMotivosBajaResponse NEO_ListadoMotivosBaja(NEO_ListadoMotivosBajaRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoMotivosBajaResponse respuestaConsulta = new NEO_ListadoMotivosBajaResponse();
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoMotivosBajaResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_ListadoMotivosBajaResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoMotivosBajaResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoMotivosBajaResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoMotivosBajaResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoMotivosBajaResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoMotivosBajaResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                string tipoMotivoBaja = "014001001";

                QueryExpression q = new QueryExpression(HidrotecMotivo.EntityName);
                q.ColumnSet.AddColumns(HidrotecMotivo.HidrotecIdmotivo, HidrotecMotivo.PrimaryName);

                LinkEntity leSubtipo = new LinkEntity(HidrotecMotivo.EntityName, HidrotecHidrotecSubtipoHidrotecMotivo.EntityName,
                    HidrotecMotivo.PrimaryKey, HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecMotivoId, JoinOperator.Inner);

                LinkEntity leMotivo = new LinkEntity(HidrotecHidrotecSubtipoHidrotecMotivo.EntityName, HidrotecSubtipo.EntityName,
                    HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecSubtipoId, HidrotecSubtipo.PrimaryKey, JoinOperator.Inner);

                leMotivo.LinkCriteria.AddCondition(new ConditionExpression(HidrotecSubtipo.HidrotecIdsubtipo, ConditionOperator.Equal, tipoMotivoBaja));
                leSubtipo.LinkEntities.Add(leMotivo);

                q.LinkEntities.Add(leSubtipo);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    List<motivos> lMotivos = new List<motivos>();
                    foreach (Entity item in res.Entities)
                    {
                        motivos motivo = new motivos();
                        motivo.codigomotivobaja = item.Contains(HidrotecMotivo.HidrotecIdmotivo) ? (string)item[HidrotecMotivo.HidrotecIdmotivo] : null;
                        motivo.textomotivobaja = item.Contains(HidrotecMotivo.PrimaryName) ? (string)item[HidrotecMotivo.PrimaryName] : null;
                        lMotivos.Add(motivo);
                    }
                    respuestaConsulta.codigorespuesta = "00001";
                    respuestaConsulta.textorespuesta = "OK";
                    respuestaConsulta.motivosbaja = lMotivos;
                }
                else
                {
                    //return (NEO_ListadoMotivosBajaResponse)respuestaWs.NoMotivosDeBaja();
                    return (NEO_ListadoMotivosBajaResponse)respuestaWs.GetError("99999", idiomaorigen, "NoMotivosDeBaja");
                }
                return respuestaConsulta;
            }
            catch (Exception e)
            {
                return (NEO_ListadoMotivosBajaResponse)respuestaWs.Exception(e,"99999");
            }

        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC: 
        /// Este servicio devuelve la pregunta de seguridad utilizada en el momento de registro del usuario pasado por parámetros. Para ello, se hacen las validaciones pertinentes, 
        /// la de si existe el usuario; con la función fapp_selusu. Una vez validado se devuelve la pregunta a través de la función fapp_selpregseguridadusu.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_PreguntaSeguridadUsuarioResponse NEO_PreguntaSeguridadUsuario(NEO_PreguntaSeguridadUsuarioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_PreguntaSeguridadUsuarioResponse resp = new NEO_PreguntaSeguridadUsuarioResponse();
            /*
             request.usuario = "05453623A";
             */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.Idioma();
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                string strAliasPreguntasSeguridadOV = "preguntaOV";
                string strQueryPreguntasSeguridadOV = "preguntaOV.";

                QueryExpression q = new QueryExpression(HidrotecOvUsuario.EntityName);

                LinkEntity leContacto = new LinkEntity(HidrotecOvUsuario.EntityName, Contact.EntityName,
                    HidrotecOvUsuario.HidrotecContactOprincipalId, Contact.PrimaryKey, JoinOperator.Inner);
                leContacto.LinkCriteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);

                LinkEntity leRespuesta = new LinkEntity(HidrotecOvUsuario.EntityName, HidrotecOvRespuesta.EntityName,
                    HidrotecOvUsuario.PrimaryKey, HidrotecOvRespuesta.HidrotecOvContactOId, JoinOperator.Inner);

                LinkEntity lePreguntaSeg = new LinkEntity(HidrotecOvRespuesta.EntityName, HidrotecOvPreguntasdeseguridad.EntityName,
                    HidrotecOvRespuesta.HidrotecOvPreguntaId, HidrotecOvPreguntasdeseguridad.PrimaryKey, JoinOperator.Inner);
                lePreguntaSeg.EntityAlias = strAliasPreguntasSeguridadOV;
                lePreguntaSeg.Columns.AddColumns(HidrotecOvPreguntasdeseguridad.HidrotecOvIdpregunta, HidrotecOvPreguntasdeseguridad.HidrotecOvPregunta);

                leRespuesta.LinkEntities.Add(lePreguntaSeg);
                q.LinkEntities.AddRange(leContacto, leRespuesta);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    // Solo debería de ser 1 pregunta, entiendo.
                    foreach (Entity item in res.Entities)
                    {
                        PreguntaSeguridad pregunta = new PreguntaSeguridad();
                        pregunta.codigopregunta = item.Contains(strQueryPreguntasSeguridadOV + HidrotecOvPreguntasdeseguridad.HidrotecOvIdpregunta) ?
                            (string)((AliasedValue)item[strQueryPreguntasSeguridadOV + HidrotecOvPreguntasdeseguridad.HidrotecOvIdpregunta]).Value : null;
                        pregunta.tipopregunta = item.Contains(strQueryPreguntasSeguridadOV + HidrotecOvPreguntasdeseguridad.HidrotecOvPregunta) ?
                            (string)((AliasedValue)item[strQueryPreguntasSeguridadOV + HidrotecOvPreguntasdeseguridad.HidrotecOvPregunta]).Value : null;

                        resp.pregunta = pregunta;
                    }

                    resp.codigorespuesta = "00001";
                    resp.textorespuesta = "OK";
                }
                else
                {
                    //return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.NoPreguntasSeguridad();
                    return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.GetError("00002", idiomaorigen, "NoPreguntasSeguridad");
                }
                return resp;
            }
            catch (Exception e)
            {
                return (NEO_PreguntaSeguridadUsuarioResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio permite modificar la password que tenía el usuario por una temporal generada por el sistema que le sirva para volver a identificarse en el sistema. 
        /// Para ello, se hacen las validaciones pertinentes, la de existencia del usuario; con la función fapp_selusu. Si se le pasa un correo a la función, además se deben 
        /// validar la pregunta de seguridad del usuario; con la función fapp_selpregseguridadusu, si no se utiliza el que ya tiene. Una vez validado se modifica la password 
        /// con el procedimiento papp_updpassword. Para finalizar, se envían por correo las nuevas credenciales al usuario.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_RecordarClaveResponse NEO_RecordarClave(NEO_RecordarClaveRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_RecordarClaveResponse resp = new NEO_RecordarClaveResponse();
            /*
             request.usuario = "05453623A"
             */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario"); ;
                }

                if (request.codigopregunta == "" || request.codigopregunta == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "CodPregunta"); ;
                }

                if (request.respuestapregunta == "" || request.respuestapregunta == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "ResPregunta");
                }

                if (request.email == "" || request.email == null)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("99999", idiomaorigen, "Correo");
                }

                string strAliasRespuestaOV = "respuesta";
                string strQueryRespuestaOV = "respuesta.";

                QueryExpression q = new QueryExpression(HidrotecOvUsuario.EntityName);

                LinkEntity leContacto = new LinkEntity(HidrotecOvUsuario.EntityName, Contact.EntityName,
                    HidrotecOvUsuario.HidrotecContactOprincipalId, Contact.PrimaryKey, JoinOperator.Inner);
                
                leContacto.LinkCriteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);

                LinkEntity leRespuesta = new LinkEntity(HidrotecOvUsuario.EntityName, HidrotecOvRespuesta.EntityName,
                    HidrotecOvUsuario.PrimaryKey, HidrotecOvRespuesta.HidrotecOvContactOId, JoinOperator.Inner);
                leRespuesta.EntityAlias = strAliasRespuestaOV;
                leRespuesta.Columns.AddColumns(HidrotecOvRespuesta.PrimaryKey, HidrotecOvRespuesta.HidrotecOvRespuesta1);

                LinkEntity lePreguntaSeg = new LinkEntity(HidrotecOvRespuesta.EntityName, HidrotecOvPreguntasdeseguridad.EntityName,
                    HidrotecOvRespuesta.HidrotecOvPreguntaId, HidrotecOvPreguntasdeseguridad.PrimaryKey, JoinOperator.Inner);
                lePreguntaSeg.Columns.AddColumns(HidrotecOvPreguntasdeseguridad.HidrotecOvIdpregunta, HidrotecOvPreguntasdeseguridad.HidrotecOvPregunta);
                lePreguntaSeg.LinkCriteria.AddCondition(new ConditionExpression(HidrotecOvPreguntasdeseguridad.HidrotecOvIdpregunta, ConditionOperator.Equal, request.codigopregunta));
                leRespuesta.LinkEntities.Add(lePreguntaSeg);
                q.ColumnSet.AddColumns(UsuarioOV.ContactoPrincipal);
                q.LinkEntities.AddRange(leContacto, leRespuesta);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {
                    return (NEO_RecordarClaveResponse)respuestaWs.GetError("00002", idiomaorigen, "NoUsuarioRespuesta"); ;
                }
                else
                {
                    foreach (Entity item in res.Entities)
                    {
                        if ((string)((AliasedValue)item[strQueryRespuestaOV + HidrotecOvRespuesta.HidrotecOvRespuesta1]).Value == request.respuestapregunta)
                        {
                            Entity usuarioOV = new Entity(res.Entities[0].LogicalName,res.Entities[0].Id);
                            //string clave = Membership.GeneratePassword(8, 2);
                            string clave = GenerarPassword();
                            usuarioOV[UsuarioOV.Clave] = Encriptar(clave);
                            Utils.IOS.Update(usuarioOV);
                            string respuestaCorreo = enviarEmail(Utils.IOS, "AqualiaAPP Recordar Clave", "Se ha cambiado la clave del usuario asociado al documento " + request.usuario + " con la password " + clave, item.GetAttributeValue<EntityReference>(UsuarioOV.ContactoPrincipal).Id, res.Entities[0].Id,false, request.email);
                            if (respuestaCorreo != "OK")
                            {

                                throw new InvalidPluginExecutionException("Se produjo un error al enviar el correo pero se ha cambiado la clave.");

                            }
                            return (NEO_RecordarClaveResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK");
                        }
                        else
                        {


                            return(NEO_RecordarClaveResponse)respuestaWs.GetError("00002", idiomaorigen, "NoUsuarioRespuesta");
                        }
                    }
                   

                }
                throw new InvalidPluginExecutionException("Se produjo un error inesperado pongase en contacto con el Administrador.");

            }
            catch (Exception e)
            {
                return (NEO_RecordarClaveResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK
        /// DESC:
        /// Este servicio devuelve los tipos de identificación fiscal disponibles de la base de datos. Para ello, se hacen las validaciones pertinentes y 
        /// se devuelven los datos con la función fov_seltipodocid.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_TiposIdentificacionFiscalResponse NEO_TiposIdentificacionFiscal(NEO_TiposIdentificacionFiscalRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_TiposIdentificacionFiscalResponse resp = new NEO_TiposIdentificacionFiscalResponse();

            string inicio = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");
            try
            {
                Utils.InicializarVariables();
                Utils.Log("----------------------------------------------------------------------------");
                Utils.Log("$$ CRM_1 ConsultaContacto " + versionFecha + "");
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                Utils.Leer(out List<Entity> tiposidentificacion, "hidrotec_tipodedocumento", new List<string> { "hidrotec_name", "hidrotec_idtipodedocumento" }, null, null);

                if (tiposidentificacion != null && tiposidentificacion.Count > 0)
                {
                    List<tipos> listDocumentos = new List<tipos>();

                    foreach (Entity act in tiposidentificacion)
                    {
                        tipos tipo = new tipos();

                        tipo.codigotipodocumento = act.HasAttributeValue<string>("hidrotec_idtipodedocumento") ? act.GetAttributeValue<string>("hidrotec_idtipodedocumento") : null;
                        tipo.descripciontipodocumento = act.HasAttributeValue<string>("hidrotec_name") ? act.GetAttributeValue<string>("hidrotec_name") : null;

                        listDocumentos.Add(tipo);
                    }

                    resp.codigorespuesta = "00001";
                    resp.textorespuesta = "OK";
                    resp.tiposidentificador = listDocumentos;

                    return resp;

                }
                else
                {
                    return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.GetError("99999", idiomaorigen, "TiposIdentificacionBuscados");
                    //return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.TiposIdentificacionBuscados();
                }
            }
            catch (Exception e)
            {
                return (NEO_TiposIdentificacionFiscalResponse)respuestaWs.Exception(e,"99999");
            }
        }

        private static string enviarEmail(IOrganizationService service, string asunto, string cuerpo, Guid contacto,Guid GuidRegistroAsociado,bool esAltaUsuario, string emailUsuario)
        {
            try
            {
                QueryExpression q = new QueryExpression(HidrotecConfiguracionclavevalor.EntityName);
                q.ColumnSet.AddColumn(HidrotecConfiguracionclavevalor.HidrotecValor);
                q.Criteria.AddCondition(new ConditionExpression(HidrotecConfiguracionclavevalor.PrimaryName,ConditionOperator.Equal, "colaEnvioCorreosAPP"));
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                Guid BuzonInfoAqualia = Utils.takeField(service, "queue", "emailaddress", res.Entities[0].GetAttributeValue<string>(HidrotecConfiguracionclavevalor.HidrotecValor));
               
                Entity email = new Entity("email");
                //FROM
                EntityCollection ecFrom = new EntityCollection();
                var fromParty = new Entity("activityparty");
                fromParty.Attributes["partyid"] = new EntityReference("queue", BuzonInfoAqualia);
                ecFrom.Entities.Add(fromParty);
                email["from"] = ecFrom;
                EntityCollection ecTo = new EntityCollection();
                var toParty = new Entity("activityparty");
               // toParty.Attributes["partyid"] = new EntityReference(Contact.EntityName, contacto);
                toParty["addressused"] = emailUsuario;
                ecTo.Entities.Add(toParty);
                email["to"] = ecTo;
                
                 email.Attributes["subject"] = asunto;
                email.Attributes["description"] = cuerpo;
                email.Attributes["scheduledstart"] = DateTime.Now;
                email.Attributes["scheduledend"] = DateTime.Now;
                if (esAltaUsuario ==true)
                {
                    email.Attributes["regardingobjectid"] = new EntityReference(Expediente.LogicalName, GuidRegistroAsociado);
                }
                else
                {
                    email.Attributes["regardingobjectid"] = new EntityReference(UsuarioOV.LogicalName, GuidRegistroAsociado);
                }
                
               
                Guid emailID = service.Create(email);
                //Proceso de envio
                SendEmailRequest reqEmail = new SendEmailRequest();
                reqEmail.EmailId = emailID;
                reqEmail.TrackingToken = "";
                reqEmail.IssueSend = true;
                SendEmailResponse resp;
                resp = (SendEmailResponse)service.Execute(reqEmail);
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public static string GenerarPassword()
        {
            int longitud = 5;
            string contrasena = string.Empty;
            string[] letras = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            Random EleccionAleatoria = new Random();
            for (int i = 0; i < longitud; i++)
            {
                int LetraAleatoria = EleccionAleatoria.Next(0, 100);
                int NumeroAleatorio = EleccionAleatoria.Next(0, 9);
                if (LetraAleatoria < letras.Length)
                {
                    contrasena += letras[LetraAleatoria];
                }
                else
                {
                    contrasena += NumeroAleatorio.ToString();
                }
            }
            return contrasena;
        }
        public static string Encriptar(string rawData)

        {

            // Create a SHA256   

            using (SHA512 sha256Hash = SHA512.Create())

            {

                // ComputeHash - returns byte array  

                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   

                StringBuilder builder = new StringBuilder();

                for (int i = 0; i < bytes.Length; i++)

                {

                    builder.Append(bytes[i].ToString("x2"));

                    //builder.Append(bytes[i].ToString());

                }

                return builder.ToString();

            }

        }
    }
   

}
