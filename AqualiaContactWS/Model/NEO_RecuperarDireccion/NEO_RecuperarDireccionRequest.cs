﻿using AqualiaContactWS.Model.Comun;

namespace AqualiaContactWS.Model.NEO_RecuperarDireccion
{
    public class NEO_RecuperarDireccionRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public Identificadorcontrato contrato { get; set; }
    }
}