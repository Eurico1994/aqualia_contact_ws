﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_RecuperarDireccion
{
    public class NEO_RecuperarDireccionResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public Direccion direccion { get; set; }

        public static explicit operator NEO_RecuperarDireccionResponse(ResponseSimple v)
        {
            return new NEO_RecuperarDireccionResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Direccion
    {
        public string bloque { get; set; }
        public string codigomunicipio { get; set; }
        public string codigopedania { get; set; }
        public string codigopostal { get; set; }
        public Via crucevia { get; set; }
        public string edificio { get; set; }
        public string escalera { get; set; }
        public string nombremunicipio { get; set; }
        public string nombrepedania { get; set; }
        public string numero { get; set; }
        public string otros { get; set; }
        public string piso { get; set; }
        public string puerta { get; set; }
        public Via suministrovia { get; set; }
    }

    public partial class Via
    {
        public string Codigotipovia { get; set; }
        public string Codigovia { get; set; }
        public string Nombretipovia { get; set; }
        public string Nombrevia { get; set; }
    }

}