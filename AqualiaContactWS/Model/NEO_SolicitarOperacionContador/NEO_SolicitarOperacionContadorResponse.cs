﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_SolicitarOperacionContador
{
    public class NEO_SolicitarOperacionContadorResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_SolicitarOperacionContadorResponse(ResponseSimple v)
        {
            return new NEO_SolicitarOperacionContadorResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}