﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_DetalleDocumentoPago
{
    public class DocumentoPagoJson
    {
        public string codRespuesta { get; set; }
        public List<Documentospago> documentospago { get; set; }
        public string infoError { get; set; }
    }
    public class Contrato
    {
        public int codInstalacion { get; set; }
        public int codInternoContrato { get; set; }
        public string numContrato { get; set; }



    }



    public class Factura
    {
        public string codigofactura { get; set; }
        public Contrato contrato { get; set; }
        public int emisorcodigo { get; set; }
        public string emisornombre { get; set; }
        public int estadofactura { get; set; }
        public string fechaEmision { get; set; }
        public string fechaVencimiento { get; set; }
        public double importependientefactura { get; set; }
        public double importetotalfactura { get; set; }
        public double mora { get; set; }
        public string numerofactura { get; set; }
        public double recargo { get; set; }



    }



    public class Documentospago
    {
        public bool cobrable { get; set; }
        public string codigodocumentopago { get; set; }
        public List<Factura> facturas { get; set; }
        public String fechaemision { get; set; }
        public String fechavencimiento { get; set; }
        public double importependiente { get; set; }
        public double importetotal { get; set; }
        public double mora { get; set; }
        public string numerodocumentopago { get; set; }
        public string periodo { get; set; }
        public double recargo { get; set; }
        public bool tienefacturaelectronica { get; set; }



    }
}