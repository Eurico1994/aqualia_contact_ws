﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_DetalleDocumentoPago
{
    public class NEO_DetalleDocumentoPagoResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public bool cobrable { get; set; }
        public string consumomedio { get; set; }
        public string emisornombre { get; set; }
        public List<Factur> facturas { get; set; }
        public string fechaemision { get; set; }
        public string fechalimite { get; set; }
        public string importemedio { get; set; }
        public string importemora { get; set; }
        public string importependiente { get; set; }
        public string importerecargo { get; set; }
        public string importetotal { get; set; }
        public string lecturaactualfecha { get; set; }
        public string lecturaactualvalor { get; set; }
        public string lecturaanteriorfecha { get; set; }
        public string lecturaanteriorvalor { get; set; }
        public string lecturaconsumo { get; set; }
        public string lecturanumerodias { get; set; }
        public string numerocontador { get; set; }
        public string periodofacturacion { get; set; }
        public string suministrocodigopostal { get; set; }
        public string suministrodireccion { get; set; }
        public string suministronombremunicipio { get; set; }
        public string suministronombrepedania { get; set; }
        public string tienedeuda { get; set; }
        public string titularapellido1 { get; set; }
        public string titularapellido2 { get; set; }
        public string titularnombre { get; set; }
        public string titularnombretipodocumento { get; set; }
        public string titularnumerodocumento { get; set; }
        public string titulartipocliente { get; set; }

        public static explicit operator NEO_DetalleDocumentoPagoResponse(ResponseSimple v)
        {
            return new NEO_DetalleDocumentoPagoResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Factur
    {
        public List<Concepto> conceptos { get; set; }
        public string factemisordireccion { get; set; }
        public string factemisornombre { get; set; }
        public string factemisornombretipodocumento { get; set; }
        public string factemisornumerodocumento { get; set; }
        public string factnumero { get; set; }
        public List<Impuesto> Impuestos { get; set; }
        public double Totalfactura { get; set; }
    }

    public partial class Concepto
    {
        public string descripcion { get; set; }
        public double importebase { get; set; }
        public long m3Facturados { get; set; }
    }

    public partial class Impuesto
    {
        public double impuestocuota { get; set; }
        public string impuestonombre { get; set; }
    }
}