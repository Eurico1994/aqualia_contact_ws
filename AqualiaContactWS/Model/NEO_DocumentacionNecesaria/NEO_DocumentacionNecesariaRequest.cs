﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_DocumentacionNecesaria
{
    public class NEO_DocumentacionNecesariaRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigomunicipio { get; set; }
        public string codigotipocliente { get; set; }
        public string usuario { get; set; }
    
    }
}