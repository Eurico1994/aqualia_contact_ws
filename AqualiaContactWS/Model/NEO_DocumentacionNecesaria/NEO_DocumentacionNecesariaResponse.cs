﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_DocumentacionNecesaria
{
    public class NEO_DocumentacionNecesariaResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<documento> documentos { get; set; }

        public static explicit operator NEO_DocumentacionNecesariaResponse(ResponseSimple v)
        {
            return new NEO_DocumentacionNecesariaResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class documento
    {
        public string codigodocumento { get; set; }
        public bool obligatorio { get; set; }
        public string titulodocumento { get; set; }
    }
}