﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoPaises
{
    public class NEO_ListadoPaisesResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<pais> paises { get; set; }

        public static explicit operator NEO_ListadoPaisesResponse(ResponseSimple v)
        {
            return new NEO_ListadoPaisesResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class pais
    {
        public string codigopais { get; set; }
        public string nombrepais { get; set; }
    }
}