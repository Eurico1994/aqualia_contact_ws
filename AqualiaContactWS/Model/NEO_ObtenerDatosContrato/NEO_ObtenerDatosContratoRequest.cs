﻿using AqualiaContactWS.Model.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace WS.Contratos.Model.NEO_ObtenerDatosContrato
{
    public class NEO_ObtenerDatosContratoRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string usuario { get; set; }
    }
}