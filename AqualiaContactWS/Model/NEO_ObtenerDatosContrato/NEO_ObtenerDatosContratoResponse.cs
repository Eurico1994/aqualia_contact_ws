﻿using AqualiaContactWS.Model.Comun;
using Helper.Model;

namespace WS.Contratos.Model.NEO_ObtenerDatosContrato
{
    public class NEO_ObtenerDatosContratoResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public DatosContrato datosContrato { get; set; }

        public static explicit operator NEO_ObtenerDatosContratoResponse(ResponseSimple v)
        {
            return new NEO_ObtenerDatosContratoResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class DatosContrato
    {
        public Cobrocontrato cobrocontrato { get; set; }
        public string codigomunicipio { get; set; }
        public string codigotipocliente { get; set; }
        public Correspondenciacontrato correspondenciacontrato { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string nombremunicipio { get; set; }
        public Suministrocontrato suministrocontrato { get; set; }
        public string textotipocliente { get; set; }
        public Titularcontrato titularcontrato { get; set; }
    }

    public partial class Cobrocontrato
    {
        public string cobroapellido1 { get; set; }
        public string cobroapellido2 { get; set; }
        public string cobrobanco { get; set; }
        public string cobrocodigotipodocumento { get; set; }
        public string cobrocuenta { get; set; }
        public string cobrodc { get; set; }
        public string cobrofax { get; set; }
        public string cobronombre { get; set; }
        public string cobronombreentidad { get; set; }
        public string cobronumerodocumento { get; set; }
        public string cobrosucursal { get; set; }
        public string cobrotelefono1 { get; set; }
        public string cobrotelefono2 { get; set; }
        public string cobrotipodocumento { get; set; }
        public string fechalimitepago { get; set; }
        public string importemaximopago { get; set; }
    }

    public partial class Correspondenciacontrato
    {
        public string correspondenciaapellido1 { get; set; }
        public string correspondenciaapellido2 { get; set; }
        public string correspondenciabloque { get; set; }
        public string correspondenciacodigomunicipio { get; set; }
        public string correspondenciacodigopedania { get; set; }
        public string correspondenciacodigopostal { get; set; }
        public string correspondenciacodigotipodocumento { get; set; }
        public string correspondenciadirsincodificar { get; set; }
        public string correspondenciaedificio { get; set; }
        public string correspondenciaemail { get; set; }
        public string correspondenciaescalera { get; set; }
        public string correspondenciafax { get; set; }
        public string correspondenciaidioma { get; set; }
        public string correspondenciamunicipiosincodificar { get; set; }
        public string correspondencianombre { get; set; }
        public string correspondencianombremunicipio { get; set; }
        public string correspondencianombrepedania { get; set; }
        public string correspondencianumero { get; set; }
        public string correspondencianumerodocumento { get; set; }
        public string correspondenciaotros { get; set; }
        public string correspondenciapiso { get; set; }
        public string correspondenciapuerta { get; set; }
        public string correspondenciatelefono1 { get; set; }
        public string correspondenciatelefono2 { get; set; }
        public Via correspondenciavia { get; set; }
        public bool facturaelectronicaactivar { get; set; }
    }

    public partial class Via
    {
        public string codigotipovia { get; set; }
        public string codigovia { get; set; }
        public string nombretipovia { get; set; }
        public string nombrevia { get; set; }
    }

    public partial class Suministrocontrato
    {
        public string suministrobloque { get; set; }
        public string suministrocodigopedania { get; set; }
        public string suministrocodigopostal { get; set; }
        public string suministroedificio { get; set; }
        public string suministroescalera { get; set; }
        public string suministronombrepedania { get; set; }
        public string suministronumero { get; set; }
        public string suministrootros { get; set; }
        public string suministropiso { get; set; }
        public Via suministrovia { get; set; }
        public string suminitropuerta { get; set; }
    }

    public partial class Titularcontrato
    {
        public string titularapellido1 { get; set; }
        public string titularapellido2 { get; set; }
        public string titularcodigotipodocumento { get; set; }
        public string titularfax { get; set; }
        public string titularfechanacimiento { get; set; }
        public string titularnombre { get; set; }
        public string titularnumerodocumento { get; set; }
        public string titulartelefono1 { get; set; }
        public string titulartelefono2 { get; set; }
        public string titulartipodocumento { get; set; }
    }

}