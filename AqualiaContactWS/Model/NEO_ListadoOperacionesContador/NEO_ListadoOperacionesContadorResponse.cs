﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Helper.Model;

namespace AqualiaContactWS.Model.NEO_ListadoOperacionesContador
{
    public class NEO_ListadoOperacionesContadorResponse
    { 
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<operacionescontador> operaciones { get; set; }

        public static explicit operator NEO_ListadoOperacionesContadorResponse(ResponseSimple v)
        {
            return new NEO_ListadoOperacionesContadorResponse {  codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };

        }
    }

    public partial class operacionescontador
    {
        public string codigooperacion { get; set; }
        public string textooperacion { get; set; }
    }
}