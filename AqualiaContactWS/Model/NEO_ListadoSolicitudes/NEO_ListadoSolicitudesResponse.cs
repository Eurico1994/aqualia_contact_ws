﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoSolicitudes
{
    public class NEO_ListadoSolicitudesResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<solicitud> solicitudes { get; set; }

        public static explicit operator NEO_ListadoSolicitudesResponse(ResponseSimple v)
        {
            return new NEO_ListadoSolicitudesResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class solicitud
    {
        public string detallesolicitud { get; set; }
        public string estadosolicitud { get; set; }
        public string fecharesolucion { get; set; }
        public string fechasolicitud { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string identificadorsolicitud { get; set; }
        public string subtiporelacion { get; set; }
    }

    public partial class Identificadorcontrato
    {
        public string codigocac { get; set; }
        public string codigocontrato { get; set; }
        public string codigoinstalacion { get; set; }
        public string numerocontrato { get; set; }
    }
}