﻿using AqualiaContactWS.Model.Comun;
using Helper.Model;
using System.Collections.Generic;

namespace AqualiaContactWS.Model.NEO_ListadoReclamaciones
{
    public class NEO_ListadoReclamacionesResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<Reclamacione> reclamaciones { get; set; }

        public static explicit operator NEO_ListadoReclamacionesResponse(ResponseSimple v)
        {
            return new NEO_ListadoReclamacionesResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Reclamacione
    {
        public string detallereclamacion { get; set; }
        public string estadoreclamacion { get; set; }
        public string fechareclamacion { get; set; }
        public string fecharesolucion { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string identificadorreclamacion { get; set; }
        public string textomotivoreclamacion { get; set; }
        public string textosubmotivoreclamacion { get; set; }
    }
}