﻿using Helper.Model;

namespace WS.Usuario.Model.NEO_CambiarPreguntaSeguridadUsuario
{
    public class NEO_CambiarPreguntaSeguridadUsuarioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_CambiarPreguntaSeguridadUsuarioResponse(ResponseSimple v)
        {
            return new NEO_CambiarPreguntaSeguridadUsuarioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}