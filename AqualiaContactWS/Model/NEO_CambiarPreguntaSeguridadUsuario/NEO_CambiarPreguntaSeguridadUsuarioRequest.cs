﻿namespace WS.Usuario.Model.NEO_CambiarPreguntaSeguridadUsuario
{
    public class NEO_CambiarPreguntaSeguridadUsuarioRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigopregunta { get; set; }
        public string respuesta { get; set; }
        public string usuario { get; set; }
    }
}