﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_PedaniaMunicipio
{
    public class NEO_PedaniaMunicipioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<pedania> pedanias { get; set; }

        public static explicit operator NEO_PedaniaMunicipioResponse(ResponseSimple v)
        {
            return new NEO_PedaniaMunicipioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class pedania
    {
        public string codigopedania { get; set; }
        public string nombrepedania { get; set; }
    }
}