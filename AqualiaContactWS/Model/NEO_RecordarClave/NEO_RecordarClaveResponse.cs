﻿using Helper.Model;

namespace WS.Usuario.Model.NEO_RecordarClave
{
    public class NEO_RecordarClaveResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_RecordarClaveResponse(ResponseSimple v)
        {
            return new NEO_RecordarClaveResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}