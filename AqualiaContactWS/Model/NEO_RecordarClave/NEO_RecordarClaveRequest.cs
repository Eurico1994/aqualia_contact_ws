﻿namespace WS.Usuario.Model.NEO_RecordarClave
{
    public class NEO_RecordarClaveRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string email { get; set; }
        public string codigopregunta { get; set; }
        public string respuestapregunta { get; set; }
        public string usuario { get; set; }
    }
}