﻿namespace WS.Usuario.Model.NEO_TiposIdentificacionFiscal
{
    public class NEO_TiposIdentificacionFiscalRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string usuario { get; set; }
    }
}