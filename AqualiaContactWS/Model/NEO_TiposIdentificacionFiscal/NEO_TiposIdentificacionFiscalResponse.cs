﻿using System.Collections.Generic;
using Helper.Model;

namespace WS.Usuario.Model.NEO_TiposIdentificacionFiscal
{
    public class NEO_TiposIdentificacionFiscalResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<tipos> tiposidentificador { get; set; }

        public static explicit operator NEO_TiposIdentificacionFiscalResponse(ResponseSimple v)
        {
            return new NEO_TiposIdentificacionFiscalResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public class tipos
    {
        public string codigotipodocumento { get; set; }
        public string descripciontipodocumento { get; set; }
    }

}