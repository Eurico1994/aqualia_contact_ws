﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Helper.Model;

namespace AqualiaContactWS.Model.NEO_SolicitarCambioTarifa
{
    public class NEO_SolicitarCambioTarifaResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_SolicitarCambioTarifaResponse(ResponseSimple v)
        {
            return new NEO_SolicitarCambioTarifaResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };

        }
    }
}