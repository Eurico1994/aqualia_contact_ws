﻿using AqualiaContactWS.Model.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_SolicitarCambioTarifa
{
    public class NEO_SolicitarCambioTarifaRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigotipocliente { get; set; }
        public string correoelectronico { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string motivocambio { get; set; }
        public string telefonocontacto { get; set; }
        public string usuario { get; set; }
    }
   
}