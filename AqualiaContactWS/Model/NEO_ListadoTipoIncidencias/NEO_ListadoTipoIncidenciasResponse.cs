﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoTipoIncidencias
{
    public class NEO_ListadoTipoIncidenciasResponse
    {
        public string Codigorespuesta { get; set; }
        public string Textorespuesta { get; set; }
        public List<Tiposincidencia> Tiposincidencia { get; set; }

        public static explicit operator NEO_ListadoTipoIncidenciasResponse(ResponseSimple v)
        {
            return new NEO_ListadoTipoIncidenciasResponse { Codigorespuesta = v.codigorespuesta, Textorespuesta = v.textorespuesta };
        }
    }

    public partial class Tiposincidencia
    {
        public string Codigotipoincidencia { get; set; }
        public string Textotipoincidencia { get; set; }
    }
}