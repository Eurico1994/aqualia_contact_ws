﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoTiposCliente
{
    public class NEO_ListadoTiposClienteResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<tipos> tiposcliente { get; set; }

        public static explicit operator NEO_ListadoTiposClienteResponse(ResponseSimple v)
        {
            return new NEO_ListadoTiposClienteResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class tipos
    {
        public string codigocliente { get; set; }
        public string texto { get; set; }
    }

}