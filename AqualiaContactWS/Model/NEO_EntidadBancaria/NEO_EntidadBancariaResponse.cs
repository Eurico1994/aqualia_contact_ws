﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_EntidadBancaria
{
    public class NEO_EntidadBancariaResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public string nombreentidad { get; set; }

        public static explicit operator NEO_EntidadBancariaResponse(ResponseSimple v)
        {
            return new NEO_EntidadBancariaResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}