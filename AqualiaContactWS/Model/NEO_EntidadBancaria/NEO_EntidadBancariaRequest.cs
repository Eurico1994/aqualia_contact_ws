﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_EntidadBancaria
{
    public class NEO_EntidadBancariaRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigoentidad { get; set; }
        public string codigomunicipio { get; set; }
       // public string numerocuenta { get; set; }
        public string usuario { get; set; }
    }
}