﻿using AqualiaContactWS.Model.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_EnviarReclamacion
{
    public class NEO_EnviarReclamacionRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigomotivoreclamacion { get; set; }
        public string codigomunicipio { get; set; }
        public string codigosubmotivoreclamacion { get; set; }
        public string correoelectronico { get; set; }
        public string detallereclamacion { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string usuario { get; set; }
    }
}