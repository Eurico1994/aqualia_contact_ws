﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_EnviarReclamacion
{
    public class NEO_EnviarReclamacionResponse
    {
        public string Codigorespuesta { get; set; }
        public string Textorespuesta { get; set; }
        public string Identificadorreclamación { get; set; }

        public static explicit operator NEO_EnviarReclamacionResponse(ResponseSimple v)
        {
            return new NEO_EnviarReclamacionResponse { Codigorespuesta = v.codigorespuesta, Textorespuesta = v.textorespuesta };
        }
    }
}