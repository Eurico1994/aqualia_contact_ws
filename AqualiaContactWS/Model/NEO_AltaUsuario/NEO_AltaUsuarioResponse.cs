﻿using Helper.Model;

namespace WS.Usuario.Model.NEO_AltaUsuario
{
    public class NEO_AltaUsuarioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public string identificador { get; set; }

        public static explicit operator NEO_AltaUsuarioResponse(ResponseSimple v)
        {
                return new NEO_AltaUsuarioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}