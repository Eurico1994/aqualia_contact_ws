﻿namespace WS.Usuario.Model.NEO_AltaUsuario
{
    public class NEO_AltaUsuarioRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigopregunta { get; set; }
        public string codigotipodocumento { get; set; }
        public string correoelectronico { get; set; }
        public string numerocontrato { get; set; }
        public string numerodocumento { get; set; }
        public string respuestapregunta { get; set; }
    }
}