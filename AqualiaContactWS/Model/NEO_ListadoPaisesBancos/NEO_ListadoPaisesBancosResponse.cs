﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoPaisesBancos
{
    public class NEO_ListadoPaisesBancosResponse
{
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<Pais> paises { get; set; }

        public static explicit operator NEO_ListadoPaisesBancosResponse(ResponseSimple v)
        {
            return new NEO_ListadoPaisesBancosResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Pais
    {
        public long codigopais { get; set; }
        public string mascarapais { get; set; }
        public string nombrepais { get; set; }
        public string pais { get; set; }
    }
}