﻿namespace WS.Usuario.Model.NEO_ComprobarUsuario
{
    public class NEO_ComprobarUsuarioRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string usuario { get; set; }
    }
}