﻿using Helper.Model;

namespace WS.Usuario.Model.NEO_ComprobarUsuario
{
    public class NEO_ComprobarUsuarioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_ComprobarUsuarioResponse(ResponseSimple v)
        {
            return new NEO_ComprobarUsuarioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}