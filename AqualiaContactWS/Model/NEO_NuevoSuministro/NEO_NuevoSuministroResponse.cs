﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_NuevoSuministro
{
    public class NEO_NuevoSuministroResponse
    {
        public string Codigorespuesta { get; set; }
        public string Textorespuesta { get; set; }

        public static explicit operator NEO_NuevoSuministroResponse(ResponseSimple v)
        {
            return new NEO_NuevoSuministroResponse { Codigorespuesta = v.codigorespuesta, Textorespuesta = v.textorespuesta };
        }
    }
}