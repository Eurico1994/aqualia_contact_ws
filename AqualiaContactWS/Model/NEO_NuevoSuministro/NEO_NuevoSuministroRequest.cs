﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_NuevoSuministro
{
    public class NEO_NuevoSuministroRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigomunicipio { get; set; }
        public string codigotipocliente { get; set; }
        public string usuario { get; set; }
        public Datoscobro datoscobro { get; set; }
        public Datoscorrespondencia datoscorrespondencia { get; set; }
        public Datossuministro datossuministro { get; set; }
        public Datostitular datostitular { get; set; }
    }

    public partial class Datoscobro
    {
        public string cobroapellido1 { get; set; }
        public string cobroapellido2 { get; set; }
        public string cobrobanco { get; set; }
        public string cobrocodigopaisdocumento { get; set; }
        public string cobrocodigotipodocumento { get; set; }
        public string cobrocuenta { get; set; }
        public string cobrodc { get; set; }
        public string cobrofax { get; set; }
        public string cobronombre { get; set; }
        public string cobronombreentidad { get; set; }
       // public string cobronumerocuenta { get; set; }
        public string cobronumerodocumento { get; set; }
        public string cobrosucursal { get; set; }
        public string cobrotelefono1 { get; set; }
        public string cobrotelefono2 { get; set; }
        public string cobrotelefono3 { get; set; }
        public string cobrotipodocumento { get; set; }
        public string fechalimitepago { get; set; }
        public long importemaximopago { get; set; }
    }

    public partial class Datoscorrespondencia
    {
        public string correspondenciaapellido1 { get; set; }
        public string correspondenciaapellido2 { get; set; }
        public string correspondenciabloque { get; set; }
        public string correspondenciacodigomunicipio { get; set; }
        public string correspondenciacodigopaisdocumento { get; set; }
        public string correspondenciacodigopedania { get; set; }
        public string correspondenciacodigopostal { get; set; }
        public string correspondenciacodigotipodocumento { get; set; }
        public string correspondenciadirsincodificar { get; set; }
        public string correspondenciaedificio { get; set; }
        public string correspondenciaemail { get; set; }
        public string correspondenciaescalera { get; set; }
        public string correspondenciafax { get; set; }
        public string correspondenciaidioma { get; set; }
        public string correspondenciamunicipiosincodificar { get; set; }
        public string correspondencianombre { get; set; }
        public string correspondencianombremunicipio { get; set; }
        public string correspondencianombrepedania { get; set; }
        public string correspondencianumero { get; set; }
        public string correspondencianumerodocumento { get; set; }
        public string correspondenciaotros { get; set; }
        public string correspondenciapais { get; set; }
        public string correspondenciapiso { get; set; }
        public string correspondenciapuerta { get; set; }
        public string correspondenciatelefono1 { get; set; }
        public string correspondenciatelefono2 { get; set; }
        public Via correspondenciavia { get; set; }
        public bool facturaelectronicaactivar { get; set; }
    }

    public partial class Via
    {
        public string codigotipovia { get; set; }
        public string codigovia { get; set; }
        public string nombretipovia { get; set; }
        public string nombrevia { get; set; }
    }

    public partial class Datossuministro
    {
        public string suministrobloque { get; set; }
        public string suministrocodigopedania { get; set; }
        public string suministrocodigopostal { get; set; }
        public string suministroedificio { get; set; }
        public string suministroescalera { get; set; }
        public string suministronombrepedania { get; set; }
        public string suministronumero { get; set; }
        public string suministrootros { get; set; }
        public string suministropiso { get; set; }
        public string suministropuerta { get; set; }
        public Via suministrovia { get; set; }
    }

    public partial class Datostitular
    {
        public string titularapellido1 { get; set; }
        public string titularapellido2 { get; set; }
        public string titularcodigopaisdocumento { get; set; }
        public string titularcodigotipodocumento { get; set; }
        public string titularfax { get; set; }
        public string titularfechanacimiento { get; set; }
        public string titularnombre { get; set; }
        public string titularnumerodocumento { get; set; }
        public string titulartelefono1 { get; set; }
        public string titulartelefono2 { get; set; }
      //  public string titulartelefono3 { get; set; }
        public string titulartipodocumento { get; set; }
    }
}