﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ActivarFacturaElectronica
{
    public class NEO_ActivarFacturaElectronicaResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_ActivarFacturaElectronicaResponse(ResponseSimple v)
        {
            return new NEO_ActivarFacturaElectronicaResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}