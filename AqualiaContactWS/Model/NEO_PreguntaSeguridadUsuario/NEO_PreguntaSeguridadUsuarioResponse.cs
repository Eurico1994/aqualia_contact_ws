﻿using Helper.Model;

namespace WS.Usuario.Model.NEO_PreguntaSeguridadUsuario
{
    public class NEO_PreguntaSeguridadUsuarioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public PreguntaSeguridad pregunta { get; set; }

        public static explicit operator NEO_PreguntaSeguridadUsuarioResponse(ResponseSimple v)
        {
            return new NEO_PreguntaSeguridadUsuarioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public class PreguntaSeguridad
    {
        public string codigopregunta { get; set; }
        public string tipopregunta { get; set; }
    }
}