﻿using Helper.Model;

namespace WS.Usuario.Model.NEO_BajaUsuario
{
    public class NEO_BajaUsuarioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_BajaUsuarioResponse(ResponseSimple v)
        {
            return new NEO_BajaUsuarioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };

        }
    }
}