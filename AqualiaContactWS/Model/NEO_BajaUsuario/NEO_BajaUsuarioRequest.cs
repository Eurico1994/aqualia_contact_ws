﻿namespace WS.Usuario.Model.NEO_BajaUsuario
{
    public class NEO_BajaUsuarioRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigomotivobaja { get; set; }
        public string usuario { get; set; }
    }
}