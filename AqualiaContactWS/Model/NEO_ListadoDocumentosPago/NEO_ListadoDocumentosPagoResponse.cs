﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoDocumentosPago
{
    public class NEO_ListadoDocumentosPagoResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<Documentospago> documentospago { get; set; }

        public static explicit operator NEO_ListadoDocumentosPagoResponse(ResponseSimple v)
        {
            return new NEO_ListadoDocumentosPagoResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Documentospago
    {
        public bool cobrable { get; set; }
        public string codigodocumentopago { get; set; }
        public string fechaemision { get; set; }
        public string fechavencimiento { get; set; }
        public double importependiente { get; set; }
        public double importetotal { get; set; }
        public List<Informacioncobro> informacioncobro { get; set; }
        public double mora { get; set; }
        public string numerodocumentopago { get; set; }
        public string periodo { get; set; }
        public double recargo { get; set; }
        public bool tienefacturaelectronica { get; set; }
    }

    public partial class Informacioncobro
    {
        public string codigofactura { get; set; }
        public double importependientefactura { get; set; }
        public double mora { get; set; }
        public double recargo { get; set; }
    }
}