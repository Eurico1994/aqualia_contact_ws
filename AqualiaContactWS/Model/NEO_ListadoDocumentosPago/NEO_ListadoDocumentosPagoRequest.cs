﻿using AqualiaContactWS.Model.Comun;

namespace AqualiaContactWS.Model.NEO_ListadoDocumentosPago
{
    public class NEO_ListadoDocumentosPagoRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string fechadesde { get; set; }
        public string fechahasta { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string tienedeuda { get; set; }
        public string usuario { get; set; }
    }
}