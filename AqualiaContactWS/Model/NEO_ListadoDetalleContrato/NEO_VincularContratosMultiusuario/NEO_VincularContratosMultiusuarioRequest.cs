﻿using AqualiaContactWS.Model.Comun;
using System.Collections.Generic;


namespace WS.Contratos.Model.NEO_VincularContratosMultiusuario
{
    public class NEO_VincularContratosMultiusuarioRequest
     {
        public string Canalentrada { get; set; }
        public string Codpais { get; set; }
        public string Idioma { get; set; }
        public List<IdentificadorcontratoElement> Identificadorcontrato { get; set; }
        public string Usuario { get; set; }
    }

    public partial class IdentificadorcontratoElement
    {
        public Identificadorcontrato Identificadorcontrato { get; set; }
        public bool Marcaasociado { get; set; }
    }
}