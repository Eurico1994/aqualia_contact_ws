﻿using Helper.Model;

namespace WS.Contratos.Model.NEO_VincularContratosMultiusuario
{
    public class NEO_VincularContratosMultiusuarioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_VincularContratosMultiusuarioResponse(ResponseSimple v)
        {
            return new NEO_VincularContratosMultiusuarioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}