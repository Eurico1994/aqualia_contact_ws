﻿using System.Collections.Generic;
using AqualiaContactWS.Model.Comun;
using Helper.Model;

namespace WS.Contratos.Model.NEO_ListadoDetalleContrato
{
    public class NEO_ListadoDetalleContratoResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<Contrato> contratos { get; set; }

        public static explicit operator NEO_ListadoDetalleContratoResponse(ResponseSimple v)
        {
            return new NEO_ListadoDetalleContratoResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    
}

public partial class Contrato
{
    public string codigomunicipio { get; set; }
    public bool condeuda { get; set; }
    public Deuda deuda { get; set; }
    public List<Histogramaconsumo> histogramaconsumos { get; set; }
    public Identificadorcontrato identificadorcontrato { get; set; }
    public string nombremunicipio { get; set; }
    public string suministrodireccion { get; set; }
    public string suministronombrepedania { get; set; }
    public Ultimafactura ultimafactura { get; set; }
}

public partial class Deuda
{
    public string importependiente { get; set; }
    public string numfacturas { get; set; }
}

public partial class Histogramaconsumo
{
    public string m3 { get; set; }
    public string peridodo { get; set; }
}

public partial class Ultimafactura
{
    public string fechaemision { get; set; }
    public string identificador { get; set; }
    public decimal importefactura { get; set; }
    public bool pendiente { get; set; }
    public string peridodofacturacion { get; set; }
}
