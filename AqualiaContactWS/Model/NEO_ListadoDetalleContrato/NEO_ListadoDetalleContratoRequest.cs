﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Contratos.Model.NEO_ListadoDetalleContrato
{
    public class NEO_ListadoDetalleContratoRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string usuario { get; set; }

    }
}