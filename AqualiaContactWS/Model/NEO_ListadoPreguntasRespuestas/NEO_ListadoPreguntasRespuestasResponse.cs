﻿using System.Collections.Generic;
using Helper.Model;

namespace AqualiaContactWS.Model.NEO_ListadoPreguntasRespuestas
{
    public class NEO_ListadoPreguntasRespuestasResponse
    {
        public string Codigorespuesta { get; set; }
        public string Textorespuesta { get; set; }
        public string Pregunta { get; set; }
        public List<Respuesta> Respuestas { get; set; }

        public static explicit operator NEO_ListadoPreguntasRespuestasResponse(ResponseSimple v)
        {
            return new NEO_ListadoPreguntasRespuestasResponse { Codigorespuesta = v.codigorespuesta, Textorespuesta = v.textorespuesta };
        }
    }

    public partial class Respuesta
    {
        public Averia Averia { get; set; }
        public string Mensaje { get; set; }
        public string Siguientenodo { get; set; }
        public string TextoRespuesta { get; set; }
    }

    public partial class Averia
    {
        public string Codigosubtipoaveria { get; set; }
        public string Codigotipoaveria { get; set; }
    }
}