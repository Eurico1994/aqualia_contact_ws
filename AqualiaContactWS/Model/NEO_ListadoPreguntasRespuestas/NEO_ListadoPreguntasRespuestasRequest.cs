﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoPreguntasRespuestas
{
    public class NEO_ListadoPreguntasRespuestasRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string identificadornodo { get; set; }
        public bool tienecontrato { get; set; }
    }
}