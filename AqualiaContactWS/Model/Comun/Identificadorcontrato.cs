﻿namespace AqualiaContactWS.Model.Comun
{
    public class Identificadorcontrato
    {
        public string codigocac { get; set; }
        public string codigocontrato { get; set; }
        public string codigoinstalacion { get; set; }
        public string numerocontrato { get; set; }
    }
}