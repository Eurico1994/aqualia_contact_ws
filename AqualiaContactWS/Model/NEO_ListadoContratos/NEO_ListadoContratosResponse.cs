﻿using System.Collections.Generic;
using Helper.Model;

namespace WS.Contratos.Model.NEO_ListadoContratos
{
    public class NEO_ListadoContratosResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<contrato> contratos { get; set; }

        public static explicit operator NEO_ListadoContratosResponse(ResponseSimple v)
        {
            return new NEO_ListadoContratosResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public class contrato
    {
        public string codigomunicipio { get; set; }
        public identificadorcontrato identificadorcontrato { get; set; }
        public string nombremunicipio { get; set; }
        public string suministrodireccion { get; set; }
        public string suministronombrepedania { get; set; }

    }

    public class identificadorcontrato
    {
        public string codigocac { get; set; }
        public string codigocontrato { get; set; }
        public string codigoinstalacion { get; set; }
        public bool marcaasociado { get; set; }

    }
}
