﻿using System;
namespace WS.Usuario.Model.NEO_ListadoMotivosBaja
{
    public class NEO_ListadoMotivosBajaRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string usuario { get; set; }
    }
}