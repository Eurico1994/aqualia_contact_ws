﻿using System.Collections.Generic;
using Helper.Model;

namespace WS.Usuario.Model.NEO_ListadoMotivosBaja
{
    public class NEO_ListadoMotivosBajaResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<motivos> motivosbaja { get; set; }

        public static explicit operator NEO_ListadoMotivosBajaResponse(ResponseSimple v)
        {
            return new NEO_ListadoMotivosBajaResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public class motivos
    {
        public string codigomotivobaja { get; set; }
        public string textomotivobaja { get; set; }
    }
}