﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_RecuperarDatosContacto
{
    public class NEO_RecuperarDatosContactoResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public Datoscontacto datoscontacto { get; set; }

        public static explicit operator NEO_RecuperarDatosContactoResponse(ResponseSimple v)
        {
            return new NEO_RecuperarDatosContactoResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Datoscontacto
    {
        public string apellido1 { get; set; }
        public string apellido2 { get; set; }
        public string correoelectronico { get; set; }
        public string nombre { get; set; }
        public string telefono { get; set; }
    }

}