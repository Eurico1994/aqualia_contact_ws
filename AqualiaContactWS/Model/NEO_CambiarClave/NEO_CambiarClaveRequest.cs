﻿namespace WS.Usuario.Model.NEO_CambiarClave
{
    public class NEO_CambiarClaveRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string clave { get; set; }
        public string nuevaclave { get; set; }
        public string usuario { get; set; }
    }
}