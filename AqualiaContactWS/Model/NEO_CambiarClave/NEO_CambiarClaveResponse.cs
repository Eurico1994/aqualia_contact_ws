﻿using Helper.Model;

namespace WS.Usuario.Model.NEO_CambiarClave
{
    public class NEO_CambiarClaveResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_CambiarClaveResponse(ResponseSimple v)
        {
            return new NEO_CambiarClaveResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}