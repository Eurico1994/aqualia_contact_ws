﻿using AqualiaContactWS.Model.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoContadoresContrato
{
    public class NEO_ListadoContadoresContratoRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string usuario { get; set; }
    }
}