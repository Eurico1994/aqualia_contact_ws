﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Helper.Model;

namespace AqualiaContactWS.Model.NEO_ListadoContadoresContrato
{
    public class NEO_ListadoContadoresContratoResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<Contador> contadores { get; set; }

        public static explicit operator NEO_ListadoContadoresContratoResponse(ResponseSimple v)
        {
             return new NEO_ListadoContadoresContratoResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Contador
    {
        public string codigocontador { get; set; }
        public string modelocontador { get; set; }
        public string numerocontador { get; set; }
    }
}