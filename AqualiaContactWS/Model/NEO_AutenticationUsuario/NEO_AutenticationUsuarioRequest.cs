﻿namespace WS.Usuario.Model.NEO_AutenticationUsuario
{
    public class NEO_AutenticationUsuarioRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string clave { get; set; }
        public string usuario { get; set; }
    }
}