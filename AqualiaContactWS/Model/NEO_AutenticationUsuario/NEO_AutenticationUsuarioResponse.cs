﻿using Helper.Model;

namespace WS.Usuario.Model.NEO_AutenticationUsuario
{
    public class NEO_AutenticationUsuarioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_AutenticationUsuarioResponse(ResponseSimple v)
        {
            return new NEO_AutenticationUsuarioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };

        }
    }
}