﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoMotivosReclamacion
{
    public class NEO_ListadoMotivosReclamacionResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<Motivosreclamacion> motivosreclamacion { get; set; }

        public static explicit operator NEO_ListadoMotivosReclamacionResponse(ResponseSimple v)
        {
            return new NEO_ListadoMotivosReclamacionResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Motivosreclamacion
    {
        public string codigomotivoreclamacion { get; set; }
        public string textomotivoreclamacion { get; set; }
    }
}