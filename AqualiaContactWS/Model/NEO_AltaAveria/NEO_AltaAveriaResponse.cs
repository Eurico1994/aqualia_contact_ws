﻿using Helper.Model;

namespace AqualiaContactWS.Model.NEO_AltaAveria
{
    public class NEO_AltaAveriaResponse
    {
        public string Codigorespuesta { get; set; }
        public string Textorespuesta { get; set; }

        public static explicit operator NEO_AltaAveriaResponse(ResponseSimple v)
        {
            return new NEO_AltaAveriaResponse { Codigorespuesta = v.codigorespuesta, Textorespuesta = v.textorespuesta };
        }
    }
}