﻿using AqualiaContactWS.Model.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_AltaAveria
{
    public class NEO_AltaAveriaRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public Averia averia { get; set; }
        public Datoscontacto datoscontacto { get; set; }
        public string detalle { get; set; }
        public Direccion direccion { get; set; }
        public string direccionsincodificar { get; set; }
        public Geolocalizacion geolocalizacion { get; set; }
        public Identificadorcontrato identificadorcontrato { get; set; }
        public string imagen1 { get; set; }
        public string imagen2 { get; set; }
        public string usuario { get; set; }
    }

    public partial class Averia
    {
        public string codigosubtipoaveria { get; set; }
        public string codigotipoaveria { get; set; }
    }

    public partial class Datoscontacto
    {
        public string apellido1 { get; set; }
        public string apellido2 { get; set; }
        public string correoelectronico { get; set; }
        public string nombre { get; set; }
        public string telefono { get; set; }
    }

    public partial class Direccion
    {
        public string bloque { get; set; }
        public string codigomunicipio { get; set; }
        public string codigopedania { get; set; }
        public string codigopostal { get; set; }
        public Via crucevia { get; set; }
        public string edificio { get; set; }
        public string escalera { get; set; }
        public string nombremunicipio { get; set; }
        public string nombrepedania { get; set; }
        public string numero { get; set; }
        public string otros { get; set; }
        public string piso { get; set; }
        public string puerta { get; set; }
        public Via via { get; set; }
    }

    public partial class Via
    {
        public string codigotipovia { get; set; }
        public string codigovia { get; set; }
        public string nombretipovia { get; set; }
        public string nombrevia { get; set; }
    }

    public partial class Geolocalizacion
    {
        public string latitud { get; set; }
        public string longitud { get; set; }
    }
}