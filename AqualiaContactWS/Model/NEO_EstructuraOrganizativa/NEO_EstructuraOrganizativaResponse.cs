﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_EstructuraOrganizativa
{
    public class NEO_EstructuraOrganizativaResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<Zona> zonas { get; set; }

        public static explicit operator NEO_EstructuraOrganizativaResponse(ResponseSimple v)
        {
            return new NEO_EstructuraOrganizativaResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class Zona
    {
        public string codigozona { get; set; }
        public List<Delegacione> delegaciones { get; set; }
        public string nombrezona { get; set; }
    }

    public partial class Delegacione
    {
        public string codigodelegacion { get; set; }
        public List<Contrata> contratas { get; set; }
        public string nombredelegacion { get; set; }
    }

    public partial class Contrata
    {
        public string codigocontrata { get; set; }
        public List<Explotacione> explotaciones { get; set; }
        public string nombrecontrata { get; set; }
    }

    public partial class Explotacione
    {
        public Identificador identificador { get; set; }
        public List<Municipio> municipios { get; set; }
        public string nombreexplotacion { get; set; }
    }

    public partial class Identificador
    {
        public long Codigocontrata { get; set; }
        public long Codigoinstalacion { get; set; }
        public long Codigoservicio { get; set; }
        public string Numeroexplotacion { get; set; }
    }

    public partial class Municipio
    {
        public string codigomunicipio { get; set; }
        public string nombremunicipio { get; set; }
    }
}