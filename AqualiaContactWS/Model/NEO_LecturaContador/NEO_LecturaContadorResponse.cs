﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_LecturaContador
{
    public class NEO_LecturaContadorResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }

        public static explicit operator NEO_LecturaContadorResponse(ResponseSimple v)
        {
            return new NEO_LecturaContadorResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}