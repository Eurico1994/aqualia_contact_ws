﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_EnviarIncidencias
{
    public class NEO_EnviarIncidenciasResponse
    {
        public string Codigorespuesta { get; set; }
        public string Textorespuesta { get; set; }

        public static explicit operator NEO_EnviarIncidenciasResponse(ResponseSimple v)
        {
            return new NEO_EnviarIncidenciasResponse { Codigorespuesta = v.codigorespuesta, Textorespuesta = v.textorespuesta };
        }
    }
}