﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_EnviarIncidencias
{
    public class NEO_EnviarIncidenciasRequest
    {
        public string Canalentrada { get; set; }
        public string Codpais { get; set; }
        public string Idioma { get; set; }
        public string Codigomunicipio { get; set; }
        public string Codigotipoincidencia { get; set; }
        public string Correoelectronico { get; set; }
        public string Detalleincidencia { get; set; }
        public string Telefonocontacto { get; set; }
        public string Usuario { get; set; }
    }
}