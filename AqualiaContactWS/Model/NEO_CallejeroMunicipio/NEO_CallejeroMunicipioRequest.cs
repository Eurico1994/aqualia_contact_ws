﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_CallejeroMunicipio
{
    public class NEO_CallejeroMunicipioRequest
    {
        public string canalentrada { get; set; }
        public string codpais { get; set; }
        public string idioma { get; set; }
        public string codigomunicipio { get; set; }
        public string usuario { get; set; }
    }
}