﻿using Helper.Model;
using System.Collections.Generic;

namespace AqualiaContactWS.Model.NEO_CallejeroMunicipio
{
    public class NEO_CallejeroMunicipioResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public List<via> vias { get; set; }

        public static explicit operator NEO_CallejeroMunicipioResponse(ResponseSimple v)
        {
            return new NEO_CallejeroMunicipioResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }

    public partial class via
    {
        public string codigotipovia { get; set; }
        public string codigovia { get; set; }
        public string nombretipovia { get; set; }
        public string nombrevia { get; set; }
    }
}