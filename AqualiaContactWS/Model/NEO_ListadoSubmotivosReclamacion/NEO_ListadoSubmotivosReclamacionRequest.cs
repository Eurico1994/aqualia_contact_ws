﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoSubmotivosReclamacion
{
    public class NEO_ListadoSubmotivosReclamacionRequest
    {
        public string Canalentrada { get; set; }
        public string Codpais { get; set; }
        public string Idioma { get; set; }
        public string Codigomotivoreclamacion { get; set; }
    }

}