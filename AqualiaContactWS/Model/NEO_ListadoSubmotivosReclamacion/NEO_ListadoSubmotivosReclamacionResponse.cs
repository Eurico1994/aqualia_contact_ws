﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_ListadoSubmotivosReclamacion
{
    public class NEO_ListadoSubmotivosReclamacionResponse
    {
        public string Codigorespuesta { get; set; }
        public string Textorespuesta { get; set; }
        public List<Submotivosreclamacion> Submotivosreclamacion { get; set; }

        public static explicit operator NEO_ListadoSubmotivosReclamacionResponse(ResponseSimple v)
        {
            return new NEO_ListadoSubmotivosReclamacionResponse { Codigorespuesta = v.codigorespuesta, Textorespuesta = v.textorespuesta };
        }
    }

    public partial class Submotivosreclamacion
    {
        public string Codigosubmotivoreclamacion { get; set; }
        public string Textosubmotivoreclamacion { get; set; }
    }
}