﻿using Helper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_DescargarFacturaElectronica
{
    public class NEO_DescargarFacturaElectronicaResponse
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
        public string facturapdf { get; set; }

        public static explicit operator NEO_DescargarFacturaElectronicaResponse(ResponseSimple v)
        {
            return new NEO_DescargarFacturaElectronicaResponse { codigorespuesta = v.codigorespuesta, textorespuesta = v.textorespuesta };
        }
    }
}