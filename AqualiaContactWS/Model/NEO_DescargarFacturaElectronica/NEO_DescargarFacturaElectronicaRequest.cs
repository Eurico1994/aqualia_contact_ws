﻿using AqualiaContactWS.Model.Comun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Model.NEO_DescargarFacturaElectronica
{
    public class NEO_DescargarFacturaElectronicaRequest
    {
    public string Canalentrada { get; set; }
        public string Codpais { get; set; }
        public string Idioma { get; set; }
        public string Codigofactura { get; set; }
        public string Emailcorrespondencia { get; set; }
        public Identificadorcontrato Identificadorcontrato { get; set; }
        public string Numerodocumentopago { get; set; }
        public string Usuario { get; set; }
    }
}