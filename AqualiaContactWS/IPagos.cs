﻿using AqualiaContactWS.Model.NEO_DescargarFacturaElectronica;
using AqualiaContactWS.Model.NEO_DetalleDocumentoPago;
using AqualiaContactWS.Model.NEO_ListadoDocumentosPago;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPagos" in both code and config file together.
    [ServiceContract]
    public interface IPagos
    {
        [OperationContract]
        NEO_ListadoDocumentosPagoResponse NEO_ListadoDocumentosPago(NEO_ListadoDocumentosPagoRequest request);

        [OperationContract]
        NEO_DetalleDocumentoPagoResponse NEO_DetalleDocumentoPago(NEO_DetalleDocumentoPagoRequest request);

        [OperationContract]
        NEO_DescargarFacturaElectronicaResponse NEO_DescargarFacturaElectronica(NEO_DescargarFacturaElectronicaRequest request);
    }
}
