﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AqualiaContactWS.Helper;
using AqualiaContactWS.Model.PortalOV;
using Microsoft.Xrm.Sdk.Query;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PortalOV" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PortalOV.svc or PortalOV.svc.cs at the Solution Explorer and start debugging.
    public class PortalOV : IPortalOV
    {
        public List<basicList> getDirecciones(basicList request)
        {
            Utils.InicializarVariables();

            var ListadoDirecciones = new List<basicList>();

            if (request.tipo != null)
            {
                string[] att = null;
                var q = new QueryExpression();
                //LinkEntity le = new LinkEntity();

                switch (request.tipo)
                {
                    case "provincia":
                        att = new string[] { "hidrotec_name", "hidrotec_idprovincia" };
                        q = new QueryExpression("hidrotec_provincia");
                        q.Criteria.AddCondition("hidrotec_paisid", ConditionOperator.Equal, new Guid(request.id));
                        //le = new LinkEntity("hidrotec_provincia", "hidrotec_pais", "hidrotec_paisid", "hidrotec_paisid", JoinOperator.Inner);
                        //le.LinkCriteria.AddCondition("hidrotec_idpais", ConditionOperator.Equal, data.id);
                        break;
                    case "municipio":
                        att = new string[] { "hidrotec_name", "hidrotec_idmunicipio" };
                        q = new QueryExpression("hidrotec_municipio");
                        q.Criteria.AddCondition("hidrotec_provinciaid", ConditionOperator.Equal, new Guid(request.id));
                        //le = new LinkEntity("hidrotec_municipio", "hidrotec_provincia", "hidrotec_provinciaid", "hidrotec_provinciaid", JoinOperator.Inner);
                        //le.LinkCriteria.AddCondition("hidrotec_idprovincia", ConditionOperator.Equal, data.id);
                        break;
                    case "via":
                        att = new string[] { "hidrotec_name", "hidrotec_idvia" };
                        q = new QueryExpression("hidrotec_via");
                        q.Criteria.AddCondition("hidrotec_municipioid", ConditionOperator.Equal, new Guid(request.id));
                        //le = new LinkEntity("hidrotec_via", "hidrotec_municipio", "hidrotec_municipioid", "hidrotec_municipioid", JoinOperator.Inner);
                        //le.LinkCriteria.AddCondition("hidrotec_idmunicipio", ConditionOperator.Equal, data.id);
                        break;
                    case "pedania":
                        att = new string[] { "hidrotec_name", "hidrotec_idpedania" };
                        q = new QueryExpression("hidrotec_pedania");
                        q.Criteria.AddCondition("hidrotec_municipioid", ConditionOperator.Equal, new Guid(request.id));
                        //le = new LinkEntity("hidrotec_pedania", "hidrotec_municipio", "hidrotec_municipioid", "hidrotec_municipioid", JoinOperator.Inner);
                        //le.LinkCriteria.AddCondition("hidrotec_idmunicipio", ConditionOperator.Equal, data.id);
                        break;
                    case "barrio":
                        att = new string[] { "hidrotec_name", "hidrotec_idbarrio" };
                        q = new QueryExpression("hidrotec_barrio");
                        q.Criteria.AddCondition("hidrotec_pedaniaid", ConditionOperator.Equal, new Guid(request.id));
                        //le = new LinkEntity("hidrotec_barrio", "hidrotec_pedania", "hidrotec_pedaniaid", "hidrotec_pedaniaid", JoinOperator.Inner);
                        //le.LinkCriteria.AddCondition("hidrotec_idpedania", ConditionOperator.Equal, data.id);
                        break;
                    default:
                        break;
                }

                q.AddOrder("hidrotec_name", OrderType.Ascending);
                q.ColumnSet = new ColumnSet(att);
                //q.LinkEntities.Add(le);

                var res = Utils.IOS.RetrieveMultiple(q);
                foreach (var an in res.Entities)
                {

                    var direccion = new basicList();
                    direccion.id = an.Id.ToString();
                    direccion.nombre = an.Contains("hidrotec_name") ? (string)an["hidrotec_name"] : null;

                    ListadoDirecciones.Add(direccion);
                }
                return ListadoDirecciones;
            }
            return null;
        }
    }
}
