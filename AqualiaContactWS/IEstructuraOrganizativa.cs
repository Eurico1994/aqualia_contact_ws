﻿using AqualiaContactWS.Model.NEO_EstructuraOrganizativa;
using AqualiaContactWS.Model.NEO_ListadoPaises;
using AqualiaContactWS.Model.NEO_ListadoPaisesBancos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEstructuraOrganizativa" in both code and config file together.
    [ServiceContract]
    public interface IEstructuraOrganizativa
    {
        [OperationContract]//ok
        NEO_EstructuraOrganizativaResponse NEO_EstructuraOrganizativa(NEO_EstructuraOrganizativaRequest request);
        [OperationContract]//ok
        NEO_ListadoPaisesResponse NEO_ListadoPaises(NEO_ListadoPaisesRequest request);
        [OperationContract]//ok
        NEO_ListadoPaisesBancosResponse NEO_ListadoPaisesBancos(NEO_ListadoPaisesBancosRequest request);
    }
}
