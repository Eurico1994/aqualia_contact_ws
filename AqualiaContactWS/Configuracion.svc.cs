﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AqualiaContactWS.Helper;
using AqualiaContactWS.Model.NEO_EnviarIncidencias;
using AqualiaContactWS.Model.NEO_ListadoTipoIncidencias;
using Entidades;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Configuracion" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Configuracion.svc or Configuracion.svc.cs at the Solution Explorer and start debugging.
    public class Configuracion : IConfiguracion
    {
        /// <summary>
        /// STATUS: KO
        /// TODOO: No procede, no se entiende, no aplica... no sabemos que es.
        /// DESC:
        /// Este servicio inserta una incidencia de un usuario en base de datos. Para ello se hacen las validaciones 
        /// pertinentes y se inserta la incidencia a través del procedimiento papp_insincidencia.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_EnviarIncidenciasResponse NEO_EnviarIncidencias(NEO_EnviarIncidenciasRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_EnviarIncidenciasResponse resp = new NEO_EnviarIncidenciasResponse();
            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.SinConexion();
                }

                if (request == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.SinParametros();
                }

                if (request.Canalentrada == "" || request.Canalentrada == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.CanalEntrada();
                }

                if (request.Idioma == "" || request.Idioma == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.Idioma();
                }

                if (request.Codpais == "" || request.Codpais == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.Pais();
                }

                if (request.Usuario == "" || request.Usuario == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.Usuario();
                }

                if (request.Codigotipoincidencia == "" || request.Codigotipoincidencia == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.Codigotipoincidencia();
                }

                if (request.Telefonocontacto == "" || request.Telefonocontacto == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.Telefonocontacto();
                }

                if (request.Codigomunicipio == "" || request.Codigomunicipio == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.Codigomunicipio();
                }

                if (request.Correoelectronico == "" || request.Correoelectronico == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.Correo();
                }

                if (request.Detalleincidencia  == "" || request.Detalleincidencia  == null)
                {
                    return (NEO_EnviarIncidenciasResponse)respuestaWs.Detalleincidencia();
                }

                QueryExpression q = new QueryExpression(Contact.EntityName);
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                return (NEO_EnviarIncidenciasResponse)respuestaWs.ok();
            }
            catch (Exception e)
            {
                return (NEO_EnviarIncidenciasResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: KO
        /// TODOO: No procede, no se entiende, no aplica... no sabemos que es.
        /// DESC:
        /// Este servicio devuelve el listado de tipos de incidencias existentes en la base de datos. Para ello se hacen las validaciones 
        /// pertinentes y se devuelve los datos a través de la función fapp_seltiposincidencias.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoTipoIncidenciasResponse NEO_ListadoTipoIncidencias(NEO_ListadoTipoIncidenciasRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoTipoIncidenciasResponse resp = new NEO_ListadoTipoIncidenciasResponse();
            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.SinConexion();
                }

                if (request == null)
                {
                    return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.SinParametros();
                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.CanalEntrada();
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.Idioma();
                }

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.Pais();
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.Usuario();
                }

                QueryExpression q = new QueryExpression(HidrotecSubtipo.EntityName);
                q.NoLock = true;
                q.ColumnSet.AddColumns(HidrotecSubtipo.PrimaryName,HidrotecSubtipo.HidrotecIdsubtipo);
                LinkEntity leTipo = new LinkEntity(HidrotecSubtipo.EntityName, HidrotecTipo.EntityName,
                HidrotecSubtipo.HidrotecTipoId, HidrotecTipo.PrimaryKey, JoinOperator.Inner);
                leTipo.Columns.AddColumn(HidrotecTipo.PrimaryKey);
                leTipo.LinkCriteria.AddCondition(HidrotecTipo.HidrotecIdtipo, ConditionOperator.Equal, "008001");
              //  leTipo.LinkEntities.Add(leRelacion);
                q.LinkEntities.Add(leTipo);
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {

                    resp.Codigorespuesta = "OK";
                    resp.Textorespuesta = "OK";
                    resp.Tiposincidencia = new List<Tiposincidencia>();
                    foreach (Entity EntidadTipoIncidencia in res.Entities)
                    {
                        Tiposincidencia TipoIncidencia = new Tiposincidencia();
                        TipoIncidencia.Codigotipoincidencia = EntidadTipoIncidencia.GetAttributeValue<string>(HidrotecSubtipo.HidrotecIdsubtipo);
                        TipoIncidencia.Textotipoincidencia= EntidadTipoIncidencia.GetAttributeValue<string>(HidrotecSubtipo.PrimaryName);
                        resp.Tiposincidencia.Add(TipoIncidencia);
                    }
                    return resp;

                }
                else
                {
                    return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.NoHayTipoIncidencia();

                }


               // return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.ok();
            }
            catch (Exception e)
            {
                return (NEO_ListadoTipoIncidenciasResponse)respuestaWs.Exception(e);
            }
        }
    }
}
