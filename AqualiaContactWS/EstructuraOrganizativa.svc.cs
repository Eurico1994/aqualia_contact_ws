﻿using AqualiaContactWS.Helper;
using AqualiaContactWS.Model.NEO_EstructuraOrganizativa;
using AqualiaContactWS.Model.NEO_ListadoPaises;
using AqualiaContactWS.Model.NEO_ListadoPaisesBancos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EstructuraOrganizativa" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EstructuraOrganizativa.svc or EstructuraOrganizativa.svc.cs at the Solution Explorer and start debugging.
    public class EstructuraOrganizativa : IEstructuraOrganizativa
    {
        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve la estructura organizativa de FCC. Para ello se hacen las validaciones pertinentes y se recuperan los datos a través de cuatro procedimientos:
        /// - Para recuperar las zonas: fapp_selzonas
        /// - Para recuperar las delegaciones: fapp_seldelegaciones
        /// - Para recuperar las contratas: fapp_selcontratas
        /// - Para recuperar las explotaciones: fapp_selexplotaciones
        /// - Para recuperar los municipios: fapp_selmunicipios
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_EstructuraOrganizativaResponse NEO_EstructuraOrganizativa(NEO_EstructuraOrganizativaRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_EstructuraOrganizativaResponse resp = new NEO_EstructuraOrganizativaResponse();
            /*
            request.Canalentrada = "App";
            request.Idioma = "es-es";
            request.Codpais = "34";
            */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_EstructuraOrganizativaResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }
                
                if (request == null)
                {
                    return (NEO_EstructuraOrganizativaResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_EstructuraOrganizativaResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_EstructuraOrganizativaResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_EstructuraOrganizativaResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada"); ;
                }
                
      
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_EstructuraOrganizativaResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais"); ;
                }

                string strEstructuraZona = "2";
                string strEstructuraDelegacion = "3";
                string strEstructuraUnGest = "4";
                string strEstructuraContrata = "5";
                string strEstructuraExplotacion = "6";

                string strAliasMunicipio = "municipio";
                string strQueryMunicipio = "municipio.";

                QueryExpression q = new QueryExpression(BusinessUnit.EntityName);
                q.Criteria.AddCondition(BusinessUnit.HidrotecEstructuraorganizativa, ConditionOperator.Equal, strEstructuraZona);
                q.ColumnSet.AddColumns(BusinessUnit.PrimaryKey, BusinessUnit.PrimaryName, BusinessUnit.HidrotecIdunidadedenegocio,
                                       BusinessUnit.ParentBusinessUnitId, BusinessUnit.HidrotecEstructuraorganizativa);
                q.NoLock = true;
                EntityCollection res = new EntityCollection();
                try
                {
                    res = Utils.IOS.RetrieveMultiple(q);
                    resp.zonas = new List<Zona>();
                    foreach (var item in res.Entities)
                    {
                        Zona zona = new Zona();
                        Guid guidIDZona = (Guid)item[BusinessUnit.PrimaryKey];
                        zona.codigozona = item[BusinessUnit.HidrotecIdunidadedenegocio].ToString();
                        zona.nombrezona = (string)item[BusinessUnit.PrimaryName];

                       q = new QueryExpression(BusinessUnit.EntityName);
                        q.Criteria.AddCondition(BusinessUnit.ParentBusinessUnitId, ConditionOperator.Equal, guidIDZona);
                        q.Criteria.AddCondition(BusinessUnit.HidrotecEstructuraorganizativa, ConditionOperator.Equal, strEstructuraDelegacion);
                        q.ColumnSet.AddColumns(BusinessUnit.PrimaryKey, BusinessUnit.PrimaryName, BusinessUnit.HidrotecIdunidadedenegocio,
                                               BusinessUnit.ParentBusinessUnitId, BusinessUnit.HidrotecEstructuraorganizativa);
                        q.NoLock = true;
                        EntityCollection res2 = new EntityCollection();
                        try
                        {
                            res2 = Utils.IOS.RetrieveMultiple(q);
                            zona.delegaciones = new List<Delegacione>();
                            foreach (Entity item2 in res2.Entities)
                            {
                                Delegacione delegacion = new Delegacione();
                                Guid guidIDDelegacion = (Guid)item2[BusinessUnit.PrimaryKey];
                                delegacion.codigodelegacion = item2[BusinessUnit.HidrotecIdunidadedenegocio].ToString();
                                delegacion.nombredelegacion = (string)item2[BusinessUnit.PrimaryName];
                                
                                q = new QueryExpression(BusinessUnit.EntityName);
                                q.Criteria.AddCondition(BusinessUnit.ParentBusinessUnitId, ConditionOperator.Equal, guidIDDelegacion);
                                q.Criteria.AddCondition(BusinessUnit.HidrotecEstructuraorganizativa, ConditionOperator.Equal, strEstructuraUnGest);
                                q.ColumnSet.AddColumns(BusinessUnit.PrimaryKey, BusinessUnit.PrimaryName, BusinessUnit.HidrotecIdunidadedenegocio,
                                                       BusinessUnit.ParentBusinessUnitId, BusinessUnit.HidrotecEstructuraorganizativa);
                                q.NoLock = true;
                                EntityCollection res3 = new EntityCollection();
                                try
                                {
                                    res3 = Utils.IOS.RetrieveMultiple(q);

                                    delegacion.contratas = new List<Contrata>();
                                    foreach (Entity item3 in res3.Entities)
                                    {
                                        Guid guidIDUnGest = (Guid)item3[BusinessUnit.PrimaryKey];
                                        q = new QueryExpression(BusinessUnit.EntityName);
                                        q.Criteria.AddCondition(BusinessUnit.ParentBusinessUnitId, ConditionOperator.Equal, guidIDUnGest);
                                        q.Criteria.AddCondition(BusinessUnit.HidrotecEstructuraorganizativa, ConditionOperator.Equal, strEstructuraContrata);
                                        q.ColumnSet.AddColumns(BusinessUnit.PrimaryKey, BusinessUnit.PrimaryName, BusinessUnit.HidrotecIdunidadedenegocio,
                                                               BusinessUnit.ParentBusinessUnitId, BusinessUnit.HidrotecEstructuraorganizativa);
                                        q.NoLock = true;
                                        EntityCollection res4 = new EntityCollection();
                                        try
                                        {
                                            res4 = Utils.IOS.RetrieveMultiple(q);

                                            foreach (Entity item4 in res4.Entities)
                                            {
                                                Contrata contrata = new Contrata();
                                                Guid guidIDContrata = (Guid)item4[BusinessUnit.PrimaryKey];
                                                contrata.codigocontrata = item4[BusinessUnit.HidrotecIdunidadedenegocio].ToString();
                                                contrata.nombrecontrata = (string)item4[BusinessUnit.PrimaryName];
                                                
                                                q = new QueryExpression(BusinessUnit.EntityName);
                                                q.Criteria.AddCondition(BusinessUnit.ParentBusinessUnitId, ConditionOperator.Equal, guidIDContrata);
                                                q.Criteria.AddCondition(BusinessUnit.HidrotecEstructuraorganizativa, ConditionOperator.Equal, strEstructuraExplotacion);
                                                q.ColumnSet = new ColumnSet(true);
                                                q.NoLock = true;
                                                EntityCollection res5 = new EntityCollection();
                                                try
                                                {
                                                    res5 = Utils.IOS.RetrieveMultiple(q);

                                                    contrata.explotaciones = new List<Explotacione>();
                                                    foreach (Entity item5 in res5.Entities)
                                                    {
                                                        Explotacione explotacion = new Explotacione();
                                                        Guid guidIDExplotacion = (Guid)item5[BusinessUnit.PrimaryKey];
                                                        explotacion.nombreexplotacion = (string)item5[BusinessUnit.PrimaryName];
                                                         if (item5.HasAttributeValue<EntityReference>(BusinessUnit.HidrotecInstalacion))
                                                         {
                                                             q = new QueryExpression(HidrotecInstalacion.EntityName);
                                                             q.Criteria.AddCondition(HidrotecInstalacion.PrimaryKey, ConditionOperator.Equal, ((EntityReference)item5[BusinessUnit.HidrotecInstalacion]).Id);
                                                             q.ColumnSet.AddColumns(HidrotecInstalacion.HidrotecExplotaciones, HidrotecInstalacion.HidrotecIdinstalacion);
                                                             q.NoLock = true;
                                                             EntityCollection res6 = new EntityCollection();
                                                             try
                                                             {
                                                                 res6 = Utils.IOS.RetrieveMultiple(q);

                                                                 explotacion.identificador = new Identificador();
                                                                 explotacion.identificador.Codigocontrata = item4.Contains(BusinessUnit.HidrotecIdunidadedenegocio) ?
                                                                     long.Parse(item4[BusinessUnit.HidrotecIdunidadedenegocio].ToString()) : -1;
                                                                 explotacion.identificador.Codigoinstalacion = res6.Entities[0].Contains(HidrotecInstalacion.HidrotecIdinstalacion) ?
                                                                     long.Parse(res6.Entities[0][HidrotecInstalacion.HidrotecIdinstalacion].ToString()) : -1;
                                                                 explotacion.identificador.Numeroexplotacion = res6.Entities[0].Contains(HidrotecInstalacion.HidrotecExplotaciones) ?
                                                                     res6.Entities[0][HidrotecInstalacion.HidrotecExplotaciones].ToString() : null;
                                                                 explotacion.identificador.Codigoservicio = 1;
                                                                
                                                             }
                                                             catch (Exception e5)
                                                             {
                                                                 //throw;
                                                                 return (NEO_EstructuraOrganizativaResponse)respuestaWs.Exception(e5,"99999");
                                                             }
                                                            // contrata.explotaciones.Add(explotacion);
                                                         }
                                                        //
                                                        q = new QueryExpression(HidrotecConfiguracionorganizativa.EntityName);
                                                        q.Criteria.AddCondition(HidrotecConfiguracionorganizativa.HidrotecExplotacionId, ConditionOperator.Equal, guidIDExplotacion);
                                                        //q.ColumnSet = new ColumnSet(true);

                                                        LinkEntity leMunicipio = new LinkEntity(HidrotecConfiguracionorganizativa.EntityName, HidrotecMunicipio.EntityName,
                                                            HidrotecConfiguracionorganizativa.HidrotecMunicipioId, HidrotecMunicipio.PrimaryKey, JoinOperator.Inner);
                                                        leMunicipio.EntityAlias = strAliasMunicipio;
                                                        leMunicipio.Columns.AddColumns(HidrotecMunicipio.PrimaryName, HidrotecMunicipio.HidrotecIdmunicipio);

                                                        q.LinkEntities.Add(leMunicipio);
                                                        q.NoLock = true;
                                                        EntityCollection res7 = new EntityCollection();
                                                        try
                                                        {
                                                            res7 = Utils.IOS.RetrieveMultiple(q);

                                                            explotacion.municipios = new List<Model.NEO_EstructuraOrganizativa.Municipio>();
                                                            foreach (Entity item7 in res7.Entities)
                                                            {
                                                                Model.NEO_EstructuraOrganizativa.Municipio municipio = new Model.NEO_EstructuraOrganizativa.Municipio();
                                                                municipio.codigomunicipio = item7.Contains(strQueryMunicipio + HidrotecMunicipio.HidrotecIdmunicipio) ?
                                                                    (string)((AliasedValue)item7[strQueryMunicipio + HidrotecMunicipio.HidrotecIdmunicipio]).Value : null;
                                                                municipio.nombremunicipio = item7.Contains(strQueryMunicipio + HidrotecMunicipio.PrimaryName) ?
                                                                    ((string)((AliasedValue)item7[strQueryMunicipio + HidrotecMunicipio.PrimaryName]).Value).Trim() : null;

                                                                explotacion.municipios.Add(municipio);
                                                            }
                                                        }
                                                        catch (Exception e6)
                                                        {
                                                            //throw;
                                                            return (NEO_EstructuraOrganizativaResponse)respuestaWs.Exception(e6,"99999");


                                                        }

                                                        //
                                                        contrata.explotaciones.Add(explotacion);
                                                    }
                                                    
                                                }
                                                catch (Exception e4)
                                                {
                                                    //throw;
                                                    return (NEO_EstructuraOrganizativaResponse)respuestaWs.Exception(e4,"99999");
                                                }
                                                delegacion.contratas.Add(contrata);
                                            }
                                        }
                                        catch (Exception e3)
                                        {
                                            // throw;
                                            return (NEO_EstructuraOrganizativaResponse)respuestaWs.Exception(e3,"99999");
                                        }
                                    }
                                }
                                catch (Exception e2)
                                { //throw;
                                    return (NEO_EstructuraOrganizativaResponse)respuestaWs.Exception(e2,"99999");

                                }
                                zona.delegaciones.Add(delegacion);
                            }
                        }
                        catch (Exception e1)
                        { //throw;
                            return (NEO_EstructuraOrganizativaResponse)respuestaWs.Exception(e1,"99999");
                        }
                        resp.zonas.Add(zona);
                    }
                }
                catch (Exception e)
                { //throw;
                    return (NEO_EstructuraOrganizativaResponse)respuestaWs.Exception(e,"99999");
                }

                resp.codigorespuesta = "00001";
                resp.textorespuesta = "OK";
                return resp;
               // return (NEO_EstructuraOrganizativaResponse)respuestaWs.ok();
            }
            catch (Exception e)
            {
                return (NEO_EstructuraOrganizativaResponse)respuestaWs.Exception(e,"99999");
            }
        }
        
        /// <summary>
        /// STATUS: OK Abel Gago
        /// DUDAS: Para que se pasa codigoMunicipio?
        /// DESC:
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoPaisesResponse NEO_ListadoPaises(NEO_ListadoPaisesRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoPaisesResponse resp = new NEO_ListadoPaisesResponse();
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoPaisesResponse)respuestaWs.GetError("99999", "", "SinConexion"); ;
                }
                
                if (request == null)
                {
                    return (NEO_ListadoPaisesResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoPaisesResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoPaisesResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoPaisesResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada"); ;
                }
                
                
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoPaisesResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoPaisesResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }
                
                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    //return (NEO_ListadoPaisesResponse)respuestaWs.Codigomunicipio();
                    return (NEO_ListadoPaisesResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigomunicipio");
                }

                QueryExpression q = new QueryExpression(HidrotecPais.EntityName);
                q.ColumnSet.AddColumns(HidrotecPais.PrimaryName, HidrotecPais.HidrotecIdpais);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                resp.paises = new List<Model.NEO_ListadoPaises.pais>();

                foreach (Entity item in res.Entities) {
                    Model.NEO_ListadoPaises.pais pais = new Model.NEO_ListadoPaises.pais();
                    pais.codigopais = (string)item[HidrotecPais.HidrotecIdpais];
                    pais.nombrepais = (string)item[HidrotecPais.PrimaryName];
                    resp.paises.Add(pais);
                }

                resp.codigorespuesta = "00001";
                resp.textorespuesta = "OK";

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ListadoPaisesResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK a medias Abel Gago 
        /// DUDAS: Mascarapais y pais, no se que son, ponemos cosas del banco para ver que va bien lo que hay que sacar hasta saber que se quiere.
        /// y también al código de municipio?
        /// DESC:
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoPaisesBancosResponse NEO_ListadoPaisesBancos(NEO_ListadoPaisesBancosRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoPaisesBancosResponse resp = new NEO_ListadoPaisesBancosResponse();
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoPaisesBancosResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }
                
                if (request == null)
                {
                    return (NEO_ListadoPaisesBancosResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoPaisesBancosResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoPaisesBancosResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoPaisesBancosResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }
                
               
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoPaisesBancosResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoPaisesBancosResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }
                
                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    
                    return (NEO_ListadoPaisesBancosResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigomunicipio");
                }

                string strAliasBanco = "banco";
                string strQueryBanco = "banco.";

                QueryExpression q = new QueryExpression(HidrotecPais.EntityName);
                q.ColumnSet.AddColumns(HidrotecPais.PrimaryName, HidrotecPais.HidrotecIdpais);

                LinkEntity leBanco = new LinkEntity(HidrotecPais.EntityName, HidrotecBanco.EntityName,
                    HidrotecPais.PrimaryKey, HidrotecBanco.HidrotecPaisId, JoinOperator.Inner);
                leBanco.Columns = new ColumnSet(true);
                leBanco.EntityAlias = strAliasBanco;
                q.LinkEntities.Add(leBanco);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                resp.paises = new List<Model.NEO_ListadoPaisesBancos.Pais>();

                foreach (Entity item in res.Entities)
                {
                    Model.NEO_ListadoPaisesBancos.Pais pais = new Model.NEO_ListadoPaisesBancos.Pais();
                    pais.codigopais = long.Parse((string)item[HidrotecPais.HidrotecIdpais]);
                    pais.nombrepais = (string)item[HidrotecPais.PrimaryName];
                    /* ? -> Nombre Banco. */
                    pais.mascarapais = (string)((AliasedValue)item[strQueryBanco + HidrotecBanco.PrimaryName]).Value;
                    /* ? -> Id Banco */
                    if (item.Contains(strQueryBanco + HidrotecBanco.HidrotecIdbanco))
                    {
                        pais.pais = (string)((AliasedValue)item[strQueryBanco + HidrotecBanco.HidrotecIdbanco]).Value;
                    }
                    resp.paises.Add(pais);
                }

                resp.codigorespuesta = "00001";
                resp.textorespuesta = "OK";

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ListadoPaisesBancosResponse)respuestaWs.Exception(e,"99999");
            }
        }
    }
}
