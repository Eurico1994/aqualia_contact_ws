﻿using AqualiaContactWS.Helper;
using AqualiaContactWS.Model.NEO_EnviarReclamacion;
using AqualiaContactWS.Model.NEO_ListadoMotivosReclamacion;
using AqualiaContactWS.Model.NEO_ListadoReclamaciones;
using AqualiaContactWS.Model.NEO_ListadoSolicitudes;
using AqualiaContactWS.Model.NEO_ListadoSubmotivosReclamacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Reclamacion" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Reclamacion.svc or Reclamacion.svc.cs at the Solution Explorer and start debugging.
    public class Reclamacion : IReclamacion
    {
        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve el listado de motivos de reclamación existente en base de datos. Para ello se hacen las validaciones 
        /// pertinentes y se devuelve los datos a través de la función fov_selclasesrecl.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoMotivosReclamacionResponse NEO_ListadoMotivosReclamacion(NEO_ListadoMotivosReclamacionRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoMotivosReclamacionResponse resp = new NEO_ListadoMotivosReclamacionResponse();
            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_ListadoMotivosReclamacionResponse)respuestaWs.SinConexion();
                }

                if (request == null)
                {
                    return (NEO_ListadoMotivosReclamacionResponse)respuestaWs.SinParametros();
                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoMotivosReclamacionResponse)respuestaWs.CanalEntrada();
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoMotivosReclamacionResponse)respuestaWs.Idioma();
                }

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoMotivosReclamacionResponse)respuestaWs.Pais();
                }

                string strRelReclamacion = "011";

                QueryExpression q = new QueryExpression(HidrotecMotivo.EntityName);
                q.ColumnSet.AddColumns(HidrotecMotivo.HidrotecIdmotivo, HidrotecTipo.PrimaryName);

                LinkEntity leSubtipo = new LinkEntity(HidrotecMotivo.EntityName, HidrotecHidrotecSubtipoHidrotecMotivo.EntityName,
                HidrotecMotivo.PrimaryKey, HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecMotivoId, JoinOperator.Inner);

                LinkEntity leMotivoSubtipo = new LinkEntity(HidrotecHidrotecSubtipoHidrotecMotivo.EntityName, HidrotecSubtipo.EntityName,
                HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecSubtipoId, HidrotecSubtipo.PrimaryKey, JoinOperator.Inner);

                LinkEntity leTipo = new LinkEntity(HidrotecSubtipo.EntityName, HidrotecTipo.EntityName,
                HidrotecSubtipo.HidrotecTipoId, HidrotecTipo.PrimaryKey, JoinOperator.Inner);

                LinkEntity leRel = new LinkEntity(HidrotecTipo.EntityName, HidrotecRelacion.EntityName,
                 HidrotecTipo.HidrotecRelacionId, HidrotecRelacion.PrimaryKey, JoinOperator.Inner);
                leRel.LinkCriteria.AddCondition(new ConditionExpression(HidrotecRelacion.HidrotecIdrelacion, ConditionOperator.Equal, strRelReclamacion));

                leTipo.LinkEntities.Add(leRel);
                leMotivoSubtipo.LinkEntities.Add(leTipo);
                leSubtipo.LinkEntities.Add(leMotivoSubtipo);
                q.LinkEntities.Add(leSubtipo);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {

                    List<Motivosreclamacion> mReclamacion = new List<Motivosreclamacion>();
                    foreach (Entity fila in res.Entities)
                    {
                        Motivosreclamacion motivo = new Motivosreclamacion();
                        motivo.codigomotivoreclamacion = fila.Contains(HidrotecMotivo.HidrotecIdmotivo) ? (string)fila[HidrotecMotivo.HidrotecIdmotivo] : null;
                        motivo.textomotivoreclamacion = fila.Contains(HidrotecMotivo.PrimaryName) ? (string)fila[HidrotecMotivo.PrimaryName] : null;
                        mReclamacion.Add(motivo);
                    }

                    resp.codigorespuesta = "OK";
                    resp.textorespuesta = "OK";
                    resp.motivosreclamacion = mReclamacion;
                }
                else
                {

                    return (NEO_ListadoMotivosReclamacionResponse)respuestaWs.NoMotivosReclamacion();

                }
                

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ListadoMotivosReclamacionResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: KO -> No existen submotivos de reclamaciones.
        /// DESC:
        /// Este servicio devuelve el listado de submotivos de reclamación existente en base de datos. Para ello se hacen las validaciones
        /// pertinentes y se devuelve los datos a través de la función fov_selmotreclamo.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoSubmotivosReclamacionResponse NEO_ListadoSubmotivosReclamacion(NEO_ListadoSubmotivosReclamacionRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoSubmotivosReclamacionResponse resp = new NEO_ListadoSubmotivosReclamacionResponse();
            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_ListadoSubmotivosReclamacionResponse)respuestaWs.SinConexion();
                }

                if (request == null)
                {
                    return (NEO_ListadoSubmotivosReclamacionResponse)respuestaWs.SinParametros();
                }

                if (request.Canalentrada == "" || request.Canalentrada == null)
                {
                    return (NEO_ListadoSubmotivosReclamacionResponse)respuestaWs.CanalEntrada();
                }

                if (request.Idioma == "" || request.Idioma == null)
                {
                    return (NEO_ListadoSubmotivosReclamacionResponse)respuestaWs.Idioma();
                }

                if (request.Codpais == "" || request.Codpais == null)
                {
                    return (NEO_ListadoSubmotivosReclamacionResponse)respuestaWs.Pais();
                }

                if (request.Codigomotivoreclamacion == "" || request.Codigomotivoreclamacion == null)
                {
                    return (NEO_ListadoSubmotivosReclamacionResponse)respuestaWs.Codigomotivoreclamacion();
                }

                return (NEO_ListadoSubmotivosReclamacionResponse)respuestaWs.ok();
            }
            catch (Exception e)
            {
                return (NEO_ListadoSubmotivosReclamacionResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK A medias Abel Gago
        /// DESC:
        /// Este servicio inserta una petición de reclamación en la base de datos de CAC. Para ello, se hacen las validaciones pertinentes de datos y se inserta
        /// en las tablas historicoEstadoSolicitud con el procedimiento pov_inshistsolic y en la tabla informacionSolicitudes con el procedimiento pov_insinforsolic 
        /// y la tabla solicitudes con la función fov_inssolicitud.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_EnviarReclamacionResponse NEO_EnviarReclamacion(NEO_EnviarReclamacionRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_EnviarReclamacionResponse resp = new NEO_EnviarReclamacionResponse();

            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.SinConexion();
                }
                
                if (request == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.SinParametros();
                }
                
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.CanalEntrada();
                }
                
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Idioma();
                }
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Pais();
                }
                
                if (request.correoelectronico == "" || request.correoelectronico == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Correo();
                }
                
                if (request.codigomotivoreclamacion == "" || request.codigomotivoreclamacion == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Codigomotivoreclamacion();
                }
                
                if (request.codigosubmotivoreclamacion == "" || request.codigosubmotivoreclamacion == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Codigosubmotivoreclamacion();
                }
                
                if (request.codigomunicipio == "" || request.codigomunicipio == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Codigomunicipio();
                }
                
                if (request.detallereclamacion == "" || request.detallereclamacion == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Detallereclamacion();
                }
                
                if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Codigocac();
                }
                
                if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Codigocontrato();
                }
                
                if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Codigoinstalacion();
                }
                
                if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_EnviarReclamacionResponse)respuestaWs.Numerocontrato();
                }


                //request.Usuario = "05453623A";
                //request.Identificadorcontrato = new Model.Comun.Identificadorcontrato();
                //request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
                //request.Canalentrada = "";
                //request.Correoelectronico = "abel._____ @gmail.com";
                //request.Codigosubmotivoreclamacion = "";
                //request.Codigomotivoreclamacion = "";


                string strAliasTipo = "tipo";
                string strQueryTipo = "tipo.";
                string strAliasSubtipo = "subtipo";
                string strQuerySubtipo = "subtipo.";
                string strAliasMotivo = "motivo";
                string strQueryMotivo = "motivo.";
                string strAliasDirSum = "dirSumContrato";
                string strQueryDirSum = "dirSumContrato.";
                string strAliasConfOrg = "confOrgContrato";
                string strQueryConfOrg = "confOrgContrato.";
                string strAliasTeam = "team";
                string strQueryTeam = "team.";

                string strCodRelReclamacion = "011";

                QueryExpression q = new QueryExpression(HidrotecRelacion.EntityName);
                q.Criteria.AddCondition(HidrotecRelacion.HidrotecIdrelacion, ConditionOperator.Equal, strCodRelReclamacion);
                q.ColumnSet.AddColumns(HidrotecRelacion.PrimaryKey, HidrotecRelacion.PrimaryName);

                LinkEntity leTipo = new LinkEntity(HidrotecRelacion.EntityName, HidrotecTipo.EntityName,
                    HidrotecRelacion.PrimaryKey, HidrotecTipo.HidrotecRelacionId, JoinOperator.Inner);
                leTipo.EntityAlias = strAliasTipo;
                leTipo.Columns.AddColumns(HidrotecTipo.PrimaryKey, HidrotecTipo.PrimaryName);

                LinkEntity leSubtipo = new LinkEntity(HidrotecTipo.EntityName, HidrotecSubtipo.EntityName,
                    HidrotecTipo.PrimaryKey, HidrotecSubtipo.HidrotecTipoId, JoinOperator.Inner);
                leSubtipo.EntityAlias = strAliasSubtipo;
                leSubtipo.Columns.AddColumns(HidrotecSubtipo.PrimaryKey, HidrotecSubtipo.PrimaryName);
                leSubtipo.LinkCriteria.AddCondition(HidrotecSubtipo.HidrotecIdsubtipo, ConditionOperator.Equal, request.codigosubmotivoreclamacion);

                LinkEntity leMotivoSubtipo = new LinkEntity(HidrotecSubtipo.EntityName, HidrotecHidrotecSubtipoHidrotecMotivo.EntityName,
                    HidrotecSubtipo.PrimaryKey, HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecSubtipoId, JoinOperator.Inner);

                LinkEntity leMotivo = new LinkEntity(HidrotecHidrotecSubtipoHidrotecMotivo.EntityName, HidrotecMotivo.EntityName,
                    HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecMotivoId, HidrotecMotivo.PrimaryKey, JoinOperator.Inner);
                leMotivo.EntityAlias = strAliasMotivo;
                leMotivo.Columns.AddColumns(HidrotecMotivo.PrimaryKey, HidrotecMotivo.PrimaryName);
                leMotivo.LinkCriteria.AddCondition(HidrotecMotivo.HidrotecIdmotivo, ConditionOperator.Equal, request.codigomotivoreclamacion);

                leMotivoSubtipo.LinkEntities.Add(leMotivo);
                leSubtipo.LinkEntities.Add(leMotivoSubtipo);
                leTipo.LinkEntities.Add(leSubtipo);
                q.LinkEntities.Add(leTipo);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                if (res.Entities.Any())
                {
                    Entity incident = new Entity(Incident.EntityName);

                    incident[Incident.HidrotecRelacionId] = new EntityReference(HidrotecRelacion.EntityName, (Guid)res.Entities[0][HidrotecRelacion.PrimaryKey]);
                    incident[Incident.HidrotecTipoId] = new EntityReference(HidrotecTipo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryTipo + HidrotecTipo.PrimaryKey]).Value);
                    incident[Incident.HidrotecSubtipoId] = new EntityReference(HidrotecSubtipo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQuerySubtipo + HidrotecSubtipo.PrimaryKey]).Value);
                    incident[Incident.HidrotecMotivoId] = new EntityReference(HidrotecMotivo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryMotivo + HidrotecMotivo.PrimaryKey]).Value);

                    /* Sacamos el ID del contacto para vincularlo al expediente */

                    q = new QueryExpression(Contact.EntityName);
                    q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                    res = Utils.IOS.RetrieveMultiple(q);
                    if (res.Entities.Any() == false)
                    {
                        //Error contacto

                        return (NEO_EnviarReclamacionResponse)respuestaWs.NoContratoContacto();
                    }

                    incident[Incident.CustomerId] = new EntityReference(Contact.EntityName, (Guid)res.Entities[0][Contact.PrimaryKey]);

                    /* Sacamos los datos a través del contrato vinculado al expediente */

                    q = new QueryExpression(HidrotecContrato.EntityName);
                    q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);
                    q.ColumnSet.AddColumns(HidrotecContrato.PrimaryKey, HidrotecContrato.HidrotecEstado);

                    LinkEntity leDirSum = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                        HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                    leDirSum.EntityAlias = strAliasDirSum;
                    leDirSum.Columns.AddColumn(HidrotecDirecciondesuministro.PrimaryKey);

                    LinkEntity leConfOrg = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecConfiguracionorganizativa.EntityName,
                        HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecConfiguracionorganizativa.HidrotecMunicipioId, JoinOperator.Inner);
                    leConfOrg.EntityAlias = strAliasConfOrg;
                    leConfOrg.Columns.AddColumns(HidrotecConfiguracionorganizativa.PrimaryKey,
                                                 HidrotecConfiguracionorganizativa.HidrotecMunicipioId);

                    LinkEntity leBusUnit = new LinkEntity(HidrotecConfiguracionorganizativa.EntityName, BusinessUnit.EntityName,
                        HidrotecConfiguracionorganizativa.HidrotecExplotacionId, BusinessUnit.PrimaryKey, JoinOperator.Inner);

                    LinkEntity leTeam = new LinkEntity(BusinessUnit.EntityName, Team.EntityName,
                        BusinessUnit.PrimaryKey, Team.BusinessUnitId, JoinOperator.Inner);
                    leTeam.EntityAlias = strAliasTeam;
                    leTeam.LinkCriteria.AddCondition(Team.Isdefault, ConditionOperator.Equal, true);
                    leTeam.Columns.AddColumn(Team.PrimaryKey);

                    leBusUnit.LinkEntities.Add(leTeam);
                    leConfOrg.LinkEntities.Add(leBusUnit);
                    leDirSum.LinkEntities.AddRange(leConfOrg);
                    q.LinkEntities.Add(leDirSum);
                    res = Utils.IOS.RetrieveMultiple(q);
                    if (res.Entities.Any()==false)
                    {
                        //Error contrato
                        return (NEO_EnviarReclamacionResponse)respuestaWs.ContratoBuscadoReclamacion();

                    }
                    incident[Incident.HidrotecContratoId] = new EntityReference(HidrotecContrato.EntityName, (Guid)res.Entities[0][HidrotecContrato.PrimaryKey]);
                    if (request.canalentrada == "APP")
                    {
                        incident[Incident.HidrotecCanaldeentrada] = new OptionSetValue(0);
                    }
                    incident[Incident.HidrotecCorreoelectronico] = request.correoelectronico;

                    incident[Incident.HidrotecAsignadoalequipo] = new EntityReference(Team.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryTeam + Team.PrimaryKey]).Value);
                    incident[Incident.HidrotecDireccionsuministroId] = new EntityReference(HidrotecDirecciondesuministro.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryDirSum + HidrotecDirecciondesuministro.PrimaryKey]).Value);
                    incident[Incident.HidrotecConfiguracionorganizativaId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryConfOrg + HidrotecConfiguracionorganizativa.PrimaryKey]).Value);
                    incident[Incident.HidrotecMunicipioprincipalId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, (Guid)((EntityReference)((AliasedValue)res.Entities[0][strQueryConfOrg + HidrotecConfiguracionorganizativa.HidrotecMunicipioId]).Value).Id);
                    if (res.Entities[0].HasAttributeValue<OptionSetValue>(HidrotecContrato.HidrotecEstado))
                    {
                        incident[Incident.HidrotecEstadocontrato] = res.Entities[0].GetAttributeValue<OptionSetValue>(HidrotecContrato.HidrotecEstado);


                    }
                    Guid idExpediente=Utils.IOS.Create(incident);
                    Utils.CrearNotaTexto(idExpediente, "Detalle", request.detallereclamacion);
                    resp.Codigorespuesta = "OK";
                    resp.Textorespuesta = "OK";
                }
                else
                {
                    //Error Motivo submotivo
                    return (NEO_EnviarReclamacionResponse)respuestaWs.MotivoSubmotivoReclamacion();

                }

                

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_EnviarReclamacionResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve el listado de reclamaciones de un contrato y usuario pasados por parametros. Para ello se hacen las validaciones 
        /// pertinentes y se devuelve los datos a través de la función fapp_selreclamaciones.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoReclamacionesResponse NEO_ListadoReclamaciones(NEO_ListadoReclamacionesRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoReclamacionesResponse resp = new NEO_ListadoReclamacionesResponse();

            /*
            request.Usuario = "05453623A";
            request.Identificadorcontrato = new Model.Comun.Identificadorcontrato();
            request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
            */

            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.SinConexion();
                }
                
                if (request == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.SinParametros();
                }
                
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.CanalEntrada();
                }
                
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.Idioma();
                }
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.Pais();
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.Usuario();
                }
                
                if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                {
                   // return (NEO_ListadoReclamacionesResponse)respuestaWs.Codigocac();
                }
                
                if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.Codigocontrato();
                }
                
                if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.Codigoinstalacion();
                }
                
                if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.Numerocontrato();
                }

                string strCodRelReclamacion = "011";

                QueryExpression q = new QueryExpression(Incident.EntityName);
                q.ColumnSet.AddColumns(Incident.PrimaryKey,
                                       Incident.PrimaryName,
                                       Incident.StatusCode,
                                       Incident.HidrotecMotivoId,
                                       Incident.HidrotecInfoactividadestercero,
                                       Incident.CreatedOn,
                                       Incident.HidrotecFechadecierre);

                LinkEntity leCustomer = new LinkEntity(Incident.EntityName, Contact.EntityName,
                    Incident.CustomerId, Contact.PrimaryKey, JoinOperator.Inner);
                leCustomer.LinkCriteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);

                LinkEntity leContrato = new LinkEntity(Incident.EntityName, HidrotecContrato.EntityName,
                    Incident.HidrotecContratoId, HidrotecContrato.PrimaryKey, JoinOperator.Inner);
                leContrato.LinkCriteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);

                LinkEntity leRelacion = new LinkEntity(Incident.EntityName, HidrotecRelacion.EntityName,
                    Incident.HidrotecRelacionId, HidrotecRelacion.PrimaryKey, JoinOperator.Inner);
                leRelacion.LinkCriteria.AddCondition(HidrotecRelacion.HidrotecIdrelacion, ConditionOperator.Equal, strCodRelReclamacion);

                q.LinkEntities.AddRange(leContrato, leCustomer, leRelacion);
                resp.reclamaciones = new List<Reclamacione>();

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    foreach (Entity item in res.Entities)
                    {
                        Reclamacione recl = new Reclamacione();

                        recl.fechareclamacion = item.Contains(Incident.CreatedOn) ? ((DateTime)item[Incident.CreatedOn]).ToString() : null;
                        recl.fecharesolucion = item.Contains(Incident.HidrotecFechadecierre) ? ((DateTime)item[Incident.HidrotecFechadecierre]).ToString() : null;
                        recl.identificadorreclamacion = (string)item[Incident.PrimaryName];

                        q = new QueryExpression(HidrotecEstadoexpediente.EntityName);
                        q.Criteria.AddCondition(HidrotecEstadoexpediente.HidrotecIdestado, ConditionOperator.Equal, ((OptionSetValue)item[Incident.StatusCode]).Value.ToString());
                        q.ColumnSet.AddColumn(HidrotecEstadoexpediente.PrimaryName);

                        EntityCollection res2 = Utils.IOS.RetrieveMultiple(q);
                        recl.estadoreclamacion = (string)res2.Entities[0][HidrotecEstadoexpediente.PrimaryName];

                        if (item.Contains(Incident.HidrotecMotivoId))
                        {
                            q = new QueryExpression(HidrotecMotivo.EntityName);
                            q.Criteria.AddCondition(HidrotecMotivo.HidrotecIdmotivo, ConditionOperator.Equal, ((OptionSetValue)item[Incident.HidrotecMotivoId]).Value.ToString());
                            q.ColumnSet.AddColumn(HidrotecMotivo.PrimaryName);

                            res2 = Utils.IOS.RetrieveMultiple(q);
                            recl.textomotivoreclamacion = (string)res2.Entities[0][HidrotecMotivo.PrimaryName];
                        }
                        else { recl.textomotivoreclamacion = null; }

                        q = new QueryExpression(Annotation.EntityName);
                        q.ColumnSet = new ColumnSet(true);
                        q.Criteria.AddCondition(Annotation.ObjectId, ConditionOperator.Equal, (Guid)(item[Incident.PrimaryKey]));

                        res2 = Utils.IOS.RetrieveMultiple(q);
                        foreach (Entity item2 in res2.Entities)
                        {
                            if (recl.detallereclamacion != "") recl.detallereclamacion += ",";
                            recl.detallereclamacion += (string)item2[Annotation.Notetext];
                        }

                        if (null != request.identificadorcontrato)
                        {
                            recl.identificadorcontrato = new Model.Comun.Identificadorcontrato();
                            recl.identificadorcontrato.codigocac = request.identificadorcontrato.codigocac;
                            recl.identificadorcontrato.codigocontrato = request.identificadorcontrato.codigocontrato;
                            recl.identificadorcontrato.codigoinstalacion = request.identificadorcontrato.codigoinstalacion;
                            recl.identificadorcontrato.numerocontrato = request.identificadorcontrato.numerocontrato;
                        }

                        // NOTA: No existen submotivos, se enviará siempre null
                        recl.textosubmotivoreclamacion = null;

                        resp.reclamaciones.Add(recl);
                    }

                    resp.codigorespuesta = "OK";
                    resp.textorespuesta = "OK";
                }
                else
                {
                    return (NEO_ListadoReclamacionesResponse)respuestaWs.ReclamacionesNoEncontradas();

                }
                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ListadoReclamacionesResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve el listado de solicitudes de un contrato y contador pasados por parametros. Para ello se hacen las 
        /// validaciones pertinentes y se devuelve los datos a través de la función fapp_selsolicitudes.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoSolicitudesResponse NEO_ListadoSolicitudes(NEO_ListadoSolicitudesRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoSolicitudesResponse resp = new NEO_ListadoSolicitudesResponse();

            /*
            request.Usuario = "05453623A";
            request.Identificadorcontrato = new Identificadorcontrato();
            request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
            */

            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.SinConexion();
                }

                if (request == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.SinParametros();
                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.CanalEntrada();
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.Idioma();
                }

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.Pais();
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.Usuario();
                }

                if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                {
                   // return (NEO_ListadoSolicitudesResponse)respuestaWs.Codigocac();
                }

                if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.Codigocontrato();
                }

                if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.Codigoinstalacion();
                }

                if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.Numerocontrato();
                }

                QueryExpression q = new QueryExpression(Incident.EntityName);
                q.ColumnSet.AddColumns(Incident.PrimaryKey,
                                       Incident.PrimaryName,
                                       Incident.StatusCode,
                                       Incident.HidrotecMotivoId,
                                       Incident.HidrotecSubtipoId,
                                       Incident.HidrotecInfoactividadestercero,
                                       Incident.CreatedOn,
                                       Incident.HidrotecFechadecierre);

                LinkEntity leCustomer = new LinkEntity(Incident.EntityName, Contact.EntityName,
                    Incident.CustomerId, Contact.PrimaryKey, JoinOperator.Inner);
                leCustomer.LinkCriteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);

                LinkEntity leContrato = new LinkEntity(Incident.EntityName, HidrotecContrato.EntityName,
                    Incident.HidrotecContratoId, HidrotecContrato.PrimaryKey, JoinOperator.Inner);
                leContrato.LinkCriteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);

                LinkEntity leRelacion = new LinkEntity(Incident.EntityName, HidrotecRelacion.EntityName,
                    Incident.HidrotecRelacionId, HidrotecRelacion.PrimaryKey, JoinOperator.Inner);
                leRelacion.LinkCriteria.AddCondition(new ConditionExpression(HidrotecRelacion.HidrotecIdrelacion,ConditionOperator.In, "013", "016"));
                q.LinkEntities.AddRange(leContrato, leCustomer, leRelacion);
                resp.solicitudes = new List<solicitud>();

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    foreach (Entity item in res.Entities)
                    {
                        solicitud sol = new solicitud();

                        sol.fecharesolucion = item.Contains(Incident.HidrotecFechadecierre) ? ((DateTime)item[Incident.HidrotecFechadecierre]).ToString() : null;
                        sol.fechasolicitud = item.Contains(Incident.CreatedOn) ? ((DateTime)item[Incident.CreatedOn]).ToString() : null;
                        sol.identificadorsolicitud = (string)item[Incident.PrimaryName];

                        q = new QueryExpression(HidrotecEstadoexpediente.EntityName);
                        q.Criteria.AddCondition(HidrotecEstadoexpediente.HidrotecIdestado, ConditionOperator.Equal, ((OptionSetValue)item[Incident.StatusCode]).Value.ToString());
                        q.ColumnSet.AddColumn(HidrotecEstadoexpediente.PrimaryName);

                        EntityCollection res2 = Utils.IOS.RetrieveMultiple(q);
                        sol.estadosolicitud = (string)res2.Entities[0][HidrotecEstadoexpediente.PrimaryName];

                        if (item.Contains(Incident.HidrotecSubtipoId))
                        {
                            q = new QueryExpression(HidrotecSubtipo.EntityName);
                            q.Criteria.AddCondition(HidrotecSubtipo.PrimaryKey, ConditionOperator.Equal, ((EntityReference)item[Incident.HidrotecSubtipoId]).Id.ToString());
                            q.ColumnSet.AddColumn(HidrotecSubtipo.PrimaryName);

                            res2 = Utils.IOS.RetrieveMultiple(q);
                            sol.subtiporelacion = (string)res2.Entities[0][HidrotecSubtipo.PrimaryName];
                        }

                        q = new QueryExpression(Annotation.EntityName);
                        q.ColumnSet = new ColumnSet(true);
                        q.Criteria.AddCondition(Annotation.ObjectId, ConditionOperator.Equal, (Guid)(item[Incident.PrimaryKey]));

                        res2 = Utils.IOS.RetrieveMultiple(q);
                        foreach (Entity item2 in res2.Entities)
                        {
                            if (sol.detallesolicitud != "") sol.detallesolicitud += ",";
                            sol.detallesolicitud += (string)item2[Annotation.Notetext];
                        }
                        sol.fecharesolucion = item.Contains(Incident.HidrotecFechadecierre) ? ((DateTime)item[Incident.HidrotecFechadecierre]).ToString() : null;
                        if (null != request.identificadorcontrato)
                        {
                            sol.identificadorcontrato = new Identificadorcontrato();
                            sol.identificadorcontrato.codigocac = request.identificadorcontrato.codigocac;
                            sol.identificadorcontrato.codigocontrato = request.identificadorcontrato.codigocontrato;
                            sol.identificadorcontrato.codigoinstalacion = request.identificadorcontrato.codigoinstalacion;
                            sol.identificadorcontrato.numerocontrato = request.identificadorcontrato.numerocontrato;
                        }

                        resp.solicitudes.Add(sol);
                    }

                    resp.codigorespuesta = "OK";
                    resp.textorespuesta = "OK";

                    return resp;
                }
                else
                {
                    return (NEO_ListadoSolicitudesResponse)respuestaWs.SolicitudesNoEncontradas();

                }
            }
            catch (Exception e)
            {
                return (NEO_ListadoSolicitudesResponse)respuestaWs.Exception(e);
            }
        }
    }
}
