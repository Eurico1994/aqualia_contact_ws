// *********************************************************************
// Created by: Latebound Constant Generator 1.2019.5.2 for XrmToolBox
// Author    : Jonas Rapp http://twitter.com/rappen
// Repo      : https://github.com/rappen/LateboundConstantGenerator
// Source Org: http://a2aqcrm001d.fcc.intfcc.local/AQUALIACRM
// Filename  : D:\Proyectos\Aqualia\EntidadesTotal.cs
// Created   : 2019-06-10 17:47:36
// *********************************************************************

namespace Entidades
{
    /// <summary>DisplayName: Actividad, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Tarea realizada o que va a realizar un usuario. Una actividad es cualquier acción para la que se puede crear una entrada en un calendario.</remarks>
    public static class Activitypointer
    {
        public const string EntityName = "activitypointer";
        public const string EntityCollectionName = "activitypointers";
    }

    /// <summary>DisplayName: Actividad de Tercero, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que se encarga de las actividades que tiene que realizar un tercero, pueden ser o no bloqueantes y se puede indicar si se requieren al principio del proceso.</remarks>
    public static class HidrotecActividaddetercero
    {
        public const string EntityName = "hidrotec_actividaddetercero";
        public const string EntityCollectionName = "hidrotec_actividaddeterceros";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_actividaddeterceroid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Documentación válida alternativa a un tipo de documento solicitado</remarks>
        public const string HidrotecAlternativa = "hidrotec_alternativa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si la actividad impide el progreso de la gestión del expediente.</remarks>
        public const string HidrotecBloqueante = "hidrotec_bloqueante";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Descripción del motivo por el que se ha declarado no válida la documentación entregada</remarks>
        public const string HidrotecCausainvalidez = "hidrotec_causainvalidez";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: contact</summary>
        /// <remarks>Identificador del contacto.</remarks>
        public const string HidrotecContactOId = "hidrotec_contactoid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la actividad.</remarks>
        public const string HidrotecDescripcion = "hidrotec_descripcion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada a la entidad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 500, Format: Text</summary>
        /// <remarks>Nombre del archivo que contiene el documento original.</remarks>
        public const string HidrotecDocumentooriginal = "hidrotec_documentooriginal";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Actividad de Tercero</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Actividad de Tercero</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: incident</summary>
        /// <remarks>Identificador de expediente.</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha de recepción de documento</remarks>
        public const string HidrotecFecharecepcion = "hidrotec_fecharecepcion";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        public const string HidrotecFechayhoraregistro = "hidrotec_fechayhoraregistro";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Campo que almacena los estados por los que va pasando la actividad.</remarks>
        public const string HidrotecHistoriCodeTransiciones = "hidrotec_historicodetransiciones";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Idioma de la documentación</remarks>
        public const string HidrotecIdioma = "hidrotec_idioma";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_importe</summary>
        /// <remarks>Valor de Importe en divisa base.</remarks>
        public const string HidrotecImportEBase = "hidrotec_importe_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Importe de la actividad de pago</remarks>
        public const string HidrotecImportE = "hidrotec_importe";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_parametrizacionactividades</summary>
        /// <remarks>Campo de búsqueda a la parametrización de actividad que originó esta Actividad de tercero.</remarks>
        public const string HidrotecParamactividadesoriginariaId = "hidrotec_paramactividadesoriginariaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_periodo</summary>
        /// <remarks>Período en el cual se debe completar la actividad</remarks>
        public const string HidrotecPeriodoId = "hidrotec_periodoid";
        /// <summary>Type: Integer, RequiredLevel: ApplicationRequired, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Plazos en los que se desarrolla la actividad de tercero.</remarks>
        public const string HidrotecPlazo = "hidrotec_plazo";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo Sí/No para contabilizar el tiempo de un plazo en horas.</remarks>
        public const string HidrotecPlazoporhoras = "hidrotec_plazoporhoras";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Check que marca si la actividad se ha terminado o no</remarks>
        public const string HidrotecTerminada = "hidrotec_terminada";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Tipo actividad de tercero, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Tipo de actividad: Pago, obra, aportar documentación o no aplica.</remarks>
        public const string HidrotecTipoactividad = "hidrotec_tipoactividad";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tipo de cambio de la divisa asociada a la entidad en relación con la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de días, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Este campo especifica si los días que aparecen en el período son naturales o laborables</remarks>
        public const string HidrotecTipodias = "hidrotec_tipodias";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_documento</summary>
        /// <remarks>Identificador del tipo de documento</remarks>
        public const string HidrotecTipodocumentoId = "hidrotec_tipodocumentoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 500, Format: Text</summary>
        /// <remarks>URL al sharepoint donde se encuentra la documentación.</remarks>
        public const string HidrotecUrlsharepoint = "hidrotec_urlsharepoint";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            PendientedeenviaraContactO = 1,
            PendientederecibirdeContactO = 500000000,
            Recibidoypendientedevalidar = 500000001,
            Validadoypendientedeadjuntar = 500000002,
            Recibidonovalido = 500000003,
            Validadoyadjuntado = 500000004,
            Pendiente = 500000005,
            Inactivo = 2
        }
        public enum HidrotecIdioma_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum HidrotecTipoactividad_OptionSet
        {
            Pago = 4,
            ObraAcondicionamiento = 2,
            Otros = 5,
            Autolectura = 3,
            Aportardocumento = 1
        }
        public enum HidrotecTipodias_OptionSet
        {
            Laborables = 1,
            Naturales = 2
        }
    }

    /// <summary>DisplayName: Agrupación de roles de contrato, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que se encarga de la agrupación de los distintos roles de un contrato.</remarks>
    public static class HidrotecAgrupacionderolesdecontrato
    {
        public const string EntityName = "hidrotec_agrupacionderolesdecontrato";
        public const string EntityCollectionName = "hidrotec_agrupacionderolesdecontratos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_agrupacionderolesdecontratoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: contact</summary>
        /// <remarks>Identificador del contacto que está asociado a la actividad</remarks>
        public const string HidrotecContactOId = "hidrotec_contactoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_contrato</summary>
        /// <remarks>Identificador del contrato al que está asociado la actividad</remarks>
        public const string HidrotecContratoId = "hidrotec_contratoid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Agrupación de roles de contrato</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Agrupación de roles de contrato</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Área Afectada, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad de las áreas afectadas en una avería o en un corte programado.</remarks>
    public static class HidrotecAreaafectada
    {
        public const string EntityName = "hidrotec_areaafectada";
        public const string EntityCollectionName = "hidrotec_areaafectadas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_areaafectadaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Nombre que la acometida que se encuentra en el área afectada tiene en el GIS.</remarks>
        public const string HidrotecAcometidagis = "hidrotec_acometidagis";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Ámbito, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Números afectados en una calle: pares, impares o N/A.</remarks>
        public const string HidrotecAmbito = "hidrotec_ambito";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        /// <remarks>Identificador del barrio.</remarks>
        public const string HidrotecBarrioId = "hidrotec_barrioid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Comunicación automática de averia</remarks>
        public const string HidrotecComunicacionautomatica = "hidrotec_comunicacionautomatica";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_crucevia</summary>
        /// <remarks>lookup al cruce de vías</remarks>
        public const string HidrotecCrucevias = "hidrotec_crucevias";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Primer número afectado de una calle.</remarks>
        public const string HidrotecDesdenumero = "hidrotec_desdenumero";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        public const string HidrotecNumerodesde = "hidrotec_numerodesde";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Área Afectada</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si actualmente hay un corte.</remarks>
        public const string HidrotecExistecorte = "hidrotec_existecorte";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: incident</summary>
        public const string HidrotecExpedienteoriginarioId = "hidrotec_expedienteoriginarioid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha y hora previstos para el fin de la avería o del corte programado.</remarks>
        public const string HidrotecFechayhorafinprevisto = "hidrotec_fechayhorafinprevisto";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha y hora reales en los que terminó la avería o el corte programado.</remarks>
        public const string HidrotecFechayhorafinreal = "hidrotec_fechayhorafinreal";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha y hora en la que se detectó la avería o que comienza el corte programado.</remarks>
        public const string HidrotecFechayhorainicio = "hidrotec_fechayhorainicio";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        public const string HidrotecFechayhorainicioreal = "hidrotec_fechayhorainicioreal";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        public const string HidrotecNumerohasta = "hidrotec_numerohasta";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_municipio</summary>
        /// <remarks>Identificador del municipio afectado.</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Identificador de la pedanía afectada.</remarks>
        public const string HidrotecPedaniaId = "hidrotec_pedaniaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Polígono de corte que informa GIS cuando ocurre una avería o un corte programado.</remarks>
        public const string HidrotecPoligonodecorte = "hidrotec_poligonodecorte";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        public const string HidrotecProcesada = "hidrotec_procesada";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Área Afectada</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Tipo área afectada, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Tipo del área afectada: Lista de Acometida GIS, cruce de vías, Barrio, Punto de Suministro y tramo de vía.</remarks>
        public const string HidrotecTipoareaafectada = "hidrotec_tipoareaafectada";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Identificador de la vía afectada.</remarks>
        public const string HidrotecViaId = "hidrotec_viaid";
        public enum HidrotecAmbito_OptionSet
        {
            Ambos = 0,
            Impares = 1,
            Pares = 2
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipoareaafectada_OptionSet
        {
            ListaacometidaGIS = 1,
            Crucedevias = 2,
            Barrio = 3,
            Puntodesuministro = 4,
            Tramodevia = 5
        }
    }

    /// <summary>DisplayName: Artículo, OwnershipType: OrganizationOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Contenido estructurado que forma parte de Knowledge Base.</remarks>
    public static class Kbarticle
    {
        public const string EntityName = "kbarticle";
        public const string EntityCollectionName = "kbarticles";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Muestra el identificador del artículo.</remarks>
        public const string PrimaryKey = "kbarticleid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 500, Format: Text</summary>
        /// <remarks>Escriba un tema o un nombre descriptivo del artículo para ayudar en la búsqueda de artículos.</remarks>
        public const string PrimaryName = "title";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: subject</summary>
        /// <remarks>Elija el tema del artículo para ayudar en la búsqueda de artículos. Puede configurar temas en Administración de empresas del área de Configuración.</remarks>
        public const string SubjectId = "subjectid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el artículo de Knowledge Base.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el artículo.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Comentarios acerca del artículo de Knowledge Base.</remarks>
        public const string Comments = "comments";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunidadautonoma</summary>
        /// <remarks>Identificador único de Comunidad Autónoma asociado con Artículo.</remarks>
        public const string HidrotecComunidadautonomaId = "hidrotec_comunidadautonomaid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 1073741823</summary>
        /// <remarks>Descripción del contenido del artículo de Knowledge Base.</remarks>
        public const string Content = "content";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Escriba información adicional que describa el artículo de la Knowledge Base.</remarks>
        public const string Description = "description";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Elija la divisa local del registro para asegurarse de que en los presupuestos se utiliza la divisa correcta.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Muestra si el artículo de Knowledge Base se encuentra en estado de borrador, no aprobado o publicado. Los artículos publicados son de solo lectura y no se pueden editar a menos que estén sin publicar.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el artículo de Knowledge Base.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el artículo de Knowledge Base por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        public const string HidrotecFechafinvigencia = "hidrotec_fechafinvigencia";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        public const string HidrotecFechainiciovigencia = "hidrotec_fechainiciovigencia";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string EntityImageId = "entityimageid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Seleccione en qué idioma debe estar disponible el artículo. Esta lista se basa en la lista de paquetes de idiomas que se instalan en el entorno de Microsoft Dynamics CRM.</remarks>
        public const string LanguageCode = "languagecode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecIdiomaContactO = "hidrotec_idioma_contacto";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el artículo de KB por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el artículo de Knowledge Base por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Identificador único de Municipio asociado con Artículo.</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Número del artículo de Knowledge Base.</remarks>
        public const string Number = "number";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: organization</summary>
        /// <remarks>Identificador único de la organización asociada con el artículo.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Palabras clave para realizar búsquedas en artículos de Knowledge Base.</remarks>
        public const string KeyWords = "keywords";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: kbarticletemplate</summary>
        /// <remarks>Elija la plantilla que desea utilizar como base para crear el nuevo artículo.</remarks>
        public const string KbarticletemplateId = "kbarticletemplateid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Identificador único de Provincia asociado con Artículo.</remarks>
        public const string HidrotecProvinciaId = "hidrotec_provinciaid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Si se configura en Sí, el artículo será visible y permitirá búsquedas en los portales conectados a esta organización.</remarks>
        public const string HidrotecPublicarenweb = "hidrotec_publicarenweb";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Seleccione el estado del artículo.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Muestra la tasa de conversión de la divisa del registro. El tipo de cambio se utiliza para convertir todos los campos de dinero del registro desde la divisa local hasta la divisa predeterminada del sistema.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Título del artículo de Knowledge Base.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 1073741823</summary>
        /// <remarks>Muestra el contenido y el formato del artículo, almacenado como XML.</remarks>
        public const string Articlexml = "articlexml";
        public enum StateCode_OptionSet
        {
            Borrador = 1,
            Noaprobado = 2,
            Publicado = 3
        }
        public enum HidrotecIdiomaContactO_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum StatusCode_OptionSet
        {
            Borrador = 1,
            Noaprobado = 2,
            Publicado = 3
        }
    }

    /// <summary>DisplayName: Banco, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de bancos</remarks>
    public static class HidrotecBanco
    {
        public const string EntityName = "hidrotec_banco";
        public const string EntityCollectionName = "hidrotec_bancos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_bancoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código BIC de identificador del banco.</remarks>
        public const string HidrotecBic = "hidrotec_bic";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Banco</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador del banco en el CRM.</remarks>
        public const string HidrotecIdbanco = "hidrotec_idbanco";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_instalacion</summary>
        /// <remarks>Identificador de la instalación.</remarks>
        public const string HidrotecInstalacionId = "hidrotec_instalacionid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_pais</summary>
        public const string HidrotecPaisId = "hidrotec_paisid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Banco</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Barrio, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que recoge los barrios.</remarks>
    public static class HidrotecBarrio
    {
        public const string EntityName = "hidrotec_barrio";
        public const string EntityCollectionName = "hidrotec_barrios";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_barrioid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Barrio</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador del barrio parametrizable en el CRM.</remarks>
        public const string HidrotecIdbarrio = "hidrotec_idbarrio";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Campo de búsqueda de pedanía.</remarks>
        public const string HidrotecPedaniaId = "hidrotec_pedaniaid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Barrio</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Barrio Vía, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que relaciona Barrio y Vía.</remarks>
    public static class HidrotecBarriovia
    {
        public const string EntityName = "hidrotec_barriovia";
        public const string EntityCollectionName = "hidrotec_barriovias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_barrioviaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        /// <remarks>Campo de búsqueda de barrio.</remarks>
        public const string HidrotecBarrioId = "hidrotec_barrioid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Barrio Vía</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador de barrio parametrizado en el CRM.</remarks>
        public const string HidrotecIdbarriovia = "hidrotec_idbarriovia";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Barrio Vía</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Campo de búsqueda de vía.</remarks>
        public const string HidrotecViaId = "hidrotec_viaid";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: CampoImagen, OwnershipType: UserOwned, IntroducedVersion: 1.0.0.0</summary>
    public static class HidrotecCampoimagen
    {
        public const string EntityName = "hidrotec_campoimagen";
        public const string EntityCollectionName = "hidrotec_campoimagens";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_campoimagenid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        public const string EntityImageId = "entityimageid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del CampoImagen</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del CampoImagen</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Carga de parametrización, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecCargadeparametrizacion
    {
        public const string EntityName = "hidrotec_cargadeparametrizacion";
        public const string EntityCollectionName = "hidrotec_cargadeparametrizacions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_cargadeparametrizacionid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Carga de parametrización</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: businessunit</summary>
        public const string HidrotecExplotacionId = "hidrotec_explotacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        public const string HidrotecFechadeprocesamiento = "hidrotec_fechadeprocesamiento";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 5000</summary>
        public const string HidrotecLogdeprocesamiento = "hidrotec_logdeprocesamiento";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        public const string HidrotecProcesadocorrectamente = "hidrotec_procesadocorrectamente";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Carga de parametrización</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum HidrotecProcesadocorrectamente_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Carta, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Actividad que mantiene un seguimiento de la entrega de una carta. Puede contener la copia electrónica de la carta.</remarks>
    public static class Letter
    {
        public const string EntityName = "letter";
        public const string EntityCollectionName = "letters";
    }

    /// <summary>DisplayName: Causa, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de Causas</remarks>
    public static class HidrotecCausa
    {
        public const string EntityName = "hidrotec_causa";
        public const string EntityCollectionName = "hidrotec_causas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_causaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Causa</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador de la causa parametrizado en el CRM.</remarks>
        public const string HidrotecIdcausa = "hidrotec_idcausa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Causa</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Cita, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Compromiso que representa un intervalo de tiempo con las fechas de inicio y fin, y la duración.</remarks>
    public static class Appointment
    {
        public const string EntityName = "appointment";
        public const string EntityCollectionName = "appointments";
    }

    /// <summary>DisplayName: Comentario de artículo, OwnershipType: None, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Comentario en un artículo de Knowledge Base.</remarks>
    public static class Kbarticlecomment
    {
        public const string EntityName = "kbarticlecomment";
        public const string EntityCollectionName = "kbarticlecomments";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del comentario de artículo de Knowledge Base.</remarks>
        public const string PrimaryKey = "kbarticlecommentid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Título del comentario de artículo de Knowledge Base.</remarks>
        public const string PrimaryName = "title";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: kbarticle</summary>
        /// <remarks>Identificador único del artículo de Knowledge Base al que se aplica el comentario.</remarks>
        public const string KbarticleId = "kbarticleid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el comentario de artículo de Knowledge Base.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el comentario del artículo de KB.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el comentario de artículo de Knowledge Base.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el comentario de artículo de Knowledge Base por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el comentario del artículo de KB por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el comentario de artículo de Knowledge Base por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Texto del comentario del artículo de Knowledge Base.</remarks>
        public const string Commenttext = "commenttext";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: Comunicación, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de tipos de comunicación</remarks>
    public static class HidrotecComunicacion
    {
        public const string EntityName = "hidrotec_comunicacion";
        public const string EntityCollectionName = "hidrotec_comunicacions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_comunicacionid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la comunicación puede realizarse via APP.</remarks>
        public const string HidrotecAdmiteapp = "hidrotec_admiteapp";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la comunicación puede realizarse via correo electrónico.</remarks>
        public const string HidrotecAdmitecorreoelectronico = "hidrotec_admitecorreoelectronico";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la comunicacón puede realizarse mediante el envío masivo de correo postal.</remarks>
        public const string HidrotecAdmitecorreoPostalMasivo = "hidrotec_admitecorreopostalmasivo";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la comunicación puede realizarse vía SMS.</remarks>
        public const string HidrotecAdmitesms = "hidrotec_admitesms";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la comunicación se asigna automáticamente a la explotación a la que pertenece el cliente.</remarks>
        public const string HidrotecAsignacionautomaticaaexplotacion = "hidrotec_asignacionautomaticaaexplotacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Comunicación</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador del valor del campo Comunicación parametrizado en el CRM.</remarks>
        public const string HidrotecIdcomunicacion = "hidrotec_idcomunicacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Propósito, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Objetivo de la realización de la comunicación al cliente. Puede ser Alarma, Com Resolución, Com Actividad, Alarm y Com Resolución, Alarm y Com actividad, Com Resolución y Com. Alarm y todos.</remarks>
        public const string HidrotecProposito = "hidrotec_proposito";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Comunicación</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecProposito_OptionSet
        {
            Alarma = 1,
            Comunicacionresolucion = 2,
            Comunicacionactividad = 3,
            Alarmaycomunicacionresolucion = 4,
            Alarmaycomunicacionactividad = 5,
            Comresolucionycomactividad = 6,
            ComunicacionaveriaCorte = 7,
            Todos = 8
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Comunidad Autónoma, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Callejero en el que viene recogida la información referida a la Comunidad Autónoma.</remarks>
    public static class HidrotecComunidadautonoma
    {
        public const string EntityName = "hidrotec_comunidadautonoma";
        public const string EntityCollectionName = "hidrotec_comunidadautonomas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_comunidadautonomaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Comunidad Autónoma</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador de la comunidad autónoma parametrizado en el CRM.</remarks>
        public const string HidrotecIdcomunidadautonoma = "hidrotec_idcomunidadautonoma";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo de búsqueda de país al que pertenece la Comunidad Autónoma</remarks>
        public const string HidrotecPaisId = "hidrotec_paisid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Comunidad Autónoma</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Configuración clave valor, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad de configuración para establecer pares de clave valor.</remarks>
    public static class HidrotecConfiguracionclavevalor
    {
        public const string EntityName = "hidrotec_configuracionclavevalor";
        public const string EntityCollectionName = "hidrotec_configuracionclavevalors";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_configuracionclavevalorid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>La clave del par clave-valor.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Configuración clave valor</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Configuración clave valor</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>Valor del par clave-valor.</remarks>
        public const string HidrotecValor = "hidrotec_valor";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Configuración Integración, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Configuración de las integraciones.</remarks>
    public static class HidrotecConfiguracionintegracion
    {
        public const string EntityName = "hidrotec_configuracionintegracion";
        public const string EntityCollectionName = "hidrotec_configuracionintegracions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_configuracionintegracionid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Códigos de error para la integración.</remarks>
        public const string HidrotecCodigoserroresfuncionales = "hidrotec_codigoserroresfuncionales";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Configuración Integración</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador de la integración parametrizable en el CRM.</remarks>
        public const string HidrotecIdintegracion = "hidrotec_idintegracion";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indicador del estado activo/inactivo de la integración.</remarks>
        public const string HidrotecIntegracionactiva = "hidrotec_integracionactiva";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la integración permite reintento manual o no.</remarks>
        public const string HidrotecPermitereintentomanual = "hidrotec_permitereintentomanual";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Configuración Integración</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Número de reintentos permitidos.</remarks>
        public const string HidrotecReintentospermitidos = "hidrotec_reintentospermitidos";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>URL de la integración.</remarks>
        public const string HidrotecUrl = "hidrotec_url";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>URL para pruebas de la integración.</remarks>
        public const string HidrotecUrlpruebas = "hidrotec_urlpruebas";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Configuración organizativa, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa la configuración organizativa.</remarks>
    public static class HidrotecConfiguracionorganizativa
    {
        public const string EntityName = "hidrotec_configuracionorganizativa";
        public const string EntityCollectionName = "hidrotec_configuracionorganizativas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_configuracionorganizativaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Número de la contrata de la explotación.</remarks>
        public const string HidrotecContrata = "hidrotec_contrata";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Configuración organizativa</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Campo de búsqueda de la explotación.</remarks>
        public const string HidrotecExplotacionId = "hidrotec_explotacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Identificador de la configuracion organizativa parametrizada en el CRM.</remarks>
        public const string HidrotecIdconfiguracionorganizativa = "hidrotec_idconfiguracionorganizativa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Campo de búsqueda de municipio.</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Configuración organizativa</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Número del servicio de la configuración organizativa.</remarks>
        public const string HidrotecServicio = "hidrotec_servicio";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Contacto, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Persona con la que tiene una relación una unidad de negocio, como por ejemplo, un cliente, un distribuidor o un colega.</remarks>
    public static class Contact
    {
        public const string EntityName = "contact";
        public const string EntityCollectionName = "contacts";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del contacto.</remarks>
        public const string PrimaryKey = "contactid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Combina y muestra el nombre y los apellidos del contacto para que pueda mostrarse el nombre completo en vistas e informes.</remarks>
        public const string PrimaryName = "fullname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Escriba el nombre del jefe del contacto para su uso al remitir a una instancia superior problemas u otras comunicaciones de seguimiento con el contacto.</remarks>
        public const string ManagerName = "managername";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: DateOnly</summary>
        /// <remarks>Escriba la fecha de la boda o del aniversario de servicio del contacto para utilizarla en programas de regalos u otras comunicaciones con clientes.</remarks>
        public const string Anniversary = "anniversary";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el apellido del contacto para asegurarse de que se dirige correctamente a él en llamadas de ventas, correo electrónico y campañas de marketing.</remarks>
        public const string LastName = "lastname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 150, Format: PhoneticGuide</summary>
        /// <remarks>Escriba la transcripción fonética del apellido del contacto, si se especifica en japonés, para asegurarse de pronunciarlo correctamente en llamadas de teléfono con el contacto.</remarks>
        public const string YomiLastName = "yomilastname";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra quién creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra quién creó el registro en nombre de otro usuario.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Escriba el nombre del ayudante del contacto.</remarks>
        public const string AssistantName = "assistantname";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        public const string HidrotecBarrioId = "hidrotec_barrioid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecBloque = "hidrotec_bloque";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecCanalmodificacion = "hidrotec_canalmodificacion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Campo que indica si el contacto es cliente de Aqualia.</remarks>
        public const string HidrotecCliente = "hidrotec_cliente";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto existe en una contabilidad aparte u otro sistema, como Microsoft Dynamics GP u otra base de datos de ERP, para su uso en procesos de integración.</remarks>
        public const string IsbackofficeCustomer = "isbackofficecustomer";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: lead</summary>
        /// <remarks>Muestra el cliente potencial a partir del cual se creó el contacto si este se creó convirtiendo un cliente potencial a Microsoft Dynamics CRM. Se utiliza para relacionar el contacto con los datos del cliente potencial original para su uso en generación de informes y análisis.</remarks>
        public const string OriginatingleadId = "originatingleadid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        public const string HidrotecCodigoPostal = "hidrotec_codigopostal";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Muestra si se ha combinado la cuenta con un contacto principal.</remarks>
        public const string Merged = "merged";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Condiciones de pago, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione las condiciones de pago para indicar cuándo debe pagar el cliente el importe total.</remarks>
        public const string PaymenttermsCode = "paymenttermscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Escriba la dirección de correo electrónico principal para el contacto.</remarks>
        public const string EMailAddress1 = "emailaddress1";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Información que especifica si el contacto se creó automáticamente al promover un correo electrónico o una cita.</remarks>
        public const string Isautocreate = "isautocreate";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: externalparty</summary>
        /// <remarks>Muestra la parte externa que creó el registro.</remarks>
        public const string CreatedByExternalParty = "createdbyexternalparty";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: DateOnly</summary>
        /// <remarks>Escriba el cumpleaños del contacto para utilizarlo en programas de regalos u otras comunicaciones con clientes.</remarks>
        public const string Birthdate = "birthdate";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string MsdyusdCurrentprofile = "msdyusd_currentprofile";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Escriba el departamento o la unidad de negocio donde trabaja el contacto en la compañía o empresa primaria.</remarks>
        public const string Department = "department";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Escriba información adicional para describir el contacto, por ejemplo, un extracto del sitio web de la compañía.</remarks>
        public const string Description = "description";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Día preferido, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione el día de la semana preferido para citas de servicio.</remarks>
        public const string PreferredAppointmentdayCode = "preferredappointmentdaycode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Composición de los campos de la dirección.</remarks>
        public const string HidrotecDireccioncompuesta = "hidrotec_direccioncompuesta";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Escriba la dirección de correo electrónico secundaria para el contacto.</remarks>
        public const string EMailAddress2 = "emailaddress2";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Escriba una dirección de correo electrónico alternativa para el contacto.</remarks>
        public const string EMailAddress3 = "emailaddress3";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Elija la divisa local del registro para asegurarse de que en los presupuestos se utiliza la divisa correcta.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica que si el cliente está identificado por un número de documento válido que no sea “Contrato”.</remarks>
        public const string HidrotecIdentificado = "hidrotec_identificado";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto para introducir el edificio.</remarks>
        public const string HidrotecEdificio = "hidrotec_edificio";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Educación, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione el nivel superior de educación del contacto para su uso en segmentación y análisis.</remarks>
        public const string EducationCode = "educationcode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el identificador o número de empleado del contacto para tenerlo como referencia en pedidos, casos de servicio u otras comunicaciones con la organización del contacto.</remarks>
        public const string EmployeeId = "employeeid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        public const string HidrotecEnviardatos = "hidrotec_enviardatos";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto acepta materiales de marketing, como folletos o catálogos. Los contactos que optan por no participar pueden quedar excluidos de iniciativas de marketing.</remarks>
        public const string DonotSendmm = "donotsendmm";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de la dirección para informar con la escalera.</remarks>
        public const string HidrotecEscalera = "hidrotec_escalera";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Muestra si el contacto está activo o inactivo. Los contactos inactivos son de solo lectura y no se pueden editar si no se reactivan.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estado civil, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione el estado civil del contacto para tenerlo como referencia en llamadas de teléfono de seguimiento u otras comunicaciones.</remarks>
        public const string FamilyStatusCode = "familystatuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 300, Format: Text</summary>
        public const string MsdyusdFacebook = "msdyusd_facebook";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Muestra el identificador de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el número de fax del contacto.</remarks>
        public const string Fax = "fax";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Muestra la fecha y la hora en que se creó el registro. La fecha y la hora se muestran en la zona horaria seleccionada en las opciones de Microsoft Dynamics CRM.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Muestra la fecha y la hora en que se actualizó el registro por última vez. La fecha y la hora se muestran en la zona horaria seleccionada en las opciones de Microsoft Dynamics CRM.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el número de pasaporte u otra identificación gubernamental del contacto para su uso en documentos e informes.</remarks>
        public const string GovernmentId = "governmentid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Hora preferida, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione la hora preferida del día para citas de servicio.</remarks>
        public const string PreferredAppointmenttimeCode = "preferredappointmenttimecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador único construido con el país y un secuencial.
        /// Utilizado para la unificación de contacto.</remarks>
        public const string HidrotecIdunicoContactO = "hidrotec_idunicocontacto";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Identificador único del contacto principal para combinación.</remarks>
        public const string MasterId = "masterid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 12, Format: Text</summary>
        /// <remarks>Campo para introducir el número de identificación.</remarks>
        public const string HidrotecNumerodocumento = "hidrotec_numerodocumento";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string EntityImageId = "entityimageid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Identificador de un usuario externo.</remarks>
        public const string ExternalUserIdentifier = "externaluseridentifier";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: 0, MaxValue: 100000000000000, Precision: 2</summary>
        /// <remarks>Escriba los ingresos anuales del contacto para utilizarlos en la elaboración de perfiles y análisis financieros.</remarks>
        public const string AnnualIncome = "annualincome";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: annualincome</summary>
        /// <remarks>Muestra el campo Ingresos anuales convertido a la divisa base predeterminada del sistema. Los cálculos utilizan el tipo de cambio especificado en el área Divisas.</remarks>
        public const string AnnualIncomeBase = "annualincome_base";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: equipment</summary>
        /// <remarks>Elija las instalaciones o equipamiento de servicio preferido del contacto para asegurarse de que los servicios se programan correctamente para el cliente.</remarks>
        public const string PreferredEquipmentId = "preferredequipmentid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        public const string IsPrivate = "isprivate";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: creditlimit</summary>
        /// <remarks>Muestra el campo Límite de crédito convertido a la divisa base predeterminada del sistema para la generación de informes. Los cálculos utilizan el tipo de cambio especificado en el área Divisas.</remarks>
        public const string CreditLimitBase = "creditlimit_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: 0, MaxValue: 100000000000000, Precision: 2</summary>
        /// <remarks>Escriba el límite de crédito del contacto para tenerlo como referencia al tratar problemas de facturación y contabilidad con el cliente.</remarks>
        public const string CreditLimit = "creditlimit";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: pricelevel</summary>
        /// <remarks>Elija la lista de precios predeterminada asociada con el contacto para asegurarse de que se aplican los precios correctos de los productos para este cliente en oportunidades de ventas, ofertas y pedidos.</remarks>
        public const string DefaultPricelevelId = "defaultpricelevelid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el número de localizador del contacto.</remarks>
        public const string Pager = "pager";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Método de contacto preferido, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione el método de contacto preferido.</remarks>
        public const string PreferredContactMethodCode = "preferredcontactmethodcode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra quién actualizó el registro en nombre de otro usuario por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra quién actualizó el registro por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: externalparty</summary>
        /// <remarks>Muestra la parte externa que modificó el registro.</remarks>
        public const string ModifiedByExternalParty = "modifiedbyexternalparty";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Modo de envío , OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione un método de envío para las entregas enviadas a esta dirección.</remarks>
        public const string ShippingMethodCode = "shippingmethodcode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Campo de búsqueda a Municipio</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto para introducir el municipio cuando no sea del callejero.</remarks>
        public const string HidrotecMunicipio = "hidrotec_municipio";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000</summary>
        /// <remarks>Escriba el número de hijos del contacto para tenerlo como referencia en llamadas de teléfono de seguimiento u otras comunicaciones.</remarks>
        public const string NumberOfchildren = "numberofchildren";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto permite correo directo. Si selecciona No permitir, el contacto quedará excluido de las actividades de cartas distribuidas en campañas de marketing.</remarks>
        public const string DonotPostalMail = "donotpostalmail";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto acepta correo electrónico en masa a través de campañas de marketing o campañas exprés. Si selecciona No permitir, el contacto podrá agregarse a listas de marketing, pero quedará excluida del correo electrónico.</remarks>
        public const string DonotBulkeMail = "donotbulkemail";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto permite enviar correo electrónico directo desde Microsoft Dynamics CRM. Si selecciona No permitir, Microsoft Dynamics CRM no enviará el correo electrónico.</remarks>
        public const string DonotEMail = "donotemail";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto acepta correo postal masivo a través de campañas de marketing o campañas exprés. Si selecciona No permitir, el contacto podrá agregarse a listas de marketing, pero quedará excluida de las cartas.</remarks>
        public const string DonotBulkPostalMail = "donotbulkpostalmail";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto permite faxes. Si selecciona No permitir, el contacto podrá quedar excluido de las actividades de fax distribuidas en campañas de marketing.</remarks>
        public const string DonotFax = "donotfax";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto acepta llamadas de teléfono. Si selecciona No permitir, el contacto podrá quedar excluido de las actividades de llamada de teléfono distribuidas en campañas de marketing.</remarks>
        public const string DonotPhone = "donotphone";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        public const string HidrotecNombre = "hidrotec_nombre";
        /// <summary>Type: Customer, RequiredLevel: None, Targets: account,contact</summary>
        /// <remarks>Seleccione la cuenta primaria o el contacto primario del contacto para proporcionar un vínculo rápido a detalles adicionales, como información financiera, actividades y oportunidades.</remarks>
        public const string ParentCustomerId = "parentcustomerid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Escriba el nombre de pila del contacto para asegurarse de que se dirige correctamente a él en llamadas de ventas, correo electrónico y campañas de marketing.</remarks>
        public const string FirstName = "firstname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 150, Format: PhoneticGuide</summary>
        /// <remarks>Escriba la transcripción fonética del nombre de pila del contacto, si se especifica en japonés, para asegurarse de pronunciarlo correctamente en llamadas de teléfono con el contacto.</remarks>
        public const string YomiFirstName = "yomifirstname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Escriba el nombre del cónyuge o la pareja del contacto para tenerlo como referencia en llamadas, eventos u otras comunicaciones con el contacto.</remarks>
        public const string SpousesName = "spousesname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 450, Format: PhoneticGuide</summary>
        /// <remarks>Muestra el nombre y los apellidos Yomi del contacto para que pueda mostrarse el nombre fonético completo en vistas e informes.</remarks>
        public const string YomiFullname = "yomifullname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 255, Format: Text</summary>
        /// <remarks>Escriba los nombres de los hijos del contacto para tenerlos como referencia en comunicaciones y programas de cliente.</remarks>
        public const string ChildrensNameS = "childrensnames";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        public const string HidrotecNumero = "hidrotec_numero";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba un número de teléfono de devolución de llamada para este contacto.</remarks>
        public const string Callback = "callback";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión del contacto.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Origen del cliente potencial, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione el origen de marketing principal que dirigió el contacto a su organización.</remarks>
        public const string LeadsourceCode = "leadsourcecode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Origen modificación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecOrigenmodificacion = "hidrotec_origenmodificacion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecOtros = "hidrotec_otros";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo de búsqueda a País.</remarks>
        public const string HidrotecPaisId = "hidrotec_paisid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Identificador único de País asociado con Contacto.</remarks>
        public const string HidrotecPaisidDocumento = "hidrotec_paisid_documento";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 160, Format: Text</summary>
        public const string ParentCustomerIdName = "parentcustomeridname";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 450, Format: Text</summary>
        public const string ParentCustomerIdyomiName = "parentcustomeridyominame";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Muestra si el contacto participa en reglas de flujo de trabajo.</remarks>
        public const string Participatesinworkflow = "participatesinworkflow";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Campo de búsqueda a Pedanía</remarks>
        public const string HidrotecPedaniaId = "hidrotec_pedaniaid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra durante cuánto tiempo, en minutos, se retuvo el registro.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecPiso = "hidrotec_piso";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        public const string HidrotecPrimerapellido = "hidrotec_primerapellido";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Muestra el identificador del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Escriba el usuario o el equipo que está asignado para administrar el registro. Este campo se actualiza cada vez que se asigna el registro a otro usuario.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Campo de búsqueda a Provincia</remarks>
        public const string HidrotecProvinciaId = "hidrotec_provinciaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecProvincia = "hidrotec_provincia";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        public const string HidrotecPuerta = "hidrotec_puerta";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Escriba el puesto del contacto para asegurarse de que se dirige correctamente a él en llamadas de ventas, correo electrónico y campañas de marketing.</remarks>
        public const string Jobtitle = "jobtitle";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Seleccione el estado del contacto.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Rol, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione el rol del contacto en la compañía o el proceso de ventas, como responsable de toma de decisiones, empleado o persona con influencia.</remarks>
        public const string AccountRoleCode = "accountrolecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Escriba el saludo del contacto para asegurarse de que se dirige correctamente a él en llamadas de ventas, mensajes de correo electrónico y campañas de marketing.</remarks>
        public const string Salutation = "salutation";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecSegundoapellido = "hidrotec_segundoapellido";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el segundo nombre o la inicial del contacto para asegurarse de que se dirige correctamente a él.</remarks>
        public const string MiddleName = "middlename";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 150, Format: PhoneticGuide</summary>
        /// <remarks>Escriba la transcripción fonética del segundo nombre del contacto, si se especifica en japonés, para asegurarse de pronunciarlo correctamente en llamadas de teléfono con el contacto.</remarks>
        public const string YomiMiddleName = "yomimiddlename";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: service</summary>
        /// <remarks>Elija el servicio preferido del contacto para asegurarse de que los servicios se programan correctamente para el cliente.</remarks>
        public const string PreferredServiceId = "preferredserviceid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sexo, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione el sexo del contacto para asegurarse de que se dirige correctamente a él en llamadas de ventas, correo electrónico y campañas de marketing.</remarks>
        public const string GenderCode = "gendercode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: True</summary>
        public const string HidrotecSincronizado = "hidrotec_sincronizado";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Url</summary>
        /// <remarks>Escriba la dirección URL del sitio FTP del contacto para permitir a los usuarios acceder a datos y compartir documentos.</remarks>
        public const string Ftpsiteurl = "ftpsiteurl";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Url</summary>
        /// <remarks>Escriba la dirección URL del sitio web o del blog profesional o personal del contacto.</remarks>
        public const string Websiteurl = "websiteurl";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro de contacto.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el sobrenombre del contacto.</remarks>
        public const string NickName = "nickname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Escriba el sufijo utilizado en el nombre del contacto, como Jr. o Sr., para asegurarse de que se dirige correctamente a él en llamadas de ventas, correo electrónico y campañas de marketing.</remarks>
        public const string Suffix = "suffix";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string SubscriptionId = "subscriptionid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el contacto está en suspensión de crédito para tenerlo como referencia al tratar problemas de facturación y contabilidad.</remarks>
        public const string CreditOnhold = "creditonhold";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tamaño del cliente, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione el tamaño de la compañía del contacto para segmentación y generación de informes.</remarks>
        public const string CustomerSizeCode = "customersizecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba un tercer número de teléfono para este contacto.</remarks>
        public const string TelePhone3 = "telephone3";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el teléfono de la compañía del contacto.</remarks>
        public const string Company = "company";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el número de teléfono del ayudante del contacto.</remarks>
        public const string AssistantPhone = "assistantphone";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el número de teléfono del jefe del contacto.</remarks>
        public const string ManagerPhone = "managerphone";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el número de teléfono principal para este contacto.</remarks>
        public const string TelePhone1 = "telephone1";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba un segundo número de teléfono del trabajo para este contacto.</remarks>
        public const string Business2 = "business2";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba el número de teléfono móvil del contacto.</remarks>
        public const string MobilePhone = "mobilephone";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba un segundo número de teléfono para este contacto.</remarks>
        public const string TelePhone2 = "telephone2";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Escriba un segundo número de teléfono particular para este contacto.</remarks>
        public const string Home2 = "home2";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tiene hijos, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione si el contacto tiene hijos para tenerlo como referencia en llamadas de teléfono de seguimiento u otras comunicaciones.</remarks>
        public const string HaschildrenCode = "haschildrencode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Muestra la tasa de conversión de la divisa del registro. El tipo de cambio se utiliza para convertir todos los campos de dinero del registro desde la divisa local hasta la divisa predeterminada del sistema.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        public const string ParentCustomerIdType = "parentcustomeridtype";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de Documento, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones con todos los documentos de todos los países más “Contrato” P.A documentos por país.</remarks>
        public const string HidrotecTipodedocumento = "hidrotec_tipodedocumento";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipodedocumento</summary>
        /// <remarks>Tipo de documento de identificación del contacto.</remarks>
        public const string HidrotecTipodedocumentoId = "hidrotec_tipodedocumentoid";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Tipo de persona, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecTipodepersona = "hidrotec_tipodepersona";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de relación, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione la categoría que mejor describe la relación entre el contacto y la organización.</remarks>
        public const string CustomerTypeCode = "customertypecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 300, Format: Text</summary>
        public const string MsdyusdTwitter = "msdyusd_twitter";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        public const string HidrotecUltimasincronizacion = "hidrotec_ultimasincronizacion";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Muestra la fecha en la que el contacto se incluyó por última vez en una campaña de marketing o una campaña exprés.</remarks>
        public const string LastusedIncampaign = "lastusedincampaign";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Último SLA que se aplicó a este caso. Este campo es solo para uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del contacto.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Elija el representante de servicio al cliente regular o preferido para tenerlo como referencia cuando programe actividades de servicio para el contacto.</remarks>
        public const string PreferredSystemUserId = "preferredsystemuserid";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: 0, MaxValue: 100000000000000, Precision: 2</summary>
        /// <remarks>Solo para uso del sistema.</remarks>
        public const string Aging30 = "aging30";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: aging30</summary>
        /// <remarks>Muestra el campo Vence 30 convertido a la divisa base predeterminada del sistema. Los cálculos utilizan el tipo de cambio especificado en el área Divisas.</remarks>
        public const string Aging30Base = "aging30_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: 0, MaxValue: 100000000000000, Precision: 2</summary>
        /// <remarks>Solo para uso del sistema.</remarks>
        public const string Aging60 = "aging60";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: aging60</summary>
        /// <remarks>Muestra el campo Vence 60 convertido a la divisa base predeterminada del sistema. Los cálculos utilizan el tipo de cambio especificado en el área Divisas.</remarks>
        public const string Aging60Base = "aging60_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: 0, MaxValue: 100000000000000, Precision: 2</summary>
        /// <remarks>Solo para uso del sistema.</remarks>
        public const string Aging90 = "aging90";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: aging90</summary>
        /// <remarks>Muestra el campo Vence 90 convertido a la divisa base predeterminada del sistema. Los cálculos utilizan el tipo de cambio especificado en el área Divisas.</remarks>
        public const string Aging90Base = "aging90_base";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Campo de búsqueda a Vía</remarks>
        public const string HidrotecViaId = "hidrotec_viaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecVia = "hidrotec_via";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Zona de ventas, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione una región o un territorio del contacto para su uso en segmentación y análisis.</remarks>
        public const string TerritoryCode = "territorycode";
        public enum HidrotecCanalmodificacion_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum HidrotecCliente_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum PaymenttermsCode_OptionSet
        {
            Pagoa30dias = 1,
            _210pagoa30dias = 2,
            Pagoa45dias = 3,
            Pagoa60dias = 4,
            Pagoalcontado = 10,
            _306090 = 38,
            Aplazado = 39
        }
        public enum PreferredAppointmentdayCode_OptionSet
        {
            Domingo = 0,
            Lunes = 1,
            Martes = 2,
            Miercoles = 3,
            Jueves = 4,
            Viernes = 5,
            Sabado = 6
        }
        public enum HidrotecIdentificado_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum EducationCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum FamilyStatusCode_OptionSet
        {
            Unico = 1,
            CasadoA = 2,
            DivorciadoA = 3,
            Viudo = 4
        }
        public enum PreferredAppointmenttimeCode_OptionSet
        {
            Manana = 1,
            Tarde = 2,
            Ultimahora = 3
        }
        public enum PreferredContactMethodCode_OptionSet
        {
            Cualquiera = 1,
            Correoelectronico = 2,
            Telefono = 3,
            Fax = 4,
            Correo = 5
        }
        public enum ShippingMethodCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum LeadsourceCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum HidrotecOrigenmodificacion_OptionSet
        {
            CRM = 1,
            ETL = 2
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum AccountRoleCode_OptionSet
        {
            Responsabledetomadedecisiones = 1,
            Empleado = 2,
            Personaconinfluencia = 3
        }
        public enum GenderCode_OptionSet
        {
            Hombre = 1,
            Mujer = 2
        }
        public enum CustomerSizeCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum HaschildrenCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum ParentCustomerIdType_OptionSet
        {
        }
        public enum HidrotecTipodedocumento_OptionSet
        {
            NIFPersonaFisica = 1,
            NIFPersonaJuridica = 2,
            TarjetadeResidentes = 3,
            OrganismosPublicos = 4,
            Pasaporte = 5,
            NumeroIdentificacionExtranjero = 6
        }
        public enum HidrotecTipodepersona_OptionSet
        {
            Fisica = 1,
            Juridica = 2
        }
        public enum CustomerTypeCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum TerritoryCode_OptionSet
        {
            Valorpredeterminado = 1
        }
    }

    /// <summary>DisplayName: Contenido Comunicación, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que recoge el contenido de la comunicación.</remarks>
    public static class HidrotecContenidocomunicacion
    {
        public const string EntityName = "hidrotec_contenidocomunicacion";
        public const string EntityCollectionName = "hidrotec_contenidocomunicacions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_contenidocomunicacionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Canal por el que se realiza la comunicación.</remarks>
        public const string HidrotecCanal = "hidrotec_canal";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Campo de búsqueda a la entidad Comunicación.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Descripción asociada al contenido de la comunicación.</remarks>
        public const string HidrotecDescripcion = "hidrotec_descripcion";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Contenido Comunicación</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Campo de búsqueda a la explotación.</remarks>
        public const string HidrotecExplotacionId = "hidrotec_explotacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador del contenido parametrizado en el CRM.</remarks>
        public const string HidrotecIdcontenido = "hidrotec_idcontenido";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Desplegable de selección de idioma del contenido de la comunicación.</remarks>
        public const string HidrotecIdioma = "hidrotec_idioma";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Contenido Comunicación</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Texto de que consta el contenido de la comunicación.</remarks>
        public const string HidrotecTexto = "hidrotec_texto";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Texto del asunto de la comunicación.</remarks>
        public const string HidrotecTextoasunto = "hidrotec_textoasunto";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecCanal_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecIdioma_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Contrato, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa un contrato de Aqualia.</remarks>
    public static class HidrotecContrato
    {
        public const string EntityName = "hidrotec_contrato";
        public const string EntityCollectionName = "hidrotec_contratos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_contratoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Accesibilidad contador, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Desplegable para contador: accesible, no accesible, parcialmente accesible.</remarks>
        public const string HidrotecAccesibilidadcontador = "hidrotec_accesibilidadcontador";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Desplegable para afectado por avería: sí, no, N/A</remarks>
        public const string HidrotecAfectadoaveria = "hidrotec_afectadoaveria";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Desplegable para afectado por corte programado: sí, no, N/A.</remarks>
        public const string HidrotecAfectadocorteprogramado = "hidrotec_afectadocorteprogramado";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Desplegable para avisado por avería: sí, no, N/A.</remarks>
        public const string HidrotecAvisoaveria = "hidrotec_avisoaveria";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Desplegable para afectado por avería: sí, no, N/A.</remarks>
        public const string HidrotecCampaniadecorte = "hidrotec_campaniadecorte";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Canal por el que irán las alarmas si hay que informar al contacto.</remarks>
        public const string HidrotecCanalalarmas = "hidrotec_canalalarmas";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Canal por el cual el contacto deberá hacer llegar la documentación.</remarks>
        public const string HidrotecCanaldocumentacion = "hidrotec_canaldocumentacion";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Canal por el que se comunicará la resolución.</remarks>
        public const string HidrotecCanalnotificacionresolucion = "hidrotec_canalnotificacionresolucion";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la clave de modificación ha sido activada o no.</remarks>
        public const string HidrotecClavedemodificacionactivada = "hidrotec_clavedemodificacionactivada";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Clave de modificación que está activa para el contrato.</remarks>
        public const string HidrotecClavemodificacion = "hidrotec_clavemodificacion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código que la acometida tiene en Diversa.</remarks>
        public const string HidrotecCodigoacometidadiversa = "hidrotec_codigoacometidadiversa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código que la acometida tiene en GIS.</remarks>
        public const string HidrotecCodigoacometidagis = "hidrotec_codigoacometidagis";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 10, Format: Text</summary>
        /// <remarks>Código de la contrata del contrato.</remarks>
        public const string HidrotecCodigocontrata = "hidrotec_codigocontrata";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 10, Format: Text</summary>
        /// <remarks>Código del servicio del contrato</remarks>
        public const string HidrotecCodigoservicio = "hidrotec_codigoservicio";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Email</summary>
        /// <remarks>Dirección de correo electrónico</remarks>
        public const string HidrotecCorreoelectronico = "hidrotec_correoelectronico";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si tiene derecho ARCO de cancelación</remarks>
        public const string HidrotecDerechoarcocancelacion = "hidrotec_derechoarcocancelacion";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si tiene derecho ARCO de oposición</remarks>
        public const string HidrotecDerechoarcooposicion = "hidrotec_derechoarcooposicion";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_direcciondesuministro</summary>
        /// <remarks>Dirección de suministro.</remarks>
        public const string HidrotecDirecciondesuministroId = "hidrotec_direcciondesuministroid";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Conjunto de opciones que indica si el contrato tiene domiciliación bancaria o no.</remarks>
        public const string HidrotecDomiciliacionbancaria = "hidrotec_domiciliacionbancaria";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Dirección de email a la que enviar la factura para imprimirse.</remarks>
        public const string HidrotecEMailImpresionfactura = "hidrotec_emailimpresionfactura";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Estado contrato diversa, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Desplegable para el estado del contrato.</remarks>
        public const string HidrotecEstado = "hidrotec_estado";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Contrato</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Desplegable para indicar si exsite deuda asociada al contrato.</remarks>
        public const string HidrotecExistedeuda = "hidrotec_existedeuda";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. de la explotación propietaria del contrato.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Falta Información, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Desplegable para indicar si falta información en el contrato. Las opciones son: No falta, Falta leve y Falta grave,</remarks>
        public const string HidrotecFaltainformacion = "hidrotec_faltainformacion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Número de Fax asociado al contrato.</remarks>
        public const string HidrotecFax = "hidrotec_fax";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha de alta del contrato.</remarks>
        public const string HidrotecFechaalta = "hidrotec_fechaalta";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha de baja del contrato.</remarks>
        public const string HidrotecFechabaja = "hidrotec_fechabaja";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha de caducidad de la clave de contrato.</remarks>
        public const string HidrotecFechadecaducidadclave = "hidrotec_fechadecaducidadclave";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha de emision del contrato.</remarks>
        public const string HidrotecFechaemision = "hidrotec_fechaemision";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha de la última facturación asociada al contato.</remarks>
        public const string HidrotecFechaultimafacturacion = "hidrotec_fechaultimafacturacion";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Desplegable para indicar si el contrato es de un gran consumidor.</remarks>
        public const string HidrotecGranconsumidor = "hidrotec_granconsumidor";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador del contrato en Diversa.</remarks>
        public const string HidrotecIdcontratodiversa = "hidrotec_idcontratodiversa";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Contiene el Id. de la fase donde se encuentra la entidad.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Contiene el Id. del proceso asociado con la entidad.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Desplegable para el idioma del contrato.</remarks>
        public const string HidrotecIdiomacontrato = "hidrotec_idiomacontrato";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Impresión Factura, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Desplegable para la procedencia de la factura que se va a imprimir.</remarks>
        public const string HidrotecImpresionfactura = "hidrotec_impresionfactura";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1000, Format: Text</summary>
        /// <remarks>Campo que contiene el objeto de configuraciones AAPP serializado en una cadena.</remarks>
        public const string HidrotecInformacionaapp = "hidrotec_informacionaapp";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_instalacion</summary>
        /// <remarks>Dirección de la instalación asociada al contrato.</remarks>
        public const string HidrotecInstalacionId = "hidrotec_instalacionid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica el nombre de Plantilla.</remarks>
        public const string HidrotecNombreplantilla = "hidrotec_nombreplantilla";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Número del contador asociado al contrato.</remarks>
        public const string HidrotecNumerocontador = "hidrotec_numerocontador";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 300, Format: Text</summary>
        public const string HidrotecOrigencarga = "hidrotec_origencarga";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Dirección a la que enviar la pagos para imprimirse.</remarks>
        public const string HidrotecPlandepagos = "hidrotec_plandepagos";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_plantillatarifa</summary>
        /// <remarks>Plantilla de Tarifa del contrato.</remarks>
        public const string HidrotecPlantillatarifaId = "hidrotec_plantillatarifaid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Contrato</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Una lista de valores de cadena separados por coma que representa los identificadores únicos de las fases de una instancia de flujo de proceso de negocio en el orden en que ocurren.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipodecliente</summary>
        /// <remarks>Identificador único de Subtipo de cliente asociado con Contrato.</remarks>
        public const string HidrotecSubtipodeclienteId = "hidrotec_subtipodeclienteid";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Desplegable para indicar si exsite cortable asociada al contrato.</remarks>
        public const string HidrotecSuministrocortable = "hidrotec_suministrocortable";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Teléfono fijo asociado al contrato.</remarks>
        public const string HidrotecTelefonofijo = "hidrotec_telefonofijo";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Teléfono móvil asociado el contrato.</remarks>
        public const string HidrotecTelefonomovil = "hidrotec_telefonomovil";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipodecliente</summary>
        /// <remarks>Identificador único de Tipo de cliente asociado con Contrato.</remarks>
        public const string HidrotecTipodeclienteId = "hidrotec_tipodeclienteid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Código del Titular</remarks>
        public const string HidrotecTitularId = "hidrotec_titularid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 150, Format: Text</summary>
        /// <remarks>Campo que indica la ubicación del contador.</remarks>
        public const string HidrotecUbicacioncontador = "hidrotec_ubicacioncontador";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecAccesibilidadcontador_OptionSet
        {
            Accesible = 1,
            Noaccesible = 2,
            Parcialmenteaccesible = 3
        }
        public enum HidrotecAfectadoaveria_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecAfectadocorteprogramado_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecAvisoaveria_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecCampaniadecorte_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecCanalalarmas_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum HidrotecCanaldocumentacion_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum HidrotecCanalnotificacionresolucion_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum HidrotecDomiciliacionbancaria_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecEstado_OptionSet
        {
            Altasolicitada = 1,
            Altaprovisional = 2,
            Altadefinitiva = 3,
            Altacortada = 4,
            Bajacortada = 5,
            Bajasolicitada = 6,
            Bajadefinitiva = 7,
            ContratoImportAdo = 8,
            Borrado = 9
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecExistedeuda_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecFaltainformacion_OptionSet
        {
            Nofalta = 1,
            FaltaLeve = 2,
            FaltaGrave = 3
        }
        public enum HidrotecGranconsumidor_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecIdiomacontrato_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum HidrotecImpresionfactura_OptionSet
        {
            Convencional = 1,
            FacturaElectronica = 2,
            InternetAAPP = 3,
            FacturaPDF = 4,
            FDevueltaporcorreo = 5
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum HidrotecPlandepagos_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecSuministrocortable_OptionSet
        {
            No = 0,
            Si = 1
        }
    }

    /// <summary>DisplayName: Contratos disponibles, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Muestra el rango de contratos disponibles para la copia/duplicado de contratos</remarks>
    public static class HidrotecContratosdisponibles
    {
        public const string EntityName = "hidrotec_contratosdisponibles";
        public const string EntityCollectionName = "hidrotec_contratosdisponibleses";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_contratosdisponiblesid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que establece el inicio del rango de contratos disponibles.</remarks>
        public const string HidrotecDesdecontrato = "hidrotec_desdecontrato";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Contratos disponibles</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo Sí/No que indica si existe contrato o no.</remarks>
        public const string HidrotecExistecontrato = "hidrotec_existecontrato";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: businessunit</summary>
        /// <remarks>Explotación a la que hace referencia este rango de contratos.</remarks>
        public const string HidrotecExplotacionId = "hidrotec_explotacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que establece el final del rango de contratos disponibles.</remarks>
        public const string HidrotecHastacontrato = "hidrotec_hastacontrato";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Contratos disponibles</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Conversión instalación tipo vía, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad para la conversión del tipo de vía en función de la instalación</remarks>
    public static class HidrotecConversionInstalaciontipova
    {
        public const string EntityName = "hidrotec_conversioninstalaciontipova";
        public const string EntityCollectionName = "hidrotec_conversioninstalaciontipovas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_conversioninstalaciontipovaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Conversión instalación tipo vía</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Identificador de la instalación en Diversa</remarks>
        public const string HidrotecIdinstalaciondiversa = "hidrotec_idinstalaciondiversa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Identificador de tipo vía en CRM</remarks>
        public const string HidrotecIdtipoviacrm = "hidrotec_idtipoviacrm";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Identificador del tipo vía en Diversa</remarks>
        public const string HidrotecIdtipoviadiversa = "hidrotec_idtipoviadiversa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Conversión instalación tipo vía</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Conversión municipio instalación pedanía, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Tabla de conversión de municipio, instalación y pedanía.</remarks>
    public static class HidrotecConversionMunicipioinstalacionpedania
    {
        public const string EntityName = "hidrotec_conversionmunicipioinstalacionpedania";
        public const string EntityCollectionName = "hidrotec_conversionmunicipioinstalacionpedanias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_conversionmunicipioinstalacionpedaniaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 40, Format: Text</summary>
        /// <remarks>Códico INE del municipio.</remarks>
        public const string HidrotecCodigoinemunicipio = "hidrotec_codigoinemunicipio";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Conversión municipio instalación pedanía</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificador de la instalación en Diversa.</remarks>
        public const string HidrotecIdinstalaciondiversa = "hidrotec_idinstalaciondiversa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificador de la pedanía en el CRM.</remarks>
        public const string HidrotecIdpedaniacrm = "hidrotec_idpedaniacrm";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificacor de la pedanía en Diversa.</remarks>
        public const string HidrotecIdpedaniadiversa = "hidrotec_idpedaniadiversa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Conversión municipio instalación pedanía</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Conversión municipio instalación vía, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Tabla de conversión de municipios, instalaciones y vías</remarks>
    public static class HidrotecConversionMunicipioinstalacionvia
    {
        public const string EntityName = "hidrotec_conversionmunicipioinstalacionvia";
        public const string EntityCollectionName = "hidrotec_conversionmunicipioinstalacionvias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_conversionmunicipioinstalacionviaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 40, Format: Text</summary>
        /// <remarks>Código INE del municipio.</remarks>
        public const string HidrotecCodigoinemunicipio = "hidrotec_codigoinemunicipio";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Conversión municipio instalación vía</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificador de la instalación en Diversa.</remarks>
        public const string HidrotecIdinstalaciondiversa = "hidrotec_idinstalaciondiversa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Código de la vía en CRM.</remarks>
        public const string HidrotecIdviacrm = "hidrotec_idviacrm";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificador de la vía en Diversa.</remarks>
        public const string HidrotecIdviadiversa = "hidrotec_idviadiversa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Conversión municipio instalación vía</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Conversión tipo subtipo cliente instalación, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Tabla de conversión de tipo, subtipo de cliente para cada instalación (Diversa)</remarks>
    public static class HidrotecConversionTiposubtipoclienteinstalacion
    {
        public const string EntityName = "hidrotec_conversiontiposubtipoclienteinstalacion";
        public const string EntityCollectionName = "hidrotec_conversiontiposubtipoclienteinstalacions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_conversiontiposubtipoclienteinstalacionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Conversión tipo subtipo cliente instalación</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificador de la instalación en Diversa.</remarks>
        public const string HidrotecIdinstalaciondiversa = "hidrotec_idinstalaciondiversa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificador que tiene el subtipo en CRM.</remarks>
        public const string HidrotecIdsubtipocrm = "hidrotec_idsubtipocrm";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificador que tiene el subtipo en Diversa.</remarks>
        public const string HidrotecIdsubtipodiversa = "hidrotec_idsubtipodiversa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Identificador que tiene el tipo en CRM.</remarks>
        public const string HidrotecIdtipocrm = "hidrotec_idtipocrm";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Identificador que tiene el tipo en Diversa.</remarks>
        public const string HidrotecIdtipodiversa = "hidrotec_idtipodiversa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Conversión tipo subtipo cliente instalación</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Correo electrónico, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Actividad entregada con protocolos de correo electrónico.</remarks>
    public static class EMail
    {
        public const string EntityName = "email";
        public const string EntityCollectionName = "emails";
    }

    /// <summary>DisplayName: Cruce Vía, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad Cruce vía.</remarks>
    public static class HidrotecCrucevia
    {
        public const string EntityName = "hidrotec_crucevia";
        public const string EntityCollectionName = "hidrotec_crucevias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_cruceviaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Número que representa la distancia entre las vías.</remarks>
        public const string HidrotecDistanciaentrevias = "hidrotec_distanciaentrevias";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Cruce Vía</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 15, Format: Text</summary>
        public const string HidrotecIdcrucevia = "hidrotec_idcrucevia";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Cruce Vía</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Campo de búsqueda a la vía de cruce.</remarks>
        public const string HidrotecViacruceId = "hidrotec_viacruceid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Identificador de la vía principal.</remarks>
        public const string HidrotecViaprincipalId = "hidrotec_viaprincipalid";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Dirección correo electrónico, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad  de dirección de correo electrónico.</remarks>
    public static class HidrotecDireccioncorreoelectronico
    {
        public const string EntityName = "hidrotec_direccioncorreoelectronico";
        public const string EntityCollectionName = "hidrotec_direccioncorreoelectronicos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_direccioncorreoelectronicoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Email</summary>
        /// <remarks>La dirección de correo electrónico principal para la entidad.</remarks>
        public const string EMailAddress = "emailaddress";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Dirección correo electrónico</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Dirección correo electrónico</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Dirección de suministro, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad de Dirección de Suministro.</remarks>
    public static class HidrotecDirecciondesuministro
    {
        public const string EntityName = "hidrotec_direcciondesuministro";
        public const string EntityCollectionName = "hidrotec_direcciondesuministros";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_direcciondesuministroid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        /// <remarks>Campo de búsqueda de barrio de la dirección.</remarks>
        public const string HidrotecBarrioId = "hidrotec_barrioid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica el Bloque al que pertenece la vivienda de la dirección.</remarks>
        public const string HidrotecBloque = "hidrotec_bloque";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Código postal de la dirección.</remarks>
        public const string HidrotecCodigoPostal = "hidrotec_codigopostal";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecCodigopuntosuministro = "hidrotec_codigopuntosuministro";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Composición de la dirección formado por los campos que la forman.</remarks>
        public const string HidrotecDireccioncompuesta = "hidrotec_direccioncompuesta";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Edificio de la dirección.</remarks>
        public const string HidrotecEdificio = "hidrotec_edificio";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Escalera de la dirección.</remarks>
        public const string HidrotecEscalera = "hidrotec_escalera";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Dirección de suministro</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador de la dirección formado por el código de la instalación y el código del punto de suministro.</remarks>
        public const string HidrotecIddireccionsuministro = "hidrotec_iddireccionsuministro";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 150, Format: Text</summary>
        /// <remarks>Campo usado para la migración que concatena usando guiones el código de instalación, el código de contrato y el código dirección.</remarks>
        public const string HidrotecIdmigracion = "hidrotec_idmigracion";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_instalacion</summary>
        /// <remarks>Identificador de la instalación.</remarks>
        public const string HidrotecInstalacionId = "hidrotec_instalacionid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Identificador del municipio de la dirección.</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Número de la vivienda en la dirección.</remarks>
        public const string HidrotecNumero = "hidrotec_numero";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Otra información sobre la dirección.</remarks>
        public const string HidrotecOtros = "hidrotec_otros";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo de búsqueda a País.</remarks>
        public const string HidrotecPaisId = "hidrotec_paisid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Campo de búsqueda a Pedanía</remarks>
        public const string HidrotecPedaniaId = "hidrotec_pedaniaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Piso de la dirección.</remarks>
        public const string HidrotecPiso = "hidrotec_piso";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Campo de búsqueda a Provincia</remarks>
        public const string HidrotecProvinciaId = "hidrotec_provinciaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Puerta de la dirección</remarks>
        public const string HidrotecPuerta = "hidrotec_puerta";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Dirección de suministro</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Referencia catastral de la dirección.</remarks>
        public const string HidrotecReferenciacatastral = "hidrotec_referenciacatastral";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo punto suministro, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Tipo de punto de suministro como son: Agua, Saneamiento, Depuración o sólo basura.</remarks>
        public const string HidrotecTipopuntosuministro = "hidrotec_tipopuntosuministro";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Campo de búsqueda a Vía</remarks>
        public const string HidrotecViaId = "hidrotec_viaid";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipopuntosuministro_OptionSet
        {
            Agua = 1,
            Saneamiento = 2,
            Depuracion = 3,
            Solobasura = 4
        }
    }

    /// <summary>DisplayName: Directorio oficina, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa el directorio de la oficina.</remarks>
    public static class HidrotecDirectoriooficina
    {
        public const string EntityName = "hidrotec_directoriooficina";
        public const string EntityCollectionName = "hidrotec_directoriooficinas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_directoriooficinaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Teléfono asociado a la dirección de la oficina.</remarks>
        public const string HidrotecContactOId = "hidrotec_contactoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Dirección de correo electrónico.</remarks>
        public const string HidrotecCorreoelectronico = "hidrotec_correoelectronico";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Directorio oficina</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_oficina</summary>
        /// <remarks>Campo de búsqueda a la oficina en el CRM.</remarks>
        public const string HidrotecOficinaId = "hidrotec_oficinaid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Directorio oficina</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_roloficina</summary>
        /// <remarks>Campo de búsqueda la rol de la oficina.</remarks>
        public const string HidrotecRoloficinaId = "hidrotec_roloficinaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Teléfono asociado a la dirección de la oficina.</remarks>
        public const string HidrotecTelefono = "hidrotec_telefono";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Documento, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de documentos.</remarks>
    public static class HidrotecDocumento
    {
        public const string EntityName = "hidrotec_documento";
        public const string EntityCollectionName = "hidrotec_documentos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_documentoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Documento</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estado Inicial, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Estado inicial del documento como son: Pendiente de Recibir y Pendiente de enviar a contacto,</remarks>
        public const string HidrotecEstadoinicial = "hidrotec_estadoinicial";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador del documento en el CRM.</remarks>
        public const string HidrotecIddocumento = "hidrotec_iddocumento";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Documento</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecEstadoinicial_OptionSet
        {
            PdteRecibir = 0,
            PdteEnviaraContactO = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Empresa gestora, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Empresa gestora de una explotación.</remarks>
    public static class HidrotecEmpresagestora
    {
        public const string EntityName = "hidrotec_empresagestora";
        public const string EntityCollectionName = "hidrotec_empresagestoras";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_empresagestoraid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        /// <remarks>Campo de búsqueda al barrio de la dirección.</remarks>
        public const string HidrotecBarrioId = "hidrotec_barrioid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Bloque de la dirección.</remarks>
        public const string HidrotecBloque = "hidrotec_bloque";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1000, Format: Text</summary>
        /// <remarks>Código CTI de la dirección.</remarks>
        public const string HidrotecCodigocti = "hidrotec_codigocti";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 5, Format: Text</summary>
        /// <remarks>Código postal de la dirección de la empresa gestora.</remarks>
        public const string HidrotecCodigoPostal = "hidrotec_codigopostal";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1000, Format: Text</summary>
        /// <remarks>Campo descripción de la empresa gestora</remarks>
        public const string HidrotecDescripcion = "hidrotec_descripcion";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Dirección social de la empresa compuesta</remarks>
        public const string HidrotecDireccionsocialcompuesta = "hidrotec_direccionsocialcompuesta";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Edificio de la dirección.</remarks>
        public const string HidrotecEdificio = "hidrotec_edificio";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        public const string EntityImageId = "entityimageid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo que indica si la empresa es principal de la instalación o no. Se usa para los procesos de migración.</remarks>
        public const string HidrotecEsempresaprincipal = "hidrotec_esempresaprincipal";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Escalera de la dirección.</remarks>
        public const string HidrotecEscalera = "hidrotec_escalera";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Empresa gestora</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Campo que concatena el id de instalación en diversa y el código de empresa en el origen.</remarks>
        public const string HidrotecIdempresa = "hidrotec_idempresa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Código de identificación de la instalación de Diversa.</remarks>
        public const string HidrotecIdinstalaciondiversa = "hidrotec_idinstalaciondiversa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Campo de búsqueda a Municipio</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 15, Format: Text</summary>
        /// <remarks>Número de identificación fiscal de la empresa</remarks>
        public const string HidrotecNifempresa = "hidrotec_nifempresa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 25, Format: Text</summary>
        public const string HidrotecNumero = "hidrotec_numero";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Número TPV de la dirección</remarks>
        public const string HidrotecNumerotpv = "hidrotec_numerotpv";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Otros datos de la dirección.</remarks>
        public const string HidrotecOtros = "hidrotec_otros";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo de búsqueda a País.</remarks>
        public const string HidrotecPaisId = "hidrotec_paisid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Campo de búsqueda a Pedanía</remarks>
        public const string HidrotecPedaniaId = "hidrotec_pedaniaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Piso de la dirección.</remarks>
        public const string HidrotecPiso = "hidrotec_piso";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Campo de búsqueda a Provincia</remarks>
        public const string HidrotecProvinciaId = "hidrotec_provinciaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Puerta de la dirección.</remarks>
        public const string HidrotecPuerta = "hidrotec_puerta";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Empresa gestora</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 1048576</summary>
        /// <remarks>Teléfonos de atención del CAC asociados a la empresa.</remarks>
        public const string HidrotecTelefonosdeatencioncac = "hidrotec_telefonosdeatencioncac";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Campo de búsqueda a Vía</remarks>
        public const string HidrotecViaId = "hidrotec_viaid";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Equipo, OwnershipType: BusinessOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Grupo de usuarios del sistema que colaboran habitualmente. Los equipos pueden usarse para simplificar la tarea de compartir registros y para proporcionar a los miembros del equipo un acceso común a los datos de la organización cuando estos miembros pertenecen a unidades de negocio diferentes.</remarks>
    public static class Team
    {
        public const string EntityName = "team";
        public const string EntityCollectionName = "teams";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del equipo.</remarks>
        public const string PrimaryKey = "teamid";
        /// <summary>Type: String, RequiredLevel: SystemRequired, MaxLength: 160, Format: Text</summary>
        /// <remarks>Nombre del equipo.</remarks>
        public const string PrimaryName = "name";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario responsable del equipo.</remarks>
        public const string AdministratorId = "administratorid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el equipo.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el equipo.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: queue</summary>
        /// <remarks>Identificador único de la cola predeterminada del equipo.</remarks>
        public const string QueueId = "queueid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Dirección de correo electrónico del equipo.</remarks>
        public const string EMailAddress = "emailaddress";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción del equipo.</remarks>
        public const string Description = "description";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al equipo.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Información que especifica si el equipo es un equipo de unidad de negocio predeterminado.</remarks>
        public const string Isdefault = "isdefault";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Seleccione si el equipo será administrado por el sistema.</remarks>
        public const string SystemManaged = "systemmanaged";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Muestra el identificador de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el equipo.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el equipo por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_contrato,knowledgearticle,opportunity</summary>
        /// <remarks>Elija el registro relacionado con el equipo.</remarks>
        public const string RegardingobjectId = "regardingobjectid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: teamtemplate</summary>
        /// <remarks>Muestra la plantilla de equipo que está asociada con el equipo.</remarks>
        public const string TeamtemplateId = "teamtemplateid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el equipo por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el equipo por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 160, Format: PhoneticGuide</summary>
        /// <remarks>Pronunciación del nombre completo del equipo, escrito en caracteres fonéticos hiragana o katakana.</remarks>
        public const string YomiName = "yominame";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión del equipo.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la organización asociada con el equipo.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Muestra el identificador del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        /// <remarks>Tipo del registro asociado para equipo - utilizado solo para equipos de acceso administrados por el sistema.</remarks>
        public const string RegardingobjectTypeCode = "regardingobjecttypecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tipo de cambio de la divisa asociada al equipo con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipo de equipo, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Seleccione el tipo de equipo.</remarks>
        public const string TeamType = "teamtype";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio con la que está asociado el equipo.</remarks>
        public const string BusinessUnitId = "businessunitid";
        public enum RegardingobjectTypeCode_OptionSet
        {
        }
        public enum TeamType_OptionSet
        {
            Propietario = 0,
            Acceso = 1
        }
    }

    /// <summary>DisplayName: Estado expediente, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de estados de expediente</remarks>
    public static class HidrotecEstadoexpediente
    {
        public const string EntityName = "hidrotec_estadoexpediente";
        public const string EntityCollectionName = "hidrotec_estadoexpedientes";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_estadoexpedienteid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Estado expediente</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Identificador del estado del expediente.</remarks>
        public const string HidrotecIdestado = "hidrotec_idestado";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Estado expediente</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Expediente, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Expediente asociado con un contrato y/o contacto.</remarks>
    public static class Incident
    {
        public const string EntityName = "incident";
        public const string EntityCollectionName = "incidents";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del caso.</remarks>
        public const string PrimaryKey = "incidentid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Escriba un tema o un nombre descriptivo, como la solicitud, el problema o el nombre de la compañía, para identificar el caso en vistas de Microsoft Dynamics CRM.</remarks>
        public const string PrimaryName = "title";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Accesibilidad contador, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica la accesibilidad que hay para llegar al contador: Accesible, No accesible y Parcialmente accesible.</remarks>
        public const string HidrotecAccesibilidadcontador = "hidrotec_accesibilidadcontador";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Acceso llave, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica si el acceso a la llave es Accesible o No accesible.</remarks>
        public const string HidrotecAccesollave = "hidrotec_accesollave";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_parametrizacionactividades</summary>
        /// <remarks>Campo lookup de las Actividades de Tercero de Organismo Oficial.</remarks>
        public const string HidrotecActividaddeterceroorganismooficialId = "hidrotec_actividaddeterceroorganismooficialid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Este atributo se utiliza para procesos de negocio de servicio de ejemplo.</remarks>
        public const string Activitiescomplete = "activitiescomplete";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo Sí/No que determina si el expediente necesita o no ser aprobado.</remarks>
        public const string HidrotecAprobado = "hidrotec_aprobado";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: kbarticle</summary>
        /// <remarks>Elija el artículo que contiene información adicional o una resolución del caso, para tenerlo como referencia durante la investigación o seguimiento con el cliente.</remarks>
        public const string KbarticleId = "kbarticleid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        /// <remarks>Campo que indica a qué equipo está asignado el expediente.</remarks>
        public const string HidrotecAsignadoalequipo = "hidrotec_asignadoalequipo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Campo que indica a que usuario está asignado el expediente.</remarks>
        public const string HidrotecAsignadoalusuario = "hidrotec_asignadoalusuario";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Asume deuda, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Campo que recoge el conjunto de opciones de si se paga, se remesa, se compromete o no a pagar la deuda.</remarks>
        public const string HidrotecAsumedeuda = "hidrotec_asumedeuda";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: subject</summary>
        /// <remarks>Elija el tema del caso, como solicitud de catálogo o queja por un producto, para que los directores de servicio al cliente puedan identificar solicitudes frecuentes o áreas problemáticas. Los administradores pueden configurar temas en Administración de empresas del área de Configuración.</remarks>
        public const string SubjectId = "subjectid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo que indica si la autolectura es necesaria para el proceso</remarks>
        public const string HidrotecAutolecturanecesaria = "hidrotec_autolecturanecesaria";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra quién creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra quién creó el registro en nombre de otro usuario.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_banco</summary>
        /// <remarks>Banco de la cuenta bancaria.</remarks>
        public const string HidrotecBancoId = "hidrotec_bancoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        /// <remarks>Campo de búsqueda de barrio de la dirección</remarks>
        public const string HidrotecBarrioId = "hidrotec_barrioid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        /// <remarks>Campo de búsqueda que indica el barrio auxiliar.</remarks>
        public const string HidrotecBarrioauxiliarId = "hidrotec_barrioauxiliarid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        /// <remarks>Campo de búsqueda que indica el barrio en una dirección de suministro</remarks>
        public const string HidrotecBarriosuministroId = "hidrotec_barriosuministroid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Bloque de la dirección.</remarks>
        public const string HidrotecBloque = "hidrotec_bloque";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Bloque auxiliar de la dirección.</remarks>
        public const string HidrotecBloqueauxiliar = "hidrotec_bloqueauxiliar";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Bloque de la dirección de suministro</remarks>
        public const string HidrotecBloquesuministro = "hidrotec_bloquesuministro";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Conjunto de opciones que indica si se puede realizar la búsqueda automática de contratos o no.</remarks>
        public const string HidrotecBusquedaautomaticacontratos = "hidrotec_busquedaautomaticacontratos";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Calibre contador, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecCalibrecontad = "hidrotec_calibrecontad";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de texto que indica el Calibre de Contador.</remarks>
        public const string HidrotecCalibrecontador = "hidrotec_calibrecontador";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: 4</summary>
        /// <remarks>Canal por el que irán las alarmas si hay que informar al contacto.</remarks>
        public const string HidrotecCanalalarmas = "hidrotec_canalalarmas";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones que indica el canal de entrada del expediente.</remarks>
        public const string HidrotecCanaldeentrada = "hidrotec_canaldeentrada";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: 4</summary>
        /// <remarks>Canal por el cual el contacto deberá hacer llegar la documentación.</remarks>
        public const string HidrotecCanaldocumentacion = "hidrotec_canaldocumentacion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: 4</summary>
        /// <remarks>Conjunto de opciones en el que el contacto pide que se le envíe la comunicación.</remarks>
        public const string HidrotecCanalnotificacionresolucion = "hidrotec_canalnotificacionresolucion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Seleccione un caso existente para el cliente que se ha rellenado. Para uso interno.</remarks>
        public const string Existingcase = "existingcase";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Elija el caso principal en el que se combinó el caso actual.</remarks>
        public const string MasterId = "masterid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Elija el caso principal para un caso.</remarks>
        public const string ParentCaseId = "parentcaseid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_causa</summary>
        /// <remarks>Indica la causa cuando se resuelve el expediente y la resolución no es positiva.</remarks>
        public const string HidrotecCausaId = "hidrotec_causaid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Campo de texto en el que el gestor describe la causa por la que no se ha aprobado la resolución del expediente.</remarks>
        public const string HidrotecCausanoaprobado = "hidrotec_causanoaprobado";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Campo de texto que indica que la causa no se ha revisado.</remarks>
        public const string HidrotecCausanorevisado = "hidrotec_causanorevisado";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Clase de acometida, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica si la clase de acometida es individual o colectiva.</remarks>
        public const string HidrotecClaseacometida = "hidrotec_claseacometida";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si el representante del servicio al cliente se ha puesto en contacto con el cliente o no.</remarks>
        public const string CustomerContactEd = "customercontacted";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el cliente es tramitador.</remarks>
        public const string HidrotecClientetramitador = "hidrotec_clientetramitador";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código de Autorización de la TPV.</remarks>
        public const string HidrotecCodigoautorizaciontpv = "hidrotec_codigoautorizaciontpv";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 4, Format: Text</summary>
        /// <remarks>Código del banco.</remarks>
        public const string HidrotecCodigobanco = "hidrotec_codigobanco";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo que indica el Código Postal.</remarks>
        public const string HidrotecCodigoPostal = "hidrotec_codigopostal";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Código Postal de la dirección auxiliar.</remarks>
        public const string HidrotecCodigoPostalAuxiliar = "hidrotec_codigopostalauxiliar";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Código postal de la dirección de suministro</remarks>
        public const string HidrotecCodigoPostalSuministro = "hidrotec_codigopostalsuministro";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecCodigopuntosuministro = "hidrotec_codigopuntosuministro";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 4, Format: Text</summary>
        /// <remarks>Código de Sucursal.</remarks>
        public const string HidrotecCodigosucursal = "hidrotec_codigosucursal";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Este atributo se utiliza para procesos de negocio de servicio de ejemplo.</remarks>
        public const string CheckeMail = "checkemail";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Se rellena automáticamente con la comunicación que tenga la resolución.
        /// Antes de ello se puede modificar hasta que se establezca la resolución y el expediente transite a estado Comunicación res.</remarks>
        public const string HidrotecComunicacionalContactO = "hidrotec_comunicacionalcontacto";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Campo de búsqueda si la comunicación se realiza al final.</remarks>
        public const string HidrotecComunicacionfinalId = "hidrotec_comunicacionfinalid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Campo de búsqueda que indica la comunicación pendiente.</remarks>
        public const string HidrotecComunicacionpendienteId = "hidrotec_comunicacionpendienteid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la comunicación es automática o no.</remarks>
        public const string HidrotecComunicacionesautomaticas = "hidrotec_comunicacionesautomaticas";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si se ha comunicado la resolución o no.</remarks>
        public const string HidrotecComunicadaresolucion = "hidrotec_comunicadaresolucion";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_configuracionorganizativa</summary>
        /// <remarks>Campo de búsqueda de la Configuración Organizativa.</remarks>
        public const string HidrotecConfiguracionorganizativaId = "hidrotec_configuracionorganizativaid";
        /// <summary>Type: Customer, RequiredLevel: SystemRequired, Targets: account,contact</summary>
        /// <remarks>Seleccione la cuenta o el contacto del cliente para proporcionar un vínculo rápido a detalles adicionales del cliente, como información de cuenta, actividades y oportunidades.</remarks>
        public const string CustomerId = "customerid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Seleccione un contacto principal para este caso.</remarks>
        public const string PrimaryContactId = "primarycontactid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Contacto cobro, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones que si indica si el contacto de cobro no domicilia, es igual que el solicitante, igual que el titular, igual que correspondencia u otro.</remarks>
        public const string HidrotecContactOcobro = "hidrotec_contactocobro";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Campo de búsqueda a contacto para seleccionar quien tiene el rol de cobro.</remarks>
        public const string HidrotecContactOcobroId = "hidrotec_contactocobroid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Contacto correspondencia, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones que indica si el contacto de correspondencia es igual que el solicitante, igual que el titular u otro.</remarks>
        public const string HidrotecContactOcorrespondencia = "hidrotec_contactocorrespondencia";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Búsqueda a contactos para asignar el rol de correspondencia.</remarks>
        public const string HidrotecContactOcorrespondenciaId = "hidrotec_contactocorrespondenciaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Elija un contacto de cliente adicional que también le pueda ayudar a resolver el caso.</remarks>
        public const string ResponsibleContactId = "responsiblecontactid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Campo de búsqueda a contacto.</remarks>
        public const string HidrotecContactOtitularId = "hidrotec_contactotitularid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Campo de búsqueda a contacto.</remarks>
        public const string HidrotecContactOtitularactualId = "hidrotec_contactotitularactualid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_agrupacionderolesdecontrato</summary>
        /// <remarks>Campo de búsqueda a roles de contrato.</remarks>
        public const string HidrotecContactOsdelcontratoId = "hidrotec_contactosdelcontratoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contract</summary>
        /// <remarks>Elija el contrato de servicio bajo el cual debe registrarse el caso para asegurarse de que el cliente cumple los requisitos para servicios de soporte.</remarks>
        public const string ContractId = "contractid";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_contrato</summary>
        /// <remarks>Campo de búsqueda a contrato.</remarks>
        public const string HidrotecContratoId = "hidrotec_contratoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_agrupacionderolesdecontrato</summary>
        /// <remarks>Campo de búsqueda a roles de contrato.</remarks>
        public const string HidrotecContratosdelContactOId = "hidrotec_contratosdelcontactoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Campo para infornar del correo electrónico.</remarks>
        public const string HidrotecCorreoelectronico = "hidrotec_correoelectronico";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica la creación automática.</remarks>
        public const string HidrotecCreacionautomatica = "hidrotec_creacionautomatica";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Este atributo se utiliza para procesos de negocio de servicio de ejemplo.</remarks>
        public const string FollowuptaskCreated = "followuptaskcreated";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: externalparty</summary>
        /// <remarks>Muestra la parte externa que creó el registro.</remarks>
        public const string CreatedByExternalParty = "createdbyexternalparty";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 160, Format: Text</summary>
        public const string CustomerIdName = "customeridname";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 450, Format: Text</summary>
        public const string CustomerIdyomiName = "customeridyominame";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: entitlement</summary>
        /// <remarks>Elija el derecho aplicable para este caso.</remarks>
        public const string EntitlementId = "entitlementid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Escriba información adicional que describa el caso para ayudar al equipo de servicio a lograr una resolución.</remarks>
        public const string Description = "description";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto para informar sobre el diámetro.</remarks>
        public const string HidrotecDiametro = "hidrotec_diametro";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2, Format: Text</summary>
        /// <remarks>Dígitos de control CCC de la cuenta bancaria.</remarks>
        public const string HidrotecDigitocontrolccc = "hidrotec_digitocontrolccc";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2, Format: Text</summary>
        /// <remarks>Dígito de Control del IBAN bancario.</remarks>
        public const string HidrotecDigitocontroliban = "hidrotec_digitocontroliban";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Composición de los campos que forman la dirección.</remarks>
        public const string HidrotecDireccioncompuesta = "hidrotec_direccioncompuesta";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Composición de los campos que forman la dirección auxiliar.</remarks>
        public const string HidrotecDireccioncompuestaauxiliar = "hidrotec_direccioncompuestaauxiliar";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Dirección de la oficina a la que pertenece el contrato.</remarks>
        public const string HidrotecDireccionoficina = "hidrotec_direccionoficina";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_direcciondesuministro</summary>
        /// <remarks>Dirección de suministro del contrato.</remarks>
        public const string HidrotecDireccionsuministroId = "hidrotec_direccionsuministroid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Elija la divisa local del registro para asegurarse de que en los presupuestos se utiliza la divisa correcta.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el cliente domicilia o no.</remarks>
        public const string HidrotecDomiciliado = "hidrotec_domiciliado";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración total del expediente.</remarks>
        public const string HidrotecDuraciontotal = "hidrotec_duraciontotal";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo edificio en la dirección.</remarks>
        public const string HidrotecEdificio = "hidrotec_edificio";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo edificio en la dirección auxiliar.</remarks>
        public const string HidrotecEdificioauxiliar = "hidrotec_edificioauxiliar";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Edificio de la dirección de suministro</remarks>
        public const string HidrotecEdificiosuministro = "hidrotec_edificiosuministro";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que induca si la ejecución se ha realizado o no.</remarks>
        public const string HidrotecEjecucionrealizada = "hidrotec_ejecucionrealizada";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Email</summary>
        public const string HidrotecEMailCobro = "hidrotec_email_cobro";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Email</summary>
        public const string HidrotecEMailCorrespondencia = "hidrotec_email_correspondencia";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Email</summary>
        public const string HidrotecEMailTitular = "hidrotec_email_titular";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si se emite factura de aniticipo o no.</remarks>
        public const string HidrotecEmisionfacturaanticipoacometida = "hidrotec_emisionfacturaanticipoacometida";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si se emite factura de presupuesto de acometida o no.</remarks>
        public const string HidrotecEmisionfacturapresupuestoacometida = "hidrotec_emisionfacturapresupuestoacometida";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Solo para uso del sistema.</remarks>
        public const string Isdecrementing = "isdecrementing";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Indica si el incidente se ha enrutado a la cola o no.</remarks>
        public const string Routecase = "routecase";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de dirección que hay que informar con la escalera.</remarks>
        public const string HidrotecEscalera = "hidrotec_escalera";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de la dirección auxiliar en el que hay que infomar con la escalera.</remarks>
        public const string HidrotecEscaleraauxiliar = "hidrotec_escaleraauxiliar";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 25, Format: Text</summary>
        /// <remarks>Escalera de la dirección de suministro</remarks>
        public const string HidrotecEscalerasuministro = "hidrotec_escalerasuministro";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Muestra si el caso está activo, resuelto o cancelado. Los casos resueltos y cancelados son de solo lectura y no se pueden editar si no se reactivan.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estado cambio titular, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica el tipo de expdiente en un proceso de Cambio de Titular Baja/Alta</remarks>
        public const string HidrotecEstadocambiotitular = "hidrotec_estadocambiotitular";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estado contrato diversa, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica los estados del contrato en Diversa.</remarks>
        public const string HidrotecEstadocontrato = "hidrotec_estadocontrato";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estado de SLA de la primera respuesta, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Muestra el estado de la hora de respuesta inicial para el caso según las condiciones del SLA.</remarks>
        public const string FirstResponsesLastatus = "firstresponseslastatus";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estado del corte/avería, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica el estado del corte o avería.</remarks>
        public const string HidrotecEstadocorte = "hidrotec_estadocorte";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_estadoexpediente</summary>
        /// <remarks>Lookup que indica el estado del expediente.</remarks>
        public const string HidrotecEstadoexpedienteId = "hidrotec_estadoexpedienteid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Lookup que sirve para buscar los expedientes relacionados.</remarks>
        public const string HidrotecExpedienterelacionadoId = "hidrotec_expedienterelacionadoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Lookup de las explotaciones.</remarks>
        public const string HidrotecExplotacionId = "hidrotec_explotacionid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Muestra el identificador de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Fase de servicio, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione la fase, en el proceso de resolución de caso, en la que se encuentra el caso.</remarks>
        public const string Servicestage = "servicestage";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Fase del caso, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione la fase actual del proceso de servicios del caso para ayudar a los integrantes del equipo de servicio cuando revisen o transfieran un caso.</remarks>
        public const string IncidentstageCode = "incidentstagecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Campo que indica si la comunicación es por FAX en el expediente.</remarks>
        public const string HidrotecFax = "hidrotec_fax";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecFaxCobro = "hidrotec_fax_cobro";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecFaxCorrespondencia = "hidrotec_fax_correspondencia";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecFaxTitular = "hidrotec_fax_titular";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Campo de fecha para informar la autolectura.</remarks>
        public const string HidrotecFechaautolectura = "hidrotec_fechaautolectura";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Campo de fecha que indica el cierre del expediente.</remarks>
        public const string HidrotecFechadecierre = "hidrotec_fechadecierre";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Muestra la fecha y la hora en que se creó el registro. La fecha y la hora se muestran en la zona horaria seleccionada en las opciones de Microsoft Dynamics CRM.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Muestra la fecha y la hora en que se actualizó el registro por última vez. La fecha y la hora se muestran en la zona horaria seleccionada en las opciones de Microsoft Dynamics CRM.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha límite de domiciliación</remarks>
        public const string HidrotecFechalimite = "hidrotec_fechalimite";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Campo que indica la fecha por la que se debe presenter el motivo de la subrogación.</remarks>
        public const string HidrotecFechamotivo = "hidrotec_fechamotivo";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        public const string HidrotecFecharegistradoendiversa = "hidrotec_fecharegistradoendiversa";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        public const string HidrotecFecharegistrogesred = "hidrotec_fecharegistrogesred";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Campo fecha que indica la fecha de subrogación</remarks>
        public const string HidrotecFechasubrogacion = "hidrotec_fechasubrogacion";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Campo que indica la fecha de la última comunicación.</remarks>
        public const string HidrotecFechaultimacomunicacion = "hidrotec_fechaultimacomunicacion";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Campo que indica la fecha de la aprobación.</remarks>
        public const string HidrotecFechayhoraaprobacion = "hidrotec_fechayhoraaprobacion";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Campo que indica la fecha y la hora del envío a revisión.</remarks>
        public const string HidrotecFechayhoraenvioarevision = "hidrotec_fechayhoraenvioarevision";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Campo que indica la fecha y la hora del envío de la aprobación.</remarks>
        public const string HidrotecFechayhoraenvioaprobacion = "hidrotec_fechayhoraenvioaprobacion";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>campo que indica la fecha y la hora de la revisión.</remarks>
        public const string HidrotecFechayhorarevision = "hidrotec_fechayhorarevision";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Gravedad, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Seleccione la gravedad de este caso para indicar el impacto del incidente en el negocio del cliente.</remarks>
        public const string SeverityCode = "severitycode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 5000</summary>
        /// <remarks>Campo que recoge el historial de integraciones.</remarks>
        public const string HidrotecHistorialintegraciones = "hidrotec_historialintegraciones";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 5000</summary>
        /// <remarks>Campo que recoge el historial de transiciones.</remarks>
        public const string HidrotecHistorialtransiciones = "hidrotec_historialtransiciones";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Se refiere a la hora del día laborable a partir de la que dejará de contabilizar el cálculo de plazos en el caso de que estos sean medidos en horas</remarks>
        public const string HidrotecHorafinatencion = "hidrotec_horafinatencion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Se refiere a la hora del día laborable a partir de la que contabilizará el cálculo de plazos en el caso de que estos sean medidos en horas</remarks>
        public const string HidrotecHorainicioatencion = "hidrotec_horainicioatencion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica el IBAN bancario.</remarks>
        public const string HidrotecIban = "hidrotec_iban";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica el ID del contador.</remarks>
        public const string HidrotecIdcontador = "hidrotec_idcontador";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo que informa del Código de identificación del campo en Diversa.</remarks>
        public const string HidrotecIdexpedientediv = "hidrotec_idexpedientediv";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string EntityImageId = "entityimageid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica el idioma del contacto: Castellano, Catalán, Euskera, Gallego, Inglés, Italiano, Portugués y Checo.</remarks>
        public const string HidrotecIdiomacontrato = "hidrotec_idiomacontrato";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_importeapagar</summary>
        /// <remarks>Valor de Importe a pagar en divisa base.</remarks>
        public const string HidrotecImportEapagarBase = "hidrotec_importeapagar_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Campo que indica el importe a pagar.</remarks>
        public const string HidrotecImportEapagar = "hidrotec_importeapagar";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: 0, MaxValue: 922337203685477, Precision: 2</summary>
        /// <remarks>Importe máximo a domiciliar</remarks>
        public const string HidrotecImportEmaximo = "hidrotec_importemaximo";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 2, CalculationOf: hidrotec_importemaximo</summary>
        /// <remarks>Valor de importemaximo en divisa base.</remarks>
        public const string HidrotecImportEmaximoBase = "hidrotec_importemaximo_base";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Impresión Factura, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica la impresión factura: Convencional, Factura electrónica, Internet AA.PP, Factura PDF, Devuelta por correo.</remarks>
        public const string HidrotecImpresionfactura = "hidrotec_impresionfactura";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        public const string HidrotecIndiceautolectura = "hidrotec_indiceautolectura";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 6000</summary>
        /// <remarks>Campo que indica la información de las actividades de tercero.</remarks>
        public const string HidrotecInfoactividadestercero = "hidrotec_infoactividadestercero";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 250, Format: Text</summary>
        /// <remarks>Campo para informar de algún tipo de información adicional.</remarks>
        public const string HidrotecInformacionadicional = "hidrotec_informacionadicional";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si hay inspección previa de acometida o no.</remarks>
        public const string HidrotecInspeccionpreviaaltacometida = "hidrotec_inspeccionpreviaaltacometida";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si el caso se ha remitido a una instancia superior.</remarks>
        public const string Isescalated = "isescalated";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: IVA cliente especial, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones que indica si el IVA es de un cliente especial: No aplica, Operaciones de inversión sujeto pasivo o cliente promotor.</remarks>
        public const string HidrotecIvaclienteespecial = "hidrotec_ivaclienteespecial";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 50000</summary>
        /// <remarks>Campo que muestra el Json de los documentos de pago</remarks>
        public const string HidrotecJsondocumentosdepago = "hidrotec_jsondocumentosdepago";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 50000</summary>
        /// <remarks>Campo que muestra el Json de las facturas pagadas</remarks>
        public const string HidrotecJsonfacturaspagadas = "hidrotec_jsonfacturaspagadas";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contractdetail</summary>
        /// <remarks>Elija la línea de contrato bajo la cual debe registrarse el caso para asegurarse de que se cobra correctamente al cliente.</remarks>
        public const string ContractdetailId = "contractdetailid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra quién actualizó el registro en nombre de otro usuario por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra quién actualizó el registro por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: externalparty</summary>
        /// <remarks>Muestra la parte externa que modificó el registro.</remarks>
        public const string ModifiedByExternalParty = "modifiedbyexternalparty";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_motivo</summary>
        /// <remarks>Campo para informar con el motivo.</remarks>
        public const string HidrotecMotivoId = "hidrotec_motivoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Campo de búsqueda a Municipio</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_municipio</summary>
        /// <remarks>Campo de búsqueda del municipio principal ID.</remarks>
        public const string HidrotecMunicipioprincipalId = "hidrotec_municipioprincipalid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Campo de búsqueda a Municipio auxiliar.</remarks>
        public const string HidrotecMunicipioauxiliarId = "hidrotec_municipioauxiliarid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto para informar con el municipio auxiliar cuando la dirección no sea del callejero.</remarks>
        public const string HidrotecMunicipioauxiliar = "hidrotec_municipioauxiliar";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Municipio de dirección de suministro</remarks>
        public const string HidrotecMunicipiosuministroId = "hidrotec_municipiosuministroid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto para informar con el municipio cuando la dirección no sea del callejero.</remarks>
        public const string HidrotecMunicipio = "hidrotec_municipio";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Nivel de servicio, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione el nivel de servicio para el caso para asegurarse de que el caso se administra correctamente.</remarks>
        public const string ContractserviceLevelCode = "contractservicelevelcode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo que indica el número en una dirección.</remarks>
        public const string HidrotecNumero = "hidrotec_numero";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo que indica el número en una dirección auxiliar.</remarks>
        public const string HidrotecNumeroauxiliar = "hidrotec_numeroauxiliar";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica el número de comercio TPV.</remarks>
        public const string HidrotecNumerocomerciotpv = "hidrotec_numerocomerciotpv";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo para informar con el número de cuenta bancaria.</remarks>
        public const string HidrotecNumerodecuenta = "hidrotec_numerodecuenta";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Muestra el número de caso para tenerlo como referencia del cliente y capacidades de búsqueda. Esto no se puede modificar.</remarks>
        public const string TicketNumber = "ticketnumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecNumeroexpedientecac = "hidrotec_numeroexpedientecac";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Este campo se encarga de recibir la información de Diversa si existe Fraude o aviso de posible fraude.</remarks>
        public const string HidrotecNumerodeexpedientediversaavisodefraude = "hidrotec_numerodeexpedientediversaavisodefraude";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Este campo se encarga de reflejar si existe o no fraude.</remarks>
        public const string HidrotecNumerodeexpedientefraude = "hidrotec_numerodeexpedientefraude";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Escriba el número de serie del producto que está asociado con este caso, de modo que pueda notificarse el número de casos por producto.</remarks>
        public const string ProductserialNumber = "productserialnumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión del caso.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica el número de pedido TPV.</remarks>
        public const string HidrotecNumeropedidotpv = "hidrotec_numeropedidotpv";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Número de la dirección de suministro</remarks>
        public const string HidrotecNumerosuministro = "hidrotec_numerosuministro";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Campo de búsqueda sobre la entidad Contacto a la que pertenecen los organismos oficiales</remarks>
        public const string HidrotecOrganismooficialId = "hidrotec_organismooficialid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Origen del caso, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione cómo se originó el contacto sobre el caso, por ejemplo, por correo electrónico, teléfono o web, para su uso en generación de informes y análisis.</remarks>
        public const string CaseoriginCode = "caseorigincode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica si hay que informar la dirección con otra información adicional.</remarks>
        public const string HidrotecOtros = "hidrotec_otros";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica si hay que informar la dirección auxiliar con otra información adicional.</remarks>
        public const string HidrotecOtrosauxiliar = "hidrotec_otrosauxiliar";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Otros de la dirección de suministro</remarks>
        public const string HidrotecOtrossuministro = "hidrotec_otrossuministro";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si hay algún pago pendiente o no.</remarks>
        public const string HidrotecPagorealizado = "hidrotec_pagorealizado";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo de búsqueda a País.</remarks>
        public const string HidrotecPaisId = "hidrotec_paisid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo lookup que indica el país en la dirección auxiliar.</remarks>
        public const string HidrotecPaisauxiliarId = "hidrotec_paisauxiliarid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo Lookup que indica que el país IBAN bancario.</remarks>
        public const string HidrotecPaisibanId = "hidrotec_paisibanid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>País de la dirección de suministro.</remarks>
        public const string HidrotecPaissuministroId = "hidrotec_paissuministroid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_parametrizacionexplotacion</summary>
        /// <remarks>Campo de búsqueda que indica la parametrización de la explotación.</remarks>
        public const string HidrotecParametrizacionexplotacionId = "hidrotec_parametrizacionexplotacionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica el parentesco que tiene el contacto que realiza el expediente de Cambio de Titularidad.</remarks>
        public const string HidrotecParentEsco = "hidrotec_parentesco";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Campo de búsqueda a Pedanía</remarks>
        public const string HidrotecPedaniaId = "hidrotec_pedaniaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Campo de búsqueda que indica la pedanía auxiliar en una dirección.</remarks>
        public const string HidrotecPedaniaauxiliarId = "hidrotec_pedaniaauxiliarid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Campo de búsqueda que indica la Pedanía en una dirección de suministro.</remarks>
        public const string HidrotecPedaniasuministroId = "hidrotec_pedaniasuministroid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Detalles sobre si el perfil está bloqueado o no.</remarks>
        public const string Blockedprofile = "blockedprofile";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: socialprofile</summary>
        /// <remarks>Identificador único del perfil social al que está asociado el caso.</remarks>
        public const string SocialprofileId = "socialprofileid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra la duración en minutos durante la que se retuvo el caso.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_periodo</summary>
        /// <remarks>Campo de búsqueda que indica el periodo de dilación total.</remarks>
        public const string HidrotecPeriododilaciontotalId = "hidrotec_periododilaciontotalid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_periodo</summary>
        /// <remarks>Campo de búsqueda del periodo general.</remarks>
        public const string HidrotecPeriodogeneralId = "hidrotec_periodogeneralid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo que indica el piso en una dirección.</remarks>
        public const string HidrotecPiso = "hidrotec_piso";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 25, Format: Text</summary>
        /// <remarks>Campo que indica el piso en una dirección auxiliar.</remarks>
        public const string HidrotecPisoauxiliar = "hidrotec_pisoauxiliar";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Piso de la dirección de suministro</remarks>
        public const string HidrotecPisosuministro = "hidrotec_pisosuministro";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_plantillatarifa</summary>
        /// <remarks>Campo de búsqueda de la plantilla de tarifa.</remarks>
        public const string HidrotecPlantillatarifaId = "hidrotec_plantillatarifaid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración del periodo</remarks>
        public const string HidrotecPlazo = "hidrotec_plazo";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el plazo se realiza por horas o no.</remarks>
        public const string HidrotecPlazoporhoras = "hidrotec_plazoporhoras";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si se ha enviado la primera respuesta.</remarks>
        public const string FirstResponsesent = "firstresponsesent";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Responseby = "responseby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: slakpiinstance</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string FirstResponsebykpiId = "firstresponsebykpiid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad, OptionSetType: Picklist, DefaultFormValue: 2</summary>
        /// <remarks>Seleccione la prioridad de modo que se administren con rapidez las cuestiones críticas o preferidas por el cliente.</remarks>
        public const string PriorityCode = "prioritycode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Muestra el identificador del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el proceso es arbitral o no.</remarks>
        public const string HidrotecProcesoarbitral = "hidrotec_procesoarbitral";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Proceso cambio titular, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica el proceso de cambio de Titular: Alta o Baja.</remarks>
        public const string HidrotecProcesocambiotitular = "hidrotec_procesocambiotitular";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_procesodenegocio</summary>
        /// <remarks>Campo de búsqueda del proceso de negocio.</remarks>
        public const string HidrotecProcesodenegocioId = "hidrotec_procesodenegocioid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si hay proceso judicial o no.</remarks>
        public const string HidrotecProcesojudicial = "hidrotec_procesojudicial";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: product</summary>
        /// <remarks>Elija el producto asociado con el caso para identificar problemas de garantía, servicio o de otro tipo con el producto y poder notificar el número de incidentes para cada producto.</remarks>
        public const string ProductId = "productid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Escriba el usuario o el equipo que está asignado para administrar el registro. Este campo se actualiza cada vez que se asigna el registro a otro usuario.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Campo de búsqueda a Provincia</remarks>
        public const string HidrotecProvinciaId = "hidrotec_provinciaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Campo de búsqueda a provincia en una dirección auxiliar.</remarks>
        public const string HidrotecProvinciaauxiliarId = "hidrotec_provinciaauxiliarid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto para informar de la provincia en una dirección auxiliar cuando no aparezca en el callejero.</remarks>
        public const string HidrotecProvinciaauxiliar = "hidrotec_provinciaauxiliar";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Provincia de la dirección de suministro</remarks>
        public const string HidrotecProvinciasuministroId = "hidrotec_provinciasuministroid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto para informar de la provincia en una dirección cuando no aparezca en el callejero de direcciones.</remarks>
        public const string HidrotecProvincia = "hidrotec_provincia";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de texto para informar sobre la puerta en una dirección.</remarks>
        public const string HidrotecPuerta = "hidrotec_puerta";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de texto para informar la puerta en una dirección auxiliar.</remarks>
        public const string HidrotecPuertaauxiliar = "hidrotec_puertaauxiliar";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Puerta de la dirección de suministro</remarks>
        public const string HidrotecPuertasuministro = "hidrotec_puertasuministro";
        /// <summary>Type: Double, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000, Precision: 2</summary>
        /// <remarks>Contendrá la puntuación de persona con influencia de NetBreeze.</remarks>
        public const string Influencescore = "influencescore";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Seleccione el estado del caso.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de mensaje de la publicación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Muestra si la publicación se originó como mensaje público o privado.</remarks>
        public const string MessageTypeCode = "messagetypecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Muestra si los términos del derecho asociado deben reducirse o no.</remarks>
        public const string Decremententitlementterm = "decremententitlementterm";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto que indica la referencia catastral.</remarks>
        public const string HidrotecReferenciacatastral = "hidrotec_referenciacatastral";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Campo que indica el intento de llamadas restantes.</remarks>
        public const string HidrotecIntentosllamadarestantes = "hidrotec_intentosllamadarestantes";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_relacion</summary>
        /// <remarks>Lookup del primer nivel de la tipificación.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto que indica la relación del contacto con el contrato.</remarks>
        public const string HidrotecRelacionContactOcontrato = "hidrotec_relacioncontactocontrato";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Indica la fecha y hora en que se remitió el caso a una instancia superior.</remarks>
        public const string Escalatedon = "escalatedon";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si requiere inspección o no.</remarks>
        public const string HidrotecRequiereinspeccion = "hidrotec_requiereinspeccion";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si requiere presupuesto o no.</remarks>
        public const string HidrotecRequierepresupuesto = "hidrotec_requierepresupuesto";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_resolucion</summary>
        /// <remarks>Lookup que indica el tipo de resolución de un expediente.</remarks>
        public const string HidrotecResolucionId = "hidrotec_resolucionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Escriba la fecha en que se debe haber resuelto el caso.</remarks>
        public const string Resolveby = "resolveby";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Resolver por estado de SLA, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Muestra el estado de la hora de resolución inicial para el caso según las condiciones del SLA.</remarks>
        public const string ResolvebysLastatus = "resolvebyslastatus";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: slakpiinstance</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string ResolvebykpiId = "resolvebykpiid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si está revisado o no.</remarks>
        public const string HidrotecRevisado = "hidrotec_revisado";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Satisfacción, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione el nivel de satisfacción del cliente con la administración y la resolución del caso.</remarks>
        public const string CustomerSatisfactionCode = "customersatisfactioncode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 9999999</summary>
        /// <remarks>Campo de número entero que alberga el secuencial para componer el nombre de los expedientes.</remarks>
        public const string HidrotecSecuencial = "hidrotec_secuencial";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Escriba la fecha en la cual un representante del servicio al cliente debe realizar seguimiento con el cliente sobre este caso.</remarks>
        public const string Followupby = "followupby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro del caso.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si el incidente se ha combinado con otro incidente.</remarks>
        public const string Merged = "merged";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        /// <remarks>Tercer nivel de la tipificación.</remarks>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipodecliente</summary>
        /// <remarks>Campo Lookup que indica el subtipo de cliente.</remarks>
        public const string HidrotecSubtipodeclienteId = "hidrotec_subtipodeclienteid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Campo de teléfono fijo.</remarks>
        public const string HidrotecTelefonofijo = "hidrotec_telefonofijo";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecTelefonofijoCobro = "hidrotec_telefonofijo_cobro";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecTelefonofijoCorrespondencia = "hidrotec_telefonofijo_correspondencia";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecTelefonofijoTitular = "hidrotec_telefonofijo_titular";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Campo teléfono móvil.</remarks>
        public const string HidrotecTelefonomovil = "hidrotec_telefonomovil";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecTelefonomovilCobro = "hidrotec_telefonomovil_cobro";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecTelefonomovilCorrespondencia = "hidrotec_telefonomovil_correspondencia";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        public const string HidrotecTelefonomovilTitular = "hidrotec_telefonomovil_titular";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Segundo nivel de tipificación.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de acometida, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones de tipo acometida: Agua y saneamiento.</remarks>
        public const string HidrotecTipoacometida = "hidrotec_tipoacometida";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Muestra la tasa de conversión de la divisa del registro. El tipo de cambio se utiliza para convertir todos los campos de dinero del registro desde la divisa local hasta la divisa predeterminada del sistema.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de caso, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Seleccione el tipo de caso para identificar el incidente para su uso en enrutamiento de casos y análisis.</remarks>
        public const string CaseTypeCode = "casetypecode";
        /// <summary>Type: EntityName, RequiredLevel: ApplicationRequired</summary>
        public const string CustomerIdType = "customeridtype";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipodecliente</summary>
        /// <remarks>Lookup a los tipos de cliente.</remarks>
        public const string HidrotecTipodeclienteId = "hidrotec_tipodeclienteid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de días, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que distingue entre días laborales y días naturales.</remarks>
        public const string HidrotecTipodias = "hidrotec_tipodias";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo expediente, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica si el expediente es individual, reapertura, Grupo o Agrupado.</remarks>
        public const string HidrotecTipoexpediente = "hidrotec_tipoexpediente";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Checkbox que indica si el solicitante es el titular.</remarks>
        public const string HidrotecTitularsolicitante = "hidrotec_titularsolicitante";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 150, Format: Text</summary>
        /// <remarks>Campo para informar con la ubicación del contador.</remarks>
        public const string HidrotecUbicacioncontador = "hidrotec_ubicacioncontador";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Lookup a última comunicación.</remarks>
        public const string HidrotecUltimacomunicacionId = "hidrotec_ultimacomunicacionid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_factura</summary>
        /// <remarks>Lookup a facturas para mostrar la última factura actualizada.</remarks>
        public const string HidrotecUltimafacturaactualizadaId = "hidrotec_ultimafacturaactualizadaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_ordendetrabajo</summary>
        /// <remarks>Lookup que muestra la última Orden de Trabajo actualizada.</remarks>
        public const string HidrotecUltimaotactualizadaId = "hidrotec_ultimaotactualizadaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que muesrea el último estado de facturas.</remarks>
        public const string HidrotecUltimoestadofacturas = "hidrotec_ultimoestadofacturas";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que informa sobre el último estado de las órdenes de trabajo.</remarks>
        public const string HidrotecUltimoestadoots = "hidrotec_ultimoestadoots";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que informa sobre el último estado de los presupuestos.</remarks>
        public const string HidrotecUltimoestadopresupuestos = "hidrotec_ultimoestadopresupuestos";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_presupuesto</summary>
        /// <remarks>Campo Lookup que informa del último presupuesto actualizado.</remarks>
        public const string HidrotecUltimopresupuestoactualizadoId = "hidrotec_ultimopresupuestoactualizadoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>El último SLA que se aplicó a este caso. Este campo es solo de uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del caso.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000</summary>
        /// <remarks>Escriba el número de unidades de servicio que se facturaron al cliente para el caso.</remarks>
        public const string Billedserviceunits = "billedserviceunits";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000</summary>
        /// <remarks>Escriba el número de unidades de servicio que se requirieron realmente para resolver el caso.</remarks>
        public const string Actualserviceunits = "actualserviceunits";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Uso acometida, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones de uso acometida: Agua, Contra-incendio, Fecal, Pluvial, Fecal y pluvial.</remarks>
        public const string HidrotecUsoacometida = "hidrotec_usoacometida";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Lookup al Usuario que se encarga de la aprobación.</remarks>
        public const string HidrotecUsuarioaprobacion = "hidrotec_usuarioaprobacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Campo de búsqueda del Usuario revisión.</remarks>
        public const string HidrotecUsuariorevisionId = "hidrotec_usuariorevisionid";
        /// <summary>Type: Double, RequiredLevel: None, MinValue: -100000000000, MaxValue: 100000000000, Precision: 2</summary>
        /// <remarks>Valor derivado después de evaluar palabras comúnmente asociadas a una opinión negativa, neutra o positiva en una publicación social. La información sobre opiniones también se puede notificar en forma de valores numéricos.</remarks>
        public const string Sentimentvalue = "sentimentvalue";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que informa si se necesita la verificación de un organismo oficial o no.</remarks>
        public const string HidrotecVerificacionorganismooficial = "hidrotec_verificacionorganismooficial";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Campo de búsqueda a Vía</remarks>
        public const string HidrotecViaId = "hidrotec_viaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Campo de búsqueda a Vía en una dirección auxiliar.</remarks>
        public const string HidrotecViaauxiliarId = "hidrotec_viaauxiliarid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo para informar la vía cuando no se encuentre la dirección auxiliar en el callejero.</remarks>
        public const string HidrotecViaauxiliar = "hidrotec_viaauxiliar";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Vía de la dirección de suministro</remarks>
        public const string HidrotecViasuministroId = "hidrotec_viasuministroid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo para informar la vía cuando no se encuentre en el callejero.</remarks>
        public const string HidrotecVia = "hidrotec_via";

        public const string HidrotecLatitud = "hidrotec_latitud";
        public const string HidrotecLongitud = "hidrotec_longitud";


        public enum HidrotecAccesibilidadcontador_OptionSet
        {
            Accesible = 1,
            Noaccesible = 2,
            Parcialmenteaccesible = 3
        }
        public enum HidrotecAccesollave_OptionSet
        {
            Accesible = 1,
            Noaccesible = 2
        }
        public enum HidrotecAsumedeuda_OptionSet
        {
            Paga = 1,
            Remesa = 2,
            Secompromete = 3,
            No = 4
        }
        public enum HidrotecCalibrecontad_OptionSet
        {
            _13mm = 13,
            _15mm = 15,
            _20mm = 20,
            _25mm = 25,
            _30mm = 30,
            _40mm = 40,
            _50mm = 50,
            Otros = 0
        }
        public enum HidrotecCanalalarmas_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum HidrotecCanaldeentrada_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum HidrotecCanaldocumentacion_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum HidrotecCanalnotificacionresolucion_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum HidrotecClaseacometida_OptionSet
        {
            Individual = 1,
            Colectiva = 2
        }
        public enum HidrotecContactOcobro_OptionSet
        {
            Igualquesolicitante = 1,
            Igualquetitular = 2,
            Igualquecorrespondencia = 3,
            Otro = 4
        }
        public enum HidrotecContactOcorrespondencia_OptionSet
        {
            Igualquesolicitante = 1,
            Igualquetitular = 2,
            Otro = 3
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Resuelto = 1,
            Cancelado = 2
        }
        public enum HidrotecEstadocambiotitular_OptionSet
        {
            Bajacontrato = 1,
            Altacontrato = 2
        }
        public enum HidrotecEstadocontrato_OptionSet
        {
            Altasolicitada = 1,
            Altaprovisional = 2,
            Altadefinitiva = 3,
            Altacortada = 4,
            Bajacortada = 5,
            Bajasolicitada = 6,
            Bajadefinitiva = 7,
            ContratoImportAdo = 8,
            Borrado = 9
        }
        public enum FirstResponsesLastatus_OptionSet
        {
            Encurso = 1,
            Cercanoanoconformidad = 2,
            Correcto = 3,
            Noconformidad = 4
        }
        public enum HidrotecEstadocorte_OptionSet
        {
            Generacion = 500000000,
            ModificacionIniciados = 500000001,
            ModificacionNoIniciados = 500000002,
            Inicio = 500000003,
            Finalizacion = 500000004,
            Anulacion = 500000005
        }
        public enum Servicestage_OptionSet
        {
            Identificar = 0,
            Investigacion = 1,
            Resolver = 2
        }
        public enum IncidentstageCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum SeverityCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum HidrotecHorafinatencion_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecHorainicioatencion_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecIdiomacontrato_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum HidrotecImpresionfactura_OptionSet
        {
            Convencional = 1,
            FacturaElectronica = 2,
            InternetAAPP = 3,
            FacturaPDF = 4,
            FDevueltaporcorreo = 5
        }
        public enum HidrotecIvaclienteespecial_OptionSet
        {
            Noaplica = 1,
            OperacionesdeinVersionSujetopasivo = 2,
            Clientepromotor = 3
        }
        public enum ContractserviceLevelCode_OptionSet
        {
            Oro = 1,
            Plata = 2,
            Bronce = 3
        }
        public enum CaseoriginCode_OptionSet
        {
            Telefono = 1,
            Correoelectronico = 2,
            Web = 3,
            Facebook = 2483,
            Twitter = 3986
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum PriorityCode_OptionSet
        {
            Alta = 1,
            Normal = 2,
            Baja = 3
        }
        public enum HidrotecProcesocambiotitular_OptionSet
        {
            Alta = 1,
            Baja = 2
        }
        public enum StatusCode_OptionSet
        {
            Problemaresuelto = 5,
            Informacionproporcionada = 1000,
            Cancelado = 6,
            Combinado = 2000,
            Encurso = 1,
            Retenido = 2,
            Esperandodetalles = 3,
            Investigacion = 4
        }
        public enum MessageTypeCode_OptionSet
        {
            Mensajepublico = 0,
            Mensajeprivado = 1
        }
        public enum ResolvebysLastatus_OptionSet
        {
            Encurso = 1,
            Cercanoanoconformidad = 2,
            Correcto = 3,
            Noconformidad = 4
        }
        public enum CustomerSatisfactionCode_OptionSet
        {
            Muysatisfecho = 5,
            Satisfecho = 4,
            Neutral = 3,
            Insatisfecho = 2,
            Muyinsatisfecho = 1
        }
        public enum HidrotecTipoacometida_OptionSet
        {
            Agua = 1,
            Saneamiento = 2
        }
        public enum CaseTypeCode_OptionSet
        {
            Pregunta = 1,
            Problema = 2,
            Solicitud = 3
        }
        public enum CustomerIdType_OptionSet
        {
        }
        public enum HidrotecTipodias_OptionSet
        {
            Laborables = 1,
            Naturales = 2
        }
        public enum HidrotecTipoexpediente_OptionSet
        {
            Individual = 1,
            Reapertura = 2,
            Grupo = 3,
            Agrupado = 4
        }
        public enum HidrotecUsoacometida_OptionSet
        {
            Aguapotable = 1,
            Contraincendio = 2,
            Fecal = 3,
            Pluvial = 4,
            PluvialFecal = 5
        }
    }

    /// <summary>DisplayName: Extensión datos expediente, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad con datos adicionales al expediente necesarios para llevar a cabo el uso de pantillas de word.</remarks>
    public static class HidrotecExtensiondatosexpediente
    {
        public const string EntityName = "hidrotec_extensiondatosexpediente";
        public const string EntityCollectionName = "hidrotec_extensiondatosexpedientes";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_extensiondatosexpedienteid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). BIC del banco del rol de cobro.</remarks>
        public const string HidrotecMbBic = "hidrotec_mb_bic";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). Código postal de la dirección de la empresa gestora.</remarks>
        public const string HidrotecMbCodPostalEmpresagestora = "hidrotec_mb_codpostalempresagestora";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 500, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). Dirección compuesta sin municipio, provincia ni país. Añadiendo el tipo de vía al comienzo</remarks>
        public const string HidrotecMbDireccioncompuesta = "hidrotec_mb_direccioncompuesta";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 500, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). Dirección compuesta empresa gestora sin municipio, provincia ni país.</remarks>
        public const string HidrotecMbDireccionempresagestora = "hidrotec_mb_direccionempresagestora";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 5000</summary>
        /// <remarks>Dirección compuesta de la oficina del municipio principal del expediente</remarks>
        public const string HidrotecCcDireccionoficina = "hidrotec_cc_direccionoficina";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Extensión datos expediente</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: incident</summary>
        /// <remarks>Campo de búsqueda al expediente del cual es extensión.</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1000, Format: Text</summary>
        /// <remarks>Cadena informativa con el horario de oficina.</remarks>
        public const string HidrotecCcHorariooficina = "hidrotec_cc_horariooficina";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). IBAN del rol de cobro.</remarks>
        public const string HidrotecMbIban = "hidrotec_mb_iban";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). Municipio de la empresa gestora.</remarks>
        public const string HidrotecMbMunicipioempresagestora = "hidrotec_mb_municipioempresagestora";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). NIF de la empresa gestora de la explotación</remarks>
        public const string HidrotecMbNifempresagestora = "hidrotec_mb_nifempresagestora";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). Nombre de la empresa gestora.</remarks>
        public const string HidrotecMbNombreempresa = "hidrotec_mb_nombreempresa";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 150, Format: Text</summary>
        /// <remarks>Campo para el Mandato Bancario (mb). País de la empresa gestora.</remarks>
        public const string HidrotecMbPaisempresagestora = "hidrotec_mb_paisempresagestora";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Extensión datos expediente</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 500, Format: Text</summary>
        /// <remarks>Cadena con la información de los teléfonos de oficina.</remarks>
        public const string HidrotecCcTelefonosoficina = "hidrotec_cc_telefonosoficina";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Factura, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad Facturas.</remarks>
    public static class HidrotecFactura
    {
        public const string EntityName = "hidrotec_factura";
        public const string EntityCollectionName = "hidrotec_facturas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_facturaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la factura es cobrable o no.</remarks>
        public const string HidrotecCobrable = "hidrotec_cobrable";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_contrato</summary>
        /// <remarks>Campo de búsqueda a contratos.</remarks>
        public const string HidrotecContratoId = "hidrotec_contratoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada a la entidad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Factura</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Estado de las facturas.</remarks>
        public const string HidrotecEstadofactura = "hidrotec_estadofactura";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: incident</summary>
        /// <remarks>Campo de búsqueda a expedientes.</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha de emisión de la factura.</remarks>
        public const string HidrotecFechaemision = "hidrotec_fechaemision";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha de vencimiento de la factura.</remarks>
        public const string HidrotecFechavencimiento = "hidrotec_fechavencimiento";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_importe</summary>
        /// <remarks>Valor de Importe en divisa base.</remarks>
        public const string HidrotecImportEBase = "hidrotec_importe_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Importe de la factura.</remarks>
        public const string HidrotecImportE = "hidrotec_importe";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_mora</summary>
        /// <remarks>Valor de Mora en divisa base.</remarks>
        public const string HidrotecMoraBase = "hidrotec_mora_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Mora asociada a la factura.</remarks>
        public const string HidrotecMora = "hidrotec_mora";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Número de la factura.</remarks>
        public const string HidrotecNumerodefactura = "hidrotec_numerodefactura";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Observaciones asociadas a la factura.</remarks>
        public const string HidrotecObservaciones = "hidrotec_observaciones";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Origen, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Origen de la factura: Diversa y Gesred</remarks>
        public const string HidrotecOrigen = "hidrotec_origen";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Factura</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_recargo</summary>
        /// <remarks>Valor de Recargo en divisa base.</remarks>
        public const string HidrotecRecargoBase = "hidrotec_recargo_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Recargo de la factura.</remarks>
        public const string HidrotecRecargo = "hidrotec_recargo";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tipo de cambio de la divisa asociada a la entidad en relación con la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecOrigen_OptionSet
        {
            Diversa = 1,
            Gesred = 2
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Actividad que mantiene un seguimiento del resultado de llamadas y el número de páginas para un fax y que, opcionalmente, almacena una copia electrónica del documento.</remarks>
    public static class Fax
    {
        public const string EntityName = "fax";
        public const string EntityCollectionName = "faxes";
    }

    /// <summary>DisplayName: Festivo, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa un festivo.</remarks>
    public static class HidrotecFestivo
    {
        public const string EntityName = "hidrotec_festivo";
        public const string EntityCollectionName = "hidrotec_festivos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_festivoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Ámbito Festivo, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Desplegable de tipo de festivo: Nacional, Comunidad autónoma, Local.</remarks>
        public const string HidrotecAmbitofestivo = "hidrotec_ambitofestivo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunidadautonoma</summary>
        /// <remarks>Campo de búsqueda  a las Comunidades autónomas.</remarks>
        public const string HidrotecComunidadautonoma = "hidrotec_comunidadautonoma";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Festivo</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha.</remarks>
        public const string HidrotecFecha = "hidrotec_fecha";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Municipio donde aplica el día festivo.</remarks>
        public const string HidrotecMunicipio = "hidrotec_municipio";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Festivo</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecAmbitofestivo_OptionSet
        {
            Nacional = 0,
            ComunidadAutonoma = 1,
            Local = 2
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: hidrotec_hidrotec_direcciondesuministro_hidrotec, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecDirecciondesuministroHidrotec
    {
        public const string EntityName = "hidrotec_hidrotec_direcciondesuministro_hidrotec";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_direcciondesuministro_hidrotecid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecAreaafectadaId = "hidrotec_areaafectadaid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecDirecciondesuministroId = "hidrotec_direcciondesuministroid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_municipio_hidrotec_oficina, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecMunicipioHidrotecOficina
    {
        public const string EntityName = "hidrotec_hidrotec_municipio_hidrotec_oficina";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_municipio_hidrotec_oficinaid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecOficinaId = "hidrotec_oficinaid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_periodo_hidrotec_parametrizaci, OwnershipType: None, IntroducedVersion: 1.0.0.0</summary>
    public static class HidrotecHidrotecPeriodoHidrotecParametrizaci
    {
        public const string EntityName = "hidrotec_hidrotec_periodo_hidrotec_parametrizaci";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_periodo_hidrotec_parametrizaciid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecParametrizacionalarmasId = "hidrotec_parametrizacionalarmasid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecPeriodoId = "hidrotec_periodoid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_procesodenegocio_hidrotec_rela, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecProcesodenegocioHidrotecRela
    {
        public const string EntityName = "hidrotec_hidrotec_procesodenegocio_hidrotec_rela";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_procesodenegocio_hidrotec_relaid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecProcesodenegocioId = "hidrotec_procesodenegocioid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_procesodenegocio_hidrotec_subt, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecProcesodenegocioHidrotecSubt
    {
        public const string EntityName = "hidrotec_hidrotec_procesodenegocio_hidrotec_subt";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_procesodenegocio_hidrotec_subtid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecProcesodenegocioId = "hidrotec_procesodenegocioid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_procesodenegocio_hidrotec_tipo, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecProcesodenegocioHidrotecTipo
    {
        public const string EntityName = "hidrotec_hidrotec_procesodenegocio_hidrotec_tipo";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_procesodenegocio_hidrotec_tipoid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecProcesodenegocioId = "hidrotec_procesodenegocioid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_subtipo_hidrotec_causa, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecSubtipoHidrotecCausa
    {
        public const string EntityName = "hidrotec_hidrotec_subtipo_hidrotec_causa";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_subtipo_hidrotec_causaid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecCausaId = "hidrotec_causaid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_subtipo_hidrotec_motivo, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecSubtipoHidrotecMotivo
    {
        public const string EntityName = "hidrotec_hidrotec_subtipo_hidrotec_motivo";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_subtipo_hidrotec_motivoid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecMotivoId = "hidrotec_motivoid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_subtipo_hidrotec_resolucion, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecSubtipoHidrotecResolucion
    {
        public const string EntityName = "hidrotec_hidrotec_subtipo_hidrotec_resolucion";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_subtipo_hidrotec_resolucionid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecResolucionId = "hidrotec_resolucionid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_subtipodecliente_hidrotec_plan, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecSubtipodeclienteHidrotecPlan
    {
        public const string EntityName = "hidrotec_hidrotec_subtipodecliente_hidrotec_plan";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_subtipodecliente_hidrotec_planid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecPlantillatarifaId = "hidrotec_plantillatarifaid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecSubtipodeclienteId = "hidrotec_subtipodeclienteid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_usd_mensajes_systemuser, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecUsdMensajesSystemUser
    {
        public const string EntityName = "hidrotec_hidrotec_usd_mensajes_systemuser";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_usd_mensajes_systemuserid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecUsdMensajesId = "hidrotec_usd_mensajesid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string SystemUserId = "systemuserid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_hidrotec_usd_mensajes_team, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecHidrotecUsdMensajesTeam
    {
        public const string EntityName = "hidrotec_hidrotec_usd_mensajes_team";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_hidrotec_usd_mensajes_teamid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecUsdMensajesId = "hidrotec_usd_mensajesid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string TeamId = "teamid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_incident_hidrotec_areaafectada, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecIncidentHidrotecAreaafectada
    {
        public const string EntityName = "hidrotec_incident_hidrotec_areaafectada";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_incident_hidrotec_areaafectadaid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecAreaafectadaId = "hidrotec_areaafectadaid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string IncidentId = "incidentid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_incident_hidrotec_contrato, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecIncidentHidrotecContrato
    {
        public const string EntityName = "hidrotec_incident_hidrotec_contrato";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_incident_hidrotec_contratoid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecContratoId = "hidrotec_contratoid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string IncidentId = "incidentid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_incident_hidrotec_contrato_supr_opo, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecIncidentHidrotecContratoSuprOpo
    {
        public const string EntityName = "hidrotec_incident_hidrotec_contrato_supr_opo";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_incident_hidrotec_contrato_supr_opoid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecContratoId = "hidrotec_contratoid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string IncidentId = "incidentid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_incident_hidrotec_direcciondesuministro, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecIncidentHidrotecDirecciondesuministro
    {
        public const string EntityName = "hidrotec_incident_hidrotec_direcciondesuministro";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_incident_hidrotec_direcciondesuministroid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecDirecciondesuministroId = "hidrotec_direcciondesuministroid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string IncidentId = "incidentid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: hidrotec_parametrizacionexplotacion_maestroroles, OwnershipType: None, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecParametrizacionexplotacionMaestroroles
    {
        public const string EntityName = "hidrotec_parametrizacionexplotacion_maestroroles";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string PrimaryKey = "hidrotec_parametrizacionexplotacion_maestrorolesid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecMaestroderolesId = "hidrotec_maestroderolesid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        public const string HidrotecParametrizacionexplotacionId = "hidrotec_parametrizacionexplotacionid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
    }

    /// <summary>DisplayName: Historial transiciones, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad donde quedan reflejados los cambios en los estados y en las asignaciones. Sirve para expedientes y para actividades de tercero.</remarks>
    public static class HidrotecHistorialtransiciones
    {
        public const string EntityName = "hidrotec_historialtransiciones";
        public const string EntityCollectionName = "hidrotec_historialtransicioneses";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_historialtransicionesid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 250, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_actividaddetercero</summary>
        /// <remarks>Muestra la actividad de tercero a la que está relacionado.</remarks>
        public const string HidrotecActividaddeterceroId = "hidrotec_actividaddeterceroid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        /// <remarks>Muestra el equipo al que está asignado el expediente relacionado.</remarks>
        public const string HidrotecAsignadoalequipo = "hidrotec_asignadoalequipo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Muestra el usuario al que está asignado el expediente relacionado.</remarks>
        public const string HidrotecAsignadoalusuario = "hidrotec_asignadoalusuario";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Historial transiciones</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 250, Format: Text</summary>
        /// <remarks>Muestra el estado de la actividad de tercero.</remarks>
        public const string HidrotecEstadoactividad = "hidrotec_estadoactividad";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_estadoexpediente</summary>
        /// <remarks>Estado del expediente relacionado</remarks>
        public const string HidrotecEstadoexpedienteId = "hidrotec_estadoexpedienteid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Muestra el expediente al que está relacionado el historial</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Historial transiciones</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Instalación, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa una instalación.</remarks>
    public static class HidrotecInstalacion
    {
        public const string EntityName = "hidrotec_instalacion";
        public const string EntityCollectionName = "hidrotec_instalacions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_instalacionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Instalación</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Estado del campo de informe Explotaciones.</remarks>
        public const string HidrotecExplotacionesState = "hidrotec_explotaciones_state";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora en que se actualizó el campo de informe Explotaciones por última vez.</remarks>
        public const string HidrotecExplotacionesDate = "hidrotec_explotaciones_date";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Campo que informa el total de Explotación de esta instalación.</remarks>
        public const string HidrotecExplotaciones = "hidrotec_explotaciones";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador de la instalación del sistema de origen.</remarks>
        public const string HidrotecIdinstalacion = "hidrotec_idinstalacion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Campo de conjunto de opciones que indica el idioma de la instalación.</remarks>
        public const string HidrotecIdioma = "hidrotec_idioma";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Instancia de la base de datos.</remarks>
        public const string HidrotecInstanciabd = "hidrotec_instanciabd";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Nombre de la base de datos</remarks>
        public const string HidrotecNombrebd = "hidrotec_nombrebd";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Número que representa el puerto.</remarks>
        public const string HidrotecPuerto = "hidrotec_puerto";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Instalación</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Servidor.</remarks>
        public const string HidrotecServidor = "hidrotec_servidor";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecIdioma_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Intento de llamadas agotado, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Intento de llamadas agotado</remarks>
    public static class HidrotecIntentodellamadasagotado
    {
        public const string EntityName = "hidrotec_intentodellamadasagotado";
        public const string EntityCollectionName = "hidrotec_intentodellamadasagotados";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la actividad.</remarks>
        public const string PrimaryKey = "activityid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Tema asociado con la actividad.</remarks>
        public const string PrimaryName = "subject";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la actividad está revisada.</remarks>
        public const string HidrotecActividadrevisada = "hidrotec_actividadrevisada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la actividad.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el puntero de actividad.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mailbox</summary>
        /// <remarks>Identificador único del buzón asociado con remitente del correo electrónico.</remarks>
        public const string SenderMailBoxId = "sendermailboxid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal social, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Muestra cómo se originó el contacto sobre la actividad social, por ejemplo, en Twitter o Facebook. Este campo es de solo lectura.</remarks>
        public const string Community = "community";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio real de la actividad.</remarks>
        public const string Actualstart = "actualstart";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Búsqueda a comunicación.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Dejó el correo de voz</remarks>
        public const string LeftvoiceMail = "leftvoicemail";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la actividad.</remarks>
        public const string Description = "description";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al puntero de actividad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración programada de la actividad en minutos.</remarks>
        public const string Scheduleddurationminutes = "scheduleddurationminutes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración real de la actividad en minutos.</remarks>
        public const string Actualdurationminutes = "actualdurationminutes";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string IsmapiPrivate = "ismapiprivate";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Información que especifica si la actividad es un tipo de evento o de actividad regular.</remarks>
        public const string Isregularactivity = "isregularactivity";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se creó a partir de una regla de flujo de trabajo.</remarks>
        public const string IsworkflowCreated = "isworkflowcreated";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se facturó como parte de la resolución de un caso.</remarks>
        public const string Isbilled = "isbilled";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estados actividades, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Cojunto de opciones que indica si la actividad está pendiente, completada o cancelada.</remarks>
        public const string HidrotecEstadoactividad = "hidrotec_estadoactividad";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado de la actividad, OptionSetType: State</summary>
        /// <remarks>Estado de la actividad.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la actividad.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se envió la actividad.</remarks>
        public const string Senton = "senton";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio programada de la actividad.</remarks>
        public const string Scheduledstart = "scheduledstart";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se intentó la entrega de la actividad por última vez.</remarks>
        public const string DeliveryLastAttemptedon = "deliverylastattemptedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización programada de la actividad.</remarks>
        public const string Scheduledend = "scheduledend";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización real de la actividad.</remarks>
        public const string Actualend = "actualend";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único que especifica el identificador de serie periódica de una instancia.</remarks>
        public const string SeriesId = "seriesid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el puntero de actividad por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la actividad por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la actividad.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 8192</summary>
        /// <remarks>Información adicional proporcionada por la aplicación externa como JSON. Solo para uso interno.</remarks>
        public const string Activityadditionalparams = "activityadditionalparams";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra durante cuánto tiempo, en minutos, se retuvo el registro.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de la actividad.</remarks>
        public const string PriorityCode = "prioritycode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad de entrega, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de entrega de la actividad al servidor de correo electrónico.</remarks>
        public const string DeliverypriorityCode = "deliveryprioritycode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Identificador único del usuario o equipo propietario de la actividad.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado de la actividad.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: account,bookableresourcebooking,bookableresourcebookingheader,bulkoperation,campaign,campaignactivity,contact,contract,entitlement,entitlementtemplate,hidrotec_campoimagen,hidrotec_logerrorintegracion,hidrotec_ov_usuario,hidrotec_usd_mensajes,incident,invoice,knowledgearticle,knowledgebaserecord,lead,msdyn_postalbum,opportunity,quote,salesorder,uii_action,uii_hostedapplication,uii_nonhostedapplication,uii_option,uii_savedsession,uii_workflow,uii_workflowstep,uii_workflow_workflowstep_mapping</summary>
        /// <remarks>Identificador único del objeto al que está asociada la actividad.</remarks>
        public const string RegardingobjectId = "regardingobjectid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidName = "regardingobjectidname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidyomiName = "regardingobjectidyominame";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        public const string RegardingobjectTypeCode = "regardingobjecttypecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_relacion</summary>
        /// <remarks>Campo que indica el primer nivel de la tipificación del expediente.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string PostponeactivityProcessingUntil = "postponeactivityprocessinguntil";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: service</summary>
        /// <remarks>Identificador único de un servicio asociado.</remarks>
        public const string ServiceId = "serviceid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro de caso.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        /// <remarks>Campo que indica el tercer nivel de la tipificación del expediente.</remarks>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Campo que indica el segundo nivel de la tipificación del expediente.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired, DisplayName: Tipo de actividad, OptionSetType: Picklist</summary>
        /// <remarks>Tipo de actividad.</remarks>
        public const string ActivityTypeCode = "activitytypecode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tasa de cambio de la divisa asociada al puntero de actividad con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipo de cita, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de instancia de una serie periódica.</remarks>
        public const string InstanceTypeCode = "instancetypecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la actividad por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Último SLA que se aplicó a este caso. Este campo es solo para uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria de la actividad.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        public enum Community_OptionSet
        {
            Facebook = 1,
            Twitter = 2,
            Otro = 0
        }
        public enum HidrotecEstadoactividad_OptionSet
        {
            Pendiente = 1,
            Completada = 2,
            Cancelada = 3
        }
        public enum StateCode_OptionSet
        {
            Abierto = 0,
            Completado = 1,
            Cancelado = 2,
            Programado = 3
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum PriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum DeliverypriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum StatusCode_OptionSet
        {
            Abierto = 1,
            Completado = 2,
            Cancelado = 3,
            Programado = 4
        }
        public enum RegardingobjectTypeCode_OptionSet
        {
        }
        public enum ActivityTypeCode_OptionSet
        {
            Fax = 4204,
            Llamadadetelefono = 4210,
            Correoelectronico = 4202,
            Carta = 4207,
            Cita = 4201,
            Actividaddeservicio = 4214,
            Cierredeoportunidad = 4208,
            Cierredepedido = 4209,
            Cierredeoferta = 4211,
            Resoluciondelcaso = 4206,
            Tarea = 4212,
            Respuestadecampana = 4401,
            Actividaddelacampana = 4402,
            Operacionmasiva = 4406,
            Citaperiodica = 4251,
            Intentodellamadasagotado = 10073,
            Notificacion = 10121,
            NotificacionOrganismoOficial = 10124,
            SMS = 10125,
            VisitaOficina = 10128,
            NotificacionAPP = 10151,
            Pendienteobraaltaacometida = 10153
        }
        public enum InstanceTypeCode_OptionSet
        {
            Noperiodica = 0,
            Periodicamaestra = 1,
            Instanciaperiodica = 2,
            Excepcionperiodica = 3,
            Excepcionfuturaperiodica = 4
        }
    }

    /// <summary>DisplayName: Llamada de teléfono, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Actividad para mantener un seguimiento de una llamada de teléfono.</remarks>
    public static class PhoneCall
    {
        public const string EntityName = "phonecall";
        public const string EntityCollectionName = "phonecalls";
    }

    /// <summary>DisplayName: Log error integración, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad donde se registrarán todos los errores que se produzcan durante las integraciones con toda la información posible.</remarks>
    public static class HidrotecLogerrorintegracion
    {
        public const string EntityName = "hidrotec_logerrorintegracion";
        public const string EntityCollectionName = "hidrotec_logerrorintegracions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_logerrorintegracionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estado, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Conjunto de opciones que indica el estado del error: Error sin tratar, Error tratado, Correcto y Descartado.</remarks>
        public const string HidrotecEstado = "hidrotec_estado";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Log error integración</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Estado del error.</remarks>
        public const string HidrotecExpediente = "hidrotec_expediente";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>GUID del registro en el CRM.</remarks>
        public const string HidrotecGuidregistro = "hidrotec_guidregistro";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_logerrorintegracion</summary>
        /// <remarks>Log del error anterior.</remarks>
        public const string HidrotecLogerroranteriorId = "hidrotec_logerroranteriorid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Nombre de la entidad relacionada con el error.</remarks>
        public const string HidrotecNombreentidad = "hidrotec_nombreentidad";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Parámetros de entrada del error.</remarks>
        public const string HidrotecParametrosdeentrada = "hidrotec_parametrosdeentrada";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Log error integración</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Reintentos que quedan.</remarks>
        public const string HidrotecReintentosrestantes = "hidrotec_reintentosrestantes";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Respuesta que da el servicio en el caso del error.</remarks>
        public const string HidrotecRespuestadelservicio = "hidrotec_respuestadelservicio";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Nombre del sistema de destino.</remarks>
        public const string HidrotecSistemadestino = "hidrotec_sistemadestino";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Nombre del sistema de origen.</remarks>
        public const string HidrotecSistemaorigen = "hidrotec_sistemaorigen";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo error, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones de Tipo del error: Funcional o Técnico.</remarks>
        public const string HidrotecTipoerror = "hidrotec_tipoerror";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_configuracionintegracion</summary>
        /// <remarks>Tipo de integración que da el error.</remarks>
        public const string HidrotecTipointegracionId = "hidrotec_tipointegracionid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecEstado_OptionSet
        {
            Errorsintratar = 0,
            Errortratado = 1,
            Correcto = 2,
            Descartado = 3
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipoerror_OptionSet
        {
            Funcional = 1,
            Tecnico = 2
        }
    }

    /// <summary>DisplayName: Log plugin, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad donde se registra la información de la ejecución de los plugins, actions y custom workflows.</remarks>
    public static class HidrotecLogPlugin
    {
        public const string EntityName = "hidrotec_logplugin";
        public const string EntityCollectionName = "hidrotec_logplugins";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_logpluginid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Descripción del contenido del log.</remarks>
        public const string HidrotecDescripcion = "hidrotec_descripcion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Entidad relacionada con el contenido del log.</remarks>
        public const string HidrotecEntidad = "hidrotec_entidad";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si se ha producido un error o no.</remarks>
        public const string HidrotecError = "hidrotec_error";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Log plugin</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Fin de la ejecución.</remarks>
        public const string HidrotecFinejecucion = "hidrotec_finejecucion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador del log.</remarks>
        public const string HidrotecIdlogPlugin = "hidrotec_idlogplugin";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Inicio de la ejecución.</remarks>
        public const string HidrotecInicioejecucion = "hidrotec_inicioejecucion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Mensaje que da el SDK para la ejecución que da lugar al log.</remarks>
        public const string HidrotecMensajesdk = "hidrotec_mensajesdk";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Origen.</remarks>
        public const string HidrotecOrigen = "hidrotec_origen";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Log plugin</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Maestro de roles, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Mestro de roles que pueden tomar los contactos en un contrato</remarks>
    public static class HidrotecMaestroderoles
    {
        public const string EntityName = "hidrotec_maestroderoles";
        public const string EntityCollectionName = "hidrotec_maestroderoleses";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_maestroderolesid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indicas si el rol permite realizar las bajas de contrato.</remarks>
        public const string HidrotecBajacontrato = "hidrotec_bajacontrato";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indicas si el rol permite realizar las bajas o altas de contrato.</remarks>
        public const string HidrotecBajaalta = "hidrotec_bajaalta";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el rol permite realizar cambios de tarifa.</remarks>
        public const string HidrotecCambiodetarifa = "hidrotec_cambiodetarifa";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el rol permite realizar cesiones.</remarks>
        public const string HidrotecCesion = "hidrotec_cesion";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Permiso de rol para los procesos de solicitudes relacionadas con contadores. Si está marcado se permite al rol realizar el proceso.</remarks>
        public const string HidrotecContadores = "hidrotec_contadores";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el rol permite tramitar copias de factura.</remarks>
        public const string HidrotecCopiadefactura = "hidrotec_copiadefactura";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el rol permite tramitar duplicados de contrato.</remarks>
        public const string HidrotecDuplicadodecontrato = "hidrotec_duplicadodecontrato";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el rol permite tramitar duplicados de factura.</remarks>
        public const string HidrotecDuplicadodefactura = "hidrotec_duplicadodefactura";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Maestro de roles</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el rol permite realizar facturas electrónicas.</remarks>
        public const string HidrotecFacturaelectronica = "hidrotec_facturaelectronica";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Identificador del rol. Proveniente de los sistemas de Aqualia.</remarks>
        public const string HidrotecIdrol = "hidrotec_idrol";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el rol permite realizar modificaciones de contrato.</remarks>
        public const string HidrotecModificaciondecontrato = "hidrotec_modificaciondecontrato";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Maestro de roles</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        public const string HidrotecReclamaciones = "hidrotec_reclamaciones";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si el rol permite realizar subrogaciones.</remarks>
        public const string HidrotecSubrogacion = "hidrotec_subrogacion";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Mensaje, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de mensajes que se mostrarán por pantalla al usuario de CRM.</remarks>
    public static class HidrotecMensaje
    {
        public const string EntityName = "hidrotec_mensaje";
        public const string EntityCollectionName = "hidrotec_mensajes";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_mensajeid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Mensaje</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador único de instancias de entidad.</remarks>
        public const string HidrotecIdmensaje = "hidrotec_idmensaje";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Mensaje</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Texto del mensaje.</remarks>
        public const string HidrotecTexto = "hidrotec_texto";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Texto en checo.</remarks>
        public const string HidrotecTextocheco = "hidrotec_textocheco";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Texto del mensaje en italiano.</remarks>
        public const string HidrotecTextoitaliano = "hidrotec_textoitaliano";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        /// <remarks>Texto del mensaje en portugués.</remarks>
        public const string HidrotecTextoportugues = "hidrotec_textoportugues";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Mensajes USD, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Mensajes entre usuarios y equipos</remarks>
    public static class HidrotecUsdMensajes
    {
        public const string EntityName = "hidrotec_usd_mensajes";
        public const string EntityCollectionName = "hidrotec_usd_mensajeses";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_usd_mensajesid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Mensajes USD</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        public const string HidrotecFechafinvigencia = "hidrotec_fechafinvigencia";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        public const string HidrotecFechavigencia = "hidrotec_fechavigencia";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Mensajes USD</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 1048576</summary>
        /// <remarks>Incluye el texto del mensaje</remarks>
        public const string HidrotecTextomensaje = "hidrotec_textomensaje";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Motivo, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de motivos.</remarks>
    public static class HidrotecMotivo
    {
        public const string EntityName = "hidrotec_motivo";
        public const string EntityCollectionName = "hidrotec_motivos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_motivoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 150, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Motivo</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificacor del motivo en el CRM.</remarks>
        public const string HidrotecIdmotivo = "hidrotec_idmotivo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Motivo</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Validación copia/duplicado factura, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Desplegable para indicar si la copia o el duplicado son válidos según rol</remarks>
        public const string HidrotecValidacioncopiaduplicadofactura = "hidrotec_validacioncopiaduplicadofactura";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecValidacioncopiaduplicadofactura_OptionSet
        {
            Validocopiasegunrol = 1,
            Validoduplicadosegunrol = 2,
            Sinvalidar = 3
        }
    }

    /// <summary>DisplayName: Municipio, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad Municipio.</remarks>
    public static class HidrotecMunicipio
    {
        public const string EntityName = "hidrotec_municipio";
        public const string EntityCollectionName = "hidrotec_municipios";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_municipioid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código INE del municipio.</remarks>
        public const string HidrotecCodigoine = "hidrotec_codigoine";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Municipio</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Estado del campo de informe Explotaciones.</remarks>
        public const string HidrotecExplotacionesState = "hidrotec_explotaciones_state";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora en que se actualizó el campo de informe Explotaciones por última vez.</remarks>
        public const string HidrotecExplotacionesDate = "hidrotec_explotaciones_date";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Explotaciones del municipio.</remarks>
        public const string HidrotecExplotaciones = "hidrotec_explotaciones";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador del municipio en el CRM.</remarks>
        public const string HidrotecIdmunicipio = "hidrotec_idmunicipio";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Campo de búsqueda a la provincia.</remarks>
        public const string HidrotecProvinciaId = "hidrotec_provinciaid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Municipio</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Nota, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Nota adjunta a uno o varios objetos, incluidas otras notas.</remarks>
    public static class Annotation
    {
        public const string EntityName = "annotation";
        public const string EntityCollectionName = "annotations";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la nota.</remarks>
        public const string PrimaryKey = "annotationid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 500, Format: Text</summary>
        /// <remarks>Tema asociado con la nota.</remarks>
        public const string PrimaryName = "subject";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la nota.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó la anotación.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Texto de la nota.</remarks>
        public const string Notetext = "notetext";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1073741823, Format: TextArea</summary>
        /// <remarks>Contenido de los datos adjuntos de la nota.</remarks>
        public const string Documentbody = "documentbody";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la nota son datos adjuntos.</remarks>
        public const string Isdocument = "isdocument";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la nota.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la nota por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2, Format: Text</summary>
        /// <remarks>Identificador de idioma de la nota.</remarks>
        public const string LangId = "langid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 32, Format: Text</summary>
        /// <remarks>identificador de paso de flujo de trabajo asociado a la nota.</remarks>
        public const string StepId = "stepid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        public const string IsPrivate = "isprivate";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó la anotación por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la nota por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 255, Format: Text</summary>
        /// <remarks>Nombre de archivo de la nota.</remarks>
        public const string FileName = "filename";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la nota.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Identificador único del usuario o equipo propietario de la nota.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: account,appointment,bookableresource,bookableresourcebooking,bookableresourcebookingheader,bookableresourcecategoryassn,bookableresourcecharacteristic,bookableresourcegroup,calendar,campaign,campaignactivity,campaignresponse,channelaccessprofile,channelaccessprofilerule,channelaccessprofileruleitem,commitment,competitor,contact,contract,contractdetail,convertrule,duplicaterule,email,emailserverprofile,entitlement,entitlementchannel,entitlementtemplate,equipment,fax,goal,hidrotec_actividaddetercero,hidrotec_campoimagen,hidrotec_cargadeparametrizacion,hidrotec_intentodellamadasagotado,hidrotec_notificacion,hidrotec_notificacionapp,hidrotec_notificacionorganismooficial,hidrotec_ov_usuario,hidrotec_pendienteobraaltaacometida,hidrotec_sms,hidrotec_usd_mensajes,hidrotec_visitaoficina,incident,incidentresolution,invoice,kbarticle,knowledgearticle,knowledgebaserecord,lead,letter,list,mailbox,msdyn_postalbum,msdyusd_agentscriptaction,msdyusd_answer,msdyusd_configuration,msdyusd_customizationfiles,msdyusd_entityassignment,msdyusd_entitysearch,msdyusd_form,msdyusd_languagemodule,msdyusd_scriptlet,msdyusd_scripttasktrigger,msdyusd_search,msdyusd_sessioninformation,msdyusd_task,msdyusd_toolbarbutton,msdyusd_toolbarstrip,msdyusd_tracesourcesetting,msdyusd_uiievent,msdyusd_windowroute,opportunity,opportunityclose,orderclose,phonecall,product,quote,quoteclose,recurringappointmentmaster,resourcespec,routingrule,routingruleitem,salesorder,service,serviceappointment,sharepointdocument,sla,socialactivity,task,uii_action,uii_hostedapplication,uii_nonhostedapplication,uii_option,uii_workflow,uii_workflowstep,uii_workflow_workflowstep_mapping,workflow</summary>
        /// <remarks>Identificador único del objeto al que está asociada la nota.</remarks>
        public const string ObjectId = "objectid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000</summary>
        /// <remarks>Tamaño de archivo de la nota.</remarks>
        public const string Filesize = "filesize";
        /// <summary>Type: EntityName, RequiredLevel: None, DisplayName: Tipo de objeto , OptionSetType: Picklist</summary>
        /// <remarks>Tipo de entidad con la que está asociada la nota.</remarks>
        public const string ObjectTypeCode = "objecttypecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 256, Format: Text</summary>
        /// <remarks>Tipo MIME de los datos adjuntos de la nota.</remarks>
        public const string MimeType = "mimetype";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria de la nota.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        public enum OwnerIdType_OptionSet
        {
        }
        public enum ObjectTypeCode_OptionSet
        {
            Cuenta = 1,
            Cita = 4201,
            ImportAcionmasiva = 4407,
            Calendario = 4003,
            Campana = 4400,
            Actividaddelacampana = 4402,
            Respuestadecampana = 4401,
            Caso = 112,
            Resoluciondelcaso = 4206,
            Compromiso = 4215,
            Competidor = 123,
            ContactO = 2,
            Contrato = 1010,
            Lineadecontrato = 1011,
            Correoelectronico = 4202,
            InstalacionesEquipamiento = 4000,
            Fax = 4204,
            Factura = 1090,
            Clientepotencial = 4,
            Carta = 4207,
            ListadeMarketing = 4300,
            Oportunidad = 3,
            Cierredeoportunidad = 4208,
            Pedido = 1088,
            Cierredepedido = 4209,
            Llamadadetelefono = 4210,
            Producto = 1024,
            Oferta = 1084,
            Cierredeoferta = 4211,
            Especificacionderecursos = 4006,
            Servicio = 4001,
            Actividaddeservicio = 4214,
            Tarea = 4212,
            Regladeenrutamiento = 8181,
            Elementoderegladeenrutamiento = 8199
        }
    }

    /// <summary>DisplayName: Notificación, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Entidad de Notificación.</remarks>
    public static class HidrotecNotificacion
    {
        public const string EntityName = "hidrotec_notificacion";
        public const string EntityCollectionName = "hidrotec_notificacions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la actividad.</remarks>
        public const string PrimaryKey = "activityid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Tema asociado con la actividad.</remarks>
        public const string PrimaryName = "subject";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Checkbox que indica si la actividad está revisada o no.</remarks>
        public const string HidrotecActividadrevisada = "hidrotec_actividadrevisada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la actividad.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el puntero de actividad.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mailbox</summary>
        /// <remarks>Identificador único del buzón asociado con remitente del correo electrónico.</remarks>
        public const string SenderMailBoxId = "sendermailboxid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal social, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Muestra cómo se originó el contacto sobre la actividad social, por ejemplo, en Twitter o Facebook. Este campo es de solo lectura.</remarks>
        public const string Community = "community";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio real de la actividad.</remarks>
        public const string Actualstart = "actualstart";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Campo de búsqueda a Comunicación.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Dejó el correo de voz</remarks>
        public const string LeftvoiceMail = "leftvoicemail";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la actividad.</remarks>
        public const string Description = "description";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al puntero de actividad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración programada de la actividad en minutos.</remarks>
        public const string Scheduleddurationminutes = "scheduleddurationminutes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración real de la actividad en minutos.</remarks>
        public const string Actualdurationminutes = "actualdurationminutes";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string IsmapiPrivate = "ismapiprivate";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Información que especifica si la actividad es un tipo de evento o de actividad regular.</remarks>
        public const string Isregularactivity = "isregularactivity";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se creó a partir de una regla de flujo de trabajo.</remarks>
        public const string IsworkflowCreated = "isworkflowcreated";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se facturó como parte de la resolución de un caso.</remarks>
        public const string Isbilled = "isbilled";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estados actividades, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones de los distintos estados de la actividad: Pendiente, Cancelada o Completada.</remarks>
        public const string HidrotecEstadoactividad = "hidrotec_estadoactividad";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado de la actividad, OptionSetType: State</summary>
        /// <remarks>Estado de la actividad.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la actividad.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se envió la actividad.</remarks>
        public const string Senton = "senton";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio programada de la actividad.</remarks>
        public const string Scheduledstart = "scheduledstart";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se intentó la entrega de la actividad por última vez.</remarks>
        public const string DeliveryLastAttemptedon = "deliverylastattemptedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización programada de la actividad.</remarks>
        public const string Scheduledend = "scheduledend";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización real de la actividad.</remarks>
        public const string Actualend = "actualend";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único que especifica el identificador de serie periódica de una instancia.</remarks>
        public const string SeriesId = "seriesid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el puntero de actividad por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la actividad por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la actividad.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 8192</summary>
        /// <remarks>Información adicional proporcionada por la aplicación externa como JSON. Solo para uso interno.</remarks>
        public const string Activityadditionalparams = "activityadditionalparams";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra durante cuánto tiempo, en minutos, se retuvo el registro.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de la actividad.</remarks>
        public const string PriorityCode = "prioritycode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad de entrega, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de entrega de la actividad al servidor de correo electrónico.</remarks>
        public const string DeliverypriorityCode = "deliveryprioritycode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Identificador único del usuario o equipo propietario de la actividad.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado de la actividad.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: account,bookableresourcebooking,bookableresourcebookingheader,bulkoperation,campaign,campaignactivity,contact,contract,entitlement,entitlementtemplate,hidrotec_campoimagen,hidrotec_logerrorintegracion,hidrotec_ov_usuario,hidrotec_usd_mensajes,incident,invoice,knowledgearticle,knowledgebaserecord,lead,msdyn_postalbum,opportunity,quote,salesorder,uii_action,uii_hostedapplication,uii_nonhostedapplication,uii_option,uii_savedsession,uii_workflow,uii_workflowstep,uii_workflow_workflowstep_mapping</summary>
        /// <remarks>Identificador único del objeto al que está asociada la actividad.</remarks>
        public const string RegardingobjectId = "regardingobjectid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidName = "regardingobjectidname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidyomiName = "regardingobjectidyominame";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        public const string RegardingobjectTypeCode = "regardingobjecttypecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_relacion</summary>
        /// <remarks>Campo de búsqueda de la Relación del expediente.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string PostponeactivityProcessingUntil = "postponeactivityprocessinguntil";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: service</summary>
        /// <remarks>Identificador único de un servicio asociado.</remarks>
        public const string ServiceId = "serviceid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro de caso.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        /// <remarks>Campo de búsqueda del subtipo del expediente.</remarks>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Campo de búsqueda del tipo de la tipificación del expediente.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired, DisplayName: Tipo de actividad, OptionSetType: Picklist</summary>
        /// <remarks>Tipo de actividad.</remarks>
        public const string ActivityTypeCode = "activitytypecode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tasa de cambio de la divisa asociada al puntero de actividad con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipo de cita, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de instancia de una serie periódica.</remarks>
        public const string InstanceTypeCode = "instancetypecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la actividad por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Último SLA que se aplicó a este caso. Este campo es solo para uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria de la actividad.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        public enum Community_OptionSet
        {
            Facebook = 1,
            Twitter = 2,
            Otro = 0
        }
        public enum HidrotecEstadoactividad_OptionSet
        {
            Pendiente = 1,
            Completada = 2,
            Cancelada = 3
        }
        public enum StateCode_OptionSet
        {
            Abierto = 0,
            Completado = 1,
            Cancelado = 2,
            Programado = 3
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum PriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum DeliverypriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum StatusCode_OptionSet
        {
            Abierto = 1,
            Completado = 2,
            Cancelado = 3,
            Programado = 4
        }
        public enum RegardingobjectTypeCode_OptionSet
        {
        }
        public enum ActivityTypeCode_OptionSet
        {
            Fax = 4204,
            Llamadadetelefono = 4210,
            Correoelectronico = 4202,
            Carta = 4207,
            Cita = 4201,
            Actividaddeservicio = 4214,
            Cierredeoportunidad = 4208,
            Cierredepedido = 4209,
            Cierredeoferta = 4211,
            Resoluciondelcaso = 4206,
            Tarea = 4212,
            Respuestadecampana = 4401,
            Actividaddelacampana = 4402,
            Operacionmasiva = 4406,
            Citaperiodica = 4251,
            Intentodellamadasagotado = 10073,
            Notificacion = 10121,
            NotificacionOrganismoOficial = 10124,
            SMS = 10125,
            VisitaOficina = 10128,
            NotificacionAPP = 10151,
            Pendienteobraaltaacometida = 10153
        }
        public enum InstanceTypeCode_OptionSet
        {
            Noperiodica = 0,
            Periodicamaestra = 1,
            Instanciaperiodica = 2,
            Excepcionperiodica = 3,
            Excepcionfuturaperiodica = 4
        }
    }

    /// <summary>DisplayName: Notificación APP, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    public static class HidrotecNotificacionapp
    {
        public const string EntityName = "hidrotec_notificacionapp";
        public const string EntityCollectionName = "hidrotec_notificacionapps";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la actividad.</remarks>
        public const string PrimaryKey = "activityid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Tema asociado con la actividad.</remarks>
        public const string PrimaryName = "subject";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Check que indica si la actividad está revisada o no.</remarks>
        public const string HidrotecActividadrevisada = "hidrotec_actividadrevisada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la actividad.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el puntero de actividad.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mailbox</summary>
        /// <remarks>Identificador único del buzón asociado con remitente del correo electrónico.</remarks>
        public const string SenderMailBoxId = "sendermailboxid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal social, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Muestra cómo se originó el contacto sobre la actividad social, por ejemplo, en Twitter o Facebook. Este campo es de solo lectura.</remarks>
        public const string Community = "community";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio real de la actividad.</remarks>
        public const string Actualstart = "actualstart";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Indica la comunicación que hay que realizar al contacto.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 4000</summary>
        public const string HidrotecContenidoenviado = "hidrotec_contenidoenviado";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Dejó el correo de voz</remarks>
        public const string LeftvoiceMail = "leftvoicemail";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al puntero de actividad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración programada de la actividad en minutos.</remarks>
        public const string Scheduleddurationminutes = "scheduleddurationminutes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración real de la actividad en minutos.</remarks>
        public const string Actualdurationminutes = "actualdurationminutes";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string IsmapiPrivate = "ismapiprivate";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Información que especifica si la actividad es un tipo de evento o de actividad regular.</remarks>
        public const string Isregularactivity = "isregularactivity";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se creó a partir de una regla de flujo de trabajo.</remarks>
        public const string IsworkflowCreated = "isworkflowcreated";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se facturó como parte de la resolución de un caso.</remarks>
        public const string Isbilled = "isbilled";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estados actividades, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Campo que recoge el conjunto de opciones de los estados de la actividad.</remarks>
        public const string HidrotecEstadoactividad = "hidrotec_estadoactividad";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado de la actividad, OptionSetType: State</summary>
        /// <remarks>Estado de la actividad.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        public const string HidrotecEstadoenvio = "hidrotec_estadoenvio";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la actividad.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se envió la actividad.</remarks>
        public const string Senton = "senton";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio programada de la actividad.</remarks>
        public const string Scheduledstart = "scheduledstart";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se intentó la entrega de la actividad por última vez.</remarks>
        public const string DeliveryLastAttemptedon = "deliverylastattemptedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización programada de la actividad.</remarks>
        public const string Scheduledend = "scheduledend";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización real de la actividad.</remarks>
        public const string Actualend = "actualend";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único que especifica el identificador de serie periódica de una instancia.</remarks>
        public const string SeriesId = "seriesid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la actividad.</remarks>
        public const string Description = "description";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el puntero de actividad por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la actividad por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la actividad.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 8192</summary>
        /// <remarks>Información adicional proporcionada por la aplicación externa como JSON. Solo para uso interno.</remarks>
        public const string Activityadditionalparams = "activityadditionalparams";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra durante cuánto tiempo, en minutos, se retuvo el registro.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de la actividad.</remarks>
        public const string PriorityCode = "prioritycode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad de entrega, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de entrega de la actividad al servidor de correo electrónico.</remarks>
        public const string DeliverypriorityCode = "deliveryprioritycode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Identificador único del usuario o equipo propietario de la actividad.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado de la actividad.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: account,bookableresourcebooking,bookableresourcebookingheader,bulkoperation,campaign,campaignactivity,contact,contract,entitlement,entitlementtemplate,hidrotec_campoimagen,hidrotec_logerrorintegracion,hidrotec_ov_usuario,hidrotec_usd_mensajes,incident,invoice,knowledgearticle,knowledgebaserecord,lead,msdyn_postalbum,opportunity,quote,salesorder,uii_action,uii_hostedapplication,uii_nonhostedapplication,uii_option,uii_savedsession,uii_workflow,uii_workflowstep,uii_workflow_workflowstep_mapping</summary>
        /// <remarks>Identificador único del objeto al que está asociada la actividad.</remarks>
        public const string RegardingobjectId = "regardingobjectid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidName = "regardingobjectidname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidyomiName = "regardingobjectidyominame";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        public const string RegardingobjectTypeCode = "regardingobjecttypecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_relacion</summary>
        /// <remarks>Primer nivel de la tipificación del expediente.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string PostponeactivityProcessingUntil = "postponeactivityprocessinguntil";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: service</summary>
        /// <remarks>Identificador único de un servicio asociado.</remarks>
        public const string ServiceId = "serviceid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro de caso.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        /// <remarks>Tercer nivel de la tipificación del expediente.</remarks>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Segundo nivel de la tipificación del expediente.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired, DisplayName: Tipo de actividad, OptionSetType: Picklist</summary>
        /// <remarks>Tipo de actividad.</remarks>
        public const string ActivityTypeCode = "activitytypecode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tasa de cambio de la divisa asociada al puntero de actividad con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipo de cita, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de instancia de una serie periódica.</remarks>
        public const string InstanceTypeCode = "instancetypecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la actividad por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Último SLA que se aplicó a este caso. Este campo es solo para uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria de la actividad.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        public enum Community_OptionSet
        {
            Facebook = 1,
            Twitter = 2,
            Otro = 0
        }
        public enum HidrotecEstadoactividad_OptionSet
        {
            Pendiente = 1,
            Completada = 2,
            Cancelada = 3
        }
        public enum StateCode_OptionSet
        {
            Abierto = 0,
            Completado = 1,
            Cancelado = 2,
            Programado = 3
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum PriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum DeliverypriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum StatusCode_OptionSet
        {
            Abierto = 1,
            Completado = 2,
            Cancelado = 3,
            Programado = 4
        }
        public enum RegardingobjectTypeCode_OptionSet
        {
        }
        public enum ActivityTypeCode_OptionSet
        {
            Fax = 4204,
            Llamadadetelefono = 4210,
            Correoelectronico = 4202,
            Carta = 4207,
            Cita = 4201,
            Actividaddeservicio = 4214,
            Cierredeoportunidad = 4208,
            Cierredepedido = 4209,
            Cierredeoferta = 4211,
            Resoluciondelcaso = 4206,
            Tarea = 4212,
            Respuestadecampana = 4401,
            Actividaddelacampana = 4402,
            Operacionmasiva = 4406,
            Citaperiodica = 4251,
            Intentodellamadasagotado = 10073,
            Notificacion = 10121,
            NotificacionOrganismoOficial = 10124,
            SMS = 10125,
            VisitaOficina = 10128,
            NotificacionAPP = 10151,
            Pendienteobraaltaacometida = 10153
        }
        public enum InstanceTypeCode_OptionSet
        {
            Noperiodica = 0,
            Periodicamaestra = 1,
            Instanciaperiodica = 2,
            Excepcionperiodica = 3,
            Excepcionfuturaperiodica = 4
        }
    }

    /// <summary>DisplayName: Notificación Organismo Oficial, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    public static class HidrotecNotificacionorganismooficial
    {
        public const string EntityName = "hidrotec_notificacionorganismooficial";
        public const string EntityCollectionName = "hidrotec_notificacionorganismooficials";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la actividad.</remarks>
        public const string PrimaryKey = "activityid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Tema asociado con la actividad.</remarks>
        public const string PrimaryName = "subject";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la actividad.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el puntero de actividad.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mailbox</summary>
        /// <remarks>Identificador único del buzón asociado con remitente del correo electrónico.</remarks>
        public const string SenderMailBoxId = "sendermailboxid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal social, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Muestra cómo se originó el contacto sobre la actividad social, por ejemplo, en Twitter o Facebook. Este campo es de solo lectura.</remarks>
        public const string Community = "community";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio real de la actividad.</remarks>
        public const string Actualstart = "actualstart";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Dejó el correo de voz</remarks>
        public const string LeftvoiceMail = "leftvoicemail";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la actividad.</remarks>
        public const string Description = "description";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Dirección, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecDireccion = "hidrotec_direccion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al puntero de actividad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración programada de la actividad en minutos.</remarks>
        public const string Scheduleddurationminutes = "scheduleddurationminutes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración real de la actividad en minutos.</remarks>
        public const string Actualdurationminutes = "actualdurationminutes";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string IsmapiPrivate = "ismapiprivate";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Información que especifica si la actividad es un tipo de evento o de actividad regular.</remarks>
        public const string Isregularactivity = "isregularactivity";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se creó a partir de una regla de flujo de trabajo.</remarks>
        public const string IsworkflowCreated = "isworkflowcreated";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se facturó como parte de la resolución de un caso.</remarks>
        public const string Isbilled = "isbilled";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estados actividades, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        public const string HidrotecEstadoactividad = "hidrotec_estadoactividad";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado de la actividad, OptionSetType: State</summary>
        /// <remarks>Estado de la actividad.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la actividad.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se envió la actividad.</remarks>
        public const string Senton = "senton";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio programada de la actividad.</remarks>
        public const string Scheduledstart = "scheduledstart";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se intentó la entrega de la actividad por última vez.</remarks>
        public const string DeliveryLastAttemptedon = "deliverylastattemptedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización programada de la actividad.</remarks>
        public const string Scheduledend = "scheduledend";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        public const string HidrotecFechanotificacionaapp = "hidrotec_fechanotificacionaapp";
        /// <summary>Type: DateTime, RequiredLevel: ApplicationRequired, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        public const string HidrotecFecharecepcioninterna = "hidrotec_fecharecepcioninterna";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización real de la actividad.</remarks>
        public const string Actualend = "actualend";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único que especifica el identificador de serie periódica de una instancia.</remarks>
        public const string SeriesId = "seriesid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 10000</summary>
        public const string HidrotecMensaje = "hidrotec_mensaje";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el puntero de actividad por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la actividad por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_municipio</summary>
        /// <remarks>Identificador único de Municipio asociado con Notificación Organismo Oficial.</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la actividad.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        public const string HidrotecOrganismooficialId = "hidrotec_organismooficialid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 8192</summary>
        /// <remarks>Información adicional proporcionada por la aplicación externa como JSON. Solo para uso interno.</remarks>
        public const string Activityadditionalparams = "activityadditionalparams";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra durante cuánto tiempo, en minutos, se retuvo el registro.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de la actividad.</remarks>
        public const string PriorityCode = "prioritycode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad de entrega, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de entrega de la actividad al servidor de correo electrónico.</remarks>
        public const string DeliverypriorityCode = "deliveryprioritycode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Identificador único del usuario o equipo propietario de la actividad.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado de la actividad.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: account,bookableresourcebooking,bookableresourcebookingheader,bulkoperation,campaign,campaignactivity,contact,contract,entitlement,entitlementtemplate,hidrotec_campoimagen,hidrotec_logerrorintegracion,hidrotec_ov_usuario,hidrotec_usd_mensajes,incident,invoice,knowledgearticle,knowledgebaserecord,lead,msdyn_postalbum,opportunity,quote,salesorder,uii_action,uii_hostedapplication,uii_nonhostedapplication,uii_option,uii_savedsession,uii_workflow,uii_workflowstep,uii_workflow_workflowstep_mapping</summary>
        /// <remarks>Identificador único del objeto al que está asociada la actividad.</remarks>
        public const string RegardingobjectId = "regardingobjectid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidName = "regardingobjectidname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidyomiName = "regardingobjectidyominame";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        public const string RegardingobjectTypeCode = "regardingobjecttypecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_relacion</summary>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string PostponeactivityProcessingUntil = "postponeactivityprocessinguntil";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: service</summary>
        /// <remarks>Identificador único de un servicio asociado.</remarks>
        public const string ServiceId = "serviceid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro de caso.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired, DisplayName: Tipo de actividad, OptionSetType: Picklist</summary>
        /// <remarks>Tipo de actividad.</remarks>
        public const string ActivityTypeCode = "activitytypecode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tasa de cambio de la divisa asociada al puntero de actividad con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipo de cita, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de instancia de una serie periódica.</remarks>
        public const string InstanceTypeCode = "instancetypecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la actividad por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Último SLA que se aplicó a este caso. Este campo es solo para uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria de la actividad.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        public enum Community_OptionSet
        {
            Facebook = 1,
            Twitter = 2,
            Otro = 0
        }
        public enum HidrotecDireccion_OptionSet
        {
            Entrante = 1,
            Saliente = 2
        }
        public enum HidrotecEstadoactividad_OptionSet
        {
            Pendiente = 1,
            Completada = 2,
            Cancelada = 3
        }
        public enum StateCode_OptionSet
        {
            Abierto = 0,
            Completado = 1,
            Cancelado = 2,
            Programado = 3
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum PriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum DeliverypriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum StatusCode_OptionSet
        {
            Abierto = 1,
            Completado = 2,
            Cancelado = 3,
            Programado = 4
        }
        public enum RegardingobjectTypeCode_OptionSet
        {
        }
        public enum ActivityTypeCode_OptionSet
        {
            Fax = 4204,
            Llamadadetelefono = 4210,
            Correoelectronico = 4202,
            Carta = 4207,
            Cita = 4201,
            Actividaddeservicio = 4214,
            Cierredeoportunidad = 4208,
            Cierredepedido = 4209,
            Cierredeoferta = 4211,
            Resoluciondelcaso = 4206,
            Tarea = 4212,
            Respuestadecampana = 4401,
            Actividaddelacampana = 4402,
            Operacionmasiva = 4406,
            Citaperiodica = 4251,
            Intentodellamadasagotado = 10073,
            Notificacion = 10121,
            NotificacionOrganismoOficial = 10124,
            SMS = 10125,
            VisitaOficina = 10128,
            NotificacionAPP = 10151,
            Pendienteobraaltaacometida = 10153
        }
        public enum InstanceTypeCode_OptionSet
        {
            Noperiodica = 0,
            Periodicamaestra = 1,
            Instanciaperiodica = 2,
            Excepcionperiodica = 3,
            Excepcionfuturaperiodica = 4
        }
    }

    /// <summary>DisplayName: Oficina, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que representa una oficina.</remarks>
    public static class HidrotecOficina
    {
        public const string EntityName = "hidrotec_oficina";
        public const string EntityCollectionName = "hidrotec_oficinas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_oficinaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Hora de apertura de mañana de los días laborables de Lunes a Viernes</remarks>
        public const string HidrotecAperturamanana = "hidrotec_aperturamanana";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Hora de apertura de mañana de los sábados laborables</remarks>
        public const string HidrotecAperturamananasabado = "hidrotec_aperturamananasabado";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Hora de apertura de tarde de los días laborables de Lunes a Viernes</remarks>
        public const string HidrotecAperturatarde = "hidrotec_aperturatarde";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Hora de apertura de tarde de los sábados laborables</remarks>
        public const string HidrotecAperturatardesabado = "hidrotec_aperturatardesabado";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Hora de cierre de mañana de los días laborables de Lunes a Viernes</remarks>
        public const string HidrotecCierremanana = "hidrotec_cierremanana";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Hora de cierre de mañana de los sábados laborables</remarks>
        public const string HidrotecCierremananasabado = "hidrotec_cierremananasabado";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Hora de cierre de tarde de los días laborables de Lunes a Viernes</remarks>
        public const string HidrotecCierretarde = "hidrotec_cierretarde";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Hora de cierre de tarde de los sábados laborables</remarks>
        public const string HidrotecCierretardesabado = "hidrotec_cierretardesabado";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Código identificador de la oficina.</remarks>
        public const string HidrotecCodigoId = "hidrotec_codigoid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Dirección compuesta de la oficina</remarks>
        public const string HidrotecDireccionoficina = "hidrotec_direccionoficina";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Oficina</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Número de Fax de la oficina</remarks>
        public const string HidrotecFax = "hidrotec_fax";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Campo para ampliar la descripción del horario de oficina</remarks>
        public const string HidrotecHorariooficina = "hidrotec_horariooficina";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Oficina</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Teléfono de contacto de la Oficina</remarks>
        public const string HidrotecTelefono = "hidrotec_telefono";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 150, Format: Url</summary>
        public const string HidrotecUrloficinavirtual = "hidrotec_urloficinavirtual";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecAperturamanana_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecAperturamananasabado_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecAperturatarde_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecAperturatardesabado_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecCierremanana_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecCierremananasabado_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecCierretarde_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecCierretardesabado_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Operación bancaria, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que representa una operación bancaria en los procesos de pago</remarks>
    public static class HidrotecOperacinbancaria
    {
        public const string EntityName = "hidrotec_operacinbancaria";
        public const string EntityCollectionName = "hidrotec_operacinbancarias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_operacinbancariaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Indica la autorización de la operación bancaria.</remarks>
        public const string HidrotecAutorizacion = "hidrotec_autorizacion";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Conciliación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica los distintos tipos de conciliación.</remarks>
        public const string HidrotecConciliacion = "hidrotec_conciliacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada a la entidad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica la entidad bancaria que emite la factura.</remarks>
        public const string HidrotecEmisor = "hidrotec_emisor";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Operación bancaria</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Indica el expediente al que está asociada la operación bancaria.</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_factura</summary>
        /// <remarks>Código de la factura.</remarks>
        public const string HidrotecFacturaId = "hidrotec_facturaid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha en la que se realiza la liquidación de la operación.</remarks>
        public const string HidrotecFechaliquidacion = "hidrotec_fechaliquidacion";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha en la que se realiza la operación bancaria.</remarks>
        public const string HidrotecFechaoperacion = "hidrotec_fechaoperacion";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_importeliquido</summary>
        /// <remarks>Valor de Importe líquido en divisa base.</remarks>
        public const string HidrotecImportEliquidoBase = "hidrotec_importeliquido_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Cantidad del importe a abonar.</remarks>
        public const string HidrotecImportEliquido = "hidrotec_importeliquido";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_importenominal</summary>
        /// <remarks>Valor de Importe nominal en divisa base.</remarks>
        public const string HidrotecImportEnominalBase = "hidrotec_importenominal_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Importe nominal de la operación bancaria.</remarks>
        public const string HidrotecImportEnominal = "hidrotec_importenominal";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_importetasa</summary>
        /// <remarks>Valor de Importe tasa en divisa base.</remarks>
        public const string HidrotecImportEtasaBase = "hidrotec_importetasa_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Campo que recoge el importe de la tasa.</remarks>
        public const string HidrotecImportEtasa = "hidrotec_importetasa";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Marca tarjeta, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que recoge las distintas marcas de tarjeta.</remarks>
        public const string HidrotecMarcatarjeta = "hidrotec_marcatarjeta";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que recoge el número de comercio TPV.</remarks>
        public const string HidrotecNumerocomerciotpv = "hidrotec_numerocomerciotpv";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica el número de remesa.</remarks>
        public const string HidrotecNumeroremesa = "hidrotec_numeroremesa";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo en el que hay que especificar los dígitos que forman el número de tarjeta.</remarks>
        public const string HidrotecNumerotarjeta = "hidrotec_numerotarjeta";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_oficina</summary>
        /// <remarks>Indica la oficina en la que se realiza la operación.</remarks>
        public const string HidrotecOficinaId = "hidrotec_oficinaid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Operación bancaria</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: -100000000000, MaxValue: 100000000000, Precision: 2</summary>
        /// <remarks>Indica el tanto por ciento de la tasa.</remarks>
        public const string HidrotecTasa = "hidrotec_tasa";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tipo de cambio de la divisa asociada a la entidad en relación con la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo operación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecTipooperacion = "hidrotec_tipooperacion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo tarjeta, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica el tipo de tarjeta que existen.</remarks>
        public const string HidrotecTipotarjeta = "hidrotec_tipotarjeta";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecConciliacion_OptionSet
        {
            Correcta = 1,
            Expedienteinexistente = 2,
            Operacioninexistente = 3,
            Expedientependiente = 4,
            Aceptacionincorrecta = 5,
            Rechazoincorrecto = 6,
            DiferenciaImportE = 7
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecMarcatarjeta_OptionSet
        {
            Visa = 1,
            Mastercard = 2,
            AmericanExpress = 3
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipooperacion_OptionSet
        {
            Operacion1 = 1
        }
        public enum HidrotecTipotarjeta_OptionSet
        {
            Debito = 1,
            CreditO = 2
        }
    }

    /// <summary>DisplayName: Orden de trabajo, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa una orden de trabajo</remarks>
    public static class HidrotecOrdendetrabajo
    {
        public const string EntityName = "hidrotec_ordendetrabajo";
        public const string EntityCollectionName = "hidrotec_ordendetrabajos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_ordendetrabajoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Orden de trabajo</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Refleja el estado actual de la OT.</remarks>
        public const string HidrotecEstadoactual = "hidrotec_estadoactual";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Expediente al que pertenece la OT.</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        public const string HidrotecFechafin = "hidrotec_fechafin";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        public const string HidrotecFechainicio = "hidrotec_fechainicio";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha y hora de creación de la OT.</remarks>
        public const string HidrotecFechayhoradecreacion = "hidrotec_fechayhoradecreacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Motivo por el que se realiza la Orden de Trabajo.</remarks>
        public const string HidrotecMotivo = "hidrotec_motivo";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica el motivo del cambio de estado.</remarks>
        public const string HidrotecMotivocambiodeestado = "hidrotec_motivocambiodeestado";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo en el que se registra el número de la OT.</remarks>
        public const string HidrotecNumerodeot = "hidrotec_numerodeot";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica la prioridad en la que la OT debe realizarse.</remarks>
        public const string HidrotecPrioridad = "hidrotec_prioridad";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Orden de trabajo</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Distingue entre los tipos de orden.</remarks>
        public const string HidrotecTipoorden = "hidrotec_tipoorden";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica el Tipo de trabajo a realizar.</remarks>
        public const string HidrotecTipotrabajo = "hidrotec_tipotrabajo";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica el trabajo que hay realizar.</remarks>
        public const string HidrotecTrabajo = "hidrotec_trabajo";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo Sí/No en el que se expecifica si esa OT para un expediente concreto está o no tratada.</remarks>
        public const string HidrotecTratada = "hidrotec_tratada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Organización, OwnershipType: OrganizationOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Nivel superior de la jerarquía empresarial de Microsoft Dynamics CRM. La organización puede ser una empresa específica, una compañía mayoritaria o una corporación.</remarks>
    public static class Organization
    {
        public const string EntityName = "organization";
        public const string EntityCollectionName = "organizations";
    }

    /// <summary>DisplayName: País, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad en la que viene recogida la información referida a País.</remarks>
    public static class HidrotecPais
    {
        public const string EntityName = "hidrotec_pais";
        public const string EntityCollectionName = "hidrotec_paises";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_paisid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del País</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código del país.</remarks>
        public const string HidrotecIdpais = "hidrotec_idpais";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 5, Format: Text</summary>
        /// <remarks>Indica la letra del código IBAN del país.</remarks>
        public const string HidrotecLetraiban = "hidrotec_letraiban";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del País</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Parametrización Actividades, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Parametrizaciones tanto de las Actividades de CRM como de las Actividades de Tercero en función de la parametrización de la Explotación</remarks>
    public static class HidrotecParametrizacionactividades
    {
        public const string EntityName = "hidrotec_parametrizacionactividades";
        public const string EntityCollectionName = "hidrotec_parametrizacionactividadeses";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_parametrizacionactividadesid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si la actividad es bloqueante o no.</remarks>
        public const string HidrotecBloqueante = "hidrotec_bloqueante";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Creación automática de la actividad o no.</remarks>
        public const string HidrotecCreacionautomotica = "hidrotec_creacionautomotica";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Campo de texto para dejar alguna descripción aclaratoria de la parametrización.</remarks>
        public const string HidrotecDescripcion = "hidrotec_descripcion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_documento</summary>
        /// <remarks>Documentación que hay que entregar en la actividad de tercero.</remarks>
        public const string HidrotecDocumentoId = "hidrotec_documentoid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Parametrización Actividades</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estado inicial actividad, OptionSetType: Picklist, DefaultFormValue: 500000000</summary>
        /// <remarks>Indica cuál es el estado inicial de la actividad de tercero de tipo comunicación.</remarks>
        public const string HidrotecEstadoinicialdocumento = "hidrotec_estadoinicialdocumento";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica en horas la franja de puntualidad.</remarks>
        public const string HidrotecFranjapuntualidad = "hidrotec_franjapuntualidad";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_motivo</summary>
        /// <remarks>Indica el motivo que hay que especificar en la actividad.</remarks>
        public const string HidrotecMotivoId = "hidrotec_motivoid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Actividad necesaria al inicio o no del proceso.</remarks>
        public const string HidrotecNecesariaalinicio = "hidrotec_necesariaalinicio";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_parametrizacionexplotacion</summary>
        /// <remarks>Parametrización de la explotación en la que se parametriza la actividad.</remarks>
        public const string HidrotecParamexplotacionId = "hidrotec_paramexplotacionid";
        /// <summary>Type: Integer, RequiredLevel: ApplicationRequired, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Plazos en los que se desarrolla la actividad de tercero.</remarks>
        public const string HidrotecPlazo = "hidrotec_plazo";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica cuál es el plazo máximo para acordar una cita.</remarks>
        public const string HidrotecPlazomaximoparaacordarcita = "hidrotec_plazomaximoparaacordarcita";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica cuál es el plazo máximo para crear una nueva cita tras la cancelación de una cita anterior.</remarks>
        public const string HidrotecPlazonuevacitatrascancelacion = "hidrotec_plazonuevacitatrascancelacion";
        /// <summary>Type: Boolean, RequiredLevel: ApplicationRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo Sí/No para contabilizar el tiempo de un plazo en horas.</remarks>
        public const string HidrotecPlazoporhoras = "hidrotec_plazoporhoras";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Franja de tiempo en horas en el que se puede realizar el preaviso mínimo de la cancelación de la cita.</remarks>
        public const string HidrotecPlazopreavisominimocancelacion = "hidrotec_plazopreavisominimocancelacion";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_procesodenegocio</summary>
        /// <remarks>Proceso de negocio en el que se parametriza la actividad.</remarks>
        public const string HidrotecProcesodenegocioId = "hidrotec_procesodenegocioid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Parametrización Actividades</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_maestroderoles</summary>
        /// <remarks>Rol del contacto para realizar la actividad.</remarks>
        public const string HidrotecRolId = "hidrotec_rolid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipodecliente</summary>
        /// <remarks>Subtipo de cliente del contrato.</remarks>
        public const string HidrotecSubtipoclienteId = "hidrotec_subtipoclienteid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo actividad CRM, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones de las actividad de CRM.</remarks>
        public const string HidrotecTipoactividadcrm = "hidrotec_tipoactividadcrm";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo actividad de tercero, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones en el que se recogen las distintas actividades de tercero.</remarks>
        public const string HidrotecTipoactividadtercero = "hidrotec_tipoactividadtercero";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipodecliente</summary>
        /// <remarks>Tipo de cliente del contrato.</remarks>
        public const string HidrotecTipoclienteId = "hidrotec_tipoclienteid";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Tipo de actividad, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que distingue si la Actividad es de tercero o de CRM.</remarks>
        public const string HidrotecTipodeactividad = "hidrotec_tipodeactividad";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de días, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si los días son naturales o laborales.</remarks>
        public const string HidrotecTipodias = "hidrotec_tipodias";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de días, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si el tipo de días para la cita son naturales o laborales.</remarks>
        public const string HidrotecTipodiascita = "hidrotec_tipodiascita";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecEstadoinicialdocumento_OptionSet
        {
            PdterecibirdeContactO = 500000000,
            PdteenviaraContactO = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipoactividadcrm_OptionSet
        {
            Cita = 1,
            Correoelectronico = 2,
            CorreoPostal = 3,
            Intentodellamadasagotado = 4,
            Llamada = 5,
            Notificacion = 6,
            NotificacionAPP = 7,
            Notificacionorganismooficial = 8,
            SMS = 9,
            Visitaoficina = 10
        }
        public enum HidrotecTipoactividadtercero_OptionSet
        {
            Pago = 4,
            ObraAcondicionamiento = 2,
            Otros = 5,
            Autolectura = 3,
            Aportardocumento = 1
        }
        public enum HidrotecTipodeactividad_OptionSet
        {
            Actividaddetercero = 1,
            ActividadCRM = 2
        }
        public enum HidrotecTipodias_OptionSet
        {
            Laborables = 1,
            Naturales = 2
        }
        public enum HidrotecTipodiascita_OptionSet
        {
            Laborables = 1,
            Naturales = 2
        }
    }

    /// <summary>DisplayName: Parametrización Alarmas, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Parametrización de las alarmas en función de la parametrización de la explotación.</remarks>
    public static class HidrotecParametrizacionalarmas
    {
        public const string EntityName = "hidrotec_parametrizacionalarmas";
        public const string EntityCollectionName = "hidrotec_parametrizacionalarmases";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_parametrizacionalarmasid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Check que indica si en esa Explotación si la parametrización se mide en horas.</remarks>
        public const string HidrotecAlarmaporhoras = "hidrotec_alarmaporhoras";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Indica el tipo de comunicación que hay que enviar.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Comunicación Masiva App</remarks>
        public const string HidrotecComunicacionapp = "hidrotec_comunicacionapp";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Comunicación masiva de SMS</remarks>
        public const string HidrotecComunicacionsms = "hidrotec_comunicacionsms";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica que la alarma se envía en un plazo de días determinado.</remarks>
        public const string HidrotecDiasevento = "hidrotec_diasevento";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_documento</summary>
        /// <remarks>Indica el tipo de documento que se debe enviar.</remarks>
        public const string HidrotecDocumentoId = "hidrotec_documentoid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Enviar a explotación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica que la alarma se envía a la explotación, y dentro de esta si se requiere algún equipo concreto.</remarks>
        public const string HidrotecEnviaraexplotacion = "hidrotec_enviaraexplotacion";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Check que indica si se envía la alarma al CAC o no.</remarks>
        public const string HidrotecEnviaralcac = "hidrotec_enviaralcac";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Enviar al contacto a través del canal de alarmas del expediente.</remarks>
        public const string HidrotecEnviaralContactO = "hidrotec_enviaralcontacto";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Parametrización Alarmas</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_estadoexpediente</summary>
        /// <remarks>Estado en que se debe estar el expediente para que se pueda lanzar la alarma.</remarks>
        public const string HidrotecEstadoexpedienteId = "hidrotec_estadoexpedienteid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Evento, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Evento que se tiene que cumplir para la alarma.</remarks>
        public const string HidrotecEvento = "hidrotec_evento";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_parametrizacionexplotacion</summary>
        /// <remarks>Indica el proceso y la explotación a la que está asociada la alarma.</remarks>
        public const string HidrotecParamexplotacionId = "hidrotec_paramexplotacionid";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_procesodenegocio</summary>
        /// <remarks>Proceso de negocio al que pertenece la parametrización de la alarma.</remarks>
        public const string HidrotecProcesodenegocioId = "hidrotec_procesodenegocioid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Parametrización Alarmas</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo Sí/No para decidir si hay que enviar un recordatorio de la comunicación o no.</remarks>
        public const string HidrotecRecordatoriodocumento = "hidrotec_recordatoriodocumento";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo período, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si la alarma se envía en un periodo concreto o no.</remarks>
        public const string HidrotecTipoperiodo = "hidrotec_tipoperiodo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecEnviaraexplotacion_OptionSet
        {
            No = 0,
            Gestorresponsable = 1,
            Gestorresponsableyequipoescalado1 = 2,
            Gestorresponsableyequipoescalado1y2 = 3
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecEvento_OptionSet
        {
            Diasnaturalestranscurridos = 1,
            Diasnaturalespendientes = 2,
            Retrasodiasnaturales = 3,
            Diaslaborablestranscurridos = 4,
            Diaslaborablespendientes = 5,
            Retrasodiaslaborables = 6,
            Horaslaborablestranscurridas = 7,
            Horaslaborablespendientes = 8,
            Retrasohoraslaborables = 9,
            Horasnaturalestranscurridas = 10,
            Horasnaturalespendientes = 11,
            Retrasohorasnaturales = 12
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipoperiodo_OptionSet
        {
            Generalexpediente = 1,
            Estadoexpediente = 2,
            AdjuntardocumentoTercero = 3,
            AportarautolecturaTercero = 5,
            RealizarpagoTercero = 4,
            RealizarobraoacondicionamientoTercero = 6,
            OtroTercero = 11,
            EjecutarOTAqualia = 7,
            DilacionCACAqualia = 8,
            RealizartareaAqualia = 9,
            Dilaciongeneral = 10
        }
    }

    /// <summary>DisplayName: Parametrización Explotación, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa la parametrización de la explotación en función de la tipificación.</remarks>
    public static class HidrotecParametrizacionexplotacion
    {
        public const string EntityName = "hidrotec_parametrizacionexplotacion";
        public const string EntityCollectionName = "hidrotec_parametrizacionexplotacions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_parametrizacionexplotacionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Actividades de tercero, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Campo que indica si hay o no actividades de tercero y de donde proceden.</remarks>
        public const string HidrotecActividadesdetercero = "hidrotec_actividadesdetercero";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo de Revisión Aprobación que marca si se requiere aprobación.</remarks>
        public const string HidrotecAprobacion = "hidrotec_aprobacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Campo lookup que indica quién es el el aprobador.</remarks>
        public const string HidrotecAprobador = "hidrotec_aprobador";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo que indica si el proceso y la explotación necesitan los datos de autolectura.</remarks>
        public const string HidrotecAutolecturanecesaria = "hidrotec_autolecturanecesaria";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Campo booleano que indica si se puede realizer la cesión con deuda o no.</remarks>
        public const string HidrotecCesioncondeuda = "hidrotec_cesioncondeuda";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si el expediente se cerrará automáticamente tras completar todas las actividades de tercero asociadas.</remarks>
        public const string HidrotecCierreexpacttercerosterminadas = "hidrotec_cierreexpacttercerosterminadas";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Comunicación masiva APP</remarks>
        public const string HidrotecComunicacionapp = "hidrotec_comunicacionapp";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Comunicación masiva de SMS</remarks>
        public const string HidrotecComunicacionsms = "hidrotec_comunicacionsms";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Entrega de documentación, OptionSetType: Picklist, DefaultFormValue: 2</summary>
        /// <remarks>Conjunto de opciones que indica el momento en el que se debe entregar la documentación, al inicio o al fin.</remarks>
        public const string HidrotecEntregadedocumentacion = "hidrotec_entregadedocumentacion";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Parametrización Explotación</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Campo Lookup que informa de la explotación a la que afecta la parametrización.</remarks>
        public const string HidrotecExplotacionId = "hidrotec_explotacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Se refiere a la hora del día laborable a partir de la que dejará de contabilizar el cálculo de plazos en el caso de que estos sean medidos en horas</remarks>
        public const string HidrotecHorafinatencion = "hidrotec_horafinatencion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Se refiere a la hora del día laborable a partir de la que contabilizará el cálculo de plazos en el caso de que estos sean medidos en horas</remarks>
        public const string HidrotecHorainicioatencion = "hidrotec_horainicioatencion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Integer, RequiredLevel: ApplicationRequired, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica el plazo en el que se cumple el proceso por parametrización de la explotación.</remarks>
        public const string HidrotecPlazo = "hidrotec_plazo";
        /// <summary>Type: Boolean, RequiredLevel: ApplicationRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica el plazo por horas en el que se tiene que desarrollar ese proceso por explotación.</remarks>
        public const string HidrotecPlazoporhoras = "hidrotec_plazoporhoras";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica el plazo máximo en el que se puede realizar una subrogación.</remarks>
        public const string HidrotecPlazosubrogacion = "hidrotec_plazosubrogacion";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Parametrización Explotación</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica el número de intentos de llamadas que se llevan a cabo en ese proceso dependiendo de la explotación.</remarks>
        public const string HidrotecReintentosllamada = "hidrotec_reintentosllamada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_relacion</summary>
        /// <remarks>Primer nivel de la tipificación.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo que indica si en ese proceso require o no inspección</remarks>
        public const string HidrotecRequiereinspeccion = "hidrotec_requiereinspeccion";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo que indica si requiere presupuesto o no.</remarks>
        public const string HidrotecRequierepresupuesto = "hidrotec_requierepresupuesto";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si requiere ese proceso por explotación de la revisión del supervisor o no.</remarks>
        public const string HidrotecRevisionsupervisor = "hidrotec_revisionsupervisor";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si requiere la revisión supervisor en el CAC para ese proceso en un determinada explotación.</remarks>
        public const string HidrotecRevisionsupervisorcac = "hidrotec_revisionsupervisorcac";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        /// <remarks>Tercer nivel de la tipificación.</remarks>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Indica quién es el supervisor en ese proceso concreto en una explotación determinada.</remarks>
        public const string HidrotecSupervisor = "hidrotec_supervisor";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Segundo nivel de la tipificación.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de días, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica el tipo de días en los que se debe desarrollar el proceso en una determinada explotación.</remarks>
        public const string HidrotecTipodias = "hidrotec_tipodias";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tras Vencimiento, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que marca si el proceso se hace por Cesión o por puede elegir entre Cesión o Cambio de Titular Baja/Alta.</remarks>
        public const string HidrotecTrasvencimiento = "hidrotec_trasvencimiento";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecActividadesdetercero_OptionSet
        {
            No = 1,
            OrigenCRM = 2,
            OrigenCRMyDiversa = 3
        }
        public enum HidrotecEntregadedocumentacion_OptionSet
        {
            Inicio = 1,
            Fin = 2
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecHorafinatencion_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecHorainicioatencion_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipodias_OptionSet
        {
            Laborables = 1,
            Naturales = 2
        }
        public enum HidrotecTrasvencimiento_OptionSet
        {
            Cesion = 500000000,
            CesionoCambiodeTitularbajaAlta = 2
        }
    }

    /// <summary>DisplayName: Pedanía, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Callejero en el que viene recogida la información referida a la Pedanía.</remarks>
    public static class HidrotecPedania
    {
        public const string EntityName = "hidrotec_pedania";
        public const string EntityCollectionName = "hidrotec_pedanias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_pedaniaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Pedanía</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código de identificación de la Pedanía.</remarks>
        public const string HidrotecIdpedania = "hidrotec_idpedania";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Lookup de los Municipios que se encuentran en el callejero y al que pertenece la Pedanía.</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Pedanía</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Pendiente obra alta acometida, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Entidad de Pendiente de Obra de Alta de Acometida.</remarks>
    public static class HidrotecPendienteobraaltaacometida
    {
        public const string EntityName = "hidrotec_pendienteobraaltaacometida";
        public const string EntityCollectionName = "hidrotec_pendienteobraaltaacometidas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la actividad.</remarks>
        public const string PrimaryKey = "activityid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Tema asociado con la actividad.</remarks>
        public const string PrimaryName = "subject";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Conjunto de opciones Sí/No que determina si la actividad se ha revisado o no.</remarks>
        public const string HidrotecActividadrevisada = "hidrotec_actividadrevisada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la actividad.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el puntero de actividad.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mailbox</summary>
        /// <remarks>Identificador único del buzón asociado con remitente del correo electrónico.</remarks>
        public const string SenderMailBoxId = "sendermailboxid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal social, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Muestra cómo se originó el contacto sobre la actividad social, por ejemplo, en Twitter o Facebook. Este campo es de solo lectura.</remarks>
        public const string Community = "community";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio real de la actividad.</remarks>
        public const string Actualstart = "actualstart";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Campo de búsqueda que determina la comunicación.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Dejó el correo de voz</remarks>
        public const string LeftvoiceMail = "leftvoicemail";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la actividad.</remarks>
        public const string Description = "description";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al puntero de actividad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración programada de la actividad en minutos.</remarks>
        public const string Scheduleddurationminutes = "scheduleddurationminutes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración real de la actividad en minutos.</remarks>
        public const string Actualdurationminutes = "actualdurationminutes";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string IsmapiPrivate = "ismapiprivate";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Información que especifica si la actividad es un tipo de evento o de actividad regular.</remarks>
        public const string Isregularactivity = "isregularactivity";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se creó a partir de una regla de flujo de trabajo.</remarks>
        public const string IsworkflowCreated = "isworkflowcreated";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se facturó como parte de la resolución de un caso.</remarks>
        public const string Isbilled = "isbilled";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estados actividades, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones que indica el estado de la actividad.</remarks>
        public const string HidrotecEstadoactividad = "hidrotec_estadoactividad";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado de la actividad, OptionSetType: State</summary>
        /// <remarks>Estado de la actividad.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la actividad.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se envió la actividad.</remarks>
        public const string Senton = "senton";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio programada de la actividad.</remarks>
        public const string Scheduledstart = "scheduledstart";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se intentó la entrega de la actividad por última vez.</remarks>
        public const string DeliveryLastAttemptedon = "deliverylastattemptedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización programada de la actividad.</remarks>
        public const string Scheduledend = "scheduledend";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización real de la actividad.</remarks>
        public const string Actualend = "actualend";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único que especifica el identificador de serie periódica de una instancia.</remarks>
        public const string SeriesId = "seriesid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el puntero de actividad por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la actividad por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la actividad.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 8192</summary>
        /// <remarks>Información adicional proporcionada por la aplicación externa como JSON. Solo para uso interno.</remarks>
        public const string Activityadditionalparams = "activityadditionalparams";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra durante cuánto tiempo, en minutos, se retuvo el registro.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de la actividad.</remarks>
        public const string PriorityCode = "prioritycode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad de entrega, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de entrega de la actividad al servidor de correo electrónico.</remarks>
        public const string DeliverypriorityCode = "deliveryprioritycode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Identificador único del usuario o equipo propietario de la actividad.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado de la actividad.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: account,bookableresourcebooking,bookableresourcebookingheader,bulkoperation,campaign,campaignactivity,contact,contract,entitlement,entitlementtemplate,hidrotec_campoimagen,hidrotec_logerrorintegracion,hidrotec_ov_usuario,hidrotec_usd_mensajes,incident,invoice,knowledgearticle,knowledgebaserecord,lead,msdyn_postalbum,opportunity,quote,salesorder,uii_action,uii_hostedapplication,uii_nonhostedapplication,uii_option,uii_savedsession,uii_workflow,uii_workflowstep,uii_workflow_workflowstep_mapping</summary>
        /// <remarks>Identificador único del objeto al que está asociada la actividad.</remarks>
        public const string RegardingobjectId = "regardingobjectid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidName = "regardingobjectidname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidyomiName = "regardingobjectidyominame";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        public const string RegardingobjectTypeCode = "regardingobjecttypecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_relacion</summary>
        /// <remarks>Relación del expediente. . Primer nivel de la tipificación.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string PostponeactivityProcessingUntil = "postponeactivityprocessinguntil";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: service</summary>
        /// <remarks>Identificador único de un servicio asociado.</remarks>
        public const string ServiceId = "serviceid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro de caso.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        /// <remarks>Subtipo del expediente. . Tercer nivel de la tipificación.</remarks>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Tipo del expediente. . Segundo nivel de la tipificación.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired, DisplayName: Tipo de actividad, OptionSetType: Picklist</summary>
        /// <remarks>Tipo de actividad.</remarks>
        public const string ActivityTypeCode = "activitytypecode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tasa de cambio de la divisa asociada al puntero de actividad con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipo de cita, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de instancia de una serie periódica.</remarks>
        public const string InstanceTypeCode = "instancetypecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la actividad por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Último SLA que se aplicó a este caso. Este campo es solo para uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria de la actividad.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        public enum Community_OptionSet
        {
            Facebook = 1,
            Twitter = 2,
            Otro = 0
        }
        public enum HidrotecEstadoactividad_OptionSet
        {
            Pendiente = 1,
            Completada = 2,
            Cancelada = 3
        }
        public enum StateCode_OptionSet
        {
            Abierto = 0,
            Completado = 1,
            Cancelado = 2,
            Programado = 3
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum PriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum DeliverypriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum StatusCode_OptionSet
        {
            Abierto = 1,
            Completado = 2,
            Cancelado = 3,
            Programado = 4
        }
        public enum RegardingobjectTypeCode_OptionSet
        {
        }
        public enum ActivityTypeCode_OptionSet
        {
            Fax = 4204,
            Llamadadetelefono = 4210,
            Correoelectronico = 4202,
            Carta = 4207,
            Cita = 4201,
            Actividaddeservicio = 4214,
            Cierredeoportunidad = 4208,
            Cierredepedido = 4209,
            Cierredeoferta = 4211,
            Resoluciondelcaso = 4206,
            Tarea = 4212,
            Respuestadecampana = 4401,
            Actividaddelacampana = 4402,
            Operacionmasiva = 4406,
            Citaperiodica = 4251,
            Intentodellamadasagotado = 10073,
            Notificacion = 10121,
            NotificacionOrganismoOficial = 10124,
            SMS = 10125,
            VisitaOficina = 10128,
            NotificacionAPP = 10151,
            Pendienteobraaltaacometida = 10153
        }
        public enum InstanceTypeCode_OptionSet
        {
            Noperiodica = 0,
            Periodicamaestra = 1,
            Instanciaperiodica = 2,
            Excepcionperiodica = 3,
            Excepcionfuturaperiodica = 4
        }
    }

    /// <summary>DisplayName: Período, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que recoge todos los periodos que se ha generado en CRM.</remarks>
    public static class HidrotecPeriodo
    {
        public const string EntityName = "hidrotec_periodo";
        public const string EntityCollectionName = "hidrotec_periodos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_periodoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_actividaddetercero</summary>
        /// <remarks>Campo de búsqueda de las actividades de tercero.</remarks>
        public const string HidrotecActividaddeterceroId = "hidrotec_actividaddeterceroid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica los días laborales pendientes que tiene el periodo.</remarks>
        public const string HidrotecDiaslaborablespendientes = "hidrotec_diaslaborablespendientes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica los días laborables que han transcurrido desde la fecha de inicio.</remarks>
        public const string HidrotecDiaslaborablestranscurridos = "hidrotec_diaslaborablestranscurridos";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica los días naturales pendientes que tiene el periodo.</remarks>
        public const string HidrotecDiasnaturalespendientes = "hidrotec_diasnaturalespendientes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica el número de días naturales transcurridos desde la fecha de inicio.</remarks>
        public const string HidrotecDiasnaturalestranscurrido = "hidrotec_diasnaturalestranscurrido";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica la posible dilación que puede tener el periodo.</remarks>
        public const string HidrotecDilacion = "hidrotec_dilacion";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Período</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_estadoexpediente</summary>
        /// <remarks>Indica el estado del expediente en el que se produce ese periodo.</remarks>
        public const string HidrotecEstadoexpedienteId = "hidrotec_estadoexpedienteid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Período</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Indica el número de expediente al que está asociado el periodo.</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Indica la fecha en la que el periodo debe concluir.</remarks>
        public const string HidrotecFechafin = "hidrotec_fechafin";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha en la que el periodo comienza a contar.</remarks>
        public const string HidrotecFechainicio = "hidrotec_fechainicio";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Fecha en la que el plazo del periodo concluye.</remarks>
        public const string HidrotecFechavencimiento = "hidrotec_fechavencimiento";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Indica el cálculo de las horas laborables pendientes del periodo.</remarks>
        public const string HidrotecHoraslaborablespendientes = "hidrotec_horaslaborablespendientes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Resultado del cálculo de las horas laborables transcurridas desde que se inició el periodo.</remarks>
        public const string HidrotecHoraslaborablestranscurridas = "hidrotec_horaslaborablestranscurridas";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Indica el cálculo de las horas naturales pendientes del periodo.</remarks>
        public const string HidrotecHorasnaturalespendientes = "hidrotec_horasnaturalespendientes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Resultado del cálculo de las horas naturales transcurridas desde que se inició el periodo.</remarks>
        public const string HidrotecHorasnaturalestranscurridas = "hidrotec_horasnaturalestranscurridas";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Indica el retraso de las horas naturales del periodo.</remarks>
        public const string HidrotecHorasretrasonaturales = "hidrotec_horasretrasonaturales";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Indica la fecha presente.</remarks>
        public const string HidrotecHoy = "hidrotec_hoy";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Plazo de tiempo en el que se desarrolla el periodo.</remarks>
        public const string HidrotecPlazo = "hidrotec_plazo";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si el período será calculado por horas en vez de por días.</remarks>
        public const string HidrotecPlazoporhoras = "hidrotec_plazoporhoras";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Retraso en días laborables de la fecha fin prevista.</remarks>
        public const string HidrotecRetrasodiaslaborables = "hidrotec_retrasodiaslaborables";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica el resultado del retraso de los días naturales con la fecha fin.</remarks>
        public const string HidrotecRetrasodiasnaturales = "hidrotec_retrasodiasnaturales";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Indica el retraso de las horas laborables del periodo.</remarks>
        public const string HidrotecRetrasohoraslaborables = "hidrotec_retrasohoraslaborables";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Indica el cálculo de las horas naturales pendientes del periodo.</remarks>
        public const string HidrotecRetrasohorasnaturales = "hidrotec_retrasohorasnaturales";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo período, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones de los periodos existentes.</remarks>
        public const string HidrotecTipodeperiodo = "hidrotec_tipodeperiodo";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de días, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica los tipos de día. Naturales y Laborales.</remarks>
        public const string HidrotecTipodias = "hidrotec_tipodias";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Resultado de la última hora procesada del periodo.</remarks>
        public const string HidrotecUltimahoraprocesada = "hidrotec_ultimahoraprocesada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Sininiciar = 1,
            Iniciado = 500000000,
            Terminado = 500000001,
            Inactivo = 2
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum HidrotecTipodeperiodo_OptionSet
        {
            Generalexpediente = 1,
            Estadoexpediente = 2,
            AdjuntardocumentoTercero = 3,
            AportarautolecturaTercero = 5,
            RealizarpagoTercero = 4,
            RealizarobraoacondicionamientoTercero = 6,
            OtroTercero = 11,
            EjecutarOTAqualia = 7,
            DilacionCACAqualia = 8,
            RealizartareaAqualia = 9,
            Dilaciongeneral = 10
        }
        public enum HidrotecTipodias_OptionSet
        {
            Laborables = 1,
            Naturales = 2
        }
    }

    /// <summary>DisplayName: Plantilla de artículo, OwnershipType: OrganizationOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Plantilla para un artículo de Knowledge Base que contiene los atributos estándar de un artículo.</remarks>
    public static class Kbarticletemplate
    {
        public const string EntityName = "kbarticletemplate";
        public const string EntityCollectionName = "kbarticletemplates";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la plantilla de artículo de Knowledge Base.</remarks>
        public const string PrimaryKey = "kbarticletemplateid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 500, Format: Text</summary>
        /// <remarks>Título de la plantilla de artículo de Knowledge Base.</remarks>
        public const string PrimaryName = "title";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la plantilla de artículo de Knowledge Base.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó la plantilla del artículo de KB.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la plantilla de artículo de Knowledge Base.</remarks>
        public const string Description = "description";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Especifica si está activo el artículo de Knowledge Base.</remarks>
        public const string Isactive = "isactive";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Estado del componente, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string ComponentState = "componentstate";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la plantilla de artículo de Knowledge Base.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la plantilla de artículo de Knowledge Base por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: SystemRequired, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string Overwritetime = "overwritetime";
        /// <summary>Type: Integer, RequiredLevel: ApplicationRequired, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Idioma de la plantilla de artículo</remarks>
        public const string LanguageCode = "languagecode";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        public const string Ismanaged = "ismanaged";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string Kbarticletemplateidunique = "kbarticletemplateidunique";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó la plantilla de artículo de KB por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la plantilla de artículo de Knowledge Base por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: organization</summary>
        /// <remarks>Identificador único de la organización asociada con la plantilla.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: ManagedProperty, RequiredLevel: SystemRequired</summary>
        /// <remarks>Información que especifica si se puede personalizar este componente.</remarks>
        public const string Iscustomizable = "iscustomizable";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la solución asociada.</remarks>
        public const string SolutionId = "solutionid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string SupportingsolutionId = "supportingsolutionid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 48, Format: VersionNumber</summary>
        /// <remarks>Versión en la que se introduce el formulario.</remarks>
        public const string IntroducedVersion = "introducedversion";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 1073741823</summary>
        /// <remarks>Estructura XML del artículo de Knowledge Base.</remarks>
        public const string Structurexml = "structurexml";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 1073741823</summary>
        /// <remarks>Formato XML de la plantilla de artículo de Knowledge Base.</remarks>
        public const string Formatxml = "formatxml";
        public enum ComponentState_OptionSet
        {
            Publicado = 0,
            Sinpublicar = 1,
            Eliminado = 2,
            Eliminadossinpublicar = 3
        }
    }

    /// <summary>DisplayName: Plantilla tarifa, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Indica el tipo de plantilla de tarifa.</remarks>
    public static class HidrotecPlantillatarifa
    {
        public const string EntityName = "hidrotec_plantillatarifa";
        public const string EntityCollectionName = "hidrotec_plantillatarifas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_plantillatarifaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Calibre contador, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecCalibrecontador = "hidrotec_calibrecontador";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecCalibrecontadortexto = "hidrotec_calibrecontadortexto";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Plantilla tarifa</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Indica el código de la plantilla de tarifa.</remarks>
        public const string HidrotecIdplantillatarifa = "hidrotec_idplantillatarifa";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Plantilla tarifa</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecCalibrecontador_OptionSet
        {
            _13mm = 13,
            _15mm = 15,
            _20mm = 20,
            _25mm = 25,
            _30mm = 30,
            _40mm = 40,
            _50mm = 50,
            Otros = 0
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Plazo Estado Expediente, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Parametrización de los estados de Expediente.</remarks>
    public static class HidrotecPlazoestadoexpediente
    {
        public const string EntityName = "hidrotec_plazoestadoexpediente";
        public const string EntityCollectionName = "hidrotec_plazoestadoexpedientes";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_plazoestadoexpedienteid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Plazo Estado Expediente</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_estadoexpediente</summary>
        /// <remarks>Indica el estado del expediente en el que se centra el plazo del proceso.</remarks>
        public const string HidrotecEstadoexpedienteId = "hidrotec_estadoexpedienteid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_parametrizacionexplotacion</summary>
        /// <remarks>Indica la parametrización para ajustar el plazo según la explotación.</remarks>
        public const string HidrotecParamexplotacionId = "hidrotec_paramexplotacionid";
        /// <summary>Type: Integer, RequiredLevel: ApplicationRequired, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica el plazo en horas del tiempo que tiene que estar en ese estado.</remarks>
        public const string HidrotecPlazo = "hidrotec_plazo";
        /// <summary>Type: Boolean, RequiredLevel: ApplicationRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si hay que tener un plazo en horas para el estado en un proceso concreto.</remarks>
        public const string HidrotecPlazoporhoras = "hidrotec_plazoporhoras";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_procesodenegocio</summary>
        /// <remarks>Proceso de negocio en el que se ajusta los plazos del estado del expediente.</remarks>
        public const string HidrotecProcesodenegocioId = "hidrotec_procesodenegocioid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Plazo Estado Expediente</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de días, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que distingue entre laborales y naturales.</remarks>
        public const string HidrotecTipodias = "hidrotec_tipodias";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipodias_OptionSet
        {
            Laborables = 1,
            Naturales = 2
        }
    }

    /// <summary>DisplayName: Presupuesto, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa un presupuesto en CRM.</remarks>
    public static class HidrotecPresupuesto
    {
        public const string EntityName = "hidrotec_presupuesto";
        public const string EntityCollectionName = "hidrotec_presupuestos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_presupuestoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada a la entidad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Presupuesto</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo que indica el estado en el que se encuentra el presupuesto.</remarks>
        public const string HidrotecEstadopresupuesto = "hidrotec_estadopresupuesto";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: incident</summary>
        /// <remarks>Campo lookup en el que viene informado el expediente al que pertenece el presupuesto.</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4, CalculationOf: hidrotec_importe</summary>
        /// <remarks>Valor de Importe en divisa base.</remarks>
        public const string HidrotecImportEBase = "hidrotec_importe_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 4</summary>
        /// <remarks>Importe al que asciende el presupuesto.</remarks>
        public const string HidrotecImportE = "hidrotec_importe";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código del presupuesto.</remarks>
        public const string HidrotecNumeropresupuesto = "hidrotec_numeropresupuesto";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Presupuesto</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tipo de cambio de la divisa asociada a la entidad en relación con la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Proceso de negocio, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad que recoge la parametrización general de cada proceso.</remarks>
    public static class HidrotecProcesodenegocio
    {
        public const string EntityName = "hidrotec_procesodenegocio";
        public const string EntityCollectionName = "hidrotec_procesodenegocios";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_procesodenegocioid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Actividades de tercero, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que define si determinado proceso cuenta o no con Actividades de Tercero y y cuál es su origen.</remarks>
        public const string HidrotecActividadesdetercero = "hidrotec_actividadesdetercero";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Proceso de negocio</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_estadoexpediente</summary>
        /// <remarks>primer paso que tiene el proceso.</remarks>
        public const string HidrotecEstadoinicialId = "hidrotec_estadoinicialid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Se refiere a la hora del día laborable a partir de la que dejará de contabilizar el cálculo de plazos en el caso de que estos sean medidos en horas</remarks>
        public const string HidrotecHorafinatencion = "hidrotec_horafinatencion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Horas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Se refiere a la hora del día laborable a partir de la que contabilizará el cálculo de plazos en el caso de que estos sean medidos en horas</remarks>
        public const string HidrotecHorainicioatencion = "hidrotec_horainicioatencion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Código que distingue un proceso de otro.</remarks>
        public const string HidrotecIdprocesodenegocio = "hidrotec_idprocesodenegocio";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Integer, RequiredLevel: ApplicationRequired, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Conjunto de opciones del plazo en días y horas.</remarks>
        public const string HidrotecPlazo = "hidrotec_plazo";
        /// <summary>Type: Boolean, RequiredLevel: ApplicationRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Conjunto de opciones sí/no para poder definir el plazo en horas.</remarks>
        public const string HidrotecPlazoporhoras = "hidrotec_plazoporhoras";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Proceso de negocio</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Campo de texto que refleja los reintentos de llamadas que se pueden realizar en un proceso determinado.</remarks>
        public const string HidrotecReintentosllamada = "hidrotec_reintentosllamada";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo de días, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de ideas que distingue entre los tipos de días.</remarks>
        public const string HidrotecTipodias = "hidrotec_tipodias";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecActividadesdetercero_OptionSet
        {
            No = 1,
            OrigenCRM = 2,
            OrigenCRMyDiversa = 3
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecHorafinatencion_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum HidrotecHorainicioatencion_OptionSet
        {
            _0000 = 0,
            _0030 = 30,
            _0100 = 60,
            _0130 = 90,
            _0200 = 120,
            _0230 = 150,
            _0300 = 180,
            _0330 = 210,
            _0400 = 240,
            _0430 = 270,
            _0500 = 300,
            _0530 = 330,
            _0600 = 360,
            _0630 = 390,
            _0700 = 420,
            _0730 = 450,
            _0800 = 480,
            _0830 = 510,
            _0900 = 540,
            _0930 = 570,
            _1000 = 600,
            _1030 = 630,
            _1100 = 660,
            _1130 = 690,
            _1200 = 720,
            _1230 = 750,
            _1300 = 780,
            _1330 = 810,
            _1400 = 840,
            _1430 = 870,
            _1500 = 900,
            _1530 = 930,
            _1600 = 960,
            _1630 = 990,
            _1700 = 1020,
            _1730 = 1050,
            _1800 = 1080,
            _1830 = 1110,
            _1900 = 1140,
            _1930 = 1170,
            _2000 = 1200,
            _2030 = 1230,
            _2100 = 1260,
            _2130 = 1290,
            _2200 = 1320,
            _2230 = 1350,
            _2300 = 1380,
            _2330 = 1410
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipodias_OptionSet
        {
            Laborables = 1,
            Naturales = 2
        }
    }

    /// <summary>DisplayName: Propietario, OwnershipType: None, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Grupo de usuarios del sistema y equipos no eliminados. Los propietarios pueden usarse para controlar el acceso a objetos específicos.</remarks>
    public static class Owner
    {
        public const string EntityName = "owner";
        public const string EntityCollectionName = "owners";
    }

    /// <summary>DisplayName: Provincia, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Callejero en el que viene recogida la información referida a la Provincia.</remarks>
    public static class HidrotecProvincia
    {
        public const string EntityName = "hidrotec_provincia";
        public const string EntityCollectionName = "hidrotec_provincias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_provinciaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunidadautonoma</summary>
        /// <remarks>Campo de búsqueda de las Comunidades Autónomas.</remarks>
        public const string HidrotecComunidadautonomaId = "hidrotec_comunidadautonomaid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Provincia</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código numérico de la provincia.</remarks>
        public const string HidrotecIdprovincia = "hidrotec_idprovincia";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Búsqueda de países.</remarks>
        public const string HidrotecPaisId = "hidrotec_paisid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Provincia</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Relación, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de relaciones. Primer nivel de la tipificación.</remarks>
    public static class HidrotecRelacion
    {
        public const string EntityName = "hidrotec_relacion";
        public const string EntityCollectionName = "hidrotec_relacions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_relacionid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Relación</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código númerico de la Relación.</remarks>
        public const string HidrotecIdrelacion = "hidrotec_idrelacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Relación</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Resolución, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de resoluciones</remarks>
    public static class HidrotecResolucion
    {
        public const string EntityName = "hidrotec_resolucion";
        public const string EntityCollectionName = "hidrotec_resolucions";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_resolucionid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Check que indica si la causa es obligada o no.</remarks>
        public const string HidrotecCausaobligada = "hidrotec_causaobligada";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Lookup de los tipos de comunicación.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Resolución</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código numérico de la Resolución.</remarks>
        public const string HidrotecIdresolucion = "hidrotec_idresolucion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Resolución</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Rol adicional, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Esta entidad se utiliza en los procesos de Alta de contratación y todos los correspondientes a Cambio de titular.</remarks>
    public static class HidrotecRoladicional
    {
        public const string EntityName = "hidrotec_roladicional";
        public const string EntityCollectionName = "hidrotec_roladicionals";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_roladicionalid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: contact</summary>
        /// <remarks>Campo de búsqueda de contactos.</remarks>
        public const string HidrotecContactOId = "hidrotec_contactoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Email</summary>
        /// <remarks>Campo correo electrónico para rol adicional</remarks>
        public const string HidrotecEMailRoladicional = "hidrotec_email_roladicional";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Rol adicional</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: incident</summary>
        /// <remarks>Campo de búsqueda de expediente.</remarks>
        public const string HidrotecExpedienteId = "hidrotec_expedienteid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo Fax para la entidad de rol adicional</remarks>
        public const string HidrotecFaxRoladicional = "hidrotec_fax_roladicional";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Rol adicional</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_maestroderoles</summary>
        /// <remarks>Campo de búsqueda de los roles de un contrato.</remarks>
        public const string HidrotecRolId = "hidrotec_rolid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo Teléfono fijo para la entidad de roles adicionales</remarks>
        public const string HidrotecTelefonofijoRoladicional = "hidrotec_telefonofijo_roladicional";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo Teléfono móvil para la entidad de roles adicionales</remarks>
        public const string HidrotecTelefonomovil = "hidrotec_telefonomovil";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Rol de contrato, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa el rol relacionado a un contrato.</remarks>
    public static class HidrotecRoldecontrato
    {
        public const string EntityName = "hidrotec_roldecontrato";
        public const string EntityCollectionName = "hidrotec_roldecontratos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_roldecontratoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo de conjunto de opciones que se selecciona cuando el contacto solicita dar de baja la domiciliación.</remarks>
        public const string HidrotecBajadomiciliacion = "hidrotec_bajadomiciliacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_banco</summary>
        /// <remarks>Lookup al maestro de bancos.</remarks>
        public const string HidrotecBancoId = "hidrotec_bancoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_barrio</summary>
        /// <remarks>Lookup a la entidad barrio del callejero.</remarks>
        public const string HidrotecBarrioId = "hidrotec_barrioid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo línea de texto para informar el bloque de la dirección.</remarks>
        public const string HidrotecBloque = "hidrotec_bloque";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal Comunicación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones del canal en el que se solicita la modificación en los roles.</remarks>
        public const string HidrotecCanalmodificacion = "hidrotec_canalmodificacion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 4, Format: Text</summary>
        /// <remarks>Campo de cuatro dígitos del código de banco.</remarks>
        public const string HidrotecCodigobanco = "hidrotec_codigobanco";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código interno de la dirección.</remarks>
        public const string HidrotecCodigodedireccion = "hidrotec_codigodedireccion";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de texto para introducir el código postal.</remarks>
        public const string HidrotecCodigoPostal = "hidrotec_codigopostal";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 4, Format: Text</summary>
        /// <remarks>Campo de cuatro dígitos para informar con el código de sucursal.</remarks>
        public const string HidrotecCodigosucursal = "hidrotec_codigosucursal";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Campo para seleccionar el contacto que ejercerá el rol.</remarks>
        public const string HidrotecContactOId = "hidrotec_contactoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_contrato</summary>
        /// <remarks>Contrato en el que el contacto ejercerá el rol</remarks>
        public const string HidrotecContratoId = "hidrotec_contratoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Campo para informar del email del contacto en el rol.</remarks>
        public const string HidrotecCorreoelectronico = "hidrotec_correoelectronico";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        public const string HidrotecDerechoarcocancelacion = "hidrotec_derechoarcocancelacion";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        public const string HidrotecDerechoarcooposicion = "hidrotec_derechoarcooposicion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2, Format: Text</summary>
        /// <remarks>Campo de dos dígitos de los dígitos de Control del Código Cuenta Cliente.</remarks>
        public const string HidrotecDigitocontrolccc = "hidrotec_digitocontrolccc";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2, Format: Text</summary>
        /// <remarks>Dígito de control del IBAN</remarks>
        public const string HidrotecDigitocontroliban = "hidrotec_digitocontroliban";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Dirección de texto unificada.</remarks>
        public const string HidrotecDireccioncompuesta = "hidrotec_direccioncompuesta";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada a la entidad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto de la dirección que indica el edificio.</remarks>
        public const string HidrotecEdificio = "hidrotec_edificio";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo sí/no en donde se informa si se envían los datos.</remarks>
        public const string HidrotecEnviardatos = "hidrotec_enviardatos";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de texto de la dirección que indica la escalera.</remarks>
        public const string HidrotecEscalera = "hidrotec_escalera";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Rol de contrato</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Campo para informar el FAX del contacto en el rol.</remarks>
        public const string HidrotecFax = "hidrotec_fax";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Indica la fecha límite.</remarks>
        public const string HidrotecFechalimite = "hidrotec_fechalimite";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica el IBAN completo.</remarks>
        public const string HidrotecIban = "hidrotec_iban";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: -922337203685477, MaxValue: 922337203685477, Precision: 2, CalculationOf: hidrotec_importemaximo</summary>
        /// <remarks>Valor de Importe máximo en divisa base.</remarks>
        public const string HidrotecImportEmaximoBase = "hidrotec_importemaximo_base";
        /// <summary>Type: Money, RequiredLevel: None, MinValue: 0, MaxValue: 922337203685477, Precision: 2</summary>
        /// <remarks>Importe máximo de domiciliación para los roles de cobro.</remarks>
        public const string HidrotecImportEmaximo = "hidrotec_importemaximo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Campo de búsqueda de la dirección que muestra el municipio.</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto para indicar el municipio cuando la dirección no sea del callejero.</remarks>
        public const string HidrotecMunicipio = "hidrotec_municipio";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de texto de la dirección que indica el número.</remarks>
        public const string HidrotecNumero = "hidrotec_numero";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de diez dígitos del número de cuenta en cuenta corriente.</remarks>
        public const string HidrotecNumerodecuenta = "hidrotec_numerodecuenta";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Origen modificación, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica el origen de la modificación (CRM o ETL).</remarks>
        public const string HidrotecOrigenmodificacion = "hidrotec_origenmodificacion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto de la dirección.</remarks>
        public const string HidrotecOtros = "hidrotec_otros";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo de búsqueda a País.</remarks>
        public const string HidrotecPaisId = "hidrotec_paisid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>Campo de tipo búsqueda que muestra el país IBAN de los datos bancarios.</remarks>
        public const string HidrotecPaisibanId = "hidrotec_paisibanid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pedania</summary>
        /// <remarks>Campo de búsqueda a Pedanía</remarks>
        public const string HidrotecPedaniaId = "hidrotec_pedaniaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo de texto de la dirección para informar el piso.</remarks>
        public const string HidrotecPiso = "hidrotec_piso";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_provincia</summary>
        /// <remarks>Campo de búsqueda a Provincia</remarks>
        public const string HidrotecProvinciaId = "hidrotec_provinciaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de la dirección para informar la provincia cuando no sea del callejero.</remarks>
        public const string HidrotecProvincia = "hidrotec_provincia";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Campo de texto de la dirección que indica la puerta.</remarks>
        public const string HidrotecPuerta = "hidrotec_puerta";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Rol de contrato</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_maestroderoles</summary>
        /// <remarks>Campo de búsqueda de Rol Contrato en el que se recogen todos los roles.</remarks>
        public const string HidrotecRolId = "hidrotec_rolid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Conjunto de opciones Sí/No que indica la sincronización.</remarks>
        public const string HidrotecSincronizado = "hidrotec_sincronizado";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Campo de información de contacto para el teléfono fijo.</remarks>
        public const string HidrotecTelefonofijo = "hidrotec_telefonofijo";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Campo de información de contacto para el teléfono móvil.</remarks>
        public const string HidrotecTelefonomovil = "hidrotec_telefonomovil";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tipo de cambio de la divisa asociada a la entidad en relación con la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: TimeZoneIndependent</summary>
        /// <remarks>Campo de fecha que registra la fecha de la última sincronización.</remarks>
        public const string HidrotecUltimasincronizacion = "hidrotec_ultimasincronizacion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_via</summary>
        /// <remarks>Campo de búsqueda a Vía</remarks>
        public const string HidrotecViaId = "hidrotec_viaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo de texto que hay que informar cuando la dirección no sea del callejero.</remarks>
        public const string HidrotecVia = "hidrotec_via";
        public enum HidrotecCanalmodificacion_OptionSet
        {
            APP = 0,
            OficinaPresencial = 1,
            OficinaVirtual = 2,
            LlamadaCAC = 3,
            CorreoElectronico = 4,
            CorreoPostal = 5,
            LlamadaOficina = 6,
            Fax = 8,
            NotificaciondeOrganismoOficial = 9,
            IVR = 10,
            CorreoPostalMasivo = 11,
            SMS = 12,
            PersonalInterno = 13,
            CargaMasiva = 14
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecOrigenmodificacion_OptionSet
        {
            CRM = 1,
            ETL = 2
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Rol de seguridad, OwnershipType: BusinessOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Agrupación de privilegios de seguridad. Se asignan roles a los usuarios que les autorizan el acceso al sistema de Microsoft CRM.</remarks>
    public static class Role
    {
        public const string EntityName = "role";
        public const string EntityCollectionName = "roles";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del rol.</remarks>
        public const string PrimaryKey = "roleid";
        /// <summary>Type: String, RequiredLevel: SystemRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>Nombre de rol.</remarks>
        public const string PrimaryName = "name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el rol.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el rol.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si el componente de la solución forma parte de una solución administrada.</remarks>
        public const string Ismanaged = "ismanaged";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Estado del componente, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string ComponentState = "componentstate";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el rol.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el rol por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: SystemRequired, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string Overwritetime = "overwritetime";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string Roleidunique = "roleidunique";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el rol por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el rol por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión del rol.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la organización asociada con el rol.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: ManagedProperty, RequiredLevel: SystemRequired</summary>
        /// <remarks>Información que especifica si se puede personalizar este componente.</remarks>
        public const string Iscustomizable = "iscustomizable";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: roletemplate</summary>
        /// <remarks>Identificador único de la plantilla de rol asociada con el rol.</remarks>
        public const string RoletemplateId = "roletemplateid";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: role</summary>
        /// <remarks>Identificador único del rol de raíz primario.</remarks>
        public const string ParentRootroleId = "parentrootroleid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: role</summary>
        /// <remarks>Identificador único del rol primario.</remarks>
        public const string ParentRoleId = "parentroleid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la solución asociada.</remarks>
        public const string SolutionId = "solutionid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string SupportingsolutionId = "supportingsolutionid";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio asociada con el rol.</remarks>
        public const string BusinessUnitId = "businessunitid";
        public enum ComponentState_OptionSet
        {
            Publicado = 0,
            Sinpublicar = 1,
            Eliminado = 2,
            Eliminadossinpublicar = 3
        }
    }

    /// <summary>DisplayName: Rol Oficina, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Representa un rol de oficina</remarks>
    public static class HidrotecRoloficina
    {
        public const string EntityName = "hidrotec_roloficina";
        public const string EntityCollectionName = "hidrotec_roloficinas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_roloficinaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Código del Rol de Oficina.</remarks>
        public const string HidrotecCodigoId = "hidrotec_codigoid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Rol Oficina</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Rol Oficina</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: roleprivileges, OwnershipType: None, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Grupo de privilegios usado para ordenar por categorías a los usuarios con el fin de proporcionarles acceso adecuado a las entidades.</remarks>
    public static class Roleprivileges
    {
        public const string EntityName = "roleprivileges";
        public const string EntityCollectionName = "";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del privilegio de rol.</remarks>
        public const string PrimaryKey = "roleprivilegeid";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Estado del componente, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string ComponentState = "componentstate";
        /// <summary>Type: DateTime, RequiredLevel: SystemRequired, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string Overwritetime = "overwritetime";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        public const string Ismanaged = "ismanaged";
        /// <summary>Type: Integer, RequiredLevel: SystemRequired, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Atributo generado por el sistema que almacena los privilegios asociados con el rol.</remarks>
        public const string Privilegedepthmask = "privilegedepthmask";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del privilegio asociado con el rol.</remarks>
        public const string PrivilegeId = "privilegeid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del rol asociado con el privilegio de rol.</remarks>
        public const string RoleId = "roleid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string Roleprivilegeidunique = "roleprivilegeidunique";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la solución asociada.</remarks>
        public const string SolutionId = "solutionid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string SupportingsolutionId = "supportingsolutionid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum ComponentState_OptionSet
        {
            Publicado = 0,
            Sinpublicar = 1,
            Eliminado = 2,
            Eliminadossinpublicar = 3
        }
    }

    /// <summary>DisplayName: Sesion activa usd, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    public static class HidrotecSesionactivausd
    {
        public const string EntityName = "hidrotec_sesionactivausd";
        public const string EntityCollectionName = "hidrotec_sesionactivausds";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_sesionactivausdid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecActividadLlamadaId = "hidrotec_actividad_llamadaid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecAgentscript = "hidrotec_agentscript";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecCallId = "hidrotec_callid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecClienteasociadourl = "hidrotec_clienteasociadourl";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecContacidsesion = "hidrotec_contacidsesion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2500, Format: Url</summary>
        public const string HidrotecContactOurl = "hidrotec_contactourl";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecContractoidsesion = "hidrotec_contractoidsesion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecContratoasociadourl = "hidrotec_contratoasociadourl";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2500, Format: Url</summary>
        public const string HidrotecContratourl = "hidrotec_contratourl";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecEntidadinicial = "hidrotec_entidadinicial";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Sesion activa usd</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecExpedienteasociadourl = "hidrotec_expedienteasociadourl";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecExpedienteidsesion = "hidrotec_expedienteidsesion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2500, Format: Url</summary>
        public const string HidrotecExpedienteurl = "hidrotec_expedienteurl";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecMunicipioasociadourl = "hidrotec_municipioasociadourl";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecMunicipioidsesion = "hidrotec_municipioidsesion";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2500, Format: Url</summary>
        public const string HidrotecMunicipiourl = "hidrotec_municipiourl";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Sesion activa usd</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        public const string HidrotecSesionabierta = "hidrotec_sesionabierta";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecSessionId = "hidrotec_sessionid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2500, Format: Text</summary>
        public const string HidrotecUrlinicial = "hidrotec_urlinicial";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2500, Format: Url</summary>
        public const string HidrotecUrltotal = "hidrotec_urltotal";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 2500, Format: Url</summary>
        /// <remarks>url de la actividad de llamada</remarks>
        public const string HidrotecIdActividadLlamada = "hidrotec_id_actividad_llamada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        public const string HidrotecUsuarioDestino = "hidrotec_usuario_destino";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecUsuariodestinoId = "hidrotec_usuariodestinoid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecSesionabierta_OptionSet
        {
            No = 0,
            Si = 1
        }
    }

    /// <summary>DisplayName: SMS, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Actividad de SMS para enviar la comunicación al contacto.</remarks>
    public static class HidrotecSms
    {
        public const string EntityName = "hidrotec_sms";
        public const string EntityCollectionName = "hidrotec_smses";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la actividad.</remarks>
        public const string PrimaryKey = "activityid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Tema asociado con la actividad.</remarks>
        public const string PrimaryName = "subject";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Conjunto de opciones que marca si la actividad está revisada o no.</remarks>
        public const string HidrotecActividadrevisada = "hidrotec_actividadrevisada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la actividad.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el puntero de actividad.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mailbox</summary>
        /// <remarks>Identificador único del buzón asociado con remitente del correo electrónico.</remarks>
        public const string SenderMailBoxId = "sendermailboxid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal social, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Muestra cómo se originó el contacto sobre la actividad social, por ejemplo, en Twitter o Facebook. Este campo es de solo lectura.</remarks>
        public const string Community = "community";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio real de la actividad.</remarks>
        public const string Actualstart = "actualstart";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Campo Lookup en el que muestran las distintas opciones de comunicación al contacto.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Dejó el correo de voz</remarks>
        public const string LeftvoiceMail = "leftvoicemail";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la actividad.</remarks>
        public const string Description = "description";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al puntero de actividad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración programada de la actividad en minutos.</remarks>
        public const string Scheduleddurationminutes = "scheduleddurationminutes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración real de la actividad en minutos.</remarks>
        public const string Actualdurationminutes = "actualdurationminutes";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string IsmapiPrivate = "ismapiprivate";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Información que especifica si la actividad es un tipo de evento o de actividad regular.</remarks>
        public const string Isregularactivity = "isregularactivity";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se creó a partir de una regla de flujo de trabajo.</remarks>
        public const string IsworkflowCreated = "isworkflowcreated";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se facturó como parte de la resolución de un caso.</remarks>
        public const string Isbilled = "isbilled";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estados actividades, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones que indica los estados de la actividad. Pendiente, completada y cancelada.</remarks>
        public const string HidrotecEstadoactividad = "hidrotec_estadoactividad";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado de la actividad, OptionSetType: State</summary>
        /// <remarks>Estado de la actividad.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>En este campo se refleja el status de envío del SMS a través de Comunica SMS</remarks>
        public const string HidrotecEstadoenvio = "hidrotec_estadoenvio";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Campo de opciones Sí/No, indica si el envío de la comunicación es porque el expediente está resuelto o no.</remarks>
        public const string HidrotecExpedienteresuelto = "hidrotec_expedienteresuelto";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la actividad.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se envió la actividad.</remarks>
        public const string Senton = "senton";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio programada de la actividad.</remarks>
        public const string Scheduledstart = "scheduledstart";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se intentó la entrega de la actividad por última vez.</remarks>
        public const string DeliveryLastAttemptedon = "deliverylastattemptedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización programada de la actividad.</remarks>
        public const string Scheduledend = "scheduledend";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización real de la actividad.</remarks>
        public const string Actualend = "actualend";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Indica el código del mensaje.</remarks>
        public const string HidrotecIdmensaje = "hidrotec_idmensaje";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único que especifica el identificador de serie periódica de una instancia.</remarks>
        public const string SeriesId = "seriesid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Mensaje que hay que enviar al contacto.</remarks>
        public const string HidrotecMensaje = "hidrotec_mensaje";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el puntero de actividad por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la actividad por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        public const string HidrotecNumerodeintento = "hidrotec_numerodeintento";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la actividad.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 8192</summary>
        /// <remarks>Información adicional proporcionada por la aplicación externa como JSON. Solo para uso interno.</remarks>
        public const string Activityadditionalparams = "activityadditionalparams";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra durante cuánto tiempo, en minutos, se retuvo el registro.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de la actividad.</remarks>
        public const string PriorityCode = "prioritycode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad de entrega, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de entrega de la actividad al servidor de correo electrónico.</remarks>
        public const string DeliverypriorityCode = "deliveryprioritycode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Identificador único del usuario o equipo propietario de la actividad.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado de la actividad.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: account,bookableresourcebooking,bookableresourcebookingheader,bulkoperation,campaign,campaignactivity,contact,contract,entitlement,entitlementtemplate,hidrotec_campoimagen,hidrotec_logerrorintegracion,hidrotec_ov_usuario,hidrotec_usd_mensajes,incident,invoice,knowledgearticle,knowledgebaserecord,lead,msdyn_postalbum,opportunity,quote,salesorder,uii_action,uii_hostedapplication,uii_nonhostedapplication,uii_option,uii_savedsession,uii_workflow,uii_workflowstep,uii_workflow_workflowstep_mapping</summary>
        /// <remarks>Identificador único del objeto al que está asociada la actividad.</remarks>
        public const string RegardingobjectId = "regardingobjectid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidName = "regardingobjectidname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidyomiName = "regardingobjectidyominame";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        public const string RegardingobjectTypeCode = "regardingobjecttypecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_relacion</summary>
        /// <remarks>Relación del proceso. Primer nivel de la tipificación.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string PostponeactivityProcessingUntil = "postponeactivityprocessinguntil";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: service</summary>
        /// <remarks>Identificador único de un servicio asociado.</remarks>
        public const string ServiceId = "serviceid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro de caso.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        /// <remarks>Tipo del proceso. Segundo nivel de la tipificación.</remarks>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Campo en el que se recoge el teléfono móvil.</remarks>
        public const string HidrotecTelefonomovil = "hidrotec_telefonomovil";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Subtipo del proceso. Segundo nivel de tipificación.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired, DisplayName: Tipo de actividad, OptionSetType: Picklist</summary>
        /// <remarks>Tipo de actividad.</remarks>
        public const string ActivityTypeCode = "activitytypecode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tasa de cambio de la divisa asociada al puntero de actividad con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipo de cita, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de instancia de una serie periódica.</remarks>
        public const string InstanceTypeCode = "instancetypecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la actividad por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Último SLA que se aplicó a este caso. Este campo es solo para uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria de la actividad.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        public enum Community_OptionSet
        {
            Facebook = 1,
            Twitter = 2,
            Otro = 0
        }
        public enum HidrotecEstadoactividad_OptionSet
        {
            Pendiente = 1,
            Completada = 2,
            Cancelada = 3
        }
        public enum StateCode_OptionSet
        {
            Abierto = 0,
            Completado = 1,
            Cancelado = 2,
            Programado = 3
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum PriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum DeliverypriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum StatusCode_OptionSet
        {
            Abierto = 1,
            Completado = 2,
            Cancelado = 3,
            Programado = 4
        }
        public enum RegardingobjectTypeCode_OptionSet
        {
        }
        public enum ActivityTypeCode_OptionSet
        {
            Fax = 4204,
            Llamadadetelefono = 4210,
            Correoelectronico = 4202,
            Carta = 4207,
            Cita = 4201,
            Actividaddeservicio = 4214,
            Cierredeoportunidad = 4208,
            Cierredepedido = 4209,
            Cierredeoferta = 4211,
            Resoluciondelcaso = 4206,
            Tarea = 4212,
            Respuestadecampana = 4401,
            Actividaddelacampana = 4402,
            Operacionmasiva = 4406,
            Citaperiodica = 4251,
            Intentodellamadasagotado = 10073,
            Notificacion = 10121,
            NotificacionOrganismoOficial = 10124,
            SMS = 10125,
            VisitaOficina = 10128,
            NotificacionAPP = 10151,
            Pendienteobraaltaacometida = 10153
        }
        public enum InstanceTypeCode_OptionSet
        {
            Noperiodica = 0,
            Periodicamaestra = 1,
            Instanciaperiodica = 2,
            Excepcionperiodica = 3,
            Excepcionfuturaperiodica = 4
        }
    }

    /// <summary>DisplayName: Subtipo, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de subtipos. Tercer nivel de la tipificación.</remarks>
    public static class HidrotecSubtipo
    {
        public const string EntityName = "hidrotec_subtipo";
        public const string EntityCollectionName = "hidrotec_subtipos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_subtipoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Clase acometida, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones que indica si la clase de acometida es individual o colectiva.</remarks>
        public const string HidrotecClaseacometida = "hidrotec_claseacometida";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Check en el que se informa si el contrato es obligatorio.</remarks>
        public const string HidrotecContratoobligatorio = "hidrotec_contratoobligatorio";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Subtipo</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código numérico del Subtipo.</remarks>
        public const string HidrotecIdsubtipo = "hidrotec_idsubtipo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Check que indica si el Motivo es obligatorio.</remarks>
        public const string HidrotecMotivoobligatorio = "hidrotec_motivoobligatorio";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Subtipo</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Indica el Tipo del que cuelga el subtipo.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tipo acometida, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones de tipo acometida: Agua y saneamiento.</remarks>
        public const string HidrotecTipoacometida = "hidrotec_tipoacometida";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Uso acometida, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones de uso acometida: Agua, Contraincendio, Fecal, Pluvial y Pluvial/Fecal.</remarks>
        public const string HidrotecUsoacometida = "hidrotec_usoacometida";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecClaseacometida_OptionSet
        {
            Individual = 1,
            Colectiva = 2
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
        public enum HidrotecTipoacometida_OptionSet
        {
            Agua = 1,
            Saneamiento = 2
        }
        public enum HidrotecUsoacometida_OptionSet
        {
            Aguapotable = 1,
            Contraincendio = 2,
            Fecal = 3,
            Pluvial = 4,
            PluvialFecal = 5
        }
    }

    /// <summary>DisplayName: Subtipo de cliente, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de subtipos de cliente</remarks>
    public static class HidrotecSubtipodecliente
    {
        public const string EntityName = "hidrotec_subtipodecliente";
        public const string EntityCollectionName = "hidrotec_subtipodeclientes";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_subtipodeclienteid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 5, Format: Text</summary>
        /// <remarks>Código del servicio</remarks>
        public const string HidrotecCodigoservicio = "hidrotec_codigoservicio";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Subtipo de cliente</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Check que informa si cuenta con fianza/garantía.</remarks>
        public const string HidrotecFianzagarantia = "hidrotec_fianzagarantia";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código numérico del Subtipo.</remarks>
        public const string HidrotecIdsubtipodecliente = "hidrotec_idsubtipodecliente";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_instalacion</summary>
        /// <remarks>Indica la instalación a la que pertenece ese subtipo.</remarks>
        public const string HidrotecInstalacionId = "hidrotec_instalacionid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Subtipo de cliente</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipodecliente</summary>
        /// <remarks>Conjunto de opciones de los tipos de cliente.</remarks>
        public const string HidrotecTipodeclienteId = "hidrotec_tipodeclienteid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Tarea, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Actividad genérica que representa el trabajo que se debe realizar.</remarks>
    public static class Task
    {
        public const string EntityName = "task";
        public const string EntityCollectionName = "tasks";
    }

    /// <summary>DisplayName: Tipo, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de tipos. Segundo nivel de la tipificación.</remarks>
    public static class HidrotecTipo
    {
        public const string EntityName = "hidrotec_tipo";
        public const string EntityCollectionName = "hidrotec_tipos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_tipoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Tipo</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código numérico del Tipo.</remarks>
        public const string HidrotecIdtipo = "hidrotec_idtipo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Tipo</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_relacion</summary>
        /// <remarks>Relación de la que depende el Tipo.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Tipo de cliente, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de tipos de cliente.</remarks>
    public static class HidrotecTipodecliente
    {
        public const string EntityName = "hidrotec_tipodecliente";
        public const string EntityCollectionName = "hidrotec_tipodeclientes";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_tipodeclienteid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Tipo de cliente</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código numérico del Tipo de Cliente.</remarks>
        public const string HidrotecIdtipodecliente = "hidrotec_idtipodecliente";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Campo que indica el idioma del tipo de cliente</remarks>
        public const string HidrotecIdioma = "hidrotec_idioma";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Tipo de cliente</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecIdioma_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Tipo de documento, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Maestro de tipos de documento para la identificación de contactos.</remarks>
    public static class HidrotecTipodedocumento
    {
        public const string EntityName = "hidrotec_tipodedocumento";
        public const string EntityCollectionName = "hidrotec_tipodedocumentos";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_tipodedocumentoid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 15, Format: Text</summary>
        /// <remarks>Abreviatura del tipo de documento del contacto</remarks>
        public const string HidrotecAbreviatura = "hidrotec_abreviatura";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Tipo de documento</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 10, Format: Text</summary>
        /// <remarks>Identificador del tipo de documento.</remarks>
        public const string HidrotecIdtipodedocumento = "hidrotec_idtipodedocumento";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Tipo de documento</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Tipo evento idioma, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Indica el idioma en el que se describen los plazos.</remarks>
    public static class HidrotecTipoeventoidioma
    {
        public const string EntityName = "hidrotec_tipoeventoidioma";
        public const string EntityCollectionName = "hidrotec_tipoeventoidiomas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_tipoeventoidiomaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Tipo evento idioma</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Evento, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones que divide entre los días laborales y naturales y, dentro de estos, los días transcurridos y pendientes.</remarks>
        public const string HidrotecEvento = "hidrotec_evento";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Conjunto de opciones de los idiomas en los que estarán los eventos.</remarks>
        public const string HidrotecIdioma = "hidrotec_idioma";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Tipo evento idioma</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Memo, RequiredLevel: ApplicationRequired, MaxLength: 2000</summary>
        /// <remarks>Texto que describe el evento.</remarks>
        public const string HidrotecTexto = "hidrotec_texto";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecEvento_OptionSet
        {
            Horaslaborablestranscurridas = 7,
            Horaslaborablespendientes = 8,
            Retrasohoraslaborables = 9,
            Horasnaturalestranscurridas = 10,
            Horasnaturalespendientes = 11,
            Retrasohorasnaturales = 12,
            Diasnaturalestranscurridos = 1,
            Diasnaturalespendientes = 2,
            Retrasodiasnaturales = 3,
            Diaslaborablestranscurridos = 4,
            Diaslaborablespendientes = 5,
            Retrasodiaslaborables = 6
        }
        public enum HidrotecIdioma_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Tipo Vía, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Indica el tipo de vía</remarks>
    public static class HidrotecTipovia
    {
        public const string EntityName = "hidrotec_tipovia";
        public const string EntityCollectionName = "hidrotec_tipovias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_tipoviaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Abreviatura del tipo de vía.</remarks>
        public const string HidrotecAbreviatura = "hidrotec_abreviatura";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Tipo Vía</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código interno del tipo de vía.</remarks>
        public const string HidrotecIdtipovia = "hidrotec_idtipovia";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Idioma para los tipos de via</remarks>
        public const string HidrotecIdioma = "hidrotec_idioma";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Tipo Vía</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecIdioma_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Transición Estados Expediente, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Parametrización de las transiciones entre estados de un Expediente.</remarks>
    public static class HidrotecTransicionestadosexpediente
    {
        public const string EntityName = "hidrotec_transicionestadosexpediente";
        public const string EntityCollectionName = "hidrotec_transicionestadosexpedientes";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_transicionestadosexpedienteid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Campo de texto descriptivo en el que se indica si la transición es automática.</remarks>
        public const string HidrotecAccionesautomaticas = "hidrotec_accionesautomaticas";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Asignaciones automáticas, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si para un tránsito determinado el expediente se asigna automáticamente a un equipo concreto. Estos equipos se definen en las unidades de negocio, tanto en la del CAC como en las explotaciones.</remarks>
        public const string HidrotecAsignacionesautomaticas = "hidrotec_asignacionesautomaticas";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Transición Estados Expediente</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_estadoexpediente</summary>
        /// <remarks>Estado al que llega el expediente al concluir con éxito la transición.</remarks>
        public const string HidrotecEstadodestinoId = "hidrotec_estadodestinoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_estadoexpediente</summary>
        /// <remarks>Estado en el que se encuetra el expediente en el momento de la transición.</remarks>
        public const string HidrotecEstadoorigenId = "hidrotec_estadoorigenid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_mensaje</summary>
        /// <remarks>Búsqueda en el que se selecciona el tipo de error cuando no se puede ejecutar la transición.</remarks>
        public const string HidrotecMensajeerrorId = "hidrotec_mensajeerrorid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Conjunto de opciones (Sí/No) en el que indica si el CAC puede realizar esa transición.</remarks>
        public const string HidrotecPermitiralcac = "hidrotec_permitiralcac";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_procesodenegocio</summary>
        /// <remarks>Indica el proceso al que pertenece la transición.</remarks>
        public const string HidrotecProcesodenegocioId = "hidrotec_procesodenegocioid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Transición Estados Expediente</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Indica las validaciones que debe tener el expediente para poder transitar el estado del expediente.</remarks>
        public const string HidrotecValidaciones = "hidrotec_validaciones";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum HidrotecAsignacionesautomaticas_OptionSet
        {
            EquipoCACPrincipal = 1,
            EquipoCACBackoffice = 4,
            EquipoCACRecepcion = 5,
            EquipoCACEmision = 6,
            EquipoCACAutolecturas = 7,
            Equipoescalado1explotacion = 2,
            Equipoescalado2explotacion = 3,
            EquipoAutolecturasdelaexplotacion = 10,
            EquipoReclamacionesdelaexplotacion = 8,
            EquipoAveriasdelaexplotacion = 9,
            EquipoAPPOVdelaexplotacion = 11
        }
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Unidad de negocio, OwnershipType: BusinessOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Empresa, división o departamento en la base de datos de Microsoft Dynamics CRM.</remarks>
    public static class BusinessUnit
    {
        public const string EntityName = "businessunit";
        public const string EntityCollectionName = "businessunits";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la unidad de negocio.</remarks>
        public const string PrimaryKey = "businessunitid";
        /// <summary>Type: String, RequiredLevel: SystemRequired, MaxLength: 160, Format: Text</summary>
        /// <remarks>Nombre de la unidad de negocio.</remarks>
        public const string PrimaryName = "name";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Abono deuda, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Determina en que momento se permite abonar deuda en los procesos que requieran este abono.</remarks>
        public const string HidrotecAbonodeuda = "hidrotec_abonodeuda";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si se activa la pantalla en autolecturas</remarks>
        public const string HidrotecActivarpantallaautolecturas = "hidrotec_activarpantallaautolecturas";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Nombre alternativo por el que se puede archivar la unidad de negocio.</remarks>
        public const string FileasName = "fileasname";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la unidad de negocio.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó la unidad de negocio.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: calendar</summary>
        /// <remarks>Calendario fiscal asociado con la unidad de negocio.</remarks>
        public const string CalendarId = "calendarid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Nombre del centro de costes de la unidad de negocio.</remarks>
        public const string Costcenter = "costcenter";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Dirección de correo electrónico de la unidad de negocio.</remarks>
        public const string EMailAddress = "emailaddress";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la unidad de negocio.</remarks>
        public const string Description = "description";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1500, MaxValue: 1500</summary>
        /// <remarks>Desplazamiento de UTC de la unidad de negocio (diferencia entre la hora local y el Horario universal coordinado estándar).</remarks>
        public const string Utcoffset = "utcoffset";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada a la unidad de negocio.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Nombre de la división a la que pertenece la unidad de negocio.</remarks>
        public const string DivisionName = "divisionname";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_empresagestora</summary>
        public const string HidrotecEmpresagestora = "hidrotec_empresagestora";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        public const string HidrotecEquipoapp = "hidrotec_equipoapp";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        public const string HidrotecEquipoapruebaarticulokb = "hidrotec_equipoapruebaarticulokb";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        public const string HidrotecEquipoautolecturas = "hidrotec_equipoautolecturas";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        /// <remarks>Equipo que puede atender las averías de de la explotación</remarks>
        public const string HidrotecEquipoaverias = "hidrotec_equipoaverias";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        public const string HidrotecEquipocac = "hidrotec_equipocac";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        /// <remarks>Equipo de autolecturas del CAC</remarks>
        public const string HidrotecEquipocacautolecturas = "hidrotec_equipocacautolecturas";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        /// <remarks>Equipo de Backoffice del CAC.</remarks>
        public const string HidrotecEquipocacbackoffice = "hidrotec_equipocacbackoffice";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        public const string HidrotecEquipocacemision = "hidrotec_equipocacemision";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        /// <remarks>Equipo de recepción de solicitudes del CAC.</remarks>
        public const string HidrotecEquipocacrecepcion = "hidrotec_equipocacrecepcion";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        public const string HidrotecEquipoescalado1 = "hidrotec_equipoescalado1";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        public const string HidrotecEquipoescalado2 = "hidrotec_equipoescalado2";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: team</summary>
        /// <remarks>Equipo que puede resolver las reclamaciones en la explotación</remarks>
        public const string HidrotecEquiporeclamaciones = "hidrotec_equiporeclamaciones";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la unidad de negocio está habilitada o deshabilitada.</remarks>
        public const string Isdisabled = "isdisabled";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estructura Organizativa, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica la Estructura Organizativa</remarks>
        public const string HidrotecEstructuraorganizativa = "hidrotec_estructuraorganizativa";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la unidad de negocio.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la unidad de negocio por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si se han suspendido las reglas de proceso de venta o flujo de trabajo.</remarks>
        public const string Workflowsuspended = "workflowsuspended";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 25, Format: Text</summary>
        /// <remarks>Identificador utilizado por los procesos cíclicos de carga para encontrar las unidades de negocio en caso de que el padre cambie.</remarks>
        public const string HidrotecIdcompany = "hidrotec_idcompany";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        public const string HidrotecIdjerarquico = "hidrotec_idjerarquico";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        public const string HidrotecIdunidadedenegocio = "hidrotec_idunidadedenegocio";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Identificador de la unidad de negocio padre. El campo estará oculto y se usará en los procesos de migración.</remarks>
        public const string HidrotecIdunidadnegociopadre = "hidrotec_idunidadnegociopadre";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Idioma predeterminado de esta unidad de negocio</remarks>
        public const string HidrotecIdiomapredeterminado = "hidrotec_idiomapredeterminado";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 1073741823</summary>
        /// <remarks>Imagen o diagrama de la unidad de negocio.</remarks>
        public const string Picture = "picture";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_instalacion</summary>
        public const string HidrotecInstalacion = "hidrotec_instalacion";
        /// <summary>Type: Double, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000, Precision: 2</summary>
        /// <remarks>Límite de crédito de la unidad de negocio.</remarks>
        public const string CreditLimit = "creditlimit";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000</summary>
        /// <remarks>Máscara heredada de la unidad de negocio.</remarks>
        public const string Inheritancemask = "inheritancemask";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Mercado de valores donde consta la empresa.</remarks>
        public const string StockExchange = "stockexchange";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó la unidad de negocio por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la unidad de negocio por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la unidad de negocio.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: organization</summary>
        /// <remarks>Identificador único de la organización asociada con la unidad de negocio.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_pais</summary>
        /// <remarks>País de esta unidad de negocio</remarks>
        public const string HidrotecPais = "hidrotec_pais";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecPermitircambiotitularcambiotarifa = "hidrotec_permitircambiotitularcambiotarifa";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si se permite cambio de uso en Alta/Baja</remarks>
        public const string HidrotecPermitircambiodeusoenbajaalta = "hidrotec_permitircambiodeusoenbajaalta";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecPermitircambiodeusoencambiodetarifa = "hidrotec_permitircambiodeusoencambiodetarifa";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si se permite el cambio de uso en Cesión</remarks>
        public const string HidrotecPermitircambiodeusoencesion = "hidrotec_permitircambiodeusoencesion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si se permite cambiar de uso en la Subrogación</remarks>
        public const string HidrotecPermitircambiodeusoensubrogacion = "hidrotec_permitircambiodeusoensubrogacion";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si se permite pago en CAC</remarks>
        public const string HidrotecPermitirpagoencac = "hidrotec_permitirpagoencac";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 500, Format: Text</summary>
        /// <remarks>Razón para deshabilitar la unidad de negocio.</remarks>
        public const string Disabledreason = "disabledreason";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Símbolo del valor del mercado de valores para la unidad de negocio.</remarks>
        public const string Tickersymbol = "tickersymbol";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Url</summary>
        /// <remarks>Dirección URL del sitio FTP de la unidad de negocio.</remarks>
        public const string Ftpsiteurl = "ftpsiteurl";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Url</summary>
        /// <remarks>Dirección URL del sitio web de la unidad de negocio.</remarks>
        public const string Websiteurl = "websiteurl";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si tiene APP</remarks>
        public const string HidrotecTieneapp = "hidrotec_tieneapp";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si tiene CAC</remarks>
        public const string HidrotecTienecac = "hidrotec_tienecac";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Tiene GesRed, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        public const string HidrotecTienegesred = "hidrotec_tienegesred";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si tiene GIS</remarks>
        public const string HidrotecTienegis = "hidrotec_tienegis";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Sí/No, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Indica si tiene OV</remarks>
        public const string HidrotecTieneov = "hidrotec_tieneov";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tipo de cambio de la divisa asociada a la unidad de negocio en relación con la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio primaria.</remarks>
        public const string ParentBusinessUnitId = "parentbusinessunitid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        public const string UserGroupId = "usergroupid";
        public enum HidrotecAbonodeuda_OptionSet
        {
            Antesiniciotramite = 1,
            Despuesiniciotramite = 2
        }
        public enum HidrotecActivarpantallaautolecturas_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecEstructuraorganizativa_OptionSet
        {
            CAC = 0,
            Pais = 1,
            Zona = 2,
            Delegacion = 3,
            Unidaddegestion = 4,
            Contrata = 5,
            Explotacion = 6
        }
        public enum HidrotecIdiomapredeterminado_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum HidrotecPermitircambiotitularcambiotarifa_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecPermitircambiodeusoenbajaalta_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecPermitircambiodeusoencambiodetarifa_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecPermitircambiodeusoencesion_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecPermitircambiodeusoensubrogacion_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecPermitirpagoencac_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecTieneapp_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecTienecac_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecTienegesred_OptionSet
        {
            No = 0,
            Nointegrado = 1,
            Integradocondiversa = 2
        }
        public enum HidrotecTienegis_OptionSet
        {
            No = 0,
            Si = 1
        }
        public enum HidrotecTieneov_OptionSet
        {
            No = 0,
            Si = 1
        }
    }

    /// <summary>DisplayName: Usuario, OwnershipType: BusinessOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Persona con acceso al sistema de Microsoft CRM y propietaria de objetos de la base de datos de Microsoft CRM.</remarks>
    public static class SystemUser
    {
        public const string EntityName = "systemuser";
        public const string EntityCollectionName = "systemusers";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único del usuario.</remarks>
        public const string PrimaryKey = "systemuserid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Text</summary>
        /// <remarks>Nombre completo del usuario.</remarks>
        public const string PrimaryName = "fullname";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del jefe del usuario.</remarks>
        public const string ParentSystemUserId = "parentsystemuserid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 64, Format: Text</summary>
        /// <remarks>Apellidos del usuario.</remarks>
        public const string LastName = "lastname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 64, Format: PhoneticGuide</summary>
        /// <remarks>Pronunciación del apellido del usuario, escrito en caracteres fonéticos hiragana o katakana.</remarks>
        public const string YomiLastName = "yomilastname";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el usuario.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el usuario del sistema.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mailbox</summary>
        /// <remarks>Seleccione el buzón asociado con este usuario.</remarks>
        public const string DefaultMailBox = "defaultmailbox";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: calendar</summary>
        /// <remarks>Calendario fiscal asociado con el usuario.</remarks>
        public const string CalendarId = "calendarid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: queue</summary>
        /// <remarks>Identificador único de la cola predeterminada para el usuario.</remarks>
        public const string QueueId = "queueid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: msdyusd_configuration</summary>
        /// <remarks>Identificador único de la configuración asociada al usuario.</remarks>
        public const string MsdyusdUsdConfigurationId = "msdyusd_usdconfigurationid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Conocimientos del usuario.</remarks>
        public const string Skills = "skills";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Dirección de correo electrónico personal del usuario.</remarks>
        public const string PersonaleMailAddress = "personalemailaddress";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Dirección de correo electrónico de alerta móvil del usuario.</remarks>
        public const string MobilealerteMail = "mobilealertemail";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Email</summary>
        /// <remarks>Dirección de correo electrónico de inicio de sesión en Yammer del usuario.</remarks>
        public const string YammereMailAddress = "yammeremailaddress";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Email</summary>
        /// <remarks>Dirección de correo electrónico interna del usuario.</remarks>
        public const string InternalEMailAddress = "internalemailaddress";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1024, Format: Text</summary>
        /// <remarks>Dirección de correo electrónico de trabajo de SharePoint</remarks>
        public const string SharepointeMailAddress = "sharepointemailaddress";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Dirección de correo electrónico preferida, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Dirección de correo electrónico preferida del usuario.</remarks>
        public const string PreferredEMailCode = "preferredemailcode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Dirección preferida, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Dirección preferida del usuario.</remarks>
        public const string PreferredAddressCode = "preferredaddresscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: Url</summary>
        /// <remarks>Dirección URL del sitio web en el que hay una foto del usuario.</remarks>
        public const string Photourl = "photourl";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al usuario del sistema.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador de empleado del usuario.</remarks>
        public const string EmployeeId = "employeeid";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Información acerca de si el usuario es un usuario AD.</remarks>
        public const string IsactivedirectoryUser = "isactivedirectoryuser";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si el usuario está habilitado.</remarks>
        public const string Isdisabled = "isdisabled";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Muestra el estado de aprobación de la dirección de correo electrónico por Administración O365.</remarks>
        public const string IseMailAddressApprovedbyo365admin = "isemailaddressapprovedbyo365admin";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Muestra si la dirección de correo electrónico está aprobada para cada buzón para procesar correo electrónico a través de sincronización de lado del servidor o E-mail Router., OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Muestra el estado de la dirección de correo electrónico principal.</remarks>
        public const string EMailRouteraccessapproval = "emailrouteraccessapproval";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Estado de invitación, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Estado de invitación del usuario.</remarks>
        public const string InviteStatusCode = "invitestatuscode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Muestra el identificador de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el usuario.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el usuario por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Indica si se rellenaron los filtros de Outlook predeterminados.</remarks>
        public const string Defaultfilterspopulated = "defaultfilterspopulated";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Identificador gubernamental del usuario.</remarks>
        public const string GovernmentId = "governmentid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>GUID de objeto de Active Directory para el usuario de sistema.</remarks>
        public const string ActivedirectoryguId = "activedirectoryguid";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        public const string HidrotecHabilitarcti = "hidrotec_habilitarcti";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 128, Format: Text</summary>
        /// <remarks>Id. de Yammer del usuario</remarks>
        public const string YammerUserId = "yammeruserid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string EntityImageId = "entityimageid";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Método de entrega de correo electrónico entrante, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Método de entrega de correo electrónico entrante para el usuario.</remarks>
        public const string IncomingeMailDeliveryMethod = "incomingemaildeliverymethod";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Método de entrega del correo electrónico saliente, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Método de entrega de correo electrónico saliente para el usuario.</remarks>
        public const string OutgoingeMailDeliveryMethod = "outgoingemaildeliverymethod";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el usuario del sistema por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el usuario por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Modo de acceso, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de usuario.</remarks>
        public const string Accessmode = "accessmode";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Compruebe si el usuario es un usuario de instalación.</remarks>
        public const string SetupUser = "setupuser";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Permite comprobar si el usuario es un usuario de integración.</remarks>
        public const string IsintegrationUser = "isintegrationuser";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si se debe mostrar el usuario en vistas de servicio.</remarks>
        public const string Displayinserviceviews = "displayinserviceviews";
        /// <summary>Type: String, RequiredLevel: SystemRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Escriba un nombre de carpeta predeterminado para la ubicación de OneDrive para la Empresa del usuario.</remarks>
        public const string DefaultodbfolderName = "defaultodbfoldername";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 64, Format: Text</summary>
        /// <remarks>Nombre de pila del usuario.</remarks>
        public const string FirstName = "firstname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 64, Format: PhoneticGuide</summary>
        /// <remarks>Pronunciación del nombre de pila del usuario, escrito en caracteres fonéticos hiragana o katakana.</remarks>
        public const string YomiFirstName = "yomifirstname";
        /// <summary>Type: String, RequiredLevel: SystemRequired, MaxLength: 1024, Format: Text</summary>
        /// <remarks>Dominio de Active Directory del que el usuario es integrante.</remarks>
        public const string DomainName = "domainname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 200, Format: PhoneticGuide</summary>
        /// <remarks>Pronunciación del nombre completo del usuario, escrito en caracteres fonéticos hiragana o katakana.</remarks>
        public const string YomiFullname = "yomifullname";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Identificador único de la importación o la migración de datos que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión del usuario.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la organización asociada con el usuario.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string Passporthi = "passporthi";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 1000000000</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string Passportlo = "passportlo";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mobileofflineprofile</summary>
        /// <remarks>Elementos contenidos en un usuario del sistema determinado.</remarks>
        public const string MobileofflineprofileId = "mobileofflineprofileid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: position</summary>
        /// <remarks>Posición del usuario en el modelo de seguridad jerárquica.</remarks>
        public const string PositionId = "positionid";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Muestra el identificador del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Puesto del usuario.</remarks>
        public const string Jobtitle = "jobtitle";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 500, Format: Text</summary>
        /// <remarks>Razón para deshabilitar el usuario.</remarks>
        public const string Disabledreason = "disabledreason";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Informa si el usuario tiene rol de pago o no. Y, por tanto, si puede o no realizar pagos.</remarks>
        public const string HidrotecRoldepago = "hidrotec_roldepago";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Saludo para la correspondencia con el usuario.</remarks>
        public const string Salutation = "salutation";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Segundo nombre del usuario.</remarks>
        public const string MiddleName = "middlename";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: PhoneticGuide</summary>
        /// <remarks>Pronunciación del segundo nombre del usuario, escrito en caracteres fonéticos hiragana o katakana.</remarks>
        public const string YomiMiddleName = "yomimiddlename";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: site</summary>
        /// <remarks>Ubicación del usuario.</remarks>
        public const string SiteId = "siteid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Sobrenombre del usuario.</remarks>
        public const string NickName = "nickname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 64, Format: Text</summary>
        /// <remarks>Número de teléfono móvil del usuario.</remarks>
        public const string MobilePhone = "mobilephone";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 50, Format: Text</summary>
        /// <remarks>Teléfono particular del usuario.</remarks>
        public const string HomePhone = "homephone";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Teléfono preferido, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Número de teléfono preferido del usuario.</remarks>
        public const string PreferredPhoneCode = "preferredphonecode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tipo de cambio de la divisa asociada al usuario del sistema con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipos de CAL, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de licencia de usuario.</remarks>
        public const string CalType = "caltype";
        /// <summary>Type: Integer, RequiredLevel: SystemRequired, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra el tipo de licencia de usuario.</remarks>
        public const string UserLicenseType = "userlicensetype";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 128, Format: Text</summary>
        /// <remarks>Título del usuario.</remarks>
        public const string Title = "title";
        /// <summary>Type: Lookup, RequiredLevel: SystemRequired, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio con la que está asociado el usuario.</remarks>
        public const string BusinessUnitId = "businessunitid";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Información sobre si el usuario tiene licencia.</remarks>
        public const string Islicensed = "islicensed";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Información sobre si el usuario está sincronizado con el directorio.</remarks>
        public const string Issyncwithdirectory = "issyncwithdirectory";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1024, Format: Email</summary>
        /// <remarks>Windows Live ID</remarks>
        public const string WindowsliveId = "windowsliveid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: territory</summary>
        /// <remarks>Identificador único de la zona de ventas a la que está asignado el usuario.</remarks>
        public const string TerritoryId = "territoryid";
        public enum PreferredEMailCode_OptionSet
        {
            Valorpredeterminado = 1
        }
        public enum PreferredAddressCode_OptionSet
        {
            Direcciondecorreo = 1,
            Otradireccion = 2
        }
        public enum EMailRouteraccessapproval_OptionSet
        {
            Vacio = 0,
            Aprobado = 1,
            Aprobacionpendiente = 2,
            Rechazado = 3
        }
        public enum InviteStatusCode_OptionSet
        {
            Invitacionnoenviada = 0,
            Invitado = 1,
            Invitacionapuntodeexpirar = 2,
            Invitacionexpirada = 3,
            Invitacionaceptada = 4,
            Invitacionrechazada = 5,
            Invitacionrevocada = 6
        }
        public enum IncomingeMailDeliveryMethod_OptionSet
        {
            Ninguno = 0,
            MicrosoftDynamicsCRMparaOutlook = 1,
            SincronizaciondelladodelservidoroEMailRouter = 2,
            Buzondeenrutamiento = 3
        }
        public enum OutgoingeMailDeliveryMethod_OptionSet
        {
            Ninguno = 0,
            MicrosoftDynamicsCRMparaOutlook = 1,
            SincronizaciondelladodelservidoroEMailRouter = 2
        }
        public enum Accessmode_OptionSet
        {
            Lecturayescritura = 0,
            Administrativo = 1,
            Lectura = 2,
            Usuariodesoporte = 3,
            Nointeractivo = 4,
            Administradordelegado = 5
        }
        public enum PreferredPhoneCode_OptionSet
        {
            Numerodecentralita = 1,
            Otrotelefono = 2,
            Telefonoparticular = 3,
            Telefonomovil = 4
        }
        public enum CalType_OptionSet
        {
            Professional = 0,
            Administrativo = 1,
            Basico = 2,
            Profesionaldedispositivo = 3,
            DeviceBasic = 4,
            Esencial = 5,
            DeviceEssential = 6,
            Enterprise = 7,
            Empresadedispositivo = 8
        }
    }

    /// <summary>DisplayName: Usuario OV, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Entidad donde se almacena la información de los usuarios de OV.</remarks>
    public static class HidrotecOvUsuario
    {
        public const string EntityName = "hidrotec_ov_usuario";
        public const string EntityCollectionName = "hidrotec_ov_usuarios";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_ov_usuarioid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo de texto que indica la contraseña de la Oficina Virtual.</remarks>
        public const string HidrotecCodigomultiusuario = "hidrotec_codigomultiusuario";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: contact</summary>
        /// <remarks>Campo de búsqueda a contacto</remarks>
        public const string HidrotecContactOempresaId = "hidrotec_contactoempresaid";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: contact</summary>
        /// <remarks>Campo de búsqueda a contacto.</remarks>
        public const string HidrotecContactOprincipalId = "hidrotec_contactoprincipalid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 20, Format: Email</summary>
        /// <remarks>Campo de Correo electrónico para la Oficina Virtual.</remarks>
        public const string HidrotecEMail = "hidrotec_email";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del OV usuario</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 20, Format: Text</summary>
        /// <remarks>Campo de la Oficina Virtual que indica la contraseña.</remarks>
        public const string HidrotecPassword = "hidrotec_password";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del OV usuario</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Vía, OwnershipType: OrganizationOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Callejero en el que viene recogida la información referida a las vías.</remarks>
    public static class HidrotecVia
    {
        public const string EntityName = "hidrotec_via";
        public const string EntityCollectionName = "hidrotec_vias";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_viaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Vía</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 100, Format: Text</summary>
        /// <remarks>Código en el que se identifica la vía</remarks>
        public const string HidrotecIdvia = "hidrotec_idvia";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: organization</summary>
        /// <remarks>Identificador único de la organización.</remarks>
        public const string OrganizationId = "organizationid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_municipio</summary>
        /// <remarks>Lookup en el que se recogen todos los municipios del callejero</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Vía</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipovia</summary>
        /// <remarks>Descripción de tipo del tipo de vía.</remarks>
        public const string HidrotecTipoviaId = "hidrotec_tipoviaid";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Visita Oficina, OwnershipType: UserOwned, IntroducedVersion: 5.0.0.0</summary>
    /// <remarks>Actividad que registra las visitas en la oficina.</remarks>
    public static class HidrotecVisitaoficina
    {
        public const string EntityName = "hidrotec_visitaoficina";
        public const string EntityCollectionName = "hidrotec_visitaoficinas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de la actividad.</remarks>
        public const string PrimaryKey = "activityid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 200, Format: Text</summary>
        /// <remarks>Tema asociado con la actividad.</remarks>
        public const string PrimaryName = "subject";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Conjunto de opciones que marca si la actividad está revisada o no.</remarks>
        public const string HidrotecActividadrevisada = "hidrotec_actividadrevisada";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó la actividad.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el puntero de actividad.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: mailbox</summary>
        /// <remarks>Identificador único del buzón asociado con remitente del correo electrónico.</remarks>
        public const string SenderMailBoxId = "sendermailboxid";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Canal social, OptionSetType: Picklist, DefaultFormValue: -1</summary>
        /// <remarks>Muestra cómo se originó el contacto sobre la actividad social, por ejemplo, en Twitter o Facebook. Este campo es de solo lectura.</remarks>
        public const string Community = "community";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio real de la actividad.</remarks>
        public const string Actualstart = "actualstart";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_comunicacion</summary>
        /// <remarks>Campo Lookup en el que muestran las distintas opciones de comunicación al contacto.</remarks>
        public const string HidrotecComunicacionId = "hidrotec_comunicacionid";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 100000</summary>
        /// <remarks>Campo de texto descriptivo en el que se podrá escribir alguna aclaración o concreción a la hora de comunicarse con el contacto</remarks>
        public const string HidrotecComunicacionalContactO = "hidrotec_comunicacionalcontacto";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Dejó el correo de voz</remarks>
        public const string LeftvoiceMail = "leftvoicemail";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 2000</summary>
        /// <remarks>Descripción de la actividad.</remarks>
        public const string Description = "description";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: transactioncurrency</summary>
        /// <remarks>Identificador único de la divisa asociada al puntero de actividad.</remarks>
        public const string TransactioncurrencyId = "transactioncurrencyid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración programada de la actividad en minutos.</remarks>
        public const string Scheduleddurationminutes = "scheduleddurationminutes";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Duración real de la actividad en minutos.</remarks>
        public const string Actualdurationminutes = "actualdurationminutes";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string IsmapiPrivate = "ismapiprivate";
        /// <summary>Type: Boolean, RequiredLevel: SystemRequired, True: 1, False: 0, DefaultValue: True</summary>
        /// <remarks>Información que especifica si la actividad es un tipo de evento o de actividad regular.</remarks>
        public const string Isregularactivity = "isregularactivity";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se creó a partir de una regla de flujo de trabajo.</remarks>
        public const string IsworkflowCreated = "isworkflowcreated";
        /// <summary>Type: Boolean, RequiredLevel: None, True: 1, False: 0, DefaultValue: False</summary>
        /// <remarks>Especifica si la actividad se facturó como parte de la resolución de un caso.</remarks>
        public const string Isbilled = "isbilled";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Estados actividades, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Conjunto de opciones que indica los estados de la actividad. Pendiente, completada y cancelada.</remarks>
        public const string HidrotecEstadoactividad = "hidrotec_estadoactividad";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado de la actividad, OptionSetType: State</summary>
        /// <remarks>Estado de la actividad.</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único de la fase.</remarks>
        public const string StageId = "stageid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó la actividad.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se envió la actividad.</remarks>
        public const string Senton = "senton";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de inicio programada de la actividad.</remarks>
        public const string Scheduledstart = "scheduledstart";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se intentó la entrega de la actividad por última vez.</remarks>
        public const string DeliveryLastAttemptedon = "deliverylastattemptedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización programada de la actividad.</remarks>
        public const string Scheduledend = "scheduledend";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Hora de finalización real de la actividad.</remarks>
        public const string Actualend = "actualend";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único que especifica el identificador de serie periódica de una instancia.</remarks>
        public const string SeriesId = "seriesid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el puntero de actividad por última vez.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó la actividad por última vez.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_municipio</summary>
        /// <remarks>Municipio donde se está realizando la Visita a la oficina</remarks>
        public const string HidrotecMunicipioId = "hidrotec_municipioid";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: 0, MaxValue: 2147483647</summary>
        /// <remarks>Indica el número de intentos que se ha querido ponerse en contactar con el contacto.</remarks>
        public const string HidrotecNumerodeintento = "hidrotec_numerodeintento";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        /// <remarks>Número de versión de la actividad.</remarks>
        public const string VersionNumber = "versionnumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Memo, RequiredLevel: None, MaxLength: 8192</summary>
        /// <remarks>Información adicional proporcionada por la aplicación externa como JSON. Solo para uso interno.</remarks>
        public const string Activityadditionalparams = "activityadditionalparams";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Muestra durante cuánto tiempo, en minutos, se retuvo el registro.</remarks>
        public const string Onholdtime = "onholdtime";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de la actividad.</remarks>
        public const string PriorityCode = "prioritycode";
        /// <summary>Type: Picklist, RequiredLevel: None, DisplayName: Prioridad de entrega, OptionSetType: Picklist, DefaultFormValue: 1</summary>
        /// <remarks>Prioridad de entrega de la actividad al servidor de correo electrónico.</remarks>
        public const string DeliverypriorityCode = "deliveryprioritycode";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: None</summary>
        /// <remarks>Identificador único del proceso.</remarks>
        public const string ProcessId = "processid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Identificador único del usuario o equipo propietario de la actividad.</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado de la actividad.</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: account,bookableresourcebooking,bookableresourcebookingheader,bulkoperation,campaign,campaignactivity,contact,contract,entitlement,entitlementtemplate,hidrotec_campoimagen,hidrotec_logerrorintegracion,hidrotec_ov_usuario,hidrotec_usd_mensajes,incident,invoice,knowledgearticle,knowledgebaserecord,lead,msdyn_postalbum,opportunity,quote,salesorder,uii_action,uii_hostedapplication,uii_nonhostedapplication,uii_option,uii_savedsession,uii_workflow,uii_workflowstep,uii_workflow_workflowstep_mapping</summary>
        /// <remarks>Identificador único del objeto al que está asociada la actividad.</remarks>
        public const string RegardingobjectId = "regardingobjectid";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidName = "regardingobjectidname";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 400, Format: Text</summary>
        public const string RegardingobjectidyomiName = "regardingobjectidyominame";
        /// <summary>Type: EntityName, RequiredLevel: None</summary>
        public const string RegardingobjectTypeCode = "regardingobjecttypecode";
        /// <summary>Type: Lookup, RequiredLevel: ApplicationRequired, Targets: hidrotec_relacion</summary>
        /// <remarks>Relación del proceso. Primer nivel de la tipificación.</remarks>
        public const string HidrotecRelacionId = "hidrotec_relacionid";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string PostponeactivityProcessingUntil = "postponeactivityprocessinguntil";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1250, Format: Text</summary>
        /// <remarks>Solo para uso interno.</remarks>
        public const string Traversedpath = "traversedpath";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: service</summary>
        /// <remarks>Identificador único de un servicio asociado.</remarks>
        public const string ServiceId = "serviceid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Elija el contrato de nivel de servicio (SLA) que desea aplicar al registro de caso.</remarks>
        public const string SlaId = "slaid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_subtipo</summary>
        /// <remarks>Tipo del proceso. Segundo nivel de la tipificación.</remarks>
        public const string HidrotecSubtipoId = "hidrotec_subtipoid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_tipo</summary>
        /// <remarks>Subtipo del proceso. Segundo nivel de tipificación.</remarks>
        public const string HidrotecTipoId = "hidrotec_tipoid";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired, DisplayName: Tipo de actividad, OptionSetType: Picklist</summary>
        /// <remarks>Tipo de actividad.</remarks>
        public const string ActivityTypeCode = "activitytypecode";
        /// <summary>Type: Decimal, RequiredLevel: None, MinValue: 0,0000000001, MaxValue: 100000000000, Precision: 10</summary>
        /// <remarks>Tasa de cambio de la divisa asociada al puntero de actividad con respecto a la divisa base.</remarks>
        public const string ExchangeRate = "exchangerate";
        /// <summary>Type: Picklist, RequiredLevel: SystemRequired, DisplayName: Tipo de cita, OptionSetType: Picklist, DefaultFormValue: 0</summary>
        /// <remarks>Tipo de instancia de una serie periódica.</remarks>
        public const string InstanceTypeCode = "instancetypecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó la actividad por última vez.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Contiene la marca de fecha y hora del último período de retención.</remarks>
        public const string LastOnholdtime = "lastonholdtime";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: sla</summary>
        /// <remarks>Último SLA que se aplicó a este caso. Este campo es solo para uso interno.</remarks>
        public const string SlaInvokedId = "slainvokedid";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria de la actividad.</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        public enum Community_OptionSet
        {
            Facebook = 1,
            Twitter = 2,
            Otro = 0
        }
        public enum HidrotecEstadoactividad_OptionSet
        {
            Pendiente = 1,
            Completada = 2,
            Cancelada = 3
        }
        public enum StateCode_OptionSet
        {
            Abierto = 0,
            Completado = 1,
            Cancelado = 2,
            Programado = 3
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum PriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum DeliverypriorityCode_OptionSet
        {
            Baja = 0,
            Normal = 1,
            Alta = 2
        }
        public enum StatusCode_OptionSet
        {
            Abierto = 1,
            Completado = 2,
            Cancelado = 3,
            Programado = 4
        }
        public enum RegardingobjectTypeCode_OptionSet
        {
        }
        public enum ActivityTypeCode_OptionSet
        {
            Fax = 4204,
            Llamadadetelefono = 4210,
            Correoelectronico = 4202,
            Carta = 4207,
            Cita = 4201,
            Actividaddeservicio = 4214,
            Cierredeoportunidad = 4208,
            Cierredepedido = 4209,
            Cierredeoferta = 4211,
            Resoluciondelcaso = 4206,
            Tarea = 4212,
            Respuestadecampana = 4401,
            Actividaddelacampana = 4402,
            Operacionmasiva = 4406,
            Citaperiodica = 4251,
            Intentodellamadasagotado = 10073,
            Notificacion = 10121,
            NotificacionOrganismoOficial = 10124,
            SMS = 10125,
            VisitaOficina = 10128,
            NotificacionAPP = 10151,
            Pendienteobraaltaacometida = 10153
        }
        public enum InstanceTypeCode_OptionSet
        {
            Noperiodica = 0,
            Periodicamaestra = 1,
            Instanciaperiodica = 2,
            Excepcionperiodica = 3,
            Excepcionfuturaperiodica = 4
        }
    }

    /// <summary>DisplayName: Pregunta de seguridad, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Listado de preguntas de seguridad de APP/OV</remarks>
    public static class HidrotecOvPreguntasdeseguridad
    {
        public const string EntityName = "hidrotec_ov_preguntasdeseguridad";
        public const string EntityCollectionName = "hidrotec_ov_preguntasdeseguridads";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_ov_preguntasdeseguridadid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Pregunta de seguridad</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 10, Format: Text</summary>
        /// <remarks>Id interno de la pregunta de seguridad</remarks>
        public const string HidrotecOvIdpregunta = "hidrotec_ov_idpregunta";
        /// <summary>Type: Picklist, RequiredLevel: ApplicationRequired, DisplayName: Idioma, OptionSetType: Picklist, DefaultFormValue: 3082</summary>
        /// <remarks>Idioma de la pregunta de seguridad</remarks>
        public const string HidrotecOvIdioma = "hidrotec_ov_idioma";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 1000, Format: Text</summary>
        /// <remarks>Pregunta de seguridad</remarks>
        public const string HidrotecOvPregunta = "hidrotec_ov_pregunta";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Pregunta de seguridad</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum HidrotecOvIdioma_OptionSet
        {
            Castellano = 3082,
            Euskera = 1069,
            Catalan = 1027,
            Gallego = 1110,
            Ingles = 2057,
            Portugues = 2070,
            Italiano = 1040,
            Checo = 1029
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }

    /// <summary>DisplayName: Respuesta de seguridad, OwnershipType: UserOwned, IntroducedVersion: 1.0.0</summary>
    /// <remarks>Listado de las respuestas de seguridad</remarks>
    public static class HidrotecOvRespuesta
    {
        public const string EntityName = "hidrotec_ov_respuesta";
        public const string EntityCollectionName = "hidrotec_ov_respuestas";
        /// <summary>Type: Uniqueidentifier, RequiredLevel: SystemRequired</summary>
        /// <remarks>Identificador único de instancias de entidad</remarks>
        public const string PrimaryKey = "hidrotec_ov_respuestaid";
        /// <summary>Type: String, RequiredLevel: ApplicationRequired, MaxLength: 100, Format: Text</summary>
        /// <remarks>El nombre de la entidad personalizada.</remarks>
        public const string PrimaryName = "hidrotec_name";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que creó el registro.</remarks>
        public const string CreatedBy = "createdby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que creó el registro.</remarks>
        public const string CreatedOnBehalfBy = "createdonbehalfby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Código de la zona horaria que estaba en uso cuando se creó el registro.</remarks>
        public const string UtcConversionTimezoneCode = "utcconversiontimezonecode";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_ov_usuario</summary>
        /// <remarks>Contacto de la OV</remarks>
        public const string HidrotecOvContactOId = "hidrotec_ov_contactoid";
        /// <summary>Type: State, RequiredLevel: SystemRequired, DisplayName: Estado, OptionSetType: State</summary>
        /// <remarks>Estado del Respuesta de seguridad</remarks>
        public const string StateCode = "statecode";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se creó el registro.</remarks>
        public const string CreatedOn = "createdon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateOnly, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se migró el registro.</remarks>
        public const string OverriddenCreatedOn = "overriddencreatedon";
        /// <summary>Type: DateTime, RequiredLevel: None, Format: DateAndTime, DateTimeBehavior: UserLocal</summary>
        /// <remarks>Fecha y hora en que se modificó el registro.</remarks>
        public const string ModifiedOn = "modifiedon";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario delegado que modificó el registro.</remarks>
        public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: systemuser</summary>
        /// <remarks>Identificador único del usuario que modificó el registro.</remarks>
        public const string ModifiedBy = "modifiedby";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -2147483648, MaxValue: 2147483647</summary>
        /// <remarks>Número de secuencia de la importación que creó este registro.</remarks>
        public const string ImportSequenceNumber = "importsequencenumber";
        /// <summary>Type: Integer, RequiredLevel: None, MinValue: -1, MaxValue: 2147483647</summary>
        /// <remarks>Para uso interno.</remarks>
        public const string TimezoneruleVersionNumber = "timezoneruleversionnumber";
        /// <summary>Type: EntityName, RequiredLevel: SystemRequired</summary>
        /// <remarks>Tipo de identificador de propietario</remarks>
        public const string OwnerIdType = "owneridtype";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: hidrotec_ov_preguntasdeseguridad</summary>
        /// <remarks>Pregunta de seguridad</remarks>
        public const string HidrotecOvPreguntaId = "hidrotec_ov_preguntaid";
        /// <summary>Type: Owner, RequiredLevel: SystemRequired, Targets: systemuser,team</summary>
        /// <remarks>Id. del propietario</remarks>
        public const string OwnerId = "ownerid";
        /// <summary>Type: Status, RequiredLevel: None, DisplayName: Razón para el estado, OptionSetType: Status</summary>
        /// <remarks>Razón para el estado del Respuesta de seguridad</remarks>
        public const string StatusCode = "statuscode";
        /// <summary>Type: String, RequiredLevel: None, MaxLength: 1000, Format: Text</summary>
        /// <remarks>Respuesta de la pregunta</remarks>
        public const string HidrotecOvRespuesta1 = "hidrotec_ov_respuesta";
        /// <summary>Type: Lookup, RequiredLevel: None, Targets: businessunit</summary>
        /// <remarks>Identificador único de la unidad de negocio propietaria del registro</remarks>
        public const string OwningBusinessUnit = "owningbusinessunit";
        /// <summary>Type: BigInt, RequiredLevel: None, MinValue: -9223372036854775808, MaxValue: 9223372036854775807</summary>
        public const string VersionNumber = "versionnumber";
        public enum StateCode_OptionSet
        {
            Activo = 0,
            Inactivo = 1
        }
        public enum OwnerIdType_OptionSet
        {
        }
        public enum StatusCode_OptionSet
        {
            Activo = 1,
            Inactivo = 2
        }
    }
}
