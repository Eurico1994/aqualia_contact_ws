﻿using WS.Usuario.Model.NEO_ComprobarUsuario;
using WS.Usuario.Model.NEO_AltaUsuario;
using WS.Usuario.Model.NEO_TiposIdentificacionFiscal;
using WS.Usuario.Model.NEO_AutenticationUsuario;
using WS.Usuario.Model.NEO_RecordarClave;
using WS.Usuario.Model.NEO_PreguntaSeguridadUsuario;
using WS.Usuario.Model.NEO_CambiarPreguntaSeguridadUsuario;
using WS.Usuario.Model.NEO_ListadoMotivosBaja;
using WS.Usuario.Model.NEO_BajaUsuario;
using WS.Usuario.Model.NEO_CambiarClave;

using System.ServiceModel;

namespace WS.Usuario
{
    [ServiceContract]
    public interface IUsuario
    {
        [OperationContract]
        NEO_ComprobarUsuarioResponse NEO_ComprobarUsuario(NEO_ComprobarUsuarioRequest request);
        [OperationContract]
        NEO_TiposIdentificacionFiscalResponse NEO_TiposIdentificacionFiscal(NEO_TiposIdentificacionFiscalRequest request);
        [OperationContract]
        NEO_ListadoMotivosBajaResponse NEO_ListadoMotivosBaja(NEO_ListadoMotivosBajaRequest request);
        [OperationContract]
        NEO_CambiarPreguntaSeguridadUsuarioResponse NEO_CambiarPreguntaSeguridadUsuario(NEO_CambiarPreguntaSeguridadUsuarioRequest request);
        [OperationContract]
        NEO_PreguntaSeguridadUsuarioResponse NEO_PreguntaSeguridadUsuario(NEO_PreguntaSeguridadUsuarioRequest request);
        [OperationContract]
        NEO_CambiarClaveResponse NEO_CambiarClave(NEO_CambiarClaveRequest request);
        [OperationContract]
        NEO_RecordarClaveResponse NEO_RecordarClave(NEO_RecordarClaveRequest request);
        [OperationContract]
        NEO_AutenticationUsuarioResponse NEO_AutenticationUsuario(NEO_AutenticationUsuarioRequest request);
        [OperationContract]
        NEO_AltaUsuarioResponse NEO_AltaUsuario(NEO_AltaUsuarioRequest request);
        [OperationContract]
        NEO_BajaUsuarioResponse NEO_BajaUsuario(NEO_BajaUsuarioRequest request);
    }
}
