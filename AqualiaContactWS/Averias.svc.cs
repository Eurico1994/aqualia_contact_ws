﻿using AqualiaContactWS.Helper;
using AqualiaContactWS.Model.NEO_AltaAveria;
using AqualiaContactWS.Model.NEO_ListadoPreguntasRespuestas;
using AqualiaContactWS.Model.NEO_RecuperarDatosContacto;
using AqualiaContactWS.Model.NEO_RecuperarDireccion;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Averias" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Averias.svc or Averias.svc.cs at the Solution Explorer and start debugging.
    public class Averias : IAverias
    {
        /// <summary>
        /// STATUS: KO Pdte Dudas.
        /// DUDAS: Preguntar Antonio.
        /// DESC:
        /// Este servicio devuelve la estructura de preguntas respuestas para realizar un alta de avería en la aplicación. Para ello se hacen las validaciones 
        /// pertinentes y se recuperan los datos a través de la función fapp_selaverias_arg.
        /// Expediente - Verificar competencia.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoPreguntasRespuestasResponse NEO_ListadoPreguntasRespuestas(NEO_ListadoPreguntasRespuestasRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoPreguntasRespuestasResponse resp = new NEO_ListadoPreguntasRespuestasResponse();
            try
            {
                Utils.InicializarVariables();

                
                if (Utils.IOS == null)
                {
                   
                    return (NEO_ListadoPreguntasRespuestasResponse)respuestaWs.GetError("99999","","SinConexion");
                }

                if (request == null)
                {
                   
                    return (NEO_ListadoPreguntasRespuestasResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoPreguntasRespuestasResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                     return (NEO_ListadoPreguntasRespuestasResponse)respuestaWs.CanalEntrada();

                }

               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoPreguntasRespuestasResponse)respuestaWs.Pais();
                }

                if (request.identificadornodo == "" || request.identificadornodo == null)
                {
                    return (NEO_ListadoPreguntasRespuestasResponse)respuestaWs.Identificadornodo();
                }

                return (NEO_ListadoPreguntasRespuestasResponse)respuestaWs.ok();
            }
            catch (Exception e)
            {
                return (NEO_ListadoPreguntasRespuestasResponse)respuestaWs.Exception(e);
            }
        }





        public static Entity expedienteBase(string idioma, string tipificacion, IOrganizationService service, string contrato, string contacto)
        {
            Entity exp = new Entity(Incident.EntityName);
            exp["hidrotec_idiomacontrato"] = new OptionSetValue((Convert.ToInt32(Utils.chequeoIdioma(idioma))));

            if (contrato.Split('_')[0] != "null")
            {
                var q = new QueryExpression("hidrotec_contrato");
                q.Criteria.AddCondition("hidrotec_contratoid", ConditionOperator.Equal, new Guid(contrato));
                q.ColumnSet.AddColumns(
                    HidrotecContrato.HidrotecEstado,
                    HidrotecContrato.HidrotecAccesibilidadcontador,
                    HidrotecContrato.HidrotecTipodeclienteId,
                    HidrotecContrato.HidrotecSubtipodeclienteId,
                    "hidrotec_ubicacioncontadorid",
                    "hidrotec_correoelectronico",
                    "hidrotec_telefonofijo",
                    "hidrotec_telefonomovil",
                    "hidrotec_fax",
                    "hidrotec_canalalarmas",
                    "hidrotec_canaldocumentacion",
                    "hidrotec_canalnotificacionresolucion");

                var leDirSum = new LinkEntity("hidrotec_contrato", "hidrotec_direcciondesuministro", "hidrotec_direcciondesuministroid", "hidrotec_direcciondesuministroid", JoinOperator.Inner);
                leDirSum.Columns = new ColumnSet(true);
                q.LinkEntities.Add(leDirSum);

                var res = service.RetrieveMultiple(q);
                if (res.Entities.Count > 0)
                {
                    var an = res.Entities[0];
                    //contador
                    if (an.Contains(HidrotecContrato.HidrotecEstado))
                        exp[Incident.HidrotecEstadocontrato] = an[HidrotecContrato.HidrotecEstado];
                    if (an.Contains(HidrotecContrato.HidrotecTipodeclienteId))
                        exp[Incident.HidrotecTipodeclienteId] = (EntityReference)an[HidrotecContrato.HidrotecTipodeclienteId];
                    if (an.Contains(HidrotecContrato.HidrotecSubtipodeclienteId))
                        exp[Incident.HidrotecSubtipodeclienteId] = (EntityReference)an[HidrotecContrato.HidrotecSubtipodeclienteId];
                    if (an.Contains(HidrotecContrato.HidrotecAccesibilidadcontador))
                        exp[Incident.HidrotecAccesibilidadcontador] = an[HidrotecContrato.HidrotecAccesibilidadcontador];
                    if (an.Contains("hidrotec_ubicacioncontadorid"))
                        exp["hidrotec_ubicacioncontadorid"] = an["hidrotec_ubicacioncontadorid"];

                    if (an.Contains("hidrotec_correoelectronico"))
                        exp["hidrotec_correoelectronico"] = an["hidrotec_correoelectronico"];

                    if (an.Contains("hidrotec_telefonofijo"))
                        exp["hidrotec_telefonofijo"] = an["hidrotec_telefonofijo"];

                    if (an.Contains("hidrotec_telefonomovil"))
                        exp["hidrotec_telefonomovil"] = an["hidrotec_telefonomovil"];

                    if (an.Contains("hidrotec_fax"))
                        exp["hidrotec_fax"] = an["hidrotec_fax"];

                    if (an.Contains("hidrotec_canalalarmas"))
                        exp["hidrotec_canalalarmas"] = an["hidrotec_canalalarmas"];

                    if (an.Contains("hidrotec_canaldocumentacion"))
                        exp["hidrotec_canaldocumentacion"] = an["hidrotec_canaldocumentacion"];

                    if (an.Contains("hidrotec_canalnotificacionresolucion"))
                        exp["hidrotec_canalnotificacionresolucion"] = an["hidrotec_canalnotificacionresolucion"];

                    exp["hidrotec_paisid"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_paisid") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_paisid"]).Value : null;
                    exp["hidrotec_provinciaid"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_provinciaid") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_provinciaid"]).Value : null;
                    exp["hidrotec_provincia"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_provincia") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_provincia"]).Value : null;
                    exp["hidrotec_municipioid"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_municipioid") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_municipioid"]).Value : null;
                    exp["hidrotec_municipio"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_municipio") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_municipio"]).Value : null;
                    exp["hidrotec_viaid"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_viaid") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_viaid"]).Value : null;
                    exp["hidrotec_via"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_via") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_via"]).Value : null;
                    exp["hidrotec_numero"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_numero") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_numero"]).Value : null;
                    exp["hidrotec_codigopostal"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_codigopostal") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_codigopostal"]).Value : null;
                    exp["hidrotec_piso"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_piso") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_piso"]).Value : null;
                    exp["hidrotec_puerta"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_puerta") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_puerta"]).Value : null;
                    exp["hidrotec_escalera"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_escalera") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_escalera"]).Value : null;
                    exp["hidrotec_otros"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_otros") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_otros"]).Value : null;
                    exp["hidrotec_edificio"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_edificio") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_edificio"]).Value : null;
                    exp["hidrotec_bloque"] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_bloque") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_bloque"]).Value : null;

                    exp[Incident.HidrotecPaisauxiliarId] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_paisid") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_paisid"]).Value : null;
                    exp[Incident.HidrotecProvinciaauxiliarId] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_provinciaid") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_provinciaid"]).Value : null;
                    exp[Incident.HidrotecProvinciaauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_provincia") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_provincia"]).Value : null;
                    exp[Incident.HidrotecMunicipioauxiliarId] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_municipioid") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_municipioid"]).Value : null;
                    exp[Incident.HidrotecMunicipioauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_municipio") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_municipio"]).Value : null;
                    exp[Incident.HidrotecViaauxiliarId] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_viaid") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_viaid"]).Value : null;
                    exp[Incident.HidrotecViaauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_via") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_via"]).Value : null;
                    exp[Incident.HidrotecNumeroauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_numero") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_numero"]).Value : null;
                    exp[Incident.HidrotecCodigoPostalAuxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_codigopostal") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_codigopostal"]).Value : null;
                    exp[Incident.HidrotecPisoauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_piso") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_piso"]).Value : null;
                    exp[Incident.HidrotecPuertaauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_puerta") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_puerta"]).Value : null;
                    exp[Incident.HidrotecEscaleraauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_escalera") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_escalera"]).Value : null;
                    exp[Incident.HidrotecOtrosauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_otros") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_otros"]).Value : null;
                    exp[Incident.HidrotecEdificioauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_edificio") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_edificio"]).Value : null;
                    exp[Incident.HidrotecBloqueauxiliar] = an.Contains("hidrotec_direcciondesuministro1.hidrotec_bloque") ? ((AliasedValue)an["hidrotec_direcciondesuministro1.hidrotec_bloque"]).Value : null;



                    exp["hidrotec_contratoid"] = new EntityReference("hidrotec_contrato", new Guid(contrato));
                }

                if (contacto != null || contacto != "")
                {
                    QueryExpression q1 = new QueryExpression(HidrotecOvUsuario.EntityName);
                    q1.Criteria.Conditions.AddRange(
                        new ConditionExpression(HidrotecOvUsuario.HidrotecContactOprincipalId, ConditionOperator.Equal, new Guid(contacto)),
                        new ConditionExpression(HidrotecOvUsuario.HidrotecContactOempresaId, ConditionOperator.Null));
                    q1.ColumnSet.AddColumn(HidrotecOvUsuario.HidrotecEMail);
                    var res1 = service.RetrieveMultiple(q1);
                    if (res1.Entities.Count > 0)
                    {
                        exp["hidrotec_correoelectronico"] = res1.Entities[0].GetAttributeValue<string>(HidrotecOvUsuario.HidrotecEMail);
                    }

                    QueryExpression q2 = new QueryExpression(HidrotecRoldecontrato.EntityName);
                    q2.Criteria.Conditions.AddRange(
                        new ConditionExpression(HidrotecRoldecontrato.HidrotecContactOId, ConditionOperator.Equal, new Guid(contacto)),
                        new ConditionExpression(HidrotecRoldecontrato.HidrotecContratoId, ConditionOperator.Equal, new Guid(contrato))
                        );
                    q2.ColumnSet.AddColumns(HidrotecRoldecontrato.HidrotecTelefonofijo, HidrotecRoldecontrato.HidrotecTelefonomovil, HidrotecRoldecontrato.HidrotecFax);

                    var leRoles = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecMaestroderoles.EntityName, HidrotecRoldecontrato.HidrotecRolId, HidrotecMaestroderoles.PrimaryKey, JoinOperator.Inner);
                    leRoles.LinkCriteria.AddCondition(new ConditionExpression(HidrotecMaestroderoles.HidrotecIdrol, ConditionOperator.Equal, "1"));

                    q2.LinkEntities.Add(leRoles);

                    var res2 = service.RetrieveMultiple(q2);
                    if (res2.Entities.Count > 0)
                    {
                        exp[Incident.HidrotecTelefonofijo] = res2.Entities[0].GetAttributeValue<string>(HidrotecRoldecontrato.HidrotecTelefonofijo);
                        exp[Incident.HidrotecTelefonomovil] = res2.Entities[0].GetAttributeValue<string>(HidrotecRoldecontrato.HidrotecTelefonomovil);
                        exp[Incident.HidrotecFax] = res2.Entities[0].GetAttributeValue<string>(HidrotecRoldecontrato.HidrotecFax);
                    }
                }


            }
            else
            {
                exp[Incident.HidrotecCanalalarmas] = new OptionSetValue((int)Incident.HidrotecCanalalarmas_OptionSet.CorreoElectronico);
                exp[Incident.HidrotecCanaldocumentacion] = new OptionSetValue((int)Incident.HidrotecCanaldocumentacion_OptionSet.CorreoElectronico);
                exp[Incident.HidrotecCanalnotificacionresolucion] = new OptionSetValue((int)Incident.HidrotecCanalnotificacionresolucion_OptionSet.CorreoElectronico);
            }

            exp["customerid"] = new EntityReference("contact", new Guid(contacto));
            List<Guid> teamExplotacionConfiguracion = null;
            if (contrato.Split('_')[0] == "null")
            {
                teamExplotacionConfiguracion = getEquipoExplotacion(new Guid(contrato.Split('_')[1]), service);
                exp["hidrotec_municipioprincipalid"] = new EntityReference(HidrotecMunicipio.EntityName, new Guid(contrato.Split('_')[1]));
            }
            else
            {
                teamExplotacionConfiguracion = getEquipoExplotacion(((EntityReference)exp["hidrotec_municipioid"]).Id, service);
                exp["hidrotec_municipioprincipalid"] = exp["hidrotec_municipioid"];
            }

            exp["ownerid"] = new EntityReference("team", teamExplotacionConfiguracion.ElementAt(0));
            exp["hidrotec_asignadoalequipo"] = new EntityReference("team", teamExplotacionConfiguracion.ElementAt(0));
            exp["hidrotec_explotacionid"] = new EntityReference("businessunit", teamExplotacionConfiguracion.ElementAt(1));
            exp["hidrotec_configuracionorganizativaid"] = new EntityReference("hidrotec_configuracionorganizativa", teamExplotacionConfiguracion.ElementAt(2));

            if (tipificacion.Length >= 3)
            {
                exp[Incident.HidrotecRelacionId] = new EntityReference(HidrotecRelacion.EntityName, takeField(service, HidrotecRelacion.EntityName, HidrotecRelacion.HidrotecIdrelacion, tipificacion.Substring(0, 3)));
                if (tipificacion.Length >= 6)
                {
                    exp[Incident.HidrotecTipoId] = new EntityReference(HidrotecTipo.EntityName, takeField(service, HidrotecTipo.EntityName, HidrotecTipo.HidrotecIdtipo, tipificacion.Substring(0, 6)));
                    if (tipificacion.Length == 9)
                    {
                        exp[Incident.HidrotecSubtipoId] = new EntityReference(HidrotecSubtipo.EntityName, takeField(service, HidrotecSubtipo.EntityName, HidrotecSubtipo.HidrotecIdsubtipo, tipificacion.Substring(0, 9)));
                    }
                }
            }

            exp["hidrotec_canaldeentrada"] = new OptionSetValue(0);//OV

            return exp;
        }

        public static List<Guid> getEquipoExplotacion(Guid municipio, IOrganizationService servicio)
        {
            List<Guid> guids = new List<Guid>();


            var q = new QueryExpression("team");
            q.Criteria.AddCondition("isdefault", ConditionOperator.Equal, true);
            q.ColumnSet.AddColumn("businessunitid");

            var leConfiguracionOrganizativa = new LinkEntity("team", "hidrotec_configuracionorganizativa", "businessunitid", "hidrotec_explotacionid", JoinOperator.Inner);
            leConfiguracionOrganizativa.LinkCriteria.AddCondition("hidrotec_municipioid", ConditionOperator.Equal, municipio);
            leConfiguracionOrganizativa.Columns.AddColumn("hidrotec_configuracionorganizativaid");

            q.LinkEntities.Add(leConfiguracionOrganizativa);


            var res = servicio.RetrieveMultiple(q);
            if (res.Entities.Count > 0)
            {
                guids.Add((Guid)res.Entities[0]["teamid"]);
                guids.Add(((EntityReference)res.Entities[0]["businessunitid"]).Id);
                guids.Add((Guid)((AliasedValue)res.Entities[0]["hidrotec_configuracionorganizativa1.hidrotec_configuracionorganizativaid"]).Value);

                return guids;
            }
            return null;
        }


        public static Guid takeField(IOrganizationService service, string entity, string field, string value)
        {

            var q = new QueryExpression(entity);
            q.Criteria.AddCondition(field, ConditionOperator.Equal, value);
            q.ColumnSet.AddColumn(entity + "id");

            var res = service.RetrieveMultiple(q);
            if (res.Entities.Count > 0)
            {
                if (res.Entities[0].Contains(entity + "id"))
                    return ((Guid)res.Entities[0][entity + "id"]);
            }

            return new Guid();
        }

        internal static Guid getConfiguracion(IOrganizationService service, string explotacion, string contrata, string servicio)
        {
            Guid respuesta = Guid.Empty;
            QueryExpression q = new QueryExpression(HidrotecConfiguracionorganizativa.EntityName);
            q.Criteria.Conditions.AddRange(
                 new ConditionExpression(HidrotecConfiguracionorganizativa.HidrotecServicio, ConditionOperator.Equal, servicio),
                 new ConditionExpression(HidrotecConfiguracionorganizativa.HidrotecContrata, ConditionOperator.Equal, contrata),
                 new ConditionExpression(HidrotecConfiguracionorganizativa.HidrotecExplotacionId, ConditionOperator.Equal, new Guid(explotacion))
            );

            try
            {
                var res = service.RetrieveMultiple(q);
                foreach (var configuracion in res.Entities)
                {
                    respuesta = configuracion.Id;
                    return respuesta;
                }

                return respuesta;
            }
            catch (Exception ex)
            {
                return respuesta;
            }
        }


        public static Guid creacionContacto(string documento, string nombre, string primer, string segundo, string tipod, string paisd, IOrganizationService service)
        {
            try
            {
                if (documento != "")
                {
                    QueryExpression q = new QueryExpression(Contact.EntityName);
                    q.Criteria.Conditions.AddRange(
                            new ConditionExpression(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, documento),
                            new ConditionExpression(Contact.HidrotecNombre, ConditionOperator.Equal, nombre),
                            new ConditionExpression(Contact.HidrotecPrimerapellido, ConditionOperator.Equal, primer),
                            new ConditionExpression(Contact.HidrotecSegundoapellido, ConditionOperator.Equal, segundo)
                            );
                    var res = service.RetrieveMultiple(q);
                    if (res.Entities.Count > 0)
                    {
                        return res.Entities[0].Id;
                    }
                    else
                    {
                        //creo ese contact
                        Entity contacto = new Entity(Contact.EntityName);
                        contacto[Contact.HidrotecNombre] = nombre;
                        contacto[Contact.HidrotecPrimerapellido] = primer;
                        contacto[Contact.HidrotecSegundoapellido] = segundo;
                        contacto[Contact.HidrotecNumerodocumento] = documento;
                        contacto[Contact.PrimaryName] = nombre + " " + primer + " " + segundo;
                        contacto[Contact.HidrotecOrigenmodificacion] = new OptionSetValue((int)Contact.HidrotecOrigenmodificacion_OptionSet.CRM);
                        if (tipod != null)
                        {
                            contacto[Contact.HidrotecTipodedocumentoId] = new EntityReference(HidrotecTipodedocumento.EntityName, takeField(service, HidrotecTipodedocumento.EntityName, HidrotecTipodedocumento.HidrotecIdtipodedocumento, tipod));
                            if (tipod == "6")
                                contacto[Contact.HidrotecPaisidDocumento] = new EntityReference(HidrotecPais.EntityName, new Guid(paisd));
                        }
                        return service.Create(contacto);
                    }
                }
                else
                {
                    //creo ese contact
                    Entity contacto = new Entity(Contact.EntityName);
                    contacto[Contact.HidrotecNombre] = nombre;
                    contacto[Contact.HidrotecPrimerapellido] = primer;
                    contacto[Contact.HidrotecSegundoapellido] = segundo;
                    contacto[Contact.HidrotecNumerodocumento] = documento;
                    contacto[Contact.PrimaryName] = nombre + " " + primer + " " + segundo;
                    contacto[Contact.HidrotecOrigenmodificacion] = new OptionSetValue((int)Contact.HidrotecOrigenmodificacion_OptionSet.CRM);
                    if (tipod != null)
                    {
                        contacto[Contact.HidrotecTipodedocumentoId] = new EntityReference(HidrotecTipodedocumento.EntityName, takeField(service, HidrotecTipodedocumento.EntityName, HidrotecTipodedocumento.HidrotecIdtipodedocumento, tipod));
                        if (tipod == "6")
                            contacto[Contact.HidrotecPaisidDocumento] = new EntityReference(HidrotecPais.EntityName, new Guid(paisd));
                    }
                    return service.Create(contacto);
                }
            }
            catch (Exception ex)
            {
                return Guid.Empty;
            }
        }



        

        /// <summary>
        /// STATUS: OK Abel Gago - con dudas
        /// DUDAS: Abel Gago: No bien relacionado con el word, completar / mirar.
        /// DESC:
        /// Este servicio inserta una petición de alta avería en la base de datos de CAC. Para ello, se hacen las validaciones pertinentes de datos y se inserta en las tablas 
        /// historicoEstadoSolicitud con el procedimiento pov_inshistsolic y en la tabla informacionSolicitudes con el procedimiento pov_insinforsolic y la tabla solicitudes
        /// con la función fov_inssolicitud.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_AltaAveriaResponse NEO_AltaAveria(NEO_AltaAveriaRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_AltaAveriaResponse resp = new NEO_AltaAveriaResponse();

            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    // return (NEO_AltaAveriaResponse)respuestaWs.SinConexion();
                    return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    //return (NEO_AltaAveriaResponse)respuestaWs.SinParametros();
                    return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {
                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }
                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    //return (NEO_AltaAveriaResponse)respuestaWs.Pais();
                    return(NEO_AltaAveriaResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.detalle == "" || request.detalle == null)
                {
                    //return (NEO_AltaAveriaResponse)respuestaWs.Detalle();
                    return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", idiomaorigen, "Detalle");
                }
                if (request.direccion == null)
                {
                    
                    return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", idiomaorigen, "Direccion");
                }

                if (request.direccionsincodificar == "" && request.direccion == null)
                {
                    //return (NEO_AltaAveriaResponse)respuestaWs.Direccionsincodificar();
                    return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", request.idioma, "DireccionSinCodificar");
                }

                if (request.averia != null)
                {
                    if ((request.averia.codigotipoaveria == "" || request.averia.codigotipoaveria == null) || (request.averia.codigosubtipoaveria == "" || request.averia.codigosubtipoaveria == null))
                    {
                        //return (NEO_AltaAveriaResponse)respuestaWs.DatosAveria();
                        return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", idiomaorigen, "DatosAveria");
                    }
                }
                else
                {
                    //return (NEO_AltaAveriaResponse)respuestaWs.DatosAveria();
                    return (NEO_AltaAveriaResponse)respuestaWs.GetError("99999", idiomaorigen, "DatosAveria");
                }

                var asunto = "";
                Entity exp;
                Guid contacto = Guid.Empty;

                //JAGR Introducimos el identificador del Pais
                Guid idPais = takeField(Utils.IOS, HidrotecPais.EntityName, HidrotecPais.HidrotecIdpais, request.codpais);
                //Calculamos el municipio
                Entity Municipio = null;
                QueryExpression q = new QueryExpression(HidrotecMunicipio.EntityName);
                q.Criteria.AddCondition(new ConditionExpression(HidrotecMunicipio.HidrotecCodigoine, ConditionOperator.BeginsWith, request.direccion.codigomunicipio));
                q.ColumnSet.AddColumns(HidrotecMunicipio.HidrotecProvinciaId, HidrotecMunicipio.PrimaryName);
                EntityCollection Municipios = Utils.IOS.RetrieveMultiple(q);
                if (Municipios.Entities.Count > 0)
                {
                    
                    //Si el número es mayor que 1 se filtra por el nombre
                    if (Municipios.Entities.Count > 1)
                    {

                        q.Criteria.AddCondition(new ConditionExpression(HidrotecMunicipio.PrimaryName, ConditionOperator.Equal, request.direccion.nombremunicipio));
                        Municipios = Utils.IOS.RetrieveMultiple(q);
                        if (Municipios.Entities.Count() == 1)
                        {
                            Municipio = Municipios.Entities[0];


                        }
                    }
                    else
                    {
                        Municipio = Municipios.Entities[0];


                    }
                }



                //Miramos si será anonimo
                if (request.datoscontacto.apellido1 == "" || request.datoscontacto.nombre == "")
                {
                    contacto = takeField(Utils.IOS, Contact.EntityName, Contact.HidrotecIdunicoContactO, "ESP000000002");
                }
                else
                {
                    //Si no es anonimo miro si existe y sino lo creo
                    contacto = creacionContacto("", request.datoscontacto.nombre, request.datoscontacto.apellido1, request.datoscontacto.apellido2, null, null, Utils.IOS);
                }


                //Con y sin contrato
                if (request.identificadorcontrato.numerocontrato == "0")
                {
                   // Guid municipio = takeField(Utils.IOS, HidrotecMunicipio.EntityName, HidrotecMunicipio.HidrotecIdmunicipio, request.direccion.codigomunicipio);
                    // exp = expedienteBase(request.idioma, "002", Utils.IOS, "null_" + request.direccion.codigomunicipio, contacto.ToString());
                    exp = expedienteBase(request.idioma, "002", Utils.IOS, "null_" + Municipio.Id.ToString(), contacto.ToString());
                }
                else
                {
                    Guid contrato = takeField(Utils.IOS, HidrotecContrato.EntityName, HidrotecContrato.PrimaryName, request.identificadorcontrato.numerocontrato);
                    exp = expedienteBase(request.idioma, "002", Utils.IOS, contrato.ToString(), contacto.ToString());
                }

                if (request.direccionsincodificar != "")
                {
                    exp[Incident.HidrotecDireccioncompuesta] = request.direccionsincodificar;
                    exp[Incident.HidrotecDireccioncompuestaauxiliar] = request.direccionsincodificar;
                }
                else
                {

     
                    if (Municipio != null)
                    {

                        //  exp[Incident.HidrotecProvinciaauxiliarId] = new EntityReference(HidrotecProvincia.EntityName, new Guid(request.provinciaid));
                        exp[Incident.HidrotecMunicipioauxiliarId] = new EntityReference(HidrotecMunicipio.EntityName, Municipio.Id);

                        // exp[Incident.HidrotecProvinciaId] = new EntityReference(HidrotecProvincia.EntityName, new Guid(request.provinciaid));
                        exp[Incident.HidrotecMunicipioId] = new EntityReference(HidrotecMunicipio.EntityName, Municipio.Id);
                        if (Municipio.HasAttributeValue<EntityReference>(HidrotecMunicipio.HidrotecProvinciaId))
                        {
                            exp[Incident.HidrotecProvinciaauxiliarId] = Municipio.GetAttributeValue<EntityReference>(HidrotecMunicipio.HidrotecProvinciaId);
                            exp[Incident.HidrotecProvinciaId] = Municipio.GetAttributeValue<EntityReference>(HidrotecMunicipio.HidrotecProvinciaId);

                        }


                        //Se completa la via

                    }

                    if (request.direccion.via != null)
                    {
                        Boolean IntroducidoIdVia = false;
                        if ((request.direccion.via.codigovia != "") && (request.direccion.via.codigovia != null))
                        {
                            q = new QueryExpression(HidrotecConversionMunicipioinstalacionvia.EntityName);
                            q.ColumnSet.AddColumn(HidrotecConversionMunicipioinstalacionvia.HidrotecIdviacrm);
                            q.Criteria.AddCondition(new ConditionExpression(HidrotecConversionMunicipioinstalacionvia.HidrotecIdviadiversa,ConditionOperator.Equal, request.direccion.via.codigovia));
                            q.Criteria.AddCondition(new ConditionExpression(HidrotecConversionMunicipioinstalacionvia.HidrotecIdinstalaciondiversa, ConditionOperator.Equal, request.identificadorcontrato.codigoinstalacion));
                            EntityCollection ConversionVia = Utils.IOS.RetrieveMultiple(q);
                            if (ConversionVia.Entities.Count > 0)
                            {
                                Entity EntidadConversionVia = ConversionVia.Entities[0];
                                //Se busca la via asociada
                                if (EntidadConversionVia.HasAttributeValue<string>(HidrotecConversionMunicipioinstalacionvia.HidrotecIdviacrm))
                                {
                                    string codigoVia = EntidadConversionVia.GetAttributeValue<string>(HidrotecConversionMunicipioinstalacionvia.HidrotecIdviacrm);
                                    //Se busca la via por el codigo
                                    q = new QueryExpression(HidrotecVia.EntityName);
                                    q.ColumnSet.AddColumns(HidrotecVia.HidrotecIdvia, HidrotecVia.PrimaryName);
                                    q.Criteria.AddCondition(new ConditionExpression(HidrotecVia.HidrotecIdvia, ConditionOperator.Equal, codigoVia));
                                    EntityCollection Vias = Utils.IOS.RetrieveMultiple(q);
                                    if (Vias.Entities.Count > 0)
                                    {
                                        Entity EntidadVia = Vias.Entities[0];
                                        exp[Incident.HidrotecViaauxiliarId] = new EntityReference(EntidadVia.LogicalName,EntidadVia.Id);
                                        exp[Incident.HidrotecViaId] = new EntityReference(EntidadVia.LogicalName, EntidadVia.Id);
                                        IntroducidoIdVia = true;

                                    }


                                }



                            }

                        }
                        if (IntroducidoIdVia == false)
                        {
                            exp[Incident.HidrotecViaauxiliar] = request.direccion.via.nombrevia;
                            exp[Incident.HidrotecVia] = request.direccion.via.nombrevia;

                        }




                    }
                    exp[Incident.HidrotecPaisauxiliarId] = new EntityReference(HidrotecPais.EntityName, idPais);
                    
                    exp[Incident.HidrotecNumeroauxiliar] = request.direccion.numero;
                    exp[Incident.HidrotecCodigoPostalAuxiliar] = request.direccion.codigopostal;
                    exp[Incident.HidrotecPuertaauxiliar] = request.direccion.puerta;
                    exp[Incident.HidrotecPisoauxiliar] = request.direccion.piso;
                    exp[Incident.HidrotecEscaleraauxiliar] = request.direccion.escalera;
                    exp[Incident.HidrotecEdificioauxiliar] = request.direccion.edificio;
                    exp[Incident.HidrotecBloqueauxiliar] = request.direccion.bloque;
                    exp[Incident.HidrotecOtrosauxiliar] = request.direccion.otros;
                    exp[Incident.HidrotecPaisId] = new EntityReference(HidrotecPais.EntityName, idPais);
                    exp[Incident.HidrotecNumero] = request.direccion.numero;
                    exp[Incident.HidrotecCodigoPostal] = request.direccion.codigopostal;
                    exp[Incident.HidrotecPuerta] = request.direccion.puerta;
                    exp[Incident.HidrotecPiso] = request.direccion.piso;
                    exp[Incident.HidrotecEscalera] = request.direccion.escalera;
                    exp[Incident.HidrotecEdificio] = request.direccion.edificio;
                    exp[Incident.HidrotecBloque] = request.direccion.bloque;
                    exp[Incident.HidrotecOtros] = request.direccion.otros;
                    
                }

                if ((request.datoscontacto.telefono[4] == '6' && request.datoscontacto.telefono.Length == 9) || (request.datoscontacto.telefono[4] == '7' && request.datoscontacto.telefono.Length == 9))
                {
                    exp[Incident.HidrotecTelefonomovil] = request.datoscontacto.telefono;
                }
                else
                {
                    exp[Incident.HidrotecTelefonofijo] = request.datoscontacto.telefono;
                }


                asunto = (request.datoscontacto.nombre != null ? "( " + request.datoscontacto.nombre : " ") + " " + (request.datoscontacto.apellido1 != null ? request.datoscontacto.apellido1 : " ") + " " + (request.datoscontacto.apellido2 != null ? request.datoscontacto.apellido2 : " ") + " ) -";


                exp[Incident.HidrotecCanaldeentrada] = new OptionSetValue((int)Incident.HidrotecCanaldeentrada_OptionSet.APP);
                //Rennemamos el tipo y subtipo de Registro
                Entity Subtipo = MapearSubtipo(request.averia.codigosubtipoaveria);

                exp[Incident.HidrotecSubtipoId] = new EntityReference(Subtipo.LogicalName, Subtipo.Id);
                if (Subtipo.HasAttributeValue<EntityReference>(HidrotecSubtipo.HidrotecTipoId))
                {
                    exp[Incident.HidrotecTipoId] = Subtipo.GetAttributeValue<EntityReference>(HidrotecSubtipo.HidrotecTipoId);


                }

               var idExp = Utils.IOS.Create(exp);

               


               asunto = asunto + request.detalle;
                // Meter imagen en una nota
                if (idExp != null)
                {
                    if ((request.imagen1 != null)&&(request.imagen1!=""))
                    {
                        InsertarImagen(idExp, request.imagen1, "Imagen1");


                    }
                    if ((request.imagen2 != null) && (request.imagen2 != ""))
                    {
                        InsertarImagen(idExp, request.imagen1, "Imagen2");


                    }


                }

                // resp.Codigorespuesta = "00001";
                //  resp.Textorespuesta = "OK";
                return (NEO_AltaAveriaResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK");

                
        }
       catch (Exception e)
       {
                // return (NEO_AltaAveriaResponse) respuestaWs.Exception(e);
                return (NEO_AltaAveriaResponse)respuestaWs.Exception(e,"99999");
       }
    //}
}

/// <summary>
/// STATUS: OK Abel Gago
/// TODOO: CruceVia, ver de donde se coge eso o que es.
/// DESC:
/// Este servicio devuelve los datos de la dirección del contrato introducido por parámetros. Para ello se hacen las validaciones pertinentes y se devuelve 
/// los datos a través de las funciones fov_seldatdirctt, fapp_seldespobl y fapp_seldestipvia.
/// </summary>
/// <param name="request"></param>
/// <returns></returns>
public NEO_RecuperarDireccionResponse NEO_RecuperarDireccion(NEO_RecuperarDireccionRequest request)
{
    RespuestasWS respuestaWs = new RespuestasWS();
    NEO_RecuperarDireccionResponse resp = new NEO_RecuperarDireccionResponse();

    //request.Identificadorcontrato = new Identificadorcontrato();
    //request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";

    try
    {
        Utils.InicializarVariables();

        if (Utils.IOS == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.SinConexion();
        }

        if (request == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.SinParametros();
        }

        if (request.canalentrada == "" || request.canalentrada == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.CanalEntrada();
        }

        if (request.idioma == "" || request.idioma == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.Idioma();
        }

        if (request.codpais == "" || request.codpais == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.Pais();
        }
        /*
        if (request.Identificadorcontrato.codigocac == "" || request.Identificadorcontrato.codigocac == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.Codigocac();
        }

        if (request.Identificadorcontrato.codigocontrato == "" || request.Identificadorcontrato.codigocontrato == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.Codigocontrato();
        }

        if (request.Identificadorcontrato.codigoinstalacion == "" || request.Identificadorcontrato.codigoinstalacion == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.Codigoinstalacion();
        }

        if (request.Identificadorcontrato.numerocontrato == "" || request.Identificadorcontrato.numerocontrato == null)
        {
            return (NEO_RecuperarDireccionResponse)respuestaWs.Numerocontrato();
        }
        */
        if (request.contrato != null)
        {
            if (request.contrato.numerocontrato == "" || request.contrato.numerocontrato == null)
            {
                return (NEO_RecuperarDireccionResponse)respuestaWs.Numerocontrato();

            }
            if (request.contrato.codigocac == "" || request.contrato.codigocac == null)
            {

                //return (NEO_RecuperarDireccionResponse)respuestaWs.Codigocac();
            }
            if (request.contrato.codigocontrato == "" || request.contrato.codigocontrato == null)
            {
                return (NEO_RecuperarDireccionResponse)respuestaWs.Codigocontrato();
            }
            if (request.contrato.codigoinstalacion == "" || request.contrato.codigoinstalacion == null)
            {
                return (NEO_RecuperarDireccionResponse)respuestaWs.Codigoinstalacion();
            }
        }
        else
        {

            return (NEO_RecuperarDireccionResponse)respuestaWs.Identificadorcontrato();


        }
        string strRolCorrespondencia = "3";

        string strAliasleRolContrato = "roldecontrato";
        string strQueryleRolContrato = "roldecontrato.";
        string strAliasleMunicipio = "municipio";
        string strQueryleMunicipio = "municipio.";
        string strAliaslePedania = "pedania";
        string strQuerylePedania = "pedania.";
        string strAliasleVia = "via";
        string strQueryleVia = "via.";
        string strAliasleTipoVia = "tipovia";
        string strQueryleTipoVia = "tipovia.";
        //
        string strAliasleDirSum = "direccionsuministo";
        string strQueryleDirSum = "direccionsuministo.";


        /*
        string strAliasleCruceVia = "crucevia";
        string strQueryleCruceVia = "crucevia.";
        string strAliasleCruceTipoVia = "crucetipovia";
        string strQueryleCruceTipoVia = "crucetipovia.";
        */

        QueryExpression q = new QueryExpression(Contact.EntityName);
        q.ColumnSet.AddColumns(Contact.HidrotecNombre, Contact.HidrotecPrimerapellido, Contact.HidrotecSegundoapellido);
        q.NoLock = true;
        LinkEntity leContrato = new LinkEntity(Contact.EntityName, HidrotecContrato.EntityName,
            Contact.PrimaryKey, HidrotecContrato.HidrotecTitularId, JoinOperator.Inner);
        leContrato.LinkCriteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.contrato.numerocontrato);

        LinkEntity leRolContrato = new LinkEntity(HidrotecContrato.EntityName, HidrotecRoldecontrato.EntityName,
            HidrotecContrato.PrimaryKey, HidrotecRoldecontrato.HidrotecContratoId, JoinOperator.Inner);
        leRolContrato.EntityAlias = strAliasleRolContrato;
        leRolContrato.Columns.AddColumns(HidrotecRoldecontrato.HidrotecBloque,
                                         HidrotecRoldecontrato.HidrotecCodigoPostal,
                                         HidrotecRoldecontrato.HidrotecEdificio,
                                         HidrotecRoldecontrato.HidrotecEscalera,
                                         HidrotecRoldecontrato.HidrotecNumero,
                                         HidrotecRoldecontrato.HidrotecPiso,
                                         HidrotecRoldecontrato.HidrotecPuerta,
                                         HidrotecRoldecontrato.HidrotecOtros,
                                         HidrotecRoldecontrato.HidrotecViaId);

        LinkEntity leRoles = new LinkEntity(HidrotecRoldecontrato.EntityName, Rol.LogicalName,
            HidrotecRoldecontrato.HidrotecRolId, Rol.Id, JoinOperator.Inner);
        leRoles.LinkCriteria.AddCondition(Rol.IdRol, ConditionOperator.Equal, strRolCorrespondencia);

        LinkEntity leDirSumm = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
            HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
        leDirSumm.Columns.AddColumn(HidrotecDirecciondesuministro.HidrotecMunicipioId);
        //Se añade la pedania
        leDirSumm.Columns.AddColumn(HidrotecDirecciondesuministro.HidrotecPedaniaId);
        leDirSumm.EntityAlias = strAliasleDirSum;
        //
        LinkEntity leMunicipio = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecMunicipio.EntityName,
            HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecMunicipio.PrimaryKey, JoinOperator.Inner);
        leMunicipio.EntityAlias = strAliasleMunicipio;
        leMunicipio.Columns.AddColumns(HidrotecMunicipio.PrimaryName, HidrotecMunicipio.HidrotecIdmunicipio);

        LinkEntity lePedania = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecPedania.EntityName,
            HidrotecDirecciondesuministro.HidrotecPedaniaId, HidrotecPedania.PrimaryKey, JoinOperator.LeftOuter);
        lePedania.EntityAlias = strAliaslePedania;
        lePedania.Columns.AddColumns(HidrotecPedania.PrimaryName, HidrotecPedania.HidrotecIdpedania);

        LinkEntity leVia = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecVia.EntityName,
            HidrotecRoldecontrato.HidrotecViaId, HidrotecVia.PrimaryKey, JoinOperator.LeftOuter);
        leVia.EntityAlias = strAliasleVia;
        leVia.Columns.AddColumns(HidrotecVia.HidrotecIdvia, HidrotecVia.PrimaryName);

        LinkEntity leTipoVia = new LinkEntity(HidrotecVia.EntityName, HidrotecTipovia.EntityName,
            HidrotecVia.HidrotecTipoviaId, HidrotecTipovia.PrimaryKey, JoinOperator.Inner);
        leTipoVia.EntityAlias = strAliasleTipoVia;
        leTipoVia.Columns.AddColumns(HidrotecTipovia.HidrotecIdtipovia, HidrotecTipovia.PrimaryName);

        leVia.LinkEntities.Add(leTipoVia);
        //Quitamos Via y Pedania
        //leDirSumm.LinkEntities.AddRange(leMunicipio, lePedania);
        //leRolContrato.LinkEntities.AddRange(leRoles, leVia);
        leDirSumm.LinkEntities.AddRange(leMunicipio);
        leRolContrato.LinkEntities.AddRange(leRoles);
        leContrato.LinkEntities.AddRange(leRolContrato, leDirSumm);
        q.LinkEntities.Add(leContrato);

        EntityCollection res = Utils.IOS.RetrieveMultiple(q);
        if (res.Entities.Any())
        {
            Entity item = res.Entities[0];

            resp.direccion = new Model.NEO_RecuperarDireccion.Direccion();
            resp.direccion.bloque = item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecBloque) ? (string)((AliasedValue)item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecBloque]).Value : null;
            resp.direccion.codigopostal = item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecCodigoPostal) ? (string)((AliasedValue)item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecCodigoPostal]).Value : null;
            resp.direccion.edificio = item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecEdificio) ? (string)((AliasedValue)item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecEdificio]).Value : null;
            resp.direccion.escalera = item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecEscalera) ? (string)((AliasedValue)item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecEscalera]).Value : null;
            resp.direccion.numero = item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecNumero) ? (string)((AliasedValue)item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecNumero]).Value : null;
            resp.direccion.piso = item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecPiso) ? (string)((AliasedValue)item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecPiso]).Value : null;
            resp.direccion.puerta = item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecPuerta) ? (string)((AliasedValue)item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecPuerta]).Value : null;
            resp.direccion.otros = item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecOtros) ? (string)((AliasedValue)item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecOtros]).Value : null;
            resp.direccion.codigomunicipio = item.Contains(strQueryleMunicipio + HidrotecMunicipio.HidrotecIdmunicipio) ? (string)((AliasedValue)item[strQueryleMunicipio + HidrotecMunicipio.HidrotecIdmunicipio]).Value : null;
            resp.direccion.nombremunicipio = item.Contains(strQueryleMunicipio + HidrotecMunicipio.PrimaryName) ? (string)((AliasedValue)item[strQueryleMunicipio + HidrotecMunicipio.PrimaryName]).Value : null;
            //Si la pedania no existe no se incluye
            if (item.Contains(strQueryleDirSum + HidrotecDirecciondesuministro.HidrotecPedaniaId))
            {
                QueryExpression qPedania = new QueryExpression(Pedania.LogicalName);
                qPedania.ColumnSet.AddColumns(HidrotecPedania.HidrotecIdpedania, HidrotecPedania.PrimaryName);
                qPedania.Criteria.AddCondition(Pedania.Id, ConditionOperator.Equal, (((EntityReference)((AliasedValue)(item[strQueryleDirSum + HidrotecDirecciondesuministro.HidrotecPedaniaId])).Value)).Id.ToString());
                qPedania.NoLock = true;
                EntityCollection resPedania = Utils.IOS.RetrieveMultiple(qPedania);
                if (resPedania.Entities.Any())
                {
                    Entity itemPedania = resPedania.Entities[0];
                    resp.direccion.codigopedania = itemPedania.HasAttributeValue<string>(HidrotecPedania.HidrotecIdpedania) ? itemPedania.GetAttributeValue<string>(HidrotecPedania.HidrotecIdpedania) : null;
                    resp.direccion.nombrepedania = itemPedania.HasAttributeValue<string>(HidrotecPedania.PrimaryName) ? itemPedania.GetAttributeValue<string>(HidrotecPedania.PrimaryName) : null;


                }

            }
            if (item.Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecViaId))
            {
                QueryExpression qVia = new QueryExpression(Helper.Via.LogicalName);
                qVia.ColumnSet.AddColumns(HidrotecVia.PrimaryName, HidrotecVia.HidrotecIdvia, HidrotecVia.HidrotecTipoviaId);
                qVia.Criteria.AddCondition(Helper.Via.LogicalName + "id", ConditionOperator.Equal, (((EntityReference)((AliasedValue)(item[strQueryleRolContrato + HidrotecRoldecontrato.HidrotecViaId])).Value)).Id.ToString());
                qVia.NoLock = true;
                EntityCollection resVia = Utils.IOS.RetrieveMultiple(qVia);
                if (resVia.Entities.Any())
                {
                    Entity itemVia = resVia.Entities[0];
                    resp.direccion.suministrovia = new Model.NEO_RecuperarDireccion.Via();
                    resp.direccion.suministrovia.Nombrevia = itemVia.HasAttributeValue<string>(HidrotecVia.PrimaryName) ? itemVia.GetAttributeValue<string>(HidrotecVia.PrimaryName) : null;
                    resp.direccion.suministrovia.Codigovia = itemVia.HasAttributeValue<string>(HidrotecVia.HidrotecIdvia) ? itemVia.GetAttributeValue<string>(HidrotecVia.HidrotecIdvia) : null;
                    if (itemVia.HasAttributeValue<EntityReference>(HidrotecVia.HidrotecTipoviaId))
                    {
                        QueryExpression qTipoVia = new QueryExpression(TipoVia.LogicalName);
                        qTipoVia.ColumnSet.AddColumns(HidrotecTipovia.PrimaryName, HidrotecTipovia.HidrotecIdtipovia);
                        qTipoVia.Criteria.AddCondition(HidrotecTipovia.EntityName + "id", ConditionOperator.Equal, itemVia.GetAttributeValue<EntityReference>(HidrotecVia.HidrotecTipoviaId).Id.ToString());
                        qTipoVia.NoLock = true;
                        EntityCollection resTipoVia = Utils.IOS.RetrieveMultiple(qTipoVia);
                        if (resTipoVia.Entities.Any())
                        {
                            resp.direccion.suministrovia.Nombretipovia = itemVia.HasAttributeValue<string>(HidrotecTipovia.PrimaryName) ? itemVia.GetAttributeValue<string>(HidrotecTipovia.PrimaryName) : null; ;
                            resp.direccion.suministrovia.Codigotipovia = itemVia.HasAttributeValue<string>(HidrotecTipovia.HidrotecIdtipovia) ? itemVia.GetAttributeValue<string>(HidrotecTipovia.HidrotecIdtipovia) : null;
                        }
                    }
                }
            }
            /* resp.Direccion.Codigopedania = item.Contains(strQuerylePedania + HidrotecPedania.HidrotecIdpedania) ? (string)((AliasedValue)item[strQuerylePedania + HidrotecPedania.HidrotecIdpedania]).Value : null;
             resp.Direccion.Nombrepedania = item.Contains(strQuerylePedania + HidrotecPedania.PrimaryName) ? (string)((AliasedValue)item[strQuerylePedania + HidrotecPedania.PrimaryName]).Value : null;

             resp.Direccion.Via = new Model.NEO_RecuperarDireccion.Via();
             resp.Direccion.Via.Nombrevia = item.Contains(strQueryleVia + HidrotecVia.PrimaryName) ? (string)((AliasedValue)item[strQueryleVia + HidrotecVia.PrimaryName]).Value : null;
             resp.Direccion.Via.Codigovia = item.Contains(strQueryleVia + HidrotecVia.HidrotecIdvia) ? (string)((AliasedValue)item[strQueryleVia + HidrotecVia.HidrotecIdvia]).Value : null;
             resp.Direccion.Via.Nombretipovia = item.Contains(strQueryleTipoVia + HidrotecTipovia.PrimaryName) ? (string)((AliasedValue)item[strQueryleTipoVia + HidrotecVia.PrimaryName]).Value : null;
             resp.Direccion.Via.Codigotipovia = item.Contains(strQueryleTipoVia + HidrotecTipovia.HidrotecIdtipovia) ? (string)((AliasedValue)item[strQueryleTipoVia + HidrotecTipovia.HidrotecIdtipovia]).Value : null;*/

            resp.direccion.crucevia = null;
            //JAGR si hay datos se devuelve OK
            resp.codigorespuesta = "OK";
            resp.textorespuesta = "OK";

        }
        else
        {


            return (NEO_RecuperarDireccionResponse)respuestaWs.ContratoBuscado();

        }

        //resp.Codigorespuesta = "OK";
        //resp.Textorespuesta = "OK";

        return resp;
    }
    catch (Exception e)
    {
        return (NEO_RecuperarDireccionResponse)respuestaWs.Exception(e);
    }
}

/// <summary>
/// STATUS: OK Abel Gago
/// DUDAS: Recuperados los datos del Contacto, no del usuarioOV asociado al contacto.
/// Email y teléfono, where?
/// DESC:
/// Este servicio devuelve los datos del contacto del contrato introducido por parámetros. Para ello se hacen las validaciones pertinentes y se devuelve 
/// los datos a través de la función fov_seldatpersctt.
/// </summary>
/// <param name="request"></param>
/// <returns></returns>
public NEO_RecuperarDatosContactoResponse NEO_RecuperarDatosContacto(NEO_RecuperarDatosContactoRequest request)
{
    RespuestasWS respuestaWs = new RespuestasWS();
    NEO_RecuperarDatosContactoResponse resp = new NEO_RecuperarDatosContactoResponse();

    // request.Identificadorcontrato.= new Identificadorcontrato();
    // request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";

    try
    {
        Utils.InicializarVariables();

        if (Utils.IOS == null)
        {
            return (NEO_RecuperarDatosContactoResponse)respuestaWs.SinConexion();
        }

        if (request == null)
        {
            return (NEO_RecuperarDatosContactoResponse)respuestaWs.SinParametros();
        }

        if (request.canalentrada == "" || request.canalentrada == null)
        {
            return (NEO_RecuperarDatosContactoResponse)respuestaWs.CanalEntrada();
        }

        if (request.idioma == "" || request.idioma == null)
        {
            return (NEO_RecuperarDatosContactoResponse)respuestaWs.Idioma();
        }

        if (request.codpais == "" || request.codpais == null)
        {
            return (NEO_RecuperarDatosContactoResponse)respuestaWs.Pais();
        }

        if (request.contrato != null)
        {
            if (request.contrato.numerocontrato == "" || request.contrato.numerocontrato == null)
            {
                return (NEO_RecuperarDatosContactoResponse)respuestaWs.Numerocontrato();

            }
            if (request.contrato.codigocac == "" || request.contrato.codigocac == null)
            {

                //  return (NEO_RecuperarDatosContactoResponse)respuestaWs.Codigocac();
            }
            if (request.contrato.codigocontrato == "" || request.contrato.codigocontrato == null)
            {
                return (NEO_RecuperarDatosContactoResponse)respuestaWs.Codigocontrato();
            }
            if (request.contrato.codigoinstalacion == "" || request.contrato.codigoinstalacion == null)
            {
                return (NEO_RecuperarDatosContactoResponse)respuestaWs.Codigoinstalacion();
            }
        }
        else
        {

            return (NEO_RecuperarDatosContactoResponse)respuestaWs.Identificadorcontrato();


        }

        string strRolTitular = "1";
        string strAliasleRolContrato = "roldecontrato";
        string strQueryleRolContrato = "roldecontrato.";

        QueryExpression q = new QueryExpression(Contact.EntityName);
        q.ColumnSet.AddColumns(Contact.HidrotecNombre, Contact.HidrotecPrimerapellido, Contact.HidrotecSegundoapellido);
        q.NoLock = true;
        LinkEntity leContrato = new LinkEntity(Contact.EntityName, HidrotecContrato.EntityName,
            Contact.PrimaryKey, HidrotecContrato.HidrotecTitularId, JoinOperator.Inner);
        leContrato.LinkCriteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.contrato.numerocontrato);

        LinkEntity leRolContrato = new LinkEntity(HidrotecContrato.EntityName, HidrotecRoldecontrato.EntityName,
            HidrotecContrato.PrimaryKey, HidrotecRoldecontrato.HidrotecContratoId, JoinOperator.Inner);
        leRolContrato.Columns.AddColumns(HidrotecRoldecontrato.HidrotecCorreoelectronico, HidrotecRoldecontrato.HidrotecTelefonofijo, HidrotecRoldecontrato.HidrotecTelefonomovil);
        leRolContrato.EntityAlias = strAliasleRolContrato;

        LinkEntity leRoles = new LinkEntity(HidrotecRoldecontrato.EntityName, Rol.LogicalName,
            HidrotecRoldecontrato.HidrotecRolId, Rol.Id, JoinOperator.Inner);
        leRoles.LinkCriteria.AddCondition(Rol.IdRol, ConditionOperator.Equal, strRolTitular);

        leRolContrato.LinkEntities.Add(leRoles);
        leContrato.LinkEntities.Add(leRolContrato);
        q.LinkEntities.Add(leContrato);

        EntityCollection res = Utils.IOS.RetrieveMultiple(q);

        if (res.Entities.Any())
        {
            resp.datoscontacto = new Model.NEO_RecuperarDatosContacto.Datoscontacto();
            resp.datoscontacto.nombre = res.Entities[0].Contains(Contact.HidrotecNombre) ? (string)res.Entities[0][Contact.HidrotecNombre] : null;
            resp.datoscontacto.apellido1 = res.Entities[0].Contains(Contact.HidrotecPrimerapellido) ? (string)res.Entities[0][Contact.HidrotecPrimerapellido] : null;
            resp.datoscontacto.apellido2 = res.Entities[0].Contains(Contact.HidrotecSegundoapellido) ? (string)res.Entities[0][Contact.HidrotecSegundoapellido] : null;
            resp.datoscontacto.correoelectronico = res.Entities[0].Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecCorreoelectronico) ? (string)((AliasedValue)res.Entities[0][strQueryleRolContrato + HidrotecRoldecontrato.HidrotecCorreoelectronico]).Value : null;
            resp.datoscontacto.telefono = res.Entities[0].Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecTelefonofijo) ? (string)((AliasedValue)res.Entities[0][strQueryleRolContrato + HidrotecRoldecontrato.HidrotecTelefonofijo]).Value : null;
            resp.datoscontacto.telefono = resp.datoscontacto.telefono == null ? (res.Entities[0].Contains(strQueryleRolContrato + HidrotecRoldecontrato.HidrotecTelefonomovil) ? (string)((AliasedValue)res.Entities[0][strQueryleRolContrato + HidrotecRoldecontrato.HidrotecTelefonomovil]).Value : null) : resp.datoscontacto.telefono;
            resp.codigorespuesta = "OK";
            resp.textorespuesta = "OK";
        }
        else
        {
            return (NEO_RecuperarDatosContactoResponse)respuestaWs.ContratoBuscado();


        }
        //resp.Codigorespuesta = "OK";
        //resp.Textorespuesta = "OK";

        return resp;
    }
    catch (Exception e)
    {
        return (NEO_RecuperarDatosContactoResponse)respuestaWs.Exception(e);
    }
}

//
public static string GetImageMimeType(byte[] imageData)
{
    String mimeType = "image/unknown";

    try
    {
        Guid id;

        using (MemoryStream ms = new MemoryStream(imageData))
        {
            using (Image img = Image.FromStream(ms))
            {
                id = img.RawFormat.Guid;
            }
        }

        if (id == ImageFormat.Png.Guid)
        {
            mimeType = "image/png";
        }
        else if (id == ImageFormat.Bmp.Guid)
        {
            mimeType = "image/bmp";
        }
        else if (id == ImageFormat.Emf.Guid)
        {
            mimeType = "image/x-emf";
        }
        else if (id == ImageFormat.Exif.Guid)
        {
            mimeType = "image/jpeg";
        }
        else if (id == ImageFormat.Gif.Guid)
        {
            mimeType = "image/gif";
        }
        else if (id == ImageFormat.Icon.Guid)
        {
            mimeType = "image/ico";
        }
        else if (id == ImageFormat.Jpeg.Guid)
        {
            mimeType = "image/jpeg";
        }
        else if (id == ImageFormat.MemoryBmp.Guid)
        {
            mimeType = "image/bmp";
        }
        else if (id == ImageFormat.Tiff.Guid)
        {
            mimeType = "image/tiff";
        }
        else if (id == ImageFormat.Wmf.Guid)
        {
            mimeType = "image/wmf";
        }
    }
    catch
    {
    }

    return mimeType;
}

public static string GetImageExtension(byte[] imageData)
{
    String extension = "";

    try
    {
        Guid id;

        using (MemoryStream ms = new MemoryStream(imageData))
        {
            using (Image img = Image.FromStream(ms))
            {
                id = img.RawFormat.Guid;
            }
        }

        if (id == ImageFormat.Png.Guid)
        {
            extension = "png";
        }
        else if (id == ImageFormat.Bmp.Guid)
        {
            extension = "bmp";
        }
        else if (id == ImageFormat.Emf.Guid)
        {
            extension = "emf";
        }
        else if (id == ImageFormat.Exif.Guid)
        {
            extension = "jpg";
        }
        else if (id == ImageFormat.Gif.Guid)
        {
            extension = "gif";
        }
        else if (id == ImageFormat.Icon.Guid)
        {
            extension = "ico";
        }
        else if (id == ImageFormat.Jpeg.Guid)
        {
            extension = "jpg";
        }
        else if (id == ImageFormat.MemoryBmp.Guid)
        {
            extension = "bmp";
        }
        else if (id == ImageFormat.Tiff.Guid)
        {
            extension = "tif";
        }
        else if (id == ImageFormat.Wmf.Guid)
        {
            extension = "wmf";
        }
    }
    catch
    {
    }

    return extension;
}
/*


byte[] data = System.Convert.FromBase64String(request.imagen1);


Entity AnnotationEntityObject = new Entity(Annotation.EntityName);
AnnotationEntityObject.Attributes[Annotation.ObjectId] = new EntityReference(Incident.EntityName, id);
AnnotationEntityObject.Attributes["subject"] = "Imagen1";
                        AnnotationEntityObject.Attributes["documentbody"] = request.imagen1;
                        // Set the type of attachment
                        AnnotationEntityObject.Attributes["mimetype"] = GetImageMimeType(data);
    // Set the File Name
    AnnotationEntityObject.Attributes["filename"] = "imagen1."+ GetImageExtension(data);
    var idNotaImagen1 = Utils.IOS.Create(AnnotationEntityObject);*/
    //
    public static void InsertarImagen(Guid IdExpediente, string Imagen, string NombreImagen)

    {

        byte[] data = System.Convert.FromBase64String(Imagen);


        Entity AnnotationEntityObject = new Entity(Annotation.EntityName);
        AnnotationEntityObject.Attributes[Annotation.ObjectId] = new EntityReference(Incident.EntityName, IdExpediente);
        AnnotationEntityObject.Attributes["subject"] = NombreImagen;
        AnnotationEntityObject.Attributes["documentbody"] = Imagen;
        // Set the type of attachment
        AnnotationEntityObject.Attributes["mimetype"] = GetImageMimeType(data);
        // Set the File Name
        AnnotationEntityObject.Attributes["filename"] = NombreImagen + "." + GetImageExtension(data);
        var idNotaImagen1 = Utils.IOS.Create(AnnotationEntityObject);

    }

        public static Entity MapearSubtipo(string subTipo)
        {
            Entity Res=null;
            string idSubtipo = "";
            
            switch (subTipo)
            {
                case "96"://Poca Presion
                    idSubtipo = "002007003";
                    break;
                case "97"://Poco Caudal
                    idSubtipo = "002007004";
                    break;
                case "98"://Problemas Calidad Agua
                    idSubtipo = "002007005";
                    break;
                case "100"://Fuga Acometida
                    idSubtipo = "002005001";
                    break;
                case "101"://Fuga Red General
                    idSubtipo = "002005002";
                    break;
                case "121"://Fugas en Bocas de Riego
                    idSubtipo = "002005003";
                    break;
                case "122"://Registros rotos
                    idSubtipo = "002005005";
                    break;
                case "123"://Llaves Rotos o con Perdidas
                    idSubtipo = "002005004";
                    break;
                case "124"://Rotura o Fuga de Hidrante
                    idSubtipo = "002005006";
                    break;
                case "125"://Atranque Red General
                    idSubtipo = "002006003";
                    break;
                case "128"://Arranque Imbornales
                    idSubtipo = "002006002";
                    break;
                case "129"://Rotura Red General
                    idSubtipo = "002006007";
                    break;
                case "132"://Imbornales o sumideros rotos
                    idSubtipo = "002006005";
                    break;
                case "133"://Arquetas Rotas
                    idSubtipo = "002006001";
                    break;
                case "134"://Registros Rotos
                    idSubtipo = "002006006";
                    break;
                case "135"://Atranque Pozo de Registro
                    idSubtipo = "002006004";
                    break;
                case "136"://Contadores
                    idSubtipo = "002001001";
                    break;
                case "138"://Sin suministro
                    idSubtipo = "02005007";
                    break;
                default: break;

            }
            if (subTipo != "")
            {
                QueryExpression q = new QueryExpression(HidrotecSubtipo.EntityName);
                q.ColumnSet.AddColumn(HidrotecSubtipo.HidrotecTipoId);
                q.Criteria.AddCondition(new ConditionExpression(HidrotecSubtipo.HidrotecIdsubtipo,ConditionOperator.Equal,idSubtipo));
                EntityCollection subTipos = Utils.IOS.RetrieveMultiple(q);
                if (subTipos.Entities.Count > 0)
                {
                    Res = subTipos.Entities[0];


                }

            }
            return Res;
        }


    }
}
