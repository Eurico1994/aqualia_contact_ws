﻿using AqualiaContactWS.Helper;
using AqualiaContactWS.Model.NEO_ActivarFacturaElectronica;
using AqualiaContactWS.Model.NEO_LecturaContador;
using AqualiaContactWS.Model.NEO_ListadoContadoresContrato;
using AqualiaContactWS.Model.NEO_ListadoOperacionesContador;
using AqualiaContactWS.Model.NEO_SolicitarCambioTarifa;
using AqualiaContactWS.Model.NEO_SolicitarOperacionContador;
using System;
using WS.Contratos.Model.NEO_ListadoContratos;
using WS.Contratos.Model.NEO_ListadoDetalleContrato;
using WS.Contratos.Model.NEO_ObtenerDatosContrato;
using WS.Contratos.Model.NEO_VincularContratosMultiusuario;
using Microsoft.Xrm.Sdk.Query;
using Entidades;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Linq;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Drawing;
using System.Drawing.Imaging;

namespace WS.Contratos
{
    public class RespuestaDeuda
    {
        public string codRespuesta { get; set; }
        public List<DocumentospagoDeuda> documentospago { get; set; }
        public string infoError { get; set; }
    }
    public class ContratoDeuda
    {
        public int codInstalacion { get; set; }
        public int codInternoContrato { get; set; }
        public string numContrato { get; set; }
    }

    public class FacturaDeuda
    {
        public string codigofactura { get; set; }
        public ContratoDeuda contrato { get; set; }
        public int emisorcodigo { get; set; }
        public string emisornombre { get; set; }
        public int estadofactura { get; set; }
        public string fechaEmision { get; set; }
        public string fechaVencimiento { get; set; }
        public float importependientefactura { get; set; }
        public float importetotalfactura { get; set; }
        public float mora { get; set; }
        public string numerofactura { get; set; }
        public float recargo { get; set; }
    }

    public class DocumentospagoDeuda
    {
        public bool cobrable { get; set; }
        public string codigodocumentopago { get; set; }
        public List<FacturaDeuda> facturas { get; set; }
        public string fechaemision { get; set; }
        public string fechavencimiento { get; set; }
        public float importependiente { get; set; }
        public float importetotal { get; set; }
        public float mora { get; set; }
        public string numerodocumentopago { get; set; }
        public string periodo { get; set; }
        public float recargo { get; set; }
        public bool tienefacturaelectronica { get; set; }
    }

   

    public class Contratos : IContratos
    {
        /// <summary>
        /// STATUS: OK Abel Gago 
        /// NOTAS: Repasar
        /// DESC:
        /// Este servicio devuelve el listado de contratos asociados a un cliente de la base de datos. 
        /// Para ello, se hacen las validaciones pertinentes y la de existencia de usuario; con la función fapp_selusu. 
        /// Si es multiusuario se le elimina el guion para buscar los contratos que estarán asociados al nif del cliente 
        /// sin el guion. Una vez validado se devuelven los contratos a través de la función fapp_selcontratos.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoContratosResponse NEO_ListadoContratos(NEO_ListadoContratosRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoContratosResponse resp = new NEO_ListadoContratosResponse();
            /*
            request.Usuario = "05453623A";
            */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoContratosResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_ListadoContratosResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoContratosResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoContratosResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoContratosResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

                

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoContratosResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoContratosResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario"); ;
                }

                string strAliasDirSumm = "dirSuministro";
                string strQueryDirSumm = "dirSuministro.";
                string strAliasPedania = "pedania";
                string strQueryPedania = "pedania.";
                string strAliasMunicipio = "municipio";
                string strQueryMunicipio = "municipio.";

                QueryExpression q = new QueryExpression(HidrotecContrato.EntityName);
                q.NoLock = true;
                q.ColumnSet.AddColumns(HidrotecContrato.PrimaryKey, HidrotecContrato.HidrotecCodigocontrata, HidrotecContrato.PrimaryName);

                LinkEntity leUsuario = new LinkEntity(HidrotecContrato.EntityName, Contact.EntityName,
                HidrotecContrato.HidrotecTitularId, Contact.PrimaryKey, JoinOperator.Inner);
                leUsuario.LinkCriteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);

                LinkEntity leDirSumm = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                leDirSumm.EntityAlias = strAliasDirSumm;
                leDirSumm.Columns.AddColumns(HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecDirecciondesuministro.HidrotecDireccioncompuesta);

                LinkEntity lePedania = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecPedania.EntityName,
                HidrotecDirecciondesuministro.HidrotecPedaniaId, HidrotecPedania.PrimaryKey, JoinOperator.LeftOuter);
                lePedania.EntityAlias = strAliasPedania;
                lePedania.Columns.AddColumn(HidrotecPedania.PrimaryName);

                LinkEntity leMunic = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecMunicipio.EntityName,
                HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecMunicipio.PrimaryKey, JoinOperator.Inner);
                leMunic.EntityAlias = strAliasMunicipio;
                leMunic.Columns.AddColumn(HidrotecMunicipio.HidrotecIdmunicipio);

                leDirSumm.LinkEntities.AddRange(leMunic, lePedania);
                q.LinkEntities.AddRange(leUsuario, leDirSumm);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                if (res.Entities.Any())
                {
                    resp.contratos = new List<contrato>();
                    foreach (Entity item in res.Entities)
                    {
                        contrato cont = new contrato();

                        cont.nombremunicipio = ((EntityReference)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecMunicipioId]).Value).Name;
                        cont.suministrodireccion = ((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecDireccioncompuesta]).Value.ToString();
                        cont.codigomunicipio = ((AliasedValue)item[strQueryMunicipio + HidrotecMunicipio.HidrotecIdmunicipio]).Value.ToString();
                        cont.suministronombrepedania = item.Contains(strQueryPedania + HidrotecPedania.PrimaryName) ? ((AliasedValue)item[strQueryPedania + HidrotecPedania.PrimaryName]).Value.ToString() : null;

                        cont.identificadorcontrato = new identificadorcontrato();
                        cont.identificadorcontrato.codigocac = (string)item[HidrotecContrato.HidrotecCodigocontrata];
                        cont.identificadorcontrato.codigocontrato = (string)item[HidrotecContrato.HidrotecCodigocontrata];
                        cont.identificadorcontrato.codigoinstalacion = ((string)item[HidrotecContrato.PrimaryName]).Substring(0, 5);
                        cont.identificadorcontrato.marcaasociado = false;

                        resp.contratos.Add(cont);
                        
                    }

                    resp.codigorespuesta = "00001";
                    resp.textorespuesta = "OK";

                }
                else
                {
                   // return (NEO_ListadoContratosResponse)respuestaWs.NoHayContratosAsociadosContacto();
                    return (NEO_ListadoContratosResponse)respuestaWs.GetError("99999", idiomaorigen, "NoHayContratosAsociadosContacto");

                }
                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ListadoContratosResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: KO - Pdte Crear Entidad
        /// DUDAS Abel Gago: Según Sergio, desvincular y vincular sería poner un ID de empresa al usuario OV, y luego asociarle X contratos
        /// No se sabe bien como se hace, apuntado como duda. 
        /// DESC:
        /// Este servicio inserta en la base de datos los contratos asociados al usuario. Para ello se hacen las validaciones pertinentes y la 
        /// de existencia de usuario; con la función fapp_selusu. Una vez validado se recorre la lista de contratos pasada por parámetro y 
        /// por cada uno sacamos el numero de contrato de CAC que se pasa por parámetro y la operación de vincular o desvincular. 
        /// Según la operación se inserta o elimina en la tabla usuarioscontratos a través del procedimiento pov_insusuariocontrato, 
        /// papp_delusuariocontrato y en histórico_acciones a través del procedimiento papp_inshistaccion.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_VincularContratosMultiusuarioResponse NEO_VincularContratosMultiusuario(NEO_VincularContratosMultiusuarioRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_VincularContratosMultiusuarioResponse resp = new NEO_VincularContratosMultiusuarioResponse();
           
            //request.Usuario = "05453623A-1";
           
            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.SinConexion();
                }
                
                if (request == null)
                {
                    return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.SinParametros();
                }
                
                if (request.Canalentrada == "" || request.Canalentrada == null)
                {
                    return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.CanalEntrada();
                }
                
                if (request.Idioma == "" || request.Idioma == null)
                {
                    return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.Idioma();
                }
                
                if (request.Codpais == "" || request.Codpais == null)
                {
                    return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.Pais();
                }
                
                if (request.Usuario == "" || request.Usuario == null)
                {
                    return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.Usuario();
                }
                
                if (request.Identificadorcontrato.Count == 0)
                {
                    return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.Identificadorcontrato();
                }
                
                if (request.Usuario == "" || request.Usuario == null)
                {
                    return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.Usuario();
                }

                QueryExpression q = new QueryExpression(Contact.EntityName);
                q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.Usuario.Substring(0, request.Usuario.Length -2));
                q.ColumnSet.AddColumn(Contact.PrimaryKey);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                Guid guidContacto = (Guid)res.Entities[0][Contact.PrimaryKey];

                foreach (var item in request.Identificadorcontrato) {

                    q = new QueryExpression(HidrotecContrato.EntityName);
                    q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, item.Identificadorcontrato.numerocontrato);
                    //q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, "10801-1/1-015743");

                    res = Utils.IOS.RetrieveMultiple(q);

                    if (item.Marcaasociado) {
                        // Aqui se asocia.
                        res.Entities[0][HidrotecContrato.HidrotecTitularId] = new EntityReference(HidrotecContrato.EntityName, guidContacto);
                    } else {
                        // Aquí se desasocia.
                        res.Entities[0][HidrotecContrato.HidrotecTitularId] = null;
                    }
                    Utils.IOS.Update(res.Entities[0]);
                }

                resp.codigorespuesta = "OK";
                resp.textorespuesta = "OK";

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_VincularContratosMultiusuarioResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago 
        /// NOTAS: Seguramente se borre o no se use, porque es una función que ya tiene Diversa
        /// DESC:
        /// Este servicio devuelve el detalle de los contratos asociados del usuario. Para ello, se hacen las validaciones pertinentes y 
        /// la de existencia de usuario; con la función fapp_selusu. Una vez validado se devuelven los contratos a través de la 
        /// función fov_selcontratousu. Además se devuelve la deuda a través de la función fov_seldeudactt2
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoDetalleContratoResponse NEO_ListadoDetalleContrato(NEO_ListadoDetalleContratoRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoDetalleContratoResponse resp = new NEO_ListadoDetalleContratoResponse();
            resp.contratos = new List<Contrato>();
            List<Guid> lContractId = new List<Guid>();
            string actDevuelveDeudaContrato = "hidrotec_actconsultardeudacontrato";
            string actDevuelveFacturasPendientes = "hidrotec_ACTConsultarFacturas";
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoDetalleContratoResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }
                
                if (request == null)
                {
                    return (NEO_ListadoDetalleContratoResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoDetalleContratoResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoDetalleContratoResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoDetalleContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }
                
               
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoDetalleContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoDetalleContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                /* Pruebas
                 * request.usuario = "05453623A";
                 * string contratoTest = "10801-1/1-015742";
                 */

                string strAliasContrato = "contract";
                string strQueryContrato = "contract.";
                string strAliasPedania = "pedania";
                string strQueryPedania = "pedania.";
                string strAliasDirSumm = "dirSuministro";
                string strQueryDirSumm = "dirSuministro.";
                string strAliasMunicipio = "municipio";
                string strQueryMunicipio = "municipio.";


                QueryExpression q = new QueryExpression(Contact.EntityName);
                q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);

                LinkEntity leContrato = new LinkEntity(Contact.EntityName, HidrotecContrato.EntityName,
                    Contact.PrimaryKey, HidrotecContrato.HidrotecTitularId, JoinOperator.Inner);
                leContrato.EntityAlias = strAliasContrato;
                leContrato.Columns.AddColumns(HidrotecContrato.PrimaryKey,
                    HidrotecContrato.PrimaryName,
                    HidrotecContrato.HidrotecExistedeuda,
                    HidrotecContrato.HidrotecCodigocontrata,
                    HidrotecContrato.OwningBusinessUnit, 
                    HidrotecContrato.HidrotecIdiomacontrato);

                LinkEntity leDirSum = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                    HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                leDirSum.Columns.AddColumns(HidrotecDirecciondesuministro.HidrotecDireccioncompuesta, HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecDirecciondesuministro.HidrotecPedaniaId);
                leDirSum.EntityAlias = strAliasDirSumm;

                LinkEntity lePedania = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecPedania.EntityName,
                    HidrotecDirecciondesuministro.HidrotecPedaniaId, HidrotecPedania.PrimaryKey, JoinOperator.LeftOuter);
                lePedania.EntityAlias = strAliasPedania;
                lePedania.Columns.AddColumns(HidrotecPedania.PrimaryName);

                LinkEntity leMunicipio = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecMunicipio.EntityName,
                   HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecMunicipio.PrimaryKey, JoinOperator.LeftOuter);
                leMunicipio.EntityAlias = strAliasMunicipio;
                leMunicipio.Columns.AddColumns(HidrotecMunicipio.HidrotecIdmunicipio);

                //leDirSum.LinkEntities.AddRange(lePedania);
                leDirSum.LinkEntities.AddRange(leMunicipio);
                leContrato.LinkEntities.Add(leDirSum);
                q.LinkEntities.Add(leContrato);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                foreach (Entity item in res.Entities)
                {
                    lContractId.Add((Guid)((AliasedValue)item[strQueryContrato + HidrotecContrato.PrimaryKey]).Value);

                    Contrato cont = new Contrato();
                    cont.codigomunicipio = ((string)((AliasedValue)item[strQueryMunicipio + HidrotecMunicipio.HidrotecIdmunicipio]).Value);
                    cont.nombremunicipio = ((EntityReference)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecMunicipioId]).Value).Name;
                    cont.suministrodireccion = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecDireccioncompuesta) ? (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecDireccioncompuesta]).Value : null;
                    //cont.Suministronombrepedania = item.Contains(strQueryPedania + HidrotecPedania.PrimaryName) ? (string)((AliasedValue)item[strQueryPedania + HidrotecPedania.PrimaryName]).Value : null;
                    //Actualizamos el nombre de la pedania
                    cont.suministronombrepedania = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecPedaniaId) ? ((EntityReference)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecMunicipioId]).Value).Name : null;

                    if (item.Contains(strQueryContrato + HidrotecContrato.HidrotecExistedeuda))
                        cont.condeuda = ((OptionSetValue)((AliasedValue)item[strQueryContrato + HidrotecContrato.HidrotecExistedeuda]).Value).Value == 1 ? true : false;

                    cont.identificadorcontrato = new AqualiaContactWS.Model.Comun.Identificadorcontrato();
                    cont.identificadorcontrato.numerocontrato = (string)((AliasedValue)item[strQueryContrato + HidrotecContrato.PrimaryName]).Value;
                    cont.identificadorcontrato.codigocontrato = item.Contains(strQueryContrato + HidrotecContrato.HidrotecCodigocontrata) ? (string)((AliasedValue)item[strQueryContrato + HidrotecContrato.HidrotecCodigocontrata]).Value : null;
                    cont.identificadorcontrato.codigoinstalacion = ((string)((AliasedValue)item[strQueryContrato + HidrotecContrato.PrimaryName]).Value).Substring(0, 5);
                    cont.identificadorcontrato.codigocac = "";
                    //Comrpo


                    /* Hay que apuntar a Diversa, no tenemos esto actualmente */
                    cont.histogramaconsumos = null;

                    cont.deuda = new Deuda();
                    OrganizationResponse testCustomResponse = new OrganizationResponse();
                    OrganizationRequest testCustomAction = new OrganizationRequest() { RequestName = actDevuelveDeudaContrato };
                    testCustomAction.Parameters.Add("guidContrato", ((Guid)((AliasedValue)item[strQueryContrato + HidrotecContrato.PrimaryKey]).Value).ToString());
                    testCustomResponse = Utils.IOS.Execute(testCustomAction);

                    cont.deuda.importependiente = testCustomResponse.Results["resultado"].ToString();

                    testCustomResponse = new OrganizationResponse();
                    testCustomAction = new OrganizationRequest() { RequestName = actDevuelveFacturasPendientes };
                    testCustomAction.Parameters.Add("idExplotacion", ((EntityReference)((AliasedValue)item[strQueryContrato + HidrotecContrato.OwningBusinessUnit]).Value).Id.ToString());
                    testCustomAction.Parameters.Add("idContrato", ((Guid)((AliasedValue)item[strQueryContrato + HidrotecContrato.PrimaryKey]).Value).ToString());
                    testCustomAction.Parameters.Add("existeDeuda", "1"); /////
                    testCustomAction.Parameters.Add("idIdioma", ((OptionSetValue)((AliasedValue)item[strQueryContrato + HidrotecContrato.HidrotecIdiomacontrato]).Value).Value.ToString()); /////
                    testCustomResponse = Utils.IOS.Execute(testCustomAction);
                    
                    //RespuestaDeuda RespuestaDeudaRes=d
                   // cont.Deuda.Numfacturas = testCustomResponse.Results["respuesta"].ToString();
                    var DocumentoPago = new RespuestaDeuda();
                    var ms = new MemoryStream(Encoding.UTF8.GetBytes((string)testCustomResponse.Results["respuesta"]));
                    var ser = new DataContractJsonSerializer(DocumentoPago.GetType());
                    DocumentoPago = ser.ReadObject(ms) as RespuestaDeuda;
                    ms.Close();

                    resp.contratos.Add(cont);
                    if (DocumentoPago != null)
                    {
                        if (DocumentoPago.documentospago != null)
                        {
                            cont.deuda.numfacturas = DocumentoPago.documentospago.Count().ToString();
                          

                        }

                    }
                }

                for (int i = 0; i < lContractId.Count; i++)
                {
                    q = new QueryExpression(HidrotecFactura.EntityName);
                    q.Criteria.AddCondition(HidrotecFactura.HidrotecContratoId, ConditionOperator.Equal, lContractId[i].ToString());
                    q.AddOrder(HidrotecFactura.HidrotecFechaemision, OrderType.Descending);
                  //  q.PageInfo = new PagingInfo { PageNumber = 1, Count = 1 };

                    q.ColumnSet.AddColumns(HidrotecFactura.HidrotecFechaemision,
                        HidrotecFactura.PrimaryName,
                        HidrotecFactura.HidrotecImportE,
                        HidrotecFactura.HidrotecEstadofactura,
                        HidrotecFactura.HidrotecFechavencimiento, 
                        HidrotecFactura.HidrotecExpedienteId);

                    res = Utils.IOS.RetrieveMultiple(q);
                    if (res.Entities.Any())
                    {
                        var item = res.Entities[res.Entities.Count - 1];
                        Ultimafactura ultimafactura = new Ultimafactura();
                        ultimafactura.fechaemision = item.Contains(HidrotecFactura.HidrotecFechaemision) ? ((DateTime)item[HidrotecFactura.HidrotecFechaemision]).ToString() : null;
                        ultimafactura.identificador = item.Contains(HidrotecFactura.PrimaryName) ? (string)item[HidrotecFactura.PrimaryName] : null;
                        ultimafactura.importefactura = item.Contains(HidrotecFactura.HidrotecImportE) ? (decimal)((Money)item[HidrotecFactura.HidrotecImportE]).Value : 0;
                        ultimafactura.pendiente = (item.Contains(HidrotecFactura.HidrotecEstadofactura) && (string)item[HidrotecFactura.HidrotecEstadofactura] == "Pendiente") ? true : false;

                        if (item.Contains(HidrotecFactura.HidrotecFechaemision) && item.Contains(HidrotecFactura.HidrotecFechavencimiento))
                            ultimafactura.peridodofacturacion = ParsePeridodofacturacion((DateTime)item[HidrotecFactura.HidrotecFechaemision], (DateTime)item[HidrotecFactura.HidrotecFechavencimiento]);

                        resp.contratos[i].ultimafactura = ultimafactura;
                    }
                }
                if ((resp.contratos != null) && (resp.contratos.Count() > 0))
                {
                    resp.codigorespuesta = "00001";
                    resp.textorespuesta = "OK";
                    return resp;
                    //return (NEO_ListadoDetalleContratoResponse)respuestaWs.ok();
                }
                else
                {
                    
                    return (NEO_ListadoDetalleContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "NoHayContratosAsociadosContacto");
                }
                
            }
            catch (Exception e)
            {
                return (NEO_ListadoDetalleContratoResponse)respuestaWs.Exception(e,"99999");
            }
        }

        #region Utils_NEO_ListadoDetalleContrato
        private string ParsePeridodofacturacion(DateTime dateEmision, DateTime dateVenc)
        {
            return ParseMonthUltimaFactura(dateEmision) + "-" + ParseMonthUltimaFactura(dateVenc) + "/" + dateVenc.Year.ToString();
        }
        private string ParseMonthUltimaFactura(DateTime date)
        {
            string[] tabla = new string[] { "ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC" };
            return tabla[date.Month - 1];
        }
        #endregion

        /// <summary>
        /// STATUS: OK Abel Gago 
        /// NOTAS: Repasar -> Quitar new ColumnSet(true)
        /// DESC:
        /// Este servicio devuelve toda la información de un contrato a través del identificador de contrato y el usuario. 
        /// Para ello, se hacen las validaciones pertinentes y la de existencia de usuario; con la función fapp_selusu. 
        /// Una vez validado se obtienen la información del contrato a través de las siguientes funciones:
        /// fov_selfatgralctt -> Recupera datos generales del contrato.
        /// fov_seldatpersctt -> Recupera información clientes
        /// fov_seldatdirctt -> Recupera información de direcciones
        /// fov_seldatbcosctt -> Recupera información del cobro
        /// fapp_seldespobl -> Recuperara la información de municipio
        /// fapp_seldestipcli -> Recuperara la información del tipo cliente
        /// fapp_seldestipvia -> Recupera la informacion del tipo de la via
        /// fapp_seldesdttro -> Recupera la información de la pednía
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ObtenerDatosContratoResponse NEO_ObtenerDatosContrato(NEO_ObtenerDatosContratoRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ObtenerDatosContratoResponse resp = new NEO_ObtenerDatosContratoResponse();
            /*
            request.Identificadorcontrato = new Identificadorcontrato();
            request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
            request.Usuario = "05453623A";
            */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }
                
                if (request == null)
                {
                    return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }
                
       
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                /* if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                 { 

                     return (NEO_ObtenerDatosContratoResponse)respuestaWs.Codigocac();
                 }

                 if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                 {
                     return (NEO_ObtenerDatosContratoResponse)respuestaWs.Codigocontrato();
                 }

                 if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                 {
                     return (NEO_ObtenerDatosContratoResponse)respuestaWs.Codigoinstalacion();
                 }

                 if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                 {
                     return (NEO_ObtenerDatosContratoResponse)respuestaWs.Numerocontrato();
                 }

                 */

                if (request.identificadorcontrato != null)
                {
                    if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                    {
                       // return (NEO_ObtenerDatosContratoResponse)respuestaWs.Numerocontrato();
                        return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Numerocontrato");

                    }
                    if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                    {

                        //return (NEO_ObtenerDatosContratoResponse)respuestaWs.Codigocac();
                        return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocac");
                    }
                    if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                    {
                     //   return (NEO_ObtenerDatosContratoResponse)respuestaWs.Codigocontrato();
                        return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "codigocontrato");
                    }
                    if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                    {
                        //return (NEO_ObtenerDatosContratoResponse)respuestaWs.Codigoinstalacion();
                        return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigoinstalacion");
                    }
                }
                else
                {

                   // return (NEO_ObtenerDatosContratoResponse)respuestaWs.Identificadorcontrato();
                    return (NEO_ObtenerDatosContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato");

                }


                string strIdRolCorrespondencia = "3";
                string strIdRolCobro = "5";

                string strAliasDirSumm = "dirSuministro";
                string strQueryDirSumm = "dirSuministro.";
                string strAliasPedania = "pedania";
                string strQueryPedania = "pedania.";
                string strAliasViaSum = "viaSum";
                string strQueryViaSum = "viaSum.";
                string strAliasTipoViaSum = "tipoViaSum";
                string strQueryTipoViaSum = "tipoViaSum.";
                string strAliasViaCor = "viaCor";
                string strQueryViaCor = "viaCor.";
                string strAliasTipoViaCor = "tipoViaCor";
                string strQueryTipoViaCor = "tipoViaCor.";
                string strAliasTitular = "titular";
                string strQueryTitular = "titular.";
                string strAliasCorresp = "contactoCorrespondencia";
                string strQueryCorresp = "contactoCorrespondencia.";
                string strAliasCobro = "contactoCobro";
                string strQueryCobro = "contactoCobro.";
                string strAliasMunicipio = "municipio";
                string strQueryMunicipio = "municipio.";

                Guid contratoId = new Guid();

                QueryExpression q = new QueryExpression(HidrotecContrato.EntityName);
                q.Criteria.AddCondition(new ConditionExpression(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato));
                q.ColumnSet.AddColumn(HidrotecContrato.PrimaryKey);

                LinkEntity leDirSumm = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                    HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                leDirSumm.EntityAlias = strAliasDirSumm;
                leDirSumm.Columns.AddColumns(HidrotecDirecciondesuministro.HidrotecBloque,
                             HidrotecDirecciondesuministro.HidrotecCodigoPostal,
                             HidrotecDirecciondesuministro.HidrotecEdificio,
                             HidrotecDirecciondesuministro.HidrotecEscalera,
                             HidrotecDirecciondesuministro.HidrotecNumero,
                             HidrotecDirecciondesuministro.HidrotecOtros,
                             HidrotecDirecciondesuministro.HidrotecPiso,
                             HidrotecDirecciondesuministro.HidrotecPuerta,
                             HidrotecDirecciondesuministro.HidrotecMunicipioId,
                             HidrotecDirecciondesuministro.HidrotecPedaniaId,
                             HidrotecDirecciondesuministro.HidrotecViaId);

                LinkEntity lePedania = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecPedania.EntityName,
                    HidrotecDirecciondesuministro.HidrotecPedaniaId, HidrotecPedania.PrimaryKey, JoinOperator.LeftOuter);
                lePedania.EntityAlias = strAliasPedania;
                lePedania.Columns.AddColumns(HidrotecPedania.HidrotecIdpedania, HidrotecPedania.PrimaryName);

                LinkEntity leViaSum = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecVia.EntityName,
                    HidrotecDirecciondesuministro.HidrotecViaId, HidrotecVia.PrimaryKey, JoinOperator.LeftOuter);
                leViaSum.EntityAlias = strAliasViaSum;
                leViaSum.Columns.AddColumns(HidrotecVia.PrimaryName, HidrotecVia.HidrotecIdvia);

                LinkEntity leTipoViaSum = new LinkEntity(HidrotecVia.EntityName, HidrotecTipovia.EntityName,
                   HidrotecVia.HidrotecTipoviaId, HidrotecTipovia.PrimaryKey, JoinOperator.Inner);
                leTipoViaSum.EntityAlias = strAliasTipoViaSum;
                leTipoViaSum.Columns.AddColumns(HidrotecTipovia.PrimaryName, HidrotecTipovia.HidrotecIdtipovia);

                LinkEntity leTitular = new LinkEntity(HidrotecContrato.EntityName, Contact.EntityName,
                    HidrotecContrato.HidrotecTitularId, Contact.PrimaryKey, JoinOperator.LeftOuter);
                leTitular.EntityAlias = strAliasTitular;
                leTitular.Columns.AddColumns(Contact.FirstName,
                             Contact.MiddleName,
                             Contact.LastName,
                             Contact.HidrotecTipodedocumentoId,
                             Contact.Fax,
                             Contact.Birthdate,
                             Contact.PrimaryName,
                             Contact.HidrotecNumerodocumento,
                             Contact.TelePhone1,
                             Contact.TelePhone2);

                LinkEntity leMunicipio = new LinkEntity(Contact.EntityName, HidrotecMunicipio.EntityName,
                    Contact.HidrotecMunicipioId, HidrotecMunicipio.PrimaryKey, JoinOperator.Inner);
                leMunicipio.EntityAlias = strAliasMunicipio;
                leMunicipio.Columns.AddColumns(HidrotecMunicipio.PrimaryName, HidrotecMunicipio.HidrotecIdmunicipio);

                //Se comenta porque el tipo de cliente está nivel de contrato
                /*
                LinkEntity leTipoCliente = new LinkEntity(Contact.EntityName, HidrotecTipodecliente.EntityName,
                    Contact.OwnerIdType, HidrotecTipodecliente.PrimaryKey, JoinOperator.Inner);
                leTipoCliente.EntityAlias = "tipodeclienteTitular";
                leTipoCliente.Columns.AddColumns(HidrotecTipodecliente.PrimaryName, HidrotecTipodecliente.HidrotecIdtipodecliente);
               
               
               // leTitular.LinkEntities.AddRange(leMunicipio, leTipoCliente);
               */
                LinkEntity leTipoCliente = new LinkEntity(HidrotecContrato.EntityName, HidrotecTipodecliente.EntityName,
                    HidrotecContrato.HidrotecTipodeclienteId, HidrotecTipodecliente.PrimaryKey, JoinOperator.Inner);
                leTipoCliente.EntityAlias = "tipodecliente";
                leTipoCliente.Columns.AddColumns(HidrotecTipodecliente.PrimaryName, HidrotecTipodecliente.HidrotecIdtipodecliente);


                leTitular.LinkEntities.Add(leMunicipio);
                leViaSum.LinkEntities.Add(leTipoViaSum);
                //Se quita pedania porque a veces no está relleno
                // leDirSumm.LinkEntities.AddRange(lePedania, leViaSum);
                leDirSumm.LinkEntities.AddRange(leViaSum);
                q.LinkEntities.AddRange(leDirSumm, leTitular,leTipoCliente);
                

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {
                    return (NEO_ObtenerDatosContratoResponse)respuestaWs.ContratoBuscado();

                }
                DatosContrato data = new DatosContrato();
                foreach (Entity item in res.Entities)
                {
                    contratoId = (Guid)item[HidrotecContrato.PrimaryKey];

                    data.cobrocontrato = new Cobrocontrato();
                    data.codigomunicipio = item.Contains(strQueryMunicipio + HidrotecMunicipio.HidrotecIdmunicipio) ? (string)((AliasedValue)item[strQueryMunicipio + HidrotecMunicipio.HidrotecIdmunicipio]).Value : null;
                    data.nombremunicipio = item.Contains(strQueryMunicipio + HidrotecMunicipio.PrimaryName) ? (string)((AliasedValue)item[strQueryMunicipio + HidrotecMunicipio.PrimaryName]).Value : null;
                    data.codigotipocliente = item.Contains("tipodecliente." + HidrotecTipodecliente.HidrotecIdtipodecliente) ? (string)((AliasedValue)item["tipodecliente." + HidrotecTipodecliente.HidrotecIdtipodecliente]).Value : null;
                    data.textotipocliente = item.Contains("tipodecliente." + HidrotecTipodecliente.PrimaryName) ? (string)((AliasedValue)item["tipodecliente." + HidrotecTipodecliente.PrimaryName]).Value : null;

                    data.identificadorcontrato = new AqualiaContactWS.Model.Comun.Identificadorcontrato();
                    data.identificadorcontrato.codigocac = request.identificadorcontrato.codigocac;
                    data.identificadorcontrato.codigocontrato = request.identificadorcontrato.codigocontrato;
                    data.identificadorcontrato.codigoinstalacion = request.identificadorcontrato.codigoinstalacion;
                    data.identificadorcontrato.numerocontrato = request.identificadorcontrato.numerocontrato;

                    data.suministrocontrato = new Suministrocontrato();
                    data.suministrocontrato.suministrobloque = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecBloque) ? 
                        (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecBloque]).Value : null;
                    data.suministrocontrato.suministrocodigopostal = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecCodigoPostal) ? 
                        (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecCodigoPostal]).Value : null;
                    data.suministrocontrato.suministroedificio = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecEdificio) ? 
                        (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecEdificio]).Value : null;
                    data.suministrocontrato.suministroescalera = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecEscalera) ? 
                        (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecEscalera]).Value : null;

                    data.suministrocontrato.suministronumero = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecNumero) ? 
                        (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecNumero]).Value : null;
                    data.suministrocontrato.suministrootros = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecOtros) ?
                        (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecOtros]).Value : null;
                    data.suministrocontrato.suministropiso = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecPiso) ? 
                        (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecPiso]).Value : null;
                    data.suministrocontrato.suminitropuerta = item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecPuerta) ? 
                        (string)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecPuerta]).Value : null;
                    if (item.Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecPedaniaId))
                    {

                        QueryExpression qPedania = new QueryExpression(HidrotecPedania.EntityName);
                        qPedania.Criteria.AddCondition(new ConditionExpression(HidrotecPedania.PrimaryKey, ConditionOperator.Equal, ((EntityReference)((AliasedValue)item[strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecPedaniaId]).Value).Id.ToString()));
                        qPedania.ColumnSet.AddColumns(HidrotecPedania.PrimaryName, HidrotecPedania.HidrotecIdpedania);
                        EntityCollection resPedania = Utils.IOS.RetrieveMultiple(qPedania);
                        if (resPedania.Entities.Any())
                        {
                            data.suministrocontrato.suministrocodigopedania = resPedania.Entities[0].HasAttributeValue<string>(HidrotecPedania.HidrotecIdpedania) ? resPedania.Entities[0].GetAttributeValue<string>(HidrotecPedania.HidrotecIdpedania) : null;
                            data.suministrocontrato.suministronombrepedania = resPedania.Entities[0].HasAttributeValue<string>(HidrotecPedania.PrimaryName) ? resPedania.Entities[0].GetAttributeValue<string>(HidrotecPedania.PrimaryName) : null;
                        }

                    }
                    //data.Suministrocontrato.Suministrocodigopedania = item.Contains(strQueryPedania + HidrotecPedania.HidrotecIdpedania) ? 
                    //    (string)((AliasedValue)item[strQueryPedania + HidrotecPedania.HidrotecIdpedania]).Value : null;
                   // data.Suministrocontrato.Suministronombrepedania = item.Contains(strQueryPedania + HidrotecPedania.PrimaryName) ? 
                   //     (string)((AliasedValue)item[strQueryPedania + HidrotecPedania.PrimaryName]).Value : null;

                    data.suministrocontrato.suministrovia = new Model.NEO_ObtenerDatosContrato.Via();
                    data.suministrocontrato.suministrovia.codigovia = item.Contains(strQueryViaSum + HidrotecVia.HidrotecIdvia) ? 
                        (string)((AliasedValue)item[strQueryViaSum + HidrotecVia.HidrotecIdvia]).Value : null;
                    data.suministrocontrato.suministrovia.nombrevia = item.Contains(strQueryViaSum + HidrotecVia.PrimaryName) ? 
                        (string)((AliasedValue)item[strQueryViaSum + HidrotecVia.PrimaryName]).Value : null;
                    data.suministrocontrato.suministrovia.codigotipovia = item.Contains(strQueryTipoViaSum + HidrotecTipovia.HidrotecIdtipovia) ? 
                        (string)((AliasedValue)item[strQueryTipoViaSum + HidrotecTipovia.HidrotecIdtipovia]).Value : null;
                    data.suministrocontrato.suministrovia.nombretipovia = item.Contains(strQueryTipoViaSum + HidrotecTipovia.PrimaryName) ? 
                        (string)((AliasedValue)item[strQueryTipoViaSum + HidrotecTipovia.PrimaryName]).Value : null;

                    data.titularcontrato = new Titularcontrato();
                    data.titularcontrato.titularapellido1 = item.Contains(strQueryTitular + Contact.MiddleName) ? 
                        (string)((AliasedValue)item[strQueryTitular + Contact.MiddleName]).Value : null;
                    data.titularcontrato.titularapellido2 = item.Contains(strQueryTitular + Contact.LastName) ? 
                        (string)((AliasedValue)item[strQueryTitular + Contact.LastName]).Value : null;
                    data.titularcontrato.titularcodigotipodocumento = item.Contains(strQueryTitular + Contact.HidrotecTipodedocumentoId) ? 
                        ((EntityReference)((AliasedValue)item[strQueryTitular + Contact.HidrotecTipodedocumentoId]).Value).Id.ToString() : null;
                    data.titularcontrato.titularfax = item.Contains(strQueryTitular + Contact.Fax) ? 
                        (string)((AliasedValue)item[strQueryTitular + Contact.Fax]).Value : null;
                    data.titularcontrato.titularfechanacimiento = item.Contains(strQueryTitular + Contact.Birthdate) ? 
                        (string)((AliasedValue)item[strQueryTitular + Contact.Birthdate]).Value : null;
                    data.titularcontrato.titularnombre = item.Contains(strQueryTitular + Contact.FirstName) ? 
                        (string)((AliasedValue)item[strQueryTitular + Contact.FirstName]).Value : null;
                    data.titularcontrato.titularnumerodocumento = item.Contains(strQueryTitular + Contact.HidrotecNumerodocumento) ? 
                        (string)((AliasedValue)item[strQueryTitular + Contact.HidrotecNumerodocumento]).Value : null;
                    data.titularcontrato.titulartelefono1 = item.Contains(strQueryTitular + Contact.TelePhone1) ? 
                        (string)((AliasedValue)item[strQueryTitular + Contact.TelePhone1]).Value : null;
                    data.titularcontrato.titulartelefono2 = item.Contains(strQueryTitular + Contact.TelePhone2) ? 
                        (string)((AliasedValue)item[strQueryTitular + Contact.TelePhone2]).Value : null;
                    data.titularcontrato.titulartipodocumento = ((EntityReference)((AliasedValue)item[strQueryTitular + Contact.HidrotecTipodedocumentoId]).Value).Name;
                }

                q = new QueryExpression(HidrotecRoldecontrato.EntityName);
                q.ColumnSet.AddColumns(HidrotecRoldecontrato.HidrotecBloque,
                                       HidrotecRoldecontrato.HidrotecCodigoPostal,
                                       HidrotecRoldecontrato.HidrotecDireccioncompuesta,
                                       HidrotecRoldecontrato.HidrotecEdificio,
                                       HidrotecRoldecontrato.HidrotecEscalera,
                                       HidrotecRoldecontrato.HidrotecNumero,
                                       HidrotecRoldecontrato.HidrotecOtros,
                                       HidrotecRoldecontrato.HidrotecPiso,
                                       HidrotecRoldecontrato.HidrotecPuerta,
                                       HidrotecRoldecontrato.HidrotecPedaniaId);

                LinkEntity leRolContrato = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecContrato.EntityName,
                    HidrotecRoldecontrato.HidrotecContratoId, HidrotecContrato.PrimaryKey, JoinOperator.Inner);
                leRolContrato.LinkCriteria.AddCondition(HidrotecContrato.PrimaryKey, ConditionOperator.Equal, contratoId);

                LinkEntity leRol = new LinkEntity(HidrotecRoldecontrato.EntityName, Rol.LogicalName,
                    HidrotecRoldecontrato.HidrotecRolId, Rol.Id, JoinOperator.Inner);
                leRol.LinkCriteria.AddCondition(Rol.IdRol, ConditionOperator.Equal, strIdRolCorrespondencia);

                LinkEntity leViaCor = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecVia.EntityName,
                    HidrotecRoldecontrato.HidrotecViaId, HidrotecVia.PrimaryKey, JoinOperator.LeftOuter);
                leViaCor.EntityAlias = strAliasViaCor;
                leViaCor.Columns.AddColumns(HidrotecVia.PrimaryName, HidrotecVia.HidrotecIdvia);

                LinkEntity leTipoViaCor = new LinkEntity(HidrotecVia.EntityName, HidrotecTipovia.EntityName,
                    HidrotecVia.HidrotecTipoviaId, HidrotecTipovia.PrimaryKey, JoinOperator.Inner);
                leTipoViaCor.EntityAlias = strAliasTipoViaCor;
                leTipoViaCor.Columns.AddColumns(HidrotecTipovia.PrimaryName, HidrotecTipovia.HidrotecIdtipovia);

                LinkEntity leContact = new LinkEntity(HidrotecRoldecontrato.EntityName, Contact.EntityName,
                    HidrotecRoldecontrato.HidrotecContactOId, Contact.PrimaryKey, JoinOperator.Inner);
                leContact.EntityAlias = strAliasCorresp;
                leContact.Columns.AddColumns(Contact.FirstName,
                    Contact.MiddleName,
                    Contact.LastName,
                    Contact.HidrotecTipodedocumentoId,
                    Contact.Fax,
                    Contact.Birthdate,
                    Contact.PrimaryName,
                    Contact.HidrotecNumerodocumento,
                    Contact.TelePhone1,
                    Contact.TelePhone2);

                lePedania = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecPedania.EntityName,
                    HidrotecRoldecontrato.HidrotecPedaniaId, HidrotecPedania.PrimaryKey, JoinOperator.LeftOuter);
                lePedania.EntityAlias = strAliasPedania;
                lePedania.Columns.AddColumns(HidrotecPedania.HidrotecIdpedania, HidrotecPedania.PrimaryName);

                leMunicipio = new LinkEntity(Contact.EntityName, HidrotecMunicipio.EntityName,
                    Contact.HidrotecMunicipioId, HidrotecMunicipio.PrimaryKey, JoinOperator.Inner);
                leMunicipio.EntityAlias = strAliasMunicipio;
                leMunicipio.Columns.AddColumns(HidrotecMunicipio.PrimaryName, HidrotecMunicipio.HidrotecIdmunicipio);

                leContact.LinkEntities.Add(leMunicipio);
                leViaCor.LinkEntities.Add(leTipoViaCor);
                //Se quita la pedania porque a veces no viene relleno
                // q.LinkEntities.AddRange(leRolContrato, leRol, leViaCor, leContact, lePedania);
                q.LinkEntities.AddRange(leRolContrato, leRol, leViaCor, leContact);

                res = Utils.IOS.RetrieveMultiple(q);
                foreach (Entity item in res.Entities)
                {
                    data.correspondenciacontrato = new Correspondenciacontrato();
                    data.correspondenciacontrato.correspondencianombre = item.Contains(strQueryCorresp + Contact.HidrotecNombre) ? (string)((AliasedValue)item[strQueryCorresp + Contact.HidrotecNombre]).Value : null;
                    data.correspondenciacontrato.correspondenciaapellido1 = item.Contains(strQueryCorresp + Contact.HidrotecPrimerapellido) ? (string)((AliasedValue)item[strQueryCorresp + Contact.HidrotecPrimerapellido]).Value : null;
                    data.correspondenciacontrato.correspondenciaapellido2 = item.Contains(strQueryCorresp + Contact.HidrotecSegundoapellido) ? (string)((AliasedValue)item[strQueryCorresp + Contact.HidrotecSegundoapellido]).Value : null;
                    data.correspondenciacontrato.correspondenciabloque = item.Contains(HidrotecRoldecontrato.HidrotecBloque) ? (string)item[HidrotecRoldecontrato.HidrotecBloque] : null;
                    if (item.HasAttributeValue<EntityReference>(HidrotecRoldecontrato.HidrotecPedaniaId))
                    {
                        QueryExpression qPedania = new QueryExpression(HidrotecPedania.EntityName);
                        qPedania.Criteria.AddCondition(new ConditionExpression(HidrotecPedania.PrimaryKey, ConditionOperator.Equal, item.GetAttributeValue<EntityReference>(HidrotecRoldecontrato.HidrotecPedaniaId).Id.ToString()));
                        qPedania.ColumnSet.AddColumns(HidrotecPedania.PrimaryName, HidrotecPedania.HidrotecIdpedania);
                        EntityCollection resPedania = Utils.IOS.RetrieveMultiple(qPedania);
                        if (resPedania.Entities.Any())
                        {
                            data.correspondenciacontrato.correspondenciacodigopedania = resPedania.Entities[0].HasAttributeValue<string>(HidrotecPedania.HidrotecIdpedania) ? resPedania.Entities[0].GetAttributeValue<string>(HidrotecPedania.HidrotecIdpedania) : null;
                            data.correspondenciacontrato.correspondencianombrepedania = resPedania.Entities[0].HasAttributeValue<string>(HidrotecPedania.PrimaryName) ? resPedania.Entities[0].GetAttributeValue<string>(HidrotecPedania.PrimaryName) : null;
                        }



                    }
                    //data.Correspondenciacontrato.Correspondenciacodigopedania = item.Contains(strQueryPedania + HidrotecPedania.HidrotecIdpedania) ? (string)((AliasedValue)item[strQueryPedania + HidrotecPedania.HidrotecIdpedania]).Value : null;
                    //data.Correspondenciacontrato.Correspondencianombrepedania = item.Contains(strQueryPedania + HidrotecPedania.PrimaryName) ? (string)((AliasedValue)item[strQueryPedania + HidrotecPedania.PrimaryName]).Value : null;
                    data.correspondenciacontrato.correspondenciacodigopostal = item.Contains(HidrotecRoldecontrato.HidrotecCodigoPostal) ? (string)item[HidrotecRoldecontrato.HidrotecCodigoPostal] : null;
                    data.correspondenciacontrato.correspondenciacodigotipodocumento = item.Contains(strQueryCorresp + Contact.HidrotecTipodedocumentoId) ? ((EntityReference)((AliasedValue)item[strQueryCorresp + Contact.HidrotecTipodedocumentoId]).Value).Id.ToString() : null;
                    data.correspondenciacontrato.correspondenciadirsincodificar = item.Contains(HidrotecRoldecontrato.HidrotecDireccioncompuesta) ? (string)item[HidrotecRoldecontrato.HidrotecDireccioncompuesta] : null;
                    data.correspondenciacontrato.correspondenciaedificio = item.Contains(HidrotecRoldecontrato.HidrotecEdificio) ? (string)item[HidrotecRoldecontrato.HidrotecEdificio] : null;
                    data.correspondenciacontrato.correspondenciaemail = item.Contains(strQueryCorresp + Contact.EMailAddress1) ? (string)((AliasedValue)item[strQueryCorresp + Contact.EMailAddress1]).Value : null;
                    data.correspondenciacontrato.correspondenciaescalera = item.Contains(HidrotecRoldecontrato.HidrotecEscalera) ? (string)item[HidrotecRoldecontrato.HidrotecEscalera] : null;
                    data.correspondenciacontrato.correspondenciafax = item.Contains(strQueryCorresp + Contact.Fax) ? (string)((AliasedValue)item[strQueryCorresp + Contact.Fax]).Value : null;
                    data.correspondenciacontrato.correspondencianumero = item.Contains(HidrotecRoldecontrato.HidrotecNumero) ? (string)item[HidrotecRoldecontrato.HidrotecNumero] : null;
                    data.correspondenciacontrato.correspondencianumerodocumento = item.Contains(strQueryCorresp + Contact.HidrotecNumerodocumento) ? (string)((AliasedValue)item[strQueryCorresp + Contact.HidrotecNumerodocumento]).Value : null;
                    data.correspondenciacontrato.correspondenciaotros = item.Contains(HidrotecRoldecontrato.HidrotecOtros) ? (string)item[HidrotecRoldecontrato.HidrotecOtros] : null;
                    data.correspondenciacontrato.correspondenciapiso = item.Contains(HidrotecRoldecontrato.HidrotecPiso) ? (string)item[HidrotecRoldecontrato.HidrotecPiso] : null;
                    data.correspondenciacontrato.correspondenciapuerta = item.Contains(HidrotecRoldecontrato.HidrotecPuerta) ? (string)item[HidrotecRoldecontrato.HidrotecPuerta] : null;
                    data.correspondenciacontrato.correspondenciatelefono1 = item.Contains(strQueryCorresp + Contact.TelePhone1) ? (string)((AliasedValue)item[strQueryCorresp + Contact.TelePhone1]).Value : null;
                    data.correspondenciacontrato.correspondenciatelefono2 = item.Contains(strQueryCorresp + Contact.TelePhone2) ? (string)((AliasedValue)item[strQueryCorresp + Contact.TelePhone2]).Value : null;
                    data.correspondenciacontrato.correspondenciacodigomunicipio = item.Contains(strQueryMunicipio + HidrotecMunicipio.HidrotecIdmunicipio) ? (string)((AliasedValue)item[strQueryMunicipio + HidrotecMunicipio.HidrotecIdmunicipio]).Value : null;
                    data.correspondenciacontrato.correspondencianombremunicipio = item.Contains(strQueryMunicipio + HidrotecMunicipio.PrimaryName) ? (string)((AliasedValue)item[strQueryMunicipio + HidrotecMunicipio.PrimaryName]).Value : null;
                    data.correspondenciacontrato.correspondenciaidioma = "";
                    data.correspondenciacontrato.correspondenciamunicipiosincodificar = "";
                    data.correspondenciacontrato.facturaelectronicaactivar = false;

                    data.correspondenciacontrato.correspondenciavia = new Model.NEO_ObtenerDatosContrato.Via();
                    data.correspondenciacontrato.correspondenciavia.nombretipovia = item.Contains(strQueryViaCor + HidrotecVia.HidrotecIdvia) ? (string)((AliasedValue)item[strQueryViaCor + HidrotecVia.HidrotecIdvia]).Value : null;
                    data.correspondenciacontrato.correspondenciavia.nombrevia = item.Contains(strQueryViaCor + HidrotecVia.PrimaryName) ? (string)((AliasedValue)item[strQueryViaCor + HidrotecVia.PrimaryName]).Value : null;
                    data.correspondenciacontrato.correspondenciavia.codigotipovia = item.Contains(strQueryTipoViaCor + HidrotecTipovia.HidrotecIdtipovia) ? (string)((AliasedValue)item[strQueryTipoViaCor + HidrotecTipovia.HidrotecIdtipovia]).Value : null;
                    data.correspondenciacontrato.correspondenciavia.codigovia = item.Contains(strQueryTipoViaCor + HidrotecTipovia.PrimaryName) ? (string)((AliasedValue)item[strQueryTipoViaCor + HidrotecTipovia.PrimaryName]).Value : null;
               }

                q = new QueryExpression(HidrotecRoldecontrato.EntityName);
                q.ColumnSet.AddColumns(HidrotecRoldecontrato.HidrotecCodigobanco,
                                       HidrotecRoldecontrato.HidrotecNumerodecuenta,
                                       HidrotecRoldecontrato.HidrotecDigitocontrolccc,
                                       HidrotecRoldecontrato.HidrotecFax,
                                       HidrotecRoldecontrato.HidrotecBancoId,
                                       HidrotecRoldecontrato.HidrotecCodigosucursal,
                                       HidrotecRoldecontrato.HidrotecTelefonofijo,
                                       HidrotecRoldecontrato.HidrotecTelefonomovil,
                                       HidrotecRoldecontrato.HidrotecFechalimite,
                                       HidrotecRoldecontrato.HidrotecImportEmaximo);

                leRolContrato = new LinkEntity(HidrotecRoldecontrato.EntityName, HidrotecContrato.EntityName,
                    HidrotecRoldecontrato.HidrotecContratoId, HidrotecContrato.PrimaryKey, JoinOperator.Inner);
                leRolContrato.LinkCriteria.AddCondition(HidrotecContrato.PrimaryKey, ConditionOperator.Equal, contratoId);

                leRol = new LinkEntity(HidrotecRoldecontrato.EntityName, Rol.LogicalName,
                    HidrotecRoldecontrato.HidrotecRolId, Rol.Id, JoinOperator.Inner);
                leRol.LinkCriteria.AddCondition(Rol.IdRol, ConditionOperator.Equal, strIdRolCobro);

                leContact = new LinkEntity(HidrotecRoldecontrato.EntityName, Contact.EntityName,
                    HidrotecRoldecontrato.HidrotecContactOId, Contact.PrimaryKey, JoinOperator.Inner);
                leContact.EntityAlias = strAliasCobro;
                leContact.Columns.AddColumns(Contact.FirstName,
                    Contact.MiddleName,
                    Contact.LastName,
                    Contact.HidrotecTipodedocumentoId,
                    Contact.HidrotecNumerodocumento);

                q.LinkEntities.AddRange(leRolContrato, leRol, leContact);

                res = Utils.IOS.RetrieveMultiple(q);
                foreach (Entity item in res.Entities)
                {
                    data.cobrocontrato = new Cobrocontrato();
                    data.cobrocontrato.cobronombre = item.Contains(strQueryCobro + Contact.HidrotecNombre) ? (string)((AliasedValue)item[strQueryCobro + Contact.HidrotecNombre]).Value : null;
                    data.cobrocontrato.cobroapellido1 = item.Contains(strQueryCobro + Contact.HidrotecPrimerapellido) ? (string)((AliasedValue)item[strQueryCobro + Contact.HidrotecPrimerapellido]).Value : null;
                    data.cobrocontrato.cobroapellido2 = item.Contains(strQueryCobro + Contact.HidrotecSegundoapellido) ? (string)((AliasedValue)item[strQueryCobro + Contact.HidrotecSegundoapellido]).Value : null;
                    data.cobrocontrato.cobrobanco = item.Contains(HidrotecRoldecontrato.HidrotecCodigobanco) ? (string)item[HidrotecRoldecontrato.HidrotecCodigobanco] : null;
                    //Se comenta porque falla
                    //data.Cobrocontrato.Cobrocodigotipodocumento = item.Contains(strQueryCobro + Contact.HidrotecTipodedocumentoId) ? ((EntityReference)((AliasedValue)item[strQueryCobro + Contact.HidrotecTipodedocumentoId]).Value).Id.ToString() : null;
                    //data.Cobrocontrato.Cobrotipodocumento = ((EntityReference)((AliasedValue)item[Contact.EntityName + "3." + Contact.HidrotecTipodedocumentoId]).Value).Name;
                    if (item.Contains(strQueryCobro + Contact.HidrotecTipodedocumentoId))
                    {
                        QueryExpression QTipoDocumento = new QueryExpression(HidrotecTipodedocumento.EntityName);
                        QTipoDocumento.Criteria.AddCondition(new ConditionExpression(HidrotecTipodedocumento.PrimaryKey, ConditionOperator.Equal, ((EntityReference)((AliasedValue)item[strQueryCobro + Contact.HidrotecTipodedocumentoId]).Value).Id.ToString()));
                        QTipoDocumento.ColumnSet.AddColumns(HidrotecTipodedocumento.HidrotecIdtipodedocumento,HidrotecTipodedocumento.PrimaryName);
                        EntityCollection resTipoDocumento= Utils.IOS.RetrieveMultiple(QTipoDocumento);
                        if (resTipoDocumento.Entities.Any())
                        {
                            data.cobrocontrato.cobrocodigotipodocumento = resTipoDocumento.Entities[0].HasAttributeValue<string>(HidrotecTipodedocumento.HidrotecIdtipodedocumento) ? resTipoDocumento.Entities[0].GetAttributeValue<string>(HidrotecTipodedocumento.HidrotecIdtipodedocumento) : null;
                            data.cobrocontrato.cobrotipodocumento = resTipoDocumento.Entities[0].HasAttributeValue<string>(HidrotecTipodedocumento.PrimaryName) ? resTipoDocumento.Entities[0].GetAttributeValue<string>(HidrotecTipodedocumento.PrimaryName) : null;
                        }


                    }
                    data.cobrocontrato.cobrocuenta = item.Contains(HidrotecRoldecontrato.HidrotecNumerodecuenta) ? (string)item[HidrotecRoldecontrato.HidrotecNumerodecuenta] : null;
                    data.cobrocontrato.cobrodc = item.Contains(HidrotecRoldecontrato.HidrotecDigitocontrolccc) ? (string)item[HidrotecRoldecontrato.HidrotecDigitocontrolccc] : null;
                    data.cobrocontrato.cobrofax = item.Contains(HidrotecRoldecontrato.HidrotecFax) ? (string)item[HidrotecRoldecontrato.HidrotecFax] : null;
                    data.cobrocontrato.cobronombreentidad = item.HasAttributeValue<EntityReference>(HidrotecRoldecontrato.HidrotecBancoId) ? item.GetAttributeValue<EntityReference>(HidrotecRoldecontrato.HidrotecBancoId).Name : null;
                    data.cobrocontrato.cobronumerodocumento = item.Contains(strQueryCobro + Contact.HidrotecNumerodocumento) ? (string)((AliasedValue)item[strQueryCobro + Contact.HidrotecNumerodocumento]).Value : null;
                    data.cobrocontrato.cobrosucursal = item.Contains(HidrotecRoldecontrato.HidrotecCodigosucursal) ? (string)item[HidrotecRoldecontrato.HidrotecCodigosucursal] : null;
                    data.cobrocontrato.cobrotelefono1 = item.Contains(HidrotecRoldecontrato.HidrotecTelefonofijo) ? (string)item[HidrotecRoldecontrato.HidrotecTelefonofijo] : null;
                    data.cobrocontrato.cobrotelefono2 = item.Contains(HidrotecRoldecontrato.HidrotecTelefonomovil) ? (string)item[HidrotecRoldecontrato.HidrotecTelefonomovil] : null;
                    data.cobrocontrato.fechalimitepago = item.Contains(HidrotecRoldecontrato.HidrotecFechalimite) ? (string)item[HidrotecRoldecontrato.HidrotecFechalimite] : null;
                    data.cobrocontrato.importemaximopago = item.Contains(HidrotecRoldecontrato.HidrotecImportEmaximo) ? (string)item[HidrotecRoldecontrato.HidrotecImportEmaximo] : null;
                }

                resp.codigorespuesta = "00001";
                resp.textorespuesta = "OK";
                resp.datosContrato = data;

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ObtenerDatosContratoResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio inserta una petición de cambio de tarifa en la base de datos de CAC. Para ello, se hacen las validaciones 
        /// pertinentes de datos y se inserta en las tablas historicoEstadoSolicitud con el procedimiento pov_inshistsolic y en la 
        /// tabla informacionSolicitudes con el procedimiento pov_insinforsolic y la tabla solicitudes con la función fov_inssolicitud.
        /// </summary>
        /// <param name="request"></param>
        /// <returns> NEO_SolicitarCambioTarifaResponse </returns>
        public NEO_SolicitarCambioTarifaResponse NEO_SolicitarCambioTarifa(NEO_SolicitarCambioTarifaRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_SolicitarCambioTarifaResponse resp = new NEO_SolicitarCambioTarifaResponse();
            /*
            request.Usuario = "05453623A";
            request.Identificadorcontrato = new AqualiaContactWS.Model.Comun.Identificadorcontrato();
            request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
            request.Correoelectronico = "abel._____@gmail.com";
            request.Telefonocontacto = "685175220" ;
            */
            /*
            request.Canalentrada = "App";
            request.Codpais = "34";
            request.Idioma = "es-es";
            request.Codigotipocliente = "1";
            request.Correoelectronico = "afdsdf.es@atos.es";
            request.Identificadorcontrato = new AqualiaContactWS.Model.Comun.Identificadorcontrato();
            request.Identificadorcontrato.codigocac = "11109";
            request.Identificadorcontrato.codigocontrato = "11109";
            request.Identificadorcontrato.codigoinstalacion = "10801";
            request.Identificadorcontrato.numerocontrato = "10801-1/1-011196";
            request.Motivocambio = "Cambio de Tarifa";
            request.Telefonocontacto = "111111111";
            request.Usuario = "173261612";
            */
            try
            {
               // request.usuario = "173261612";

                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", "", "SinConexion"); ;
                }
                
                if (request == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }
                
                
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }
                else
                {
                    List<Entity> contacts = Utils.ChequearExistenciaContacto(request.usuario);

                    if (contacts.Count < 1)
                    {
                        //return (NEO_SolicitarCambioTarifaResponse)respuestaWs.UsuarioBuscado();
                        return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "UsuarioBuscado");
                    }
                }
                
                if (request.motivocambio == "" || request.motivocambio == null)
                {
                  //  return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Motivocambio();
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "MotivoCambio");
                }
                
                if (request.correoelectronico == "" || request.correoelectronico == null)
                {
                    //return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Correo();
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "Correo");
                }
                
                if (request.telefonocontacto == "" || request.telefonocontacto == null)
                {
                  //  return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Telefonocontacto();
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "Telefonocontacto");
                }
                /*
                if (request.Identificadorcontrato.codigocac == "" || request.Identificadorcontrato.codigocac == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Codigocac();
                }
                
                if (request.Identificadorcontrato.codigocontrato == "" || request.Identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Codigocontrato();
                }
                
                if (request.Identificadorcontrato.codigoinstalacion == "" || request.Identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Codigoinstalacion();
                }
                
                if (request.Identificadorcontrato.numerocontrato == "" || request.Identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Numerocontrato();
                }
                */
                if (request.identificadorcontrato != null)
                {
                    if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                    {
                        return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "Numerocontrato");

                    }
                    if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                    {

                        //  return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocac");
                    }
                    if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                    {
                        return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "codigocontrato");
                    }
                    if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                    {
                        return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigoinstalacion");
                    }
                }
                else
                {

                    //return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Identificadorcontrato();
                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato");

                }
                string strAliasTipo = "tipo";
                string strQueryTipo = "tipo.";
                string strAliasSubtipo = "subtipo";
                string strQuerySubtipo = "subtipo.";
                string strAliasMotivo = "motivo";
                string strQueryMotivo = "motivo.";
                string strAliasDirSum = "dirSumContrato";
                string strQueryDirSum = "dirSumContrato.";
                string strAliasConfOrg = "confOrgContrato";
                string strQueryConfOrg = "confOrgContrato.";
                string strAliasTeam = "team";
                string strQueryTeam = "team.";

                string strCodRelCambioTarifa = "015";

                QueryExpression q = new QueryExpression(HidrotecRelacion.EntityName);
                q.Criteria.AddCondition(HidrotecRelacion.HidrotecIdrelacion, ConditionOperator.Equal, strCodRelCambioTarifa);
                q.ColumnSet.AddColumns(HidrotecRelacion.PrimaryKey, HidrotecRelacion.PrimaryName);

                LinkEntity leTipo = new LinkEntity(HidrotecRelacion.EntityName, HidrotecTipo.EntityName,
                    HidrotecRelacion.PrimaryKey, HidrotecTipo.HidrotecRelacionId, JoinOperator.Inner);
                leTipo.EntityAlias = strAliasTipo;
                leTipo.Columns.AddColumns(HidrotecTipo.PrimaryKey, HidrotecTipo.PrimaryName);

                LinkEntity leSubtipo = new LinkEntity(HidrotecTipo.EntityName, HidrotecSubtipo.EntityName,
                    HidrotecTipo.PrimaryKey, HidrotecSubtipo.HidrotecTipoId, JoinOperator.Inner);
                leSubtipo.EntityAlias = strAliasSubtipo;
                leSubtipo.Columns.AddColumns(HidrotecSubtipo.PrimaryKey, HidrotecSubtipo.PrimaryName);

                LinkEntity leMotivoSubtipo = new LinkEntity(HidrotecSubtipo.EntityName, HidrotecHidrotecSubtipoHidrotecMotivo.EntityName,
                    HidrotecSubtipo.PrimaryKey, HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecSubtipoId, JoinOperator.Inner);

                LinkEntity leMotivo = new LinkEntity(HidrotecHidrotecSubtipoHidrotecMotivo.EntityName, HidrotecMotivo.EntityName,
                    HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecMotivoId, HidrotecMotivo.PrimaryKey, JoinOperator.Inner);
                leMotivo.EntityAlias = strAliasMotivo;
                leMotivo.Columns.AddColumns(HidrotecMotivo.PrimaryKey, HidrotecMotivo.PrimaryName);

                // Por la descripción, parece que viene el nombre del motivo y no el código de motivo, así que filtramos por el nombre de momento
                leMotivo.LinkCriteria.AddCondition(HidrotecMotivo.PrimaryName, ConditionOperator.Equal, request.motivocambio);

                leMotivoSubtipo.LinkEntities.Add(leMotivo);
                leSubtipo.LinkEntities.Add(leMotivoSubtipo);
                leTipo.LinkEntities.Add(leSubtipo);
                q.LinkEntities.Add(leTipo);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {

                    return (NEO_SolicitarCambioTarifaResponse)respuestaWs.NoMotivoCambioSolicitudTarifa();
                }



                if (res.Entities.Any())
                {
                    Entity incident = new Entity(Incident.EntityName);

                    incident[Incident.HidrotecRelacionId] = new EntityReference(HidrotecRelacion.EntityName, (Guid)res.Entities[0][HidrotecRelacion.PrimaryKey]);
                    incident[Incident.HidrotecTipoId] = new EntityReference(HidrotecTipo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryTipo + HidrotecTipo.PrimaryKey]).Value);
                    incident[Incident.HidrotecSubtipoId] = new EntityReference(HidrotecSubtipo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQuerySubtipo + HidrotecSubtipo.PrimaryKey]).Value);
                    incident[Incident.HidrotecMotivoId] = new EntityReference(HidrotecMotivo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryMotivo + HidrotecMotivo.PrimaryKey]).Value);

                    /* Sacamos el ID del contacto para vincularlo al expediente */

                    q = new QueryExpression(Contact.EntityName);
                    q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                    res = Utils.IOS.RetrieveMultiple(q);

                    if (!res.Entities.Any())
                    {
                        return (NEO_SolicitarCambioTarifaResponse)respuestaWs.ContactoNoEncontrado();
                    }

                    incident[Incident.CustomerId] = new EntityReference(Contact.EntityName, (Guid)res.Entities[0][Contact.PrimaryKey]);

                    /* Sacamos los datos a través del contrato vinculado al expediente */

                    q = new QueryExpression(HidrotecContrato.EntityName);
                    q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);
                    q.ColumnSet.AddColumns(HidrotecContrato.PrimaryKey, HidrotecContrato.HidrotecEstado);

                    LinkEntity leDirSum = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                        HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                    leDirSum.EntityAlias = strAliasDirSum;
                    leDirSum.Columns.AddColumn(HidrotecDirecciondesuministro.PrimaryKey);

                    LinkEntity leConfOrg = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecConfiguracionorganizativa.EntityName,
                        HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecConfiguracionorganizativa.HidrotecMunicipioId, JoinOperator.Inner);
                    leConfOrg.EntityAlias = strAliasConfOrg;
                    leConfOrg.Columns.AddColumns(HidrotecConfiguracionorganizativa.PrimaryKey,
                                                 HidrotecConfiguracionorganizativa.HidrotecMunicipioId);

                    LinkEntity leBusUnit = new LinkEntity(HidrotecConfiguracionorganizativa.EntityName, BusinessUnit.EntityName,
                        HidrotecConfiguracionorganizativa.HidrotecExplotacionId, BusinessUnit.PrimaryKey, JoinOperator.Inner);

                    LinkEntity leTeam = new LinkEntity(BusinessUnit.EntityName, Team.EntityName,
                        BusinessUnit.PrimaryKey, Team.BusinessUnitId, JoinOperator.Inner);
                    leTeam.EntityAlias = strAliasTeam;
                    leTeam.LinkCriteria.AddCondition(Team.Isdefault, ConditionOperator.Equal, true);
                    leTeam.Columns.AddColumn(Team.PrimaryKey);

                    leBusUnit.LinkEntities.Add(leTeam);
                    leConfOrg.LinkEntities.Add(leBusUnit);
                    leDirSum.LinkEntities.AddRange(leConfOrg);
                    q.LinkEntities.Add(leDirSum);
                    res = Utils.IOS.RetrieveMultiple(q);
                    if (res.Entities.Any() == false)
                    {
                        //return (NEO_SolicitarCambioTarifaResponse)respuestaWs.ContratoBuscado();
                        return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetError("99999", idiomaorigen, "ContratoBuscado");
                    }

                    incident[Incident.HidrotecContratoId] = new EntityReference(HidrotecContrato.EntityName, (Guid)res.Entities[0][HidrotecContrato.PrimaryKey]);
                    if (request.canalentrada.ToUpper() == "APP")
                    {
                        incident[Incident.HidrotecCanaldeentrada] = new OptionSetValue(0);
                    }
                    incident[Incident.HidrotecCorreoelectronico] = request.correoelectronico;
                    incident[Incident.HidrotecTelefonofijoTitular] = request.telefonocontacto;

                    incident[Incident.HidrotecAsignadoalequipo] = new EntityReference(Team.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryTeam + Team.PrimaryKey]).Value);
                    incident[Incident.HidrotecDireccionsuministroId] = new EntityReference(HidrotecDirecciondesuministro.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryDirSum + HidrotecDirecciondesuministro.PrimaryKey]).Value);
                    incident[Incident.HidrotecConfiguracionorganizativaId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryConfOrg + HidrotecConfiguracionorganizativa.PrimaryKey]).Value);
                    incident[Incident.HidrotecMunicipioprincipalId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, (Guid)((EntityReference)((AliasedValue)res.Entities[0][strQueryConfOrg + HidrotecConfiguracionorganizativa.HidrotecMunicipioId]).Value).Id);
                    //incident[Incident.HidrotecEstadocontrato]=
                    incident[Incident.HidrotecEstadocontrato] = res.Entities[0].HasAttributeValue<OptionSetValue>(HidrotecContrato.HidrotecEstado) ? res.Entities[0].GetAttributeValue<OptionSetValue>(HidrotecContrato.HidrotecEstado) : null;

                    Utils.IOS.Create(incident);
                }

                //resp.codigorespuesta = "OK";
                //resp.textorespuesta = "OK";

                //return resp;
                return (NEO_SolicitarCambioTarifaResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK");
            }
            catch (Exception e)
            {
                return (NEO_SolicitarCambioTarifaResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: KO - Solo hay un contador por contrato.
        /// TODOO: Devolver siempre el contador del contrato, si procede..
        /// DESC:
        /// Este servicio devuelve el listado de contadores de un contrato pasado por parametros. Para ello, se hacen las validaciones 
        /// pertinentes de datos y se devuelven los contadores con la función fapp_selcontadorctt.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoContadoresContratoResponse NEO_ListadoContadoresContrato(NEO_ListadoContadoresContratoRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoContadoresContratoResponse resp = new NEO_ListadoContadoresContratoResponse();

            /*
            request.Usuario = "05453623A";
            request.Identificadorcontrato = new AqualiaContactWS.Model.Comun.Identificadorcontrato();
            request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
            */

            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }
                /*
                if (request.Identificadorcontrato.codigocac == "" || request.Identificadorcontrato.codigocac == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.Codigocac();
                }

                if (request.Identificadorcontrato.codigocontrato == "" || request.Identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.Codigocontrato();
                }

                if (request.Identificadorcontrato.codigoinstalacion == "" || request.Identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.Codigoinstalacion();
                }

                if (request.Identificadorcontrato.numerocontrato == "" || request.Identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.Numerocontrato();
                }

                if (!Utils.ChequearExistenciaContrato(request.Identificadorcontrato))
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.ContratoBuscado();
                }
                */
                if (request.identificadorcontrato != null)
                {
                    if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                    {
                        return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Numerocontrato");

                    }
                    if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                    {

                        // return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocac");
                    }
                    if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                    {
                        return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "codigocontrato");
                    }
                    if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                    {
                        return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigoinstalacion");
                    }
                }
                else
                {

                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato");


                }

                if (!Utils.ChequearExistenciaContrato(request.identificadorcontrato))
                {
                    return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetError("99999", idiomaorigen, "ContratoBuscado");
                }
                QueryExpression q = new QueryExpression(HidrotecContrato.EntityName);
                q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);
                q.ColumnSet.AddColumn(HidrotecContrato.HidrotecNumerocontador);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                resp.contadores = new List<Contador>();
                foreach (Entity item in res.Entities)
                {
                    if (item.Contains(HidrotecContrato.HidrotecNumerocontador))
                    {
                        Contador contador = new Contador()
                        {
                            numerocontador = item.Contains(HidrotecContrato.HidrotecNumerocontador) ? (string)item[HidrotecContrato.HidrotecNumerocontador] : null
                        };
                        resp.contadores.Add(contador);
                    }
                    
                }

                // resp.codigorespuesta = "OK";
                // resp.textorespuesta = "OK";

                // return resp;
                return (NEO_ListadoContadoresContratoResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK");
            }
            catch (Exception e)
            {
                return (NEO_ListadoContadoresContratoResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio inserta una petición de lectura de contador en la base de datos de CAC. Para ello, se hacen las validaciones 
        /// pertinentes de datos y se inserta en las tablas historicoEstadoSolicitud con el procedimiento pov_inshistsolic y en la 
        /// tabla informacionSolicitudes con el procedimiento pov_insinforsolic y la tabla solicitudes con la función fov_inssolicitud.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_LecturaContadorResponse NEO_LecturaContador(NEO_LecturaContadorRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_LecturaContadorResponse resp = new NEO_LecturaContadorResponse();
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais"); ;
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario"); ;
                }

                if (request.codigocontador == "" || request.codigocontador == null)
                {
                   // return (NEO_LecturaContadorResponse)respuestaWs.Codigocontador();
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocontador"); ;
                }

                if (request.fechalectura == "" || request.fechalectura == null)
                {
                    //return (NEO_LecturaContadorResponse)respuestaWs.Fechalectura();
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Fechalectura"); ;
                }

                if (request.telefonocontacto == "" || request.telefonocontacto == null)
                {
                    //return (NEO_LecturaContadorResponse)respuestaWs.Telefonocontacto();
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Telefonocontacto"); 
                }

                if (request.imagen == "" || request.imagen == null)
                {
                    //return (NEO_LecturaContadorResponse)respuestaWs.Imagen();
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Imagen");
                }

                if (request.valorlectura == "" || request.valorlectura == null)
                {
                   // return (NEO_LecturaContadorResponse)respuestaWs.Valorlectura();
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Valorlectura");
                }
                /*
                if (request.identificadorcontrato.codigocac == "" || request.Identificadorcontrato.codigocac == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.Codigocac();
                }

                if (request.Identificadorcontrato.codigocontrato == "" || request.Identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.Codigocontrato();
                }

                if (request.Identificadorcontrato.numerocontrato == "" || request.Identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.Numerocontrato();
                }

                if (request.Identificadorcontrato.codigoinstalacion == "" || request.Identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.Codigoinstalacion();
                }
                */

                if (request.identificadorcontrato != null)
                {
                    if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                    {
                        return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Numerocontrato"); 

                    }
                    if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                    {

                        // return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocac");
                    }
                    if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                    {
                        return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "codigocontrato");
                    }
                    if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                    {
                        return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigoinstalacion");
                    }
                }
                else
                {

                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato");


                }

                if (!Utils.ChequearExistenciaContrato(request.identificadorcontrato))
                {
                    return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "ContratoBuscado");
                }
                string strAliasTipo = "tipo";
                string strQueryTipo = "tipo.";
                string strAliasSubtipo = "subtipo";
                string strQuerySubtipo = "subtipo.";
                string strAliasDirSum = "dirSumContrato";
                string strQueryDirSum = "dirSumContrato.";
                string strAliasConfOrg = "confOrgContrato";
                string strQueryConfOrg = "confOrgContrato.";
                string strAliasTeam = "team";
                string strQueryTeam = "team.";

                string strRelContador = "016";
                string strTipoContador = "016001";
                string strSubtipoContador = "016001004";

                QueryExpression q = new QueryExpression(HidrotecRelacion.EntityName);
                q.Criteria.AddCondition(HidrotecRelacion.HidrotecIdrelacion, ConditionOperator.Equal, strRelContador);
                q.ColumnSet.AddColumns(HidrotecRelacion.PrimaryKey, HidrotecRelacion.PrimaryName);

                LinkEntity leTipo = new LinkEntity(HidrotecRelacion.EntityName, HidrotecTipo.EntityName,
                    HidrotecRelacion.PrimaryKey, HidrotecTipo.HidrotecRelacionId, JoinOperator.Inner);
                leTipo.LinkCriteria.AddCondition(HidrotecTipo.HidrotecIdtipo, ConditionOperator.Equal, strTipoContador);
                leTipo.EntityAlias = strAliasTipo;
                leTipo.Columns.AddColumns(HidrotecTipo.PrimaryKey, HidrotecTipo.PrimaryName);

                LinkEntity leSubtipo = new LinkEntity(HidrotecTipo.EntityName, HidrotecSubtipo.EntityName,
                    HidrotecTipo.PrimaryKey, HidrotecSubtipo.HidrotecTipoId, JoinOperator.Inner);
                leSubtipo.EntityAlias = strAliasSubtipo;
                leSubtipo.LinkCriteria.AddCondition(HidrotecSubtipo.HidrotecIdsubtipo, ConditionOperator.Equal, strSubtipoContador);
                leSubtipo.Columns.AddColumns(HidrotecSubtipo.PrimaryKey, HidrotecSubtipo.PrimaryName);

                LinkEntity leMotivoSubtipo = new LinkEntity(HidrotecSubtipo.EntityName, HidrotecHidrotecSubtipoHidrotecMotivo.EntityName,
                    HidrotecSubtipo.PrimaryKey, HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecSubtipoId, JoinOperator.Inner);

                LinkEntity leMotivo = new LinkEntity(HidrotecHidrotecSubtipoHidrotecMotivo.EntityName, HidrotecMotivo.EntityName,
                    HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecMotivoId, HidrotecMotivo.PrimaryKey, JoinOperator.Inner);
                leMotivo.Columns.AddColumns(HidrotecMotivo.PrimaryKey, HidrotecMotivo.PrimaryName);

               // leMotivoSubtipo.LinkEntities.Add(leMotivo);
               // leSubtipo.LinkEntities.Add(leMotivoSubtipo);
                leTipo.LinkEntities.Add(leSubtipo);
                q.LinkEntities.Add(leTipo);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                if (res.Entities.Any())
                {
                    Entity incident = new Entity(Incident.EntityName);

                    incident[Incident.HidrotecRelacionId] = new EntityReference(HidrotecRelacion.EntityName, (Guid)res.Entities[0][HidrotecRelacion.PrimaryKey]);
                    incident[Incident.HidrotecTipoId] = new EntityReference(HidrotecTipo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryTipo + HidrotecTipo.PrimaryKey]).Value);
                    incident[Incident.HidrotecSubtipoId] = new EntityReference(HidrotecSubtipo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQuerySubtipo + HidrotecSubtipo.PrimaryKey]).Value);
                    incident[Incident.HidrotecMotivoId] = null;

                    /* Sacamos el ID del contacto para vincularlo al expediente */

                    q = new QueryExpression(Contact.EntityName);
                    q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                    res = Utils.IOS.RetrieveMultiple(q);

                   
                    if (res.Entities.Any() == false)
                    {

                       // return (NEO_LecturaContadorResponse)respuestaWs.ContactoNoEncontrado();
                        return (NEO_LecturaContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "ContactoNoEncontrado");

                    }
                    incident[Incident.CustomerId] = new EntityReference(Contact.EntityName, (Guid)res.Entities[0][Contact.PrimaryKey]);

                    /* Sacamos los datos a través del contrato vinculado al expediente */

                    q = new QueryExpression(HidrotecContrato.EntityName);
                    q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);
                    q.ColumnSet.AddColumns(HidrotecContrato.PrimaryKey, HidrotecContrato.HidrotecEstado);

                    LinkEntity leDirSum = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                        HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                    leDirSum.EntityAlias = strAliasDirSum;
                    leDirSum.Columns.AddColumn(HidrotecDirecciondesuministro.PrimaryKey);

                    LinkEntity leConfOrg = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecConfiguracionorganizativa.EntityName,
                        HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecConfiguracionorganizativa.HidrotecMunicipioId, JoinOperator.Inner);
                    leConfOrg.EntityAlias = strAliasConfOrg;
                    leConfOrg.Columns.AddColumns(HidrotecConfiguracionorganizativa.PrimaryKey,
                                                 HidrotecConfiguracionorganizativa.HidrotecMunicipioId);

                    LinkEntity leBusUnit = new LinkEntity(HidrotecConfiguracionorganizativa.EntityName, BusinessUnit.EntityName,
                        HidrotecConfiguracionorganizativa.HidrotecExplotacionId, BusinessUnit.PrimaryKey, JoinOperator.Inner);

                    LinkEntity leTeam = new LinkEntity(BusinessUnit.EntityName, Team.EntityName,
                        BusinessUnit.PrimaryKey, Team.BusinessUnitId, JoinOperator.Inner);
                    leTeam.EntityAlias = strAliasTeam;
                    leTeam.LinkCriteria.AddCondition(Team.Isdefault, ConditionOperator.Equal, true);
                    leTeam.Columns.AddColumn(Team.PrimaryKey);

                    leBusUnit.LinkEntities.Add(leTeam);
                    leConfOrg.LinkEntities.Add(leBusUnit);
                    leDirSum.LinkEntities.AddRange(leConfOrg);
                    q.LinkEntities.Add(leDirSum);
                    res = Utils.IOS.RetrieveMultiple(q);

                    incident[Incident.HidrotecContratoId] = new EntityReference(HidrotecContrato.EntityName, (Guid)res.Entities[0][HidrotecContrato.PrimaryKey]);
                    if(request.canalentrada.ToUpper()=="APP")
                    {
                        incident[Incident.HidrotecCanaldeentrada] = new OptionSetValue(0);

                    }
                    //incident[Incident.HidrotecCanaldeentrada] = request.canalentrada;
                    incident[Incident.HidrotecTelefonofijoTitular] = request.telefonocontacto;

                    incident[Incident.HidrotecAsignadoalequipo] = new EntityReference(Team.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryTeam + Team.PrimaryKey]).Value);
                    incident[Incident.HidrotecDireccionsuministroId] = new EntityReference(HidrotecDirecciondesuministro.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryDirSum + HidrotecDirecciondesuministro.PrimaryKey]).Value);
                    incident[Incident.HidrotecConfiguracionorganizativaId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryConfOrg + HidrotecConfiguracionorganizativa.PrimaryKey]).Value);
                    incident[Incident.HidrotecMunicipioprincipalId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, (Guid)((EntityReference)((AliasedValue)res.Entities[0][strQueryConfOrg + HidrotecConfiguracionorganizativa.HidrotecMunicipioId]).Value).Id);
                    incident[Incident.HidrotecEstadocontrato] = res.Entities[0].HasAttributeValue<OptionSetValue>(HidrotecContrato.HidrotecEstado) ? res.Entities[0].GetAttributeValue<OptionSetValue>(HidrotecContrato.HidrotecEstado):null;
                    Guid idExpediente=Utils.IOS.Create(incident);

                    InsertarImagen(idExpediente, request.imagen, "Imagen1");
                    //resp.codigorespuesta = "OK";
                    //resp.textorespuesta = "OK";
                }
               

               

                //return resp;
                return  (NEO_LecturaContadorResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK");
            }
            catch (Exception e)
            {
                return (NEO_LecturaContadorResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve el listado de operaciones de un contador. Para ello, se hacen las validaciones pertinentes de 
        /// datos y se devuelven los contadores con la función fapp_seloperacionescontador.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoOperacionesContadorResponse NEO_ListadoOperacionesContador(NEO_ListadoOperacionesContadorRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoOperacionesContadorResponse resp = new NEO_ListadoOperacionesContadorResponse();
            resp.operaciones = new List<operacionescontador>();
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoOperacionesContadorResponse)respuestaWs.GetError("99999", "", "SinConexion"); 
                }
                
                if (request == null)
                {
                    return (NEO_ListadoOperacionesContadorResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoOperacionesContadorResponse)respuestaWs.GetError("99999", "", "Idioma"); ;
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoOperacionesContadorResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }


                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoOperacionesContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }
                
               
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoOperacionesContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoOperacionesContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }

                string strCodRelContador = "016";

                QueryExpression q = new QueryExpression(HidrotecSubtipo.EntityName);
                q.ColumnSet.AddColumns(HidrotecSubtipo.HidrotecIdsubtipo, HidrotecSubtipo.PrimaryName);

                LinkEntity leTipo = new LinkEntity(HidrotecSubtipo.EntityName, HidrotecTipo.EntityName,
                HidrotecSubtipo.HidrotecTipoId, HidrotecTipo.PrimaryKey, JoinOperator.Inner);

                LinkEntity leRel = new LinkEntity(HidrotecTipo.EntityName, HidrotecRelacion.EntityName,
                HidrotecTipo.HidrotecRelacionId, HidrotecRelacion.PrimaryKey, JoinOperator.Inner);
                leRel.LinkCriteria.AddCondition(new ConditionExpression(HidrotecRelacion.HidrotecIdrelacion, ConditionOperator.Equal, strCodRelContador));

                leTipo.LinkEntities.Add(leRel);
                q.LinkEntities.Add(leTipo);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                foreach (Entity item in res.Entities)
                {
                    operacionescontador opContador = new operacionescontador();
                    opContador.codigooperacion = (string)item[HidrotecSubtipo.HidrotecIdsubtipo];
                    opContador.textooperacion = (string)item[HidrotecSubtipo.PrimaryName];
                    resp.operaciones.Add(opContador);
                }

                resp.codigorespuesta = "00001";
                resp.textorespuesta = "OK";

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ListadoOperacionesContadorResponse)respuestaWs.Exception(e, "99999");
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// NOTA: Coger este ejemplo para todas las creaciones de solicitudes de Relaciones tipos y subtipos.
        /// DESC:
        /// Este servicio inserta una petición de operación del contador en la base de datos de CAC. Para ello, se hacen las validaciones 
        /// pertinentes de datos y se inserta en las tablas historicoEstadoSolicitud con el procedimiento pov_inshistsolic y en la tabla 
        /// informacionSolicitudes con el procedimiento pov_insinforsolic y la tabla solicitudes con la función fov_inssolicitud.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_SolicitarOperacionContadorResponse NEO_SolicitarOperacionContador(NEO_SolicitarOperacionContadorRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_SolicitarOperacionContadorResponse resp = new NEO_SolicitarOperacionContadorResponse();
            /*
            request.Usuario = "05453623A";
            request.Identificadorcontrato = new Identificadorcontrato();
            request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
            request.Motivosolicitud = "Verificación solicitada por el cliente";
            request.Correoelectronico = "abel._____@gmail.com";
            request.Telefonocontacto = "685175220";
             */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }
                
                if (request == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", "", "Idioma"); ;
                }
                else
                {
                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }

                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada"); ;
                }
                
                
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }
                
             /*   if (request.correoelectronico == "" || request.correoelectronico == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Correo();
                }*/
                
                if (request.codigooperacion == "" || request.codigooperacion == null)
                {
                   // return (NEO_SolicitarOperacionContadorResponse)respuestaWs.CodigoOperacion();
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "CodigoOperacion");
                }
                
                if (request.codigocontador == "" || request.codigocontador == null)
                {
                    //return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Codigocontador();
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocontador");
                }
                
                if (request.motivosolicitud == "" || request.motivosolicitud == null)
                {
                   // return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Motivosolicitud();
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Motivosolicitud");
                }
                
           /*     if (request.telefonocontacto == "" || request.telefonocontacto == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Telefonocontacto();
                }*/
                /*
                if (request.Identificadorcontrato.codigocac == "" || request.Identificadorcontrato.codigocac == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Codigocac();
                }
                
                if (request.Identificadorcontrato.codigocontrato == "" || request.Identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Codigocontrato();
                }
                
                if (request.Identificadorcontrato.codigoinstalacion == "" || request.Identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Codigoinstalacion();
                }
                
                if (request.Identificadorcontrato.numerocontrato == "" || request.Identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Numerocontrato();
                }
                */
                if (request.identificadorcontrato != null)
                {
                    if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                    {
                        return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Numerocontrato"); ;

                    }
                    if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                    {

                        // return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocac");
                    }
                    if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                    {
                        return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "codigocontrato"); ;
                    }
                    if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                    {
                        return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigoinstalacion");
                    }
                }
                else
                {

                    return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato"); ;


                }
                string strAliasTipo = "tipo";
                string strQueryTipo = "tipo.";
                string strAliasSubtipo = "subtipo";
                string strQuerySubtipo = "subtipo.";
                string strAliasMotivo = "motivo";
                string strQueryMotivo = "motivo.";
                string strAliasDirSum = "dirSumContrato";
                string strQueryDirSum = "dirSumContrato.";
                string strAliasConfOrg = "confOrgContrato";
                string strQueryConfOrg = "confOrgContrato.";
                string strAliasTeam = "team";
                string strQueryTeam = "team.";

                string strCodRelContador = "016";

                QueryExpression q = new QueryExpression(HidrotecRelacion.EntityName);
                q.Criteria.AddCondition(HidrotecRelacion.HidrotecIdrelacion, ConditionOperator.Equal, strCodRelContador);
                q.ColumnSet.AddColumns(HidrotecRelacion.PrimaryKey, HidrotecRelacion.PrimaryName);

                LinkEntity leTipo = new LinkEntity(HidrotecRelacion.EntityName, HidrotecTipo.EntityName,
                    HidrotecRelacion.PrimaryKey, HidrotecTipo.HidrotecRelacionId, JoinOperator.Inner);
                leTipo.EntityAlias = strAliasTipo;
                leTipo.Columns.AddColumns(HidrotecTipo.PrimaryKey, HidrotecTipo.PrimaryName);

                LinkEntity leSubtipo = new LinkEntity(HidrotecTipo.EntityName, HidrotecSubtipo.EntityName,
                    HidrotecTipo.PrimaryKey, HidrotecSubtipo.HidrotecTipoId, JoinOperator.Inner);
                leSubtipo.EntityAlias = strAliasSubtipo;
                leSubtipo.Columns.AddColumns(HidrotecSubtipo.PrimaryKey, HidrotecSubtipo.PrimaryName);

                LinkEntity leMotivoSubtipo = new LinkEntity(HidrotecSubtipo.EntityName, HidrotecHidrotecSubtipoHidrotecMotivo.EntityName,
                    HidrotecSubtipo.PrimaryKey, HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecSubtipoId, JoinOperator.Inner);

                LinkEntity leMotivo = new LinkEntity(HidrotecHidrotecSubtipoHidrotecMotivo.EntityName, HidrotecMotivo.EntityName,
                    HidrotecHidrotecSubtipoHidrotecMotivo.HidrotecMotivoId, HidrotecMotivo.PrimaryKey, JoinOperator.Inner);
                leMotivo.EntityAlias = strAliasMotivo;
                leMotivo.Columns.AddColumns(HidrotecMotivo.PrimaryKey, HidrotecMotivo.PrimaryName);

                // Por la descripción, parece que viene el nombre del motivo y no el código de motivo, así que filtramos por el nombre de momento
                leMotivo.LinkCriteria.AddCondition(HidrotecMotivo.PrimaryName, ConditionOperator.Equal, request.motivosolicitud);

                leMotivoSubtipo.LinkEntities.Add(leMotivo);
                leSubtipo.LinkEntities.Add(leMotivoSubtipo);
                leTipo.LinkEntities.Add(leSubtipo);
                q.LinkEntities.Add(leTipo);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);

                if (res.Entities.Any())
                {
                    Entity incident = new Entity(Incident.EntityName);

                    incident[Incident.HidrotecRelacionId] = new EntityReference(HidrotecRelacion.EntityName, (Guid)res.Entities[0][HidrotecRelacion.PrimaryKey]);
                    incident[Incident.HidrotecTipoId] = new EntityReference(HidrotecTipo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryTipo + HidrotecTipo.PrimaryKey]).Value);
                    incident[Incident.HidrotecSubtipoId] = new EntityReference(HidrotecSubtipo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQuerySubtipo + HidrotecSubtipo.PrimaryKey]).Value);
                    incident[Incident.HidrotecMotivoId] = new EntityReference(HidrotecMotivo.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryMotivo + HidrotecMotivo.PrimaryKey]).Value);

                    /* Sacamos el ID del contacto para vincularlo al expediente */

                    q = new QueryExpression(Contact.EntityName);
                    q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                    res = Utils.IOS.RetrieveMultiple(q);
                    if (res.Entities.Any() == false)
                    {

                        //return (NEO_SolicitarOperacionContadorResponse)respuestaWs.ContactoNoEncontrado();
                        return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "ContactoNoEncontrado");

                    }
                    incident[Incident.CustomerId] = new EntityReference(Contact.EntityName, (Guid)res.Entities[0][Contact.PrimaryKey]);

                    /* Sacamos los datos a través del contrato vinculado al expediente */

                    q = new QueryExpression(HidrotecContrato.EntityName);
                    q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);
                    q.ColumnSet.AddColumns(HidrotecContrato.PrimaryKey, HidrotecContrato.HidrotecEstado);

                    LinkEntity leDirSum = new LinkEntity(HidrotecContrato.EntityName, HidrotecDirecciondesuministro.EntityName,
                        HidrotecContrato.HidrotecDirecciondesuministroId, HidrotecDirecciondesuministro.PrimaryKey, JoinOperator.Inner);
                    leDirSum.EntityAlias = strAliasDirSum;
                    leDirSum.Columns.AddColumn(HidrotecDirecciondesuministro.PrimaryKey);

                    LinkEntity leConfOrg = new LinkEntity(HidrotecDirecciondesuministro.EntityName, HidrotecConfiguracionorganizativa.EntityName,
                        HidrotecDirecciondesuministro.HidrotecMunicipioId, HidrotecConfiguracionorganizativa.HidrotecMunicipioId, JoinOperator.Inner);
                    leConfOrg.EntityAlias = strAliasConfOrg;
                    leConfOrg.Columns.AddColumns(HidrotecConfiguracionorganizativa.PrimaryKey, HidrotecConfiguracionorganizativa.HidrotecMunicipioId);

                    LinkEntity leBusUnit = new LinkEntity(HidrotecConfiguracionorganizativa.EntityName, BusinessUnit.EntityName,
                        HidrotecConfiguracionorganizativa.HidrotecExplotacionId, BusinessUnit.PrimaryKey, JoinOperator.Inner);

                    LinkEntity leTeam = new LinkEntity(BusinessUnit.EntityName, Team.EntityName,
                        BusinessUnit.PrimaryKey, Team.BusinessUnitId, JoinOperator.Inner);
                    leTeam.EntityAlias = strAliasTeam;
                    leTeam.LinkCriteria.AddCondition(Team.Isdefault, ConditionOperator.Equal, true);
                    leTeam.Columns.AddColumn(Team.PrimaryKey);

                    leBusUnit.LinkEntities.Add(leTeam);
                    leConfOrg.LinkEntities.Add(leBusUnit);
                    leDirSum.LinkEntities.AddRange(leConfOrg);
                    q.LinkEntities.Add(leDirSum);
                    res = Utils.IOS.RetrieveMultiple(q);
                    if (res.Entities.Any() == false)
                    {
                        //return (NEO_SolicitarOperacionContadorResponse)respuestaWs.ContratoBuscado();
                        return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetError("99999", idiomaorigen, "ContratoBuscado");
                    }

                    incident[Incident.HidrotecContratoId] = new EntityReference(HidrotecContrato.EntityName, (Guid)res.Entities[0][HidrotecContrato.PrimaryKey]);
                    //incident[Incident.HidrotecCanaldeentrada] = request.canalentrada;
                    if (request.canalentrada.ToUpper() == "APP")
                    {
                        incident[Incident.HidrotecCanaldeentrada] = new OptionSetValue(0);

                    }
                    //incident[Incident.HidrotecCorreoelectronico] = request.correoelectronico;
                    //incident[Incident.HidrotecTelefonofijoTitular] = request.telefonocontacto;

                    incident[Incident.HidrotecAsignadoalequipo] = new EntityReference(Team.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryTeam + Team.PrimaryKey]).Value);
                    incident[Incident.HidrotecDireccionsuministroId] = new EntityReference(HidrotecDirecciondesuministro.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryDirSum + HidrotecDirecciondesuministro.PrimaryKey]).Value);
                    incident[Incident.HidrotecConfiguracionorganizativaId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, (Guid)((AliasedValue)res.Entities[0][strQueryConfOrg + HidrotecConfiguracionorganizativa.PrimaryKey]).Value);
                    incident[Incident.HidrotecMunicipioprincipalId] = new EntityReference(HidrotecConfiguracionorganizativa.EntityName, (Guid)((EntityReference)((AliasedValue)res.Entities[0][strQueryConfOrg + HidrotecConfiguracionorganizativa.HidrotecMunicipioId]).Value).Id);
                  
                    //Se añade el estado del contrato
                    incident[Incident.HidrotecEstadocontrato]= res.Entities[0].HasAttributeValue<OptionSetValue>(HidrotecContrato.HidrotecEstado) ? res.Entities[0].GetAttributeValue<OptionSetValue>(HidrotecContrato.HidrotecEstado) : null;

                    Utils.IOS.Create(incident);
                }

                // resp.codigorespuesta = "OK";
                // resp.textorespuesta = "OK";

                //return resp;
                return (NEO_SolicitarOperacionContadorResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK");

            }
            catch (Exception e)
            {
                return (NEO_SolicitarOperacionContadorResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio registra la activación de factura electrónica para un usuario y contrato pasados por parametros. 
        /// Para ello, se hacen las validaciones pertinentes de datos y se inserta en las tablas historicoEstadoSolicitud 
        /// con el procedimiento pov_inshistsolic y en la tabla informacionSolicitudes con el procedimiento pov_insinforsolic 
        /// y la tabla solicitudes con la función fov_inssolicitud.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ActivarFacturaElectronicaResponse NEO_ActivarFacturaElectronica(NEO_ActivarFacturaElectronicaRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ActivarFacturaElectronicaResponse resp = new NEO_ActivarFacturaElectronicaResponse();
            /*
            request.Identificadorcontrato = new AqualiaContactWS.Model.Comun.Identificadorcontrato();
            request.Identificadorcontrato.numerocontrato = "10801-1/1-015742";
            request.Usuario = "05453623A";
            request.Correoelectronico = "abel.__@gmail.com";
            */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", "", "SinConexion"); ;
                }
                
                if (request == null)
                {
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", "", "SinParametros"); ;
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }
                
               
                
                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais"); ;
                }
                
                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario"); ;
                }
                
                if (request.correoelectronico == "" || request.correoelectronico == null)
                {
                    //return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.Correo();
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "Correo"); ;
                }

                /* if (request.Identificadorcontrato.codigocac == "" || request.Identificadorcontrato.codigocac == null)
                 {
                     return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.Codigocac();
                 }

                 if (request.Identificadorcontrato.codigocontrato == "" || request.Identificadorcontrato.codigocontrato == null)
                 {
                     return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.Codigocontrato();
                 }

                 if (request.Identificadorcontrato.codigoinstalacion == "" || request.Identificadorcontrato.codigoinstalacion == null)
                 {
                     return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.Codigoinstalacion();
                 }

                 if (request.Identificadorcontrato.numerocontrato == "" || request.Identificadorcontrato.numerocontrato == null)
                 {
                     return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.Numerocontrato();
                 }*/
                if (request.identificadorcontrato != null)
                {
                    if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                    {
                        return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "Numerocontrato");

                    }
                    if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                    {

                        //   return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocac");;
                    }
                    if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                    {
                        return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "codigocontrato"); ;
                    }
                    if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                    {
                        return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigoinstalacion"); 
                    }
                }
                else
                {

                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato"); ;


                }
                string actModifTipoImpresionFactura = "hidrotec_actmodificartipoimpresionfactura";

                QueryExpression q = new QueryExpression(HidrotecContrato.EntityName);
                q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);
                q.ColumnSet.AddColumns(HidrotecContrato.PrimaryKey);
                q.NoLock = true;
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {
                    //return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.ContratoBuscado();
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato"); ;
                }

                string guidContrato = res.Entities[0][HidrotecContrato.PrimaryKey].ToString();

                q = new QueryExpression(Contact.EntityName);
                q.Criteria.AddCondition(Contact.HidrotecNumerodocumento, ConditionOperator.Equal, request.usuario);
                q.ColumnSet.AddColumns(Contact.PrimaryKey);
                q.NoLock = true;
                res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any() == false)
                {

                    //return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.ContactoNoEncontrado();
                    return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "ContactoNoEncontrado"); ;
                }
                string guidContacto = res.Entities[0][Contact.PrimaryKey].ToString();

                OrganizationRequest testCustomAction = new OrganizationRequest() { RequestName = actModifTipoImpresionFactura };
                testCustomAction.Parameters.Add("tipoImpresionNuevo", (int)Incident.HidrotecImpresionfactura_OptionSet.FacturaElectronica);
                testCustomAction.Parameters.Add("tipoImpresionAnterior", -1);
                testCustomAction.Parameters.Add("contratos", guidContrato);
                testCustomAction.Parameters.Add("contacto", guidContacto);
                testCustomAction.Parameters.Add("emailImpresionFactura", request.correoelectronico);
                //testCustomAction.Parameters.Add("canalModificacion", Incident.HidrotecCanaldeentrada_OptionSet.OficinaVirtual.ToString());
                testCustomAction.Parameters.Add("canalModificacion", "2");
                OrganizationResponse testCustomResponse = Utils.IOS.Execute(testCustomAction);

                if (testCustomResponse.Results.Contains("resultado"))
                {
                    string Resultado = (string)testCustomResponse.Results["resultado"];
                    if (Resultado.ToUpper() != "KO")
                    {
                        if (Resultado.ToUpper() == "1")
                        {
                             Entity entidadContrato = new Entity("hidrotec_contrato", new Guid(guidContrato));
                             entidadContrato["hidrotec_impresionfactura"] = new OptionSetValue(2);
                             entidadContrato["hidrotec_emailimpresionfactura"] = request.correoelectronico;
                             Utils.IOS.Update(entidadContrato);
                        }
                        else
                        {
                            /*
                            if (Resultado.Substring(0, 1) == "2")
                            {
                                String[] DatosRecibidos = Resultado.Split('|');
                                Entity entidadContrato = new Entity("hidrotec_contrato", new Guid(guidContrato));
                                entidadContrato["hidrotec_impresionfactura"] = new OptionSetValue(2);
                                entidadContrato["hidrotec_emailimpresionfactura"] = request.correoelectronico;
                                Utils.IOS.Update(entidadContrato);
                            }
                            if (Resultado.ToUpper() == "3")
                            {
                                resp.codigorespuesta = "KO";
                                resp.textorespuesta = "KO";

                                return resp;
                            }*/
                            return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetError("99999", idiomaorigen, "ErrorComunicaciones"); ;
                        }
                    }
                }
                return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.GetRespuesta("00001", idiomaorigen, "OK"); ;

               
            }
            catch (Exception e)
            {
                return (NEO_ActivarFacturaElectronicaResponse)respuestaWs.Exception(e,"99999");
            }
        }

        public static string GetImageExtension(byte[] imageData)
        {
            String extension = "";

            try
            {
                Guid id;

                using (MemoryStream ms = new MemoryStream(imageData))
                {
                    using (Image img = Image.FromStream(ms))
                    {
                        id = img.RawFormat.Guid;
                    }
                }

                if (id == ImageFormat.Png.Guid)
                {
                    extension = "png";
                }
                else if (id == ImageFormat.Bmp.Guid)
                {
                    extension = "bmp";
                }
                else if (id == ImageFormat.Emf.Guid)
                {
                    extension = "emf";
                }
                else if (id == ImageFormat.Exif.Guid)
                {
                    extension = "jpg";
                }
                else if (id == ImageFormat.Gif.Guid)
                {
                    extension = "gif";
                }
                else if (id == ImageFormat.Icon.Guid)
                {
                    extension = "ico";
                }
                else if (id == ImageFormat.Jpeg.Guid)
                {
                    extension = "jpg";
                }
                else if (id == ImageFormat.MemoryBmp.Guid)
                {
                    extension = "bmp";
                }
                else if (id == ImageFormat.Tiff.Guid)
                {
                    extension = "tif";
                }
                else if (id == ImageFormat.Wmf.Guid)
                {
                    extension = "wmf";
                }
            }
            catch
            {
            }

            return extension;
        }
        public static string GetImageMimeType(byte[] imageData)
        {
            String mimeType = "image/unknown";

            try
            {
                Guid id;

                using (MemoryStream ms = new MemoryStream(imageData))
                {
                    using (Image img = Image.FromStream(ms))
                    {
                        id = img.RawFormat.Guid;
                    }
                }

                if (id == ImageFormat.Png.Guid)
                {
                    mimeType = "image/png";
                }
                else if (id == ImageFormat.Bmp.Guid)
                {
                    mimeType = "image/bmp";
                }
                else if (id == ImageFormat.Emf.Guid)
                {
                    mimeType = "image/x-emf";
                }
                else if (id == ImageFormat.Exif.Guid)
                {
                    mimeType = "image/jpeg";
                }
                else if (id == ImageFormat.Gif.Guid)
                {
                    mimeType = "image/gif";
                }
                else if (id == ImageFormat.Icon.Guid)
                {
                    mimeType = "image/ico";
                }
                else if (id == ImageFormat.Jpeg.Guid)
                {
                    mimeType = "image/jpeg";
                }
                else if (id == ImageFormat.MemoryBmp.Guid)
                {
                    mimeType = "image/bmp";
                }
                else if (id == ImageFormat.Tiff.Guid)
                {
                    mimeType = "image/tiff";
                }
                else if (id == ImageFormat.Wmf.Guid)
                {
                    mimeType = "image/wmf";
                }
            }
            catch
            {
            }

            return mimeType;
        }
        public static void InsertarImagen(Guid IdExpediente, string Imagen, string NombreImagen)

        {

            byte[] data = System.Convert.FromBase64String(Imagen);


            Entity AnnotationEntityObject = new Entity(Annotation.EntityName);
            AnnotationEntityObject.Attributes[Annotation.ObjectId] = new EntityReference(Incident.EntityName, IdExpediente);
            AnnotationEntityObject.Attributes["subject"] = NombreImagen;
            AnnotationEntityObject.Attributes["documentbody"] = Imagen;
            // Set the type of attachment
            AnnotationEntityObject.Attributes["mimetype"] = GetImageMimeType(data);
            // Set the File Name
            AnnotationEntityObject.Attributes["filename"] = NombreImagen + "." + GetImageExtension(data);
            var idNotaImagen1 = Utils.IOS.Create(AnnotationEntityObject);

        }
    }
}
