﻿using AqualiaContactWS.Helper;
using AqualiaContactWS.Model.NEO_DescargarFacturaElectronica;
using AqualiaContactWS.Model.NEO_DetalleDocumentoPago;
using AqualiaContactWS.Model.NEO_ListadoDocumentosPago;
using System;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Pagos" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Pagos.svc or Pagos.svc.cs at the Solution Explorer and start debugging.
    public class Pagos : IPagos
    {

        public Guid getConfiguracion(IOrganizationService service, string explotacion, string contrato)
        {
            Guid respuesta = Guid.Empty;

            QueryExpression q1 = new QueryExpression(HidrotecContrato.EntityName);
            q1.ColumnSet.AddColumns(HidrotecContrato.OwningBusinessUnit, HidrotecContrato.HidrotecCodigoservicio, HidrotecContrato.HidrotecCodigocontrata, HidrotecContrato.HidrotecIdiomacontrato);
            q1.Criteria.AddCondition(HidrotecContrato.PrimaryKey, ConditionOperator.Equal, new Guid(contrato));

            try
            {
                var res1 = Utils.IOS.RetrieveMultiple(q1);
                if (res1.Entities.Any())
                {
                    QueryExpression q = new QueryExpression(HidrotecConfiguracionorganizativa.EntityName);
                    q.Criteria.Conditions.AddRange(
                         new ConditionExpression(HidrotecConfiguracionorganizativa.HidrotecServicio, ConditionOperator.Equal, res1.Entities[0].GetAttributeValue<string>(HidrotecContrato.HidrotecCodigoservicio)),
                         new ConditionExpression(HidrotecConfiguracionorganizativa.HidrotecContrata, ConditionOperator.Equal, res1.Entities[0].GetAttributeValue<string>(HidrotecContrato.HidrotecCodigocontrata)),
                         new ConditionExpression(HidrotecConfiguracionorganizativa.HidrotecExplotacionId, ConditionOperator.Equal, new Guid(explotacion))
                    );

                    var res = service.RetrieveMultiple(q);
                    foreach (var configuracion in res.Entities)
                    {
                        respuesta = configuracion.Id;
                        return respuesta;
                    }
                }
                return respuesta;
            }
            catch (Exception ex)
            {
                return respuesta;
            }

        }

        /// <summary>
        /// STATUS: KO
        /// DUDAS: Preguntar a Antonio
        /// DESC:
        /// Este servicio se encargara devovler la factura electrónica del documento de pago pasado por parámetros. Para ello se hacen las 
        /// validaciones pertinentes y se realiza la petición a un servicio externo a través de u nservicio web para la generación de la factura electrónica.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_DescargarFacturaElectronicaResponse NEO_DescargarFacturaElectronica(NEO_DescargarFacturaElectronicaRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_DescargarFacturaElectronicaResponse resp = new NEO_DescargarFacturaElectronicaResponse();

            try
            {
                Utils.InicializarVariables();

                if (Utils.IOS == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.SinConexion();
                }

                if (request == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.SinParametros();
                }

                if (request.Canalentrada == "" || request.Canalentrada == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.CanalEntrada();
                }

                if (request.Idioma == "" || request.Idioma == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Idioma();
                }

                if (request.Codpais == "" || request.Codpais == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Pais();
                }

                if (request.Usuario == "" || request.Usuario == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Usuario();
                }

                if (request.Identificadorcontrato.codigocac == "" || request.Identificadorcontrato.codigocac == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Codigocac();
                }

                if (request.Identificadorcontrato.codigocontrato == "" || request.Identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Codigocontrato();
                }

                if (request.Identificadorcontrato.codigoinstalacion == "" || request.Identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Codigoinstalacion();
                }

                if (request.Identificadorcontrato.numerocontrato == "" || request.Identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Numerocontrato();
                }

                if (request.Codigofactura == "" || request.Codigofactura == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Codigofactura();
                }

                if (request.Emailcorrespondencia == "" || request.Emailcorrespondencia == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Correo();
                }
                if (request.Numerodocumentopago == "" || request.Numerodocumentopago == null)
                {
                    return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Numerodocumentopago();
                }

                request.Codigofactura = "";

                QueryExpression q = new QueryExpression(HidrotecFactura.EntityName);
                q.Criteria.AddCondition(HidrotecFactura.HidrotecNumerodefactura, ConditionOperator.Equal, request.Codigofactura);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {

                }

                resp.codigorespuesta = "OK";
                resp.textorespuesta = "OK";

                return resp;

            }
            catch (Exception e)
            {
                return (NEO_DescargarFacturaElectronicaResponse)respuestaWs.Exception(e);
            }
        }

        /// <summary>
        /// STATUS: OK a medias
        /// TODOO: No sabemos de donde obtener algunos datos.
        /// DESC:
        /// Este servicio devuelve la información del documento de pago del contrato pasado pro parámetros. Para ello se hacen las validaciones 
        /// pertinentes y devuelve los datos a través de las funciones fov_selfacturas, fapp_selfactura y fov_selfacthija.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_DetalleDocumentoPagoResponse NEO_DetalleDocumentoPago(NEO_DetalleDocumentoPagoRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_DetalleDocumentoPagoResponse resp = new NEO_DetalleDocumentoPagoResponse();

            //request.identificadorcontrato = new Model.Comun.Identificadorcontrato();
            //request.identificadorcontrato.numerocontrato = "10308-1/1-000001";
            //request.identificadorcontrato.codigocontrato = "1";
            //request.identificadorcontrato.codigoinstalacion = "10308";
            //request.numerodocumentopago = "15111701P0004481";
            //request.canalentrada = "ov";
            //request.codpais = "34";
            //request.idioma = "es-es";
            //request.usuario = "00000001R";

            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;

                if (Utils.IOS == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", "", "SinConexion"); ;
                }

                if (request == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", "", "SinParametros"); ;
                }
                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", "", "Idioma"); ;
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }

                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario"); ;
                }

                if (request.numerodocumentopago == "" || request.numerodocumentopago == null)
                {
                    //return (NEO_DetalleDocumentoPagoResponse)respuestaWs.Numerodocumentopago();
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario"); ;
                }

                /*if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                {
                    // return (NEO_DetalleDocumentoPagoResponse)respuestaWs.Codigocac();

                }

                if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.Codigocontrato();
                }

                if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.Codigoinstalacion();
                }

                if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.Numerocontrato();
                }
                */
                if (request.identificadorcontrato != null)
                {
                    if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                    {
                        //  return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocac");;
                    }

                    if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                    {
                        return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "codigocontrato");
                    }

                    if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                    {
                        return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigoinstalacion");
                    }

                    if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                    {
                        return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Numerocontrato");
                    }
                }
                else
                {
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato");

                }

                //request.identificadorcontrato = new Model.Comun.Identificadorcontrato();
                //request.identificadorcontrato.numerocontrato = "10308-1/1-000001";
                //request.identificadorcontrato.codigocontrato = "1";
                //request.identificadorcontrato.codigoinstalacion = "10308";
                //request.numerodocumentopago = "15111701P0004481";
                //request.canalentrada = "ov";
                //request.codpais = "34";
                //request.idioma = "es-es";
                //request.usuario = "00000001R";

                string actDevuelveFacturasPendientes = "hidrotec_ACTConsultarFacturas";

                string strAliasTitular = "titular";
                string strQueryTitular = "titular.";
                string strAliasTipoDoc = "tipodocumentoTitular";
                string strQueryTipoDoc = "tipodocumentoTitular.";
                string strAliasDirSumm = "dirSuministro";
                string strQueryDirSumm = "dirSuministro.";
                string strAliasDirSumPedania = "dirSuministro.pedania";
                string strQueryDirSumPedania = "dirSuministro.pedania.";
                string strAliasDirSumMunicip = "dirSuministro.municipio";
                string strQueryDirSumMunicip = "dirSuministro.municipio.";
                string strAliasTipoCliente = "tipoCliente";
                string strQueryTipoCliente = "tipoCliente.";

                QueryExpression q = new QueryExpression()
                {
                    EntityName = HidrotecContrato.EntityName,
                    ColumnSet = new ColumnSet(HidrotecContrato.PrimaryKey,
                                              HidrotecContrato.OwningBusinessUnit,
                                              HidrotecContrato.HidrotecIdiomacontrato,
                                              HidrotecContrato.HidrotecFechaemision,
                                              HidrotecContrato.HidrotecNumerocontador,
                                              HidrotecContrato.HidrotecExistedeuda,
                                              HidrotecContrato.HidrotecFechadecaducidadclave)
                };
                q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);

                LinkEntity leTitular = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecContrato.EntityName,
                    LinkFromAttributeName = HidrotecContrato.HidrotecTitularId,
                    LinkToEntityName = Contact.EntityName,
                    LinkToAttributeName = Contact.PrimaryKey,
                    JoinOperator = JoinOperator.Inner,
                    EntityAlias = strAliasTitular,
                    Columns = new ColumnSet(Contact.HidrotecNombre,
                                            Contact.HidrotecPrimerapellido,
                                            Contact.HidrotecSegundoapellido,
                                            Contact.HidrotecNumerodocumento,
                                            Contact.HidrotecTipodepersona),
                };
                LinkEntity leTipoDocumento = new LinkEntity()
                {
                    LinkFromEntityName = Contact.EntityName,
                    LinkFromAttributeName = Contact.HidrotecTipodedocumentoId,
                    LinkToEntityName = HidrotecTipodedocumento.EntityName,
                    LinkToAttributeName = HidrotecTipodedocumento.PrimaryKey,
                    JoinOperator = JoinOperator.Inner,
                    EntityAlias = strAliasTipoDoc,
                    Columns = new ColumnSet(HidrotecTipodedocumento.PrimaryName),
                };
                LinkEntity leDirSumm = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecContrato.EntityName,
                    LinkFromAttributeName = HidrotecContrato.HidrotecDirecciondesuministroId,
                    LinkToEntityName = HidrotecDirecciondesuministro.EntityName,
                    LinkToAttributeName = HidrotecDirecciondesuministro.PrimaryKey,
                    JoinOperator = JoinOperator.Inner,
                    EntityAlias = strAliasDirSumm,
                    Columns = new ColumnSet(HidrotecDirecciondesuministro.HidrotecDireccioncompuesta,
                                            HidrotecDirecciondesuministro.HidrotecCodigoPostal),
                };
                LinkEntity lePedania = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecDirecciondesuministro.EntityName,
                    LinkFromAttributeName = HidrotecDirecciondesuministro.HidrotecMunicipioId,
                    LinkToEntityName = HidrotecPedania.EntityName,
                    LinkToAttributeName = HidrotecPedania.PrimaryKey,
                    JoinOperator = JoinOperator.LeftOuter,
                    EntityAlias = strAliasDirSumPedania,
                    Columns = new ColumnSet(HidrotecPedania.PrimaryName),
                };
                LinkEntity leMunicipio = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecDirecciondesuministro.EntityName,
                    LinkFromAttributeName = HidrotecDirecciondesuministro.HidrotecMunicipioId,
                    LinkToEntityName = HidrotecMunicipio.EntityName,
                    LinkToAttributeName = HidrotecMunicipio.PrimaryKey,
                    JoinOperator = JoinOperator.LeftOuter,
                    EntityAlias = strAliasDirSumMunicip,
                    Columns = new ColumnSet(HidrotecMunicipio.PrimaryName),
                };
                LinkEntity leTipoCliente = new LinkEntity()
                {
                    LinkFromEntityName = HidrotecContrato.EntityName,
                    LinkFromAttributeName = HidrotecContrato.HidrotecTipodeclienteId,
                    LinkToEntityName = HidrotecTipodecliente.EntityName,
                    LinkToAttributeName = HidrotecTipodecliente.PrimaryKey,
                    JoinOperator = JoinOperator.LeftOuter,
                    EntityAlias = strAliasTipoCliente,
                    Columns = new ColumnSet(HidrotecTipodecliente.PrimaryName),
                };

                leDirSumm.LinkEntities.AddRange(lePedania, leMunicipio);
                leTitular.LinkEntities.Add(leTipoDocumento);
                q.LinkEntities.AddRange(leTitular, leDirSumm, leTipoCliente);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    /* Datos Contrato */
                    resp.tienedeuda = res.Entities[0].Contains(HidrotecContrato.HidrotecExistedeuda) ? ((OptionSetValue)res.Entities[0][HidrotecContrato.HidrotecExistedeuda]).Value.ToString() : null;
                    resp.fechaemision = res.Entities[0].Contains(HidrotecContrato.HidrotecFechaemision) ? res.Entities[0][HidrotecContrato.HidrotecFechaemision].ToString() : null;
                    resp.numerocontador = res.Entities[0].Contains(HidrotecContrato.HidrotecNumerocontador) ? (string)res.Entities[0][HidrotecContrato.HidrotecNumerocontador] : null;
                    resp.fechalimite = res.Entities[0].Contains(HidrotecContrato.HidrotecFechadecaducidadclave) ? res.Entities[0][HidrotecContrato.HidrotecFechadecaducidadclave].ToString() : null;

                    /* Datos Titular */
                    resp.titularnombre = res.Entities[0].Contains(strQueryTitular + Contact.HidrotecNombre) ? (string)((AliasedValue)res.Entities[0][strQueryTitular + Contact.HidrotecNombre]).Value : null;
                    resp.titularapellido1 = res.Entities[0].Contains(strQueryTitular + Contact.HidrotecPrimerapellido) ? (string)((AliasedValue)res.Entities[0][strQueryTitular + Contact.HidrotecPrimerapellido]).Value : null;
                    resp.titularapellido2 = res.Entities[0].Contains(strQueryTitular + Contact.HidrotecSegundoapellido) ? (string)((AliasedValue)res.Entities[0][strQueryTitular + Contact.HidrotecSegundoapellido]).Value : null;
                    resp.titularnumerodocumento = res.Entities[0].Contains(strQueryTitular + Contact.HidrotecNumerodocumento) ? (string)((AliasedValue)res.Entities[0][strQueryTitular + Contact.HidrotecNumerodocumento]).Value : null;
                    resp.titulartipocliente = res.Entities[0].Contains(strQueryTipoCliente + HidrotecTipodecliente.PrimaryName) ? (string)((AliasedValue)res.Entities[0][strQueryTipoCliente + HidrotecTipodecliente.PrimaryName]).Value : null;
                    resp.titularnombretipodocumento = res.Entities[0].Contains(strQueryTipoDoc + HidrotecTipodedocumento.PrimaryName) ? (string)((AliasedValue)res.Entities[0][strQueryTipoDoc + HidrotecTipodedocumento.PrimaryName]).Value : null;

                    /* Datos Suministro */
                    resp.suministrocodigopostal = res.Entities[0].Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecCodigoPostal) ? (string)((AliasedValue)res.Entities[0][strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecCodigoPostal]).Value : null;
                    resp.suministrodireccion = res.Entities[0].Contains(strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecDireccioncompuesta) ? (string)((AliasedValue)res.Entities[0][strQueryDirSumm + HidrotecDirecciondesuministro.HidrotecDireccioncompuesta]).Value : null;
                    resp.suministronombremunicipio = res.Entities[0].Contains(strQueryDirSumMunicip + HidrotecMunicipio.PrimaryName) ? (string)((AliasedValue)res.Entities[0][strQueryDirSumMunicip + HidrotecMunicipio.PrimaryName]).Value : null;
                    resp.suministronombrepedania = res.Entities[0].Contains(strQueryDirSumPedania + HidrotecPedania.PrimaryName) ? (string)((AliasedValue)res.Entities[0][strQueryDirSumPedania + HidrotecPedania.PrimaryName]).Value : null;

                    OrganizationResponse testCustomResponse = new OrganizationResponse();
                    OrganizationRequest testCustomAction = new OrganizationRequest() { RequestName = actDevuelveFacturasPendientes };


                    Guid explotacion = getConfiguracion(Utils.IOS, ((EntityReference)res.Entities[0][HidrotecContrato.OwningBusinessUnit]).Id.ToString(), res.Entities[0].Id.ToString());

                    testCustomAction.Parameters.Add("idExplotacion", explotacion.ToString());
                    testCustomAction.Parameters.Add("idContrato", ((Guid)res.Entities[0][HidrotecContrato.PrimaryKey]).ToString());
                    testCustomAction.Parameters.Add("existeDeuda", "null");
                    testCustomAction.Parameters.Add("idIdioma", ((OptionSetValue)res.Entities[0][HidrotecContrato.HidrotecIdiomacontrato]).Value.ToString());
                    testCustomResponse = Utils.IOS.Execute(testCustomAction);

                    DocumentoPagoJson detalles = Utils.JsonDeserialize<DocumentoPagoJson>((string)testCustomResponse.Results["respuesta"]);
                    if (detalles.documentospago.Count() == 0)
                    {
                        //return (NEO_DetalleDocumentoPagoResponse)respuestaWs.NoDetalleDocumentoPafo();
                        return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "NoDetalleDocumentoPago");

                    }
                    Boolean HayDetalle = false;

                    var documentoDetallado = detalles.documentospago.Find(factura => factura.numerodocumentopago == request.numerodocumentopago);

                    if (documentoDetallado.numerodocumentopago == request.numerodocumentopago)
                    {
                        HayDetalle = true;
                        resp.importemora = documentoDetallado.mora.ToString();
                        resp.importependiente = documentoDetallado.importependiente.ToString();
                        resp.importerecargo = documentoDetallado.recargo.ToString();
                        resp.importetotal = documentoDetallado.importetotal.ToString();
                        resp.importemedio = (documentoDetallado.importetotal / documentoDetallado.facturas.Count<Factura>()).ToString();
                        resp.cobrable = documentoDetallado.cobrable;
                        resp.periodofacturacion = documentoDetallado.periodo;


                        /* Datos Lecturas */
                        //resp.Lecturaactualfecha = "";
                        //resp.Lecturaactualvalor = "";
                        //resp.Lecturaanteriorfecha = "";
                        //resp.Lecturaanteriorvalor = "";
                        //resp.Lecturaconsumo = "";
                        //resp.Lecturanumerodias = "";
                        //resp.Consumomedio = "";

                        /* Esto tiene pinta de ser por cada factura, no por cada documento de pago. */
                        //resp.Emisornombre = "";

                        resp.facturas = new List<Factur>();

                        foreach (Factura fa in documentoDetallado.facturas)
                        {
                            Factur fact = new Factur();

                            fact.factemisornombre = fa.emisornombre;
                            fact.factemisornumerodocumento = fa.codigofactura;
                            fact.factnumero = fa.numerofactura;
                            fact.Totalfactura = fa.importetotalfactura;

                            resp.facturas.Add(fact);
                        }
                    }

                    if (HayDetalle == false)
                    {
                        //return (NEO_DetalleDocumentoPagoResponse)respuestaWs.NoDetalleDocumentoPafo();
                        return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "NoDetalleDocumentoPago");
                    }
                    resp.codigorespuesta = "00001";
                    resp.textorespuesta = "OK";
                }
                else
                {
                    // resp.codigorespuesta = "KO";
                    // resp.textorespuesta = "KO";
                    return (NEO_DetalleDocumentoPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "ContratoBuscado");
                }

                return resp;
            }
            catch (Exception e)
            {
                return (NEO_DetalleDocumentoPagoResponse)respuestaWs.Exception(e,"99999");
            }
        }

        /// <summary>
        /// STATUS: OK Abel Gago
        /// DESC:
        /// Este servicio devuelve una lista de todos los documentos de pago de un contrato. Para ello se hacen las validaciones 
        /// pertinentes y devuelve los datos a través de las funciones fov_selfacturas, fapp_selfactura y fov_selcontratosusu.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public NEO_ListadoDocumentosPagoResponse NEO_ListadoDocumentosPago(NEO_ListadoDocumentosPagoRequest request)
        {
            RespuestasWS respuestaWs = new RespuestasWS();
            NEO_ListadoDocumentosPagoResponse resp = new NEO_ListadoDocumentosPagoResponse();
            /*
            request.Canalentrada = "App";
            request.Codpais = "34";
            request.Fechadesde = "1817-03-08T12:48:24";
            request.Fechahasta = "2017-03-08T12:48:24";
            request.Idioma = "es-es";
            request.Tienedeuda = "S";
            request.Usuario = "42193709W";
            request.Identificadorcontrato = new Model.Comun.Identificadorcontrato();
            request.Identificadorcontrato.codigocac = "11109";
            request.Identificadorcontrato.codigocontrato = "11109";
            request.Identificadorcontrato.codigoinstalacion = "10801";
            request.Identificadorcontrato.numerocontrato = "10801-1/1-011196";
            */
            try
            {
                Utils.InicializarVariables();
                string idiomaorigen = request.idioma;
                if (Utils.IOS == null)
                {
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", "", "SinConexion");
                }

                if (request == null)
                {
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", "", "SinParametros");
                }

                if (request.idioma == "" || request.idioma == null)
                {
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", "", "Idioma");
                }
                else
                {

                    request.idioma = Utils.chequeoIdioma(request.idioma);
                    if (request.idioma == "-1")
                    {

                        return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", "", "IdiomaNoCorrecto");

                    }



                }
                if (request.canalentrada == "" || request.canalentrada == null)
                {
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "CanalEntrada");
                }

               

                if (request.codpais == "" || request.codpais == null)
                {
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Pais");
                }

                if (request.usuario == "" || request.usuario == null)
                {
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Usuario");
                }
                if (request.identificadorcontrato != null)
                {
                    if (request.identificadorcontrato.codigocac == "" || request.identificadorcontrato.codigocac == null)
                    {
                        //  return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigocac");;
                    }

                    if (request.identificadorcontrato.codigocontrato == "" || request.identificadorcontrato.codigocontrato == null)
                    {
                        return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "codigocontrato");
                    }

                    if (request.identificadorcontrato.codigoinstalacion == "" || request.identificadorcontrato.codigoinstalacion == null)
                    {
                        return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Codigoinstalacion");
                    }

                    if (request.identificadorcontrato.numerocontrato == "" || request.identificadorcontrato.numerocontrato == null)
                    {
                        return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Numerocontrato");
                    }
                }
                else
                {
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Identificadorcontrato");

                }

              
                if (!Utils.ChequearExistenciaContrato(request.identificadorcontrato))
                {
                   
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "ContratoBuscado");
                }

                if (request.fechahasta == "" || request.fechahasta == null)
                {
                   
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Fechahasta");
                }

                if (request.fechadesde == "" || request.fechadesde == null)
                {
                    //return (NEO_ListadoDocumentosPagoResponse)respuestaWs.Fechadesde();
                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Fechadesde");
                }

                if (request.tienedeuda == "" || request.tienedeuda == null)
                {

                    return (NEO_ListadoDocumentosPagoResponse)respuestaWs.GetError("99999", idiomaorigen, "Tienedeuda");
                }

                string actDevuelveFacturasPendientes = "hidrotec_ACTConsultarFacturas";

                QueryExpression q = new QueryExpression()
                {
                    EntityName = HidrotecContrato.EntityName,
                    ColumnSet = new ColumnSet(HidrotecContrato.PrimaryKey,
                                              HidrotecContrato.OwningBusinessUnit,
                                              HidrotecContrato.HidrotecIdiomacontrato)
                };
                q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, request.identificadorcontrato.numerocontrato);

                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                if (res.Entities.Any())
                {
                    OrganizationResponse testCustomResponse = new OrganizationResponse();
                    OrganizationRequest testCustomAction = new OrganizationRequest() { RequestName = actDevuelveFacturasPendientes };
                    testCustomAction.Parameters.Add("idExplotacion", ((EntityReference)res.Entities[0][HidrotecContrato.OwningBusinessUnit]).Id.ToString());
                    testCustomAction.Parameters.Add("idContrato", ((Guid)res.Entities[0][HidrotecContrato.PrimaryKey]).ToString());
                    testCustomAction.Parameters.Add("existeDeuda", request.tienedeuda);
                    testCustomAction.Parameters.Add("idIdioma", ((OptionSetValue)res.Entities[0][HidrotecContrato.HidrotecIdiomacontrato]).Value.ToString());
                    testCustomResponse = Utils.IOS.Execute(testCustomAction);

                    DocumentoPagoJson detalles = Utils.JsonDeserialize<DocumentoPagoJson>((string)testCustomResponse.Results["respuesta"]);

                    resp.documentospago = new List<Model.NEO_ListadoDocumentosPago.Documentospago>();
                    foreach (var item in detalles.documentospago)
                    {
                        if (DateTime.Parse(item.fechaemision) >= DateTime.Parse(request.fechadesde) && DateTime.Parse(item.fechavencimiento) <= DateTime.Parse(request.fechahasta))
                        {
                            Model.NEO_ListadoDocumentosPago.Documentospago doc = new Model.NEO_ListadoDocumentosPago.Documentospago();

                            doc.cobrable = item.cobrable;
                            doc.tienefacturaelectronica = item.tienefacturaelectronica;
                            doc.importependiente = item.importependiente;
                            doc.importetotal = item.importetotal;
                            doc.mora = item.mora;
                            doc.recargo = item.recargo;
                            doc.codigodocumentopago = item.codigodocumentopago;
                            doc.fechaemision = item.fechaemision.ToString();
                            doc.fechavencimiento = item.fechavencimiento.ToString();
                            doc.numerodocumentopago = item.numerodocumentopago;
                            doc.periodo = item.periodo;

                            doc.informacioncobro = new List<Informacioncobro>();
                            foreach (var fact in item.facturas)
                            {
                                Informacioncobro ic = new Informacioncobro();

                                ic.codigofactura = fact.codigofactura; ;
                                ic.importependientefactura = fact.importependientefactura;
                                ic.mora = fact.mora;
                                ic.recargo = fact.recargo;
                                doc.informacioncobro.Add(ic);

                            }
                            resp.documentospago.Add(doc);
                        }
                    }


                }
                resp.codigorespuesta = "00001";
                resp.textorespuesta = "OK";




                return resp;
            }
            catch (Exception e)
            {
                return (NEO_ListadoDocumentosPagoResponse)respuestaWs.Exception(e,"99999");
            }
        }
    }
}
