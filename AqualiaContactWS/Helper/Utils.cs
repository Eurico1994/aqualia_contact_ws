﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using System.Xml.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using Entidades;

namespace AqualiaContactWS.Helper
{
    public static class Utils
    {
        public static IOrganizationService IOS { get; set; }
        public static string CrmConnectionString { get; set; }
        public static Guid UsuarioId { get; set; }
        public static string FicheroLog { get; set; }
        public static string mensajeError { get; set; }
        public static string mensajeLog { get; set; }
        public static string Directorio { get; set; }
        public static ExecuteMultipleRequest Emr { get; set; }
        public static int BufferEmr { get; set; }
        public static bool CertificateValidation { get; set; }

        


        public static void InicializarVariables()
        {
            Directorio = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~"), "bin");
            Emr = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            XDocument xmlDoc = XDocument.Load(Path.Combine(
                 Directorio, @"Config.xml"));
            var q = from c in xmlDoc.Descendants("config")
                    select new
                    {
                        crm = c.Element("crm").Value,
                        bufferemr = c.Element("bufferemr").Value,
                        certificatevalidation = c.Element("certificatevalidation").Value,
                        fLog = c.Element("ficheroLog").Value
                    };
            foreach (var p in q)
            {
                CrmConnectionString = p.crm;
                BufferEmr = int.Parse(p.bufferemr);
                CertificateValidation = bool.Parse(p.certificatevalidation);
                FicheroLog = p.fLog + "_" + DateTime.Now.ToString("yyyyMMdd") + ".log";
            }
            Log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Log("Config > ConnectionString: " + CrmConnectionString + " | > BufferEmr: " + BufferEmr);
            GetConnection();
            Log("Conexión establecida... OK");
        }

        public static void GetConnection()
        {
            try
            {
                if (CertificateValidation)
                    ServicePointManager.ServerCertificateValidationCallback = AcceptAllCertificatePolicy;
                CrmServiceClient conn = new CrmServiceClient(CrmConnectionString);
                IOS = conn.OrganizationWebProxyClient != null ? conn.OrganizationWebProxyClient : (IOrganizationService)conn.OrganizationServiceProxy;
                UsuarioId = ((WhoAmIResponse)IOS.Execute(new WhoAmIRequest())).UserId;
            }
            catch (Exception e)
            {
                Log("Se ha producido un error al establecer la conexión con CRM. Detalles: " + e.Message + " | " + e.StackTrace);
            }
        }

        private static bool AcceptAllCertificatePolicy(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static void CrearLogErrorIntegacion(string idIntegracion, int tipoError, string paramLlamadaWS, string respuestaWS, string sistemaOrigen, string sistemaDestino, Entity e)
        {
            Entity log = new Entity(LogIntegracion.LogicalName);
            log[LogIntegracion.NombreEntidad] = e != null ? e.LogicalName : null;
            log[LogIntegracion.GuidRegistro] = e != null ? e.Id.ToString() : null;
            Entity config = LeerUno(ConfiguracionIntegracion.LogicalName, new List<string> { ConfiguracionIntegracion.Id, ConfiguracionIntegracion.ReintentosPermitidos },
                new List<string> { ConfiguracionIntegracion.IdIntegracion + "," + idIntegracion });
            if (config != null)
            {
                log[LogIntegracion.TipoIntegracion] = new EntityReference(ConfiguracionIntegracion.LogicalName, config.Id);
                log[LogIntegracion.ReintentosRestantes] = config.HasAttributeValue<int>(ConfiguracionIntegracion.ReintentosPermitidos) ? config.GetAttributeValue<int>(ConfiguracionIntegracion.ReintentosPermitidos) : 0;
            }
            log[LogIntegracion.SistemaOrigen] = sistemaOrigen;
            log[LogIntegracion.SistemaDestino] = sistemaDestino;
            log[LogIntegracion.TipoError] = new OptionSetValue(tipoError);
            log[LogIntegracion.ParamEntrada] = paramLlamadaWS;
            log[LogIntegracion.RespuestaServicio] = respuestaWS;
            Guid guidLog = IOS.Create(log);
        }

        public static bool HasAttributeValue<T>(this Entity entity, String attributeLogicalName)
        {
            Boolean entityHasAttributeValue = false;

            if (entity == null) return entityHasAttributeValue;

            if (String.IsNullOrWhiteSpace(attributeLogicalName))
            {
                throw new ArgumentException("Attribute Logical Name is missing...");
            }
            else
            {
                entityHasAttributeValue = entity.Contains(attributeLogicalName) &&
                    entity.Attributes[attributeLogicalName] != null &&
                    entity.GetAttributeValue<T>(attributeLogicalName) != null;
            }
            return entityHasAttributeValue;
        }

        public static void Leer(out List<Entity> lista,
            string nombreEntidad, List<string> campos, List<string> ordenarPor,
            List<string> condiciones)
        {
            Entity EntidadCrm = new Entity(nombreEntidad);
            lista = new List<Entity>();

            QueryExpression qe = new QueryExpression(nombreEntidad);
            qe.ColumnSet = new ColumnSet(campos.ToArray());
            if (ordenarPor != null && ordenarPor.Any())
                foreach (var o in ordenarPor)
                    qe.Orders.Add(new OrderExpression(o, OrderType.Ascending));

            if (condiciones != null)
                foreach (var condicion in condiciones)
                    if (condicion.Split(',')[1].ToLower() == "null")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.Null);
                    else if (condicion.Split(',')[1].ToLower() == "notnull")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.NotNull);
                    else if (condicion.Split(',')[1].ToLower() == "thismonth")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.ThisMonth);
                    else if (condicion.Split(',')[1].ToLower() == "today")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.Today);
                    else if (condicion.Split(',')[1].ToLower() == "yesterday")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.Yesterday);
                    else if (condicion.Split(',')[1].ToLower() == "beforeyesterday")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.LessThan, new object[] { DateTime.Today.AddDays(-1) });
                    else if (condicion.Split(',')[1].ToLower() == "beforetoday")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.LessThan, new object[] { DateTime.Today });
                    else if (condicion.Split(',')[1].ToLower() == "beforespecifieddate")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.LessThan, new object[] { DateTime.Today.AddMonths(-2) });
                    else if (condicion.Split(',')[1].ToLower() == "beforexdays")
                    {
                        double days = double.Parse(condicion.Split(',')[2]) * (-1);
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.LessThan, new object[] { DateTime.Today.AddDays(days) });
                    }

                    //else if (condicion.ToLower().StartsWith("modif,"))
                    //    qe.Criteria.AddFilter(GetFiltroFechas(Global.UltimoEnvio));
                    else if (condicion.Split(',')[0].StartsWith("!"))
                        qe.Criteria.AddCondition(condicion.Split(',')[0].Substring(1), ConditionOperator.NotEqual, new object[] { condicion.Split(',')[1] });
                    //else
                    //    qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.Equal, new object[] { condicion.Split(',')[1] });
                    else
                        qe.Criteria.AddCondition(condicion.Split(new char[] { ',' }, 2)[0], ConditionOperator.Equal, new object[] { condicion.Split(new char[] { ',' }, 2)[1] });

            qe.PageInfo = new PagingInfo();
            qe.PageInfo.Count = 5000;
            qe.PageInfo.PageNumber = 1;
            while (true)
            {
                var entidades = IOS.RetrieveMultiple(qe);

                foreach (Entity de in entidades.Entities)
                    lista.Add(de);

                if (entidades.MoreRecords)
                {
                    qe.PageInfo.PageNumber++;
                    qe.PageInfo.PagingCookie = entidades.PagingCookie;
                }
                else
                    break;
            }
        }

        internal static void CrearLogErrorIntegacion(string integracion, object tecnico, object p1, object p2, object sistemaDiversa, object sistemaCRM, object p3)
        {
            throw new NotImplementedException();
        }

        public static void CambiarEstadoOrdenServicio(Guid id, int estado)
        {
            Entity orden = LeerUno("new_ordendeservicio", new List<string> { "new_ordendeservicioid" }, new List<string> { "new_rdenesdeservicioid," + id.ToString() }, "AND");
            Entity ordenACT = new Entity(orden.LogicalName, orden.Id);
            ordenACT["statuscode"] = new OptionSetValue(estado); //Establecemos el estado
            IOS.Update(ordenACT);
        }

        public static void Leer(out List<Entity> lista,
            string nombreEntidad, List<string> campos, List<string> ordenarPor,
            List<string> condiciones, int pagina, int tamano)
        {
            if (tamano > 5000)
                throw new Exception("El tamaño máximo de página es de 5000");
            if (ordenarPor == null || !ordenarPor.Any())
                throw new Exception("Es necesario especificar una columna de ordenación si se pagina");

            Entity EntidadCrm = new Entity(nombreEntidad);
            lista = new List<Entity>();

            QueryExpression qe = new QueryExpression(nombreEntidad);
            qe.ColumnSet = new ColumnSet(campos.ToArray());
            if (ordenarPor != null && ordenarPor.Any())
                foreach (var o in ordenarPor)
                    qe.Orders.Add(new OrderExpression(o, OrderType.Ascending));

            if (condiciones != null)
                foreach (var condicion in condiciones)
                    if (condicion.Split(',')[1].ToLower() == "null")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.Null);
                    else if (condicion.Split(',')[1].ToLower() == "notnull")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.NotNull);
                    else if (condicion.Split(',')[1].ToLower() == "thismonth")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.ThisMonth);
                    else if (condicion.Split(',')[1].ToLower() == "today")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.Today);
                    else if (condicion.Split(',')[0].StartsWith("!"))
                        qe.Criteria.AddCondition(condicion.Split(',')[0].Substring(1), ConditionOperator.NotEqual, new object[] { condicion.Split(',')[1] });
                    else
                        qe.Criteria.AddCondition(condicion.Split(new char[] { ',' }, 2)[0], ConditionOperator.Equal, new object[] { condicion.Split(new char[] { ',' }, 2)[1] });

            qe.PageInfo = new PagingInfo();
            qe.PageInfo.Count = tamano;
            qe.PageInfo.PageNumber = pagina;
            while (true)
            {
                var entidades = IOS.RetrieveMultiple(qe);

                foreach (Entity de in entidades.Entities)
                    lista.Add(de);

                if (entidades.MoreRecords)
                {
                    qe.PageInfo.PageNumber++;
                    qe.PageInfo.PagingCookie = entidades.PagingCookie;
                }
                else
                    break;
            }
        }

        public static Entity LeerUnoPorId(string nombreEntidad, List<string> campos, Guid id)
        {
            return IOS.Retrieve(nombreEntidad, id, new ColumnSet(campos.ToArray()));
        }

        public static Entity LeerUno(string nombreEntidad, List<string> campos, List<string> condiciones,
            string operador = null)
        {
            if (!condiciones.Any())
                throw new Exception("Se requiere al menos una condición");
            Entity EntidadCrm = new Entity(nombreEntidad);
            QueryExpression qe = new QueryExpression(nombreEntidad);
            qe.ColumnSet = new ColumnSet(campos.ToArray());
            if (condiciones != null)
                foreach (var condicion in condiciones)
                    if (condicion.Split(',')[1].ToLower() == "null")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.Null);
                    else if (condicion.Split(',')[1].ToLower() == "notnull")
                        qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.NotNull);
                    //else
                    //    qe.Criteria.AddCondition(condicion.Split(',')[0], ConditionOperator.Equal, new object[] { condicion.Split(',')[1] });
                    else
                        qe.Criteria.AddCondition(condicion.Split(new char[] { ',' }, 2)[0], ConditionOperator.Equal, new object[] { condicion.Split(new char[] { ',' }, 2)[1] });

            if (operador != null)
            {
                if (operador == "AND")
                    qe.Criteria.FilterOperator = LogicalOperator.And;
                else if (operador == "OR")
                    qe.Criteria.FilterOperator = LogicalOperator.Or;
                else
                    throw new Exception("Operador invalido. Distinto de 'AND' u 'OR'");
            }

            var entidades = IOS.RetrieveMultiple(qe);
            if (entidades.Entities.Count > 1)
            {
                //throw new Exception("Encontrado más de un " + nombreEntidad + " para " + condiciones[1]);
                if (condiciones.Count > 1)
                    mensajeError += "Encontrado más de un " + nombreEntidad + " para " + condiciones[0] + ", " + condiciones[1] + "\n";
                else
                    mensajeError += "Encontrado más de un " + nombreEntidad + " para " + condiciones[0] + "\n";
                return null;
            }
            if (entidades.Entities.Any())
                //return (Entity)entidades.Entities.ElementAt(0);
                return entidades[0];
            return null;
        }

        /// <summary> 
        /// Resgistra un mensaje en la entidad Log Plugin 
        /// </summary> 
        /// <param name="service">Acceso a servicios de CRM</param> 
        /// <param name="nombre">Nombre para darle al mensaje</param> 
        /// <param name="mensaje">Cuerpo principal del log</param> 
        /// <param name="inicio">fecha y hora de inicio</param> 
        /// <param name="fin">fecha y hora de fin</param> 
        /// <param name="origen">Nombre del plugin que lo crea</param> 
        /// <param name="error">Indica si es un error o si es informativo</param>
        /// <param name="entidad">Indica el nombre de la entidad</param>
        /// <returns></returns>
        /// 
        public static void LogCrm(string nombre, string origen, string inicio, string fin, bool error, string mensaje, string entidad, string mensajeSDK)
        {
            Entity entidadLog = new Entity("hidrotec_logplugin");
            entidadLog["hidrotec_name"] = nombre;
            entidadLog["hidrotec_descripcion"] = mensaje;
            entidadLog["hidrotec_origen"] = origen;
            entidadLog["hidrotec_inicioejecucion"] = inicio;
            entidadLog["hidrotec_finejecucion"] = fin;
            entidadLog["hidrotec_error"] = error;
            entidadLog["hidrotec_entidad"] = entidad;
            entidadLog["hidrotec_mensajesdk"] = mensajeSDK;

            IOS.Create(entidadLog);
        }

        public static void TratarFault(FaultException<OrganizationServiceFault> ex)
        {
            mensajeError += "Error crm: " + ex.Message + Environment.NewLine + "Detalle: " + ex.Detail.Message + Environment.NewLine + "Pila faultEx: " + ex.StackTrace + "\n";
        }

        public static void TratarFaultProceso(FaultException<OrganizationServiceFault> ex, string infoAdicional)
        {
            mensajeError += "Error crm: " + ex.Message + Environment.NewLine + "Detalle soap: " + ex.Detail.Message +
                Environment.NewLine + infoAdicional + Environment.NewLine + "Pila soap: " + ex.StackTrace + "\n";
        }

        public static void TratarExcepcionBucle(Exception ex, string infoAdicional)
        {
            mensajeError += "Error en proceso: " + ex.Message + Environment.NewLine + infoAdicional +
                Environment.NewLine + "Pila: " + ex.StackTrace + "\n";
            if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                mensajeError += "Error (inner exception): " + ex.InnerException.Message + "\n";
        }

        public static void TratarExcepcionBucle(FaultException<OrganizationServiceFault> ex, string infoAdicional)
        {
            mensajeError += "Error fault en proceso: " + ex.Message + Environment.NewLine + "Detalle: " + ex.Detail.Message + Environment.NewLine + infoAdicional +
                Environment.NewLine + "Pila: " + ex.StackTrace + "\n";
            if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                mensajeError += "Error (inner exception): " + ex.InnerException.Message + "\n";
        }

        public static void AnadirElementoEmr(OrganizationRequest elemento)
        {
            Emr.Requests.Add(elemento);
            if (Emr.Requests.Count % BufferEmr == 0)
                EjecutarEMR();
        }

        public static int EjecutarEMR()
        {
            ExecuteMultipleResponse responseWithResults = null;
            try
            {
                responseWithResults = (ExecuteMultipleResponse)IOS.Execute(Emr);
            }
            catch (Exception ex)
            {
                mensajeLog += "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
                mensajeLog += "Error Crear EjecutarEMR: " + ex.Message + "\n";
                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                    mensajeLog += "Error en EjecutarEMR inner: " + ex.InnerException.Message + "\n";
                mensajeLog += "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
                Emr.Requests.Clear();
            }
            int ok = 0;
            int ko = 0;
            //SRR. Para que no salte la excepción de Null Reference
            if (responseWithResults != null)
            {
                for (int i = 0; i < responseWithResults.Responses.Count; i++)
                {
                    if (responseWithResults.Responses[i].Fault != null)
                    {
                        if (!string.IsNullOrEmpty(responseWithResults.Responses[i].Fault.Message))
                        {
                            mensajeLog += "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
                            mensajeLog += "Error en emr, fault message " + responseWithResults.Responses[i].Fault.Message + "\n";
                            mensajeLog += "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
                        }
                        ko++;
                    }
                    else
                    {
                        ok++;
                    }
                }
            }
            Emr.Requests.Clear();
            return ok;
        }

        public static void ProcesarUltimosEmr()
        {
            if (Emr.Requests.Count % BufferEmr != 0)
                EjecutarEMR();
        }

        public static List<Entity> ChequearExistenciaContacto(string nif)
        {
            List<string> atributos = new List<string> {
                    Contacto.Nombre, Contacto.PrimerApellido,
                    Contacto.SegundoApellido, Contacto.TipoDocumento,
                    Contacto.NumeroDocumento, Contacto.PaisPasaporte,
                    Contacto.MunicipioTexto, Contacto.ProvinciaTexto,
                    Contacto.ViaTexto,
                    Contacto.DireccionCompuesta, Contacto.Pais,
                    Contacto.Provincia, Contacto.Municipio,
                    Contacto.Via, Contacto.Numero,
                    Contacto.CodigoPostal, Contacto.Bloque,
                    Contacto.Edificio, Contacto.Barrio,
                    Contacto.Pedania, Contacto.Escalera,
                    Contacto.Piso, Contacto.Puerta,
                    Contacto.Otros
                };

            List<Entity> contactos = null;

            Leer(out contactos, Contacto.LogicalName, atributos, null, new List<string> { Contacto.NumeroDocumento + "," + nif });

            if (contactos != null)
                return contactos;
            else
                return null;
        }

        public static Entity ChequearExistenciaExpediente(string numExpediente)
        {
            List<string> atributos = new List<string> {
                    Expediente.EstadoExpediente,
                    Expediente.Resolucion,
                    Expediente.Contacto,
                    Expediente.IdiomaContacto
            };
            Entity e = LeerUno(Expediente.LogicalName, atributos, new List<string> { Expediente.Nombre + "," + numExpediente });

            if (e != null)
                return e;
            else
                return null;
        }

        public static bool ChequearExistenciaContrato(Model.Comun.Identificadorcontrato identificadorContrato)
        {
            if (identificadorContrato != null) {
                QueryExpression q = new QueryExpression(HidrotecContrato.EntityName);
                q.Criteria.AddCondition(HidrotecContrato.PrimaryName, ConditionOperator.Equal, identificadorContrato.numerocontrato);
                EntityCollection res = Utils.IOS.RetrieveMultiple(q);
                return res.Entities.Count > 0 ? true : false;
            } else { return false; }
        }

        public static Guid takeField(IOrganizationService _service, string entity, string field, string value)
        {

            var q = new QueryExpression(entity);
            q.Criteria.AddCondition(field, ConditionOperator.Equal, value);
            q.ColumnSet.AddColumn(entity + "id");

            var res = _service.RetrieveMultiple(q);
            if (res.Entities.Count > 0)
            {
                if (res.Entities[0].Contains(entity + "id"))
                    return ((Guid)res.Entities[0][entity + "id"]);
            }

            return new Guid();
        }

        public static List<Guid> getEquipoExplotacion(string municipio, IOrganizationService servicio)
        {
            List<Guid> guids = new List<Guid>();


            var q = new QueryExpression("team");
            q.Criteria.AddCondition("isdefault", ConditionOperator.Equal, true);
            q.ColumnSet.AddColumn("businessunitid");

            var leConfiguracionOrganizativa = new LinkEntity("team", "hidrotec_configuracionorganizativa", "businessunitid", "hidrotec_explotacionid", JoinOperator.Inner);
            leConfiguracionOrganizativa.LinkCriteria.AddCondition("hidrotec_municipioid", ConditionOperator.Equal, new Guid(municipio));
            leConfiguracionOrganizativa.Columns.AddColumn("hidrotec_configuracionorganizativaid");

            q.LinkEntities.Add(leConfiguracionOrganizativa);


            var res = servicio.RetrieveMultiple(q);
            if (res.Entities.Count > 0)
            {
                guids.Add((Guid)res.Entities[0]["teamid"]);
                guids.Add(((EntityReference)res.Entities[0]["businessunitid"]).Id);
                guids.Add((Guid)((AliasedValue)res.Entities[0]["hidrotec_configuracionorganizativa1.hidrotec_configuracionorganizativaid"]).Value);

                return guids;
            }
            return null;
        }

        public static void Log(string message)
        {
            string directorio = string.IsNullOrEmpty(Directorio) ?
                Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~"), "bin", "Logs") : Path.Combine(Directorio, "Logs");
            if (!Directory.Exists(directorio))
            {
                Directory.CreateDirectory(directorio);
            }
            if (!File.Exists(Path.Combine(directorio, FicheroLog)))
            {
                FileStream f = File.Create(Path.Combine(directorio, FicheroLog));
                f.Close();
            }

            using (StreamWriter sw = File.AppendText(Path.Combine(directorio, FicheroLog)))
            {
                sw.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss - ") + message);
            }
        }

        /// <summary>
        /// Método que devuelve un objeto deserializando una cadena en JSON
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString">Cadena en formato JSON</param>
        /// <returns></returns>
        public static T JsonDeserialize<T>(string jsonString)
        {
            jsonString = jsonString.Replace("\\", "");
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }
        /// <summary>
        /// Método crea una nota asociada a un expediente
        /// </summary>
        /// <param name="guidExpediente">Identificador del expediente</param>
        /// <param name="asunto">Asunto de la nota</param>
        /// <param name="texto">Detalle de la nota</param>
        ///<returns></returns>
        public static void CrearNotaTexto(Guid guidExpediente, string asunto, string texto)
        {
            //Crear nota
            Entity nota = new Entity("annotation");
            nota["objectid"] = new EntityReference("incident", guidExpediente);
            nota["subject"] = asunto;
            nota["notetext"] = texto;
            var id = IOS.Create(nota);
           
        }

        /// <summary>
        /// Método crea una nota asociada a un expediente
        /// </summary>
        /// <param name="Idioma">Identificador del expediente</param>
        ///<returns></returns>
        public static string chequeoIdioma(string Idioma)
        {
            string res = "-1";
            if (Idioma.ToUpper() == "ES-ES")
            {
                res = "3082";
                
            }
            //Euskera->1.069->eu - EU
            if (Idioma.ToUpper() == "ES-EU")
            {
                res = "1069";
                
            }
            //Catalán->1.027->ca - CA
            if (Idioma.ToUpper() == "ES-CA")
            {
               res = "1027";
                
            }
            //Valenciando->9.999->ca - CA
            if (Idioma.ToUpper() == "ES-CA")
            {
                res = "1027";
               
            }
            //Gallego->1.110->gl - GL
            if (Idioma.ToUpper() == "ES-GL")
            {
                res = "1110";
               
            }
            //Inglés->2.057->en-GB
            if (Idioma.ToUpper() == "EN-GB")
            {
                res = "2057";
                
            }
            //Portugues->2.070->pt - PT
            if (Idioma.ToUpper() == "PT-PT")
            {
               res = "2070";
               
            }
            //Italiano->1.040->it - IT
            if (Idioma.ToUpper() == "IT-IT")
            {
                res = "1040";
                
            }
            //Checo->1.029->cs - CS
            if (Idioma.ToUpper() == "CZ-CZ")
            {
                res = "1029";
              
            }
            return res;
        }

    }
}