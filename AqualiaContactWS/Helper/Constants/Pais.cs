﻿namespace AqualiaContactWS.Helper
{
    public class Pais
    {
        public const string LogicalName = "hidrotec_pais";
        public const string Id = "hidrotec_paisid";

        public const string LetraIBAN = "hidrotec_letraiban";
        public const string Nombre = "hidrotec_name";
        public const string IdPais = "hidrotec_idpais";
    }
}