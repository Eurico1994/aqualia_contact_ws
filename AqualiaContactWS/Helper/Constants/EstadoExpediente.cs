﻿namespace WS.Usuario.Helper
{
    public class EstadoExpediente
    {
        public const string LogicalName = "hidrotec_estadoexpediente";
        public const string Nombre = "hidrotec_name";

        public const string EstadoCerrado = "Cerrado";
    }
}