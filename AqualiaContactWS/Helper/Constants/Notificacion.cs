﻿namespace AqualiaContactWS.Helper
{
    public class Notificacion
    {
        public const string LogicalName = "hidrotec_notificacion";
        public const string Id = "hidrotec_notificacionid";

        public const string Asunto = "subject";
        public const string ReferenteA = "regardingobjectid";
        public const string Descripcion = "description";
        public const string Propietario = "ownerid";
        public const string Relacion = "hidrotec_relacionid";
        public const string Tipo = "hidrotec_tipoid";
        public const string Subtipo = "hidrotec_subtipoid";
    }
}