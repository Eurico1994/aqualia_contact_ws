﻿namespace AqualiaContactWS.Helper
{
    public class Instalacion
    {
        public const string LogicalName = "hidrotec_instalacion";
        public const string Id = "hidrotec_instalacionid";

        public const string Nombre = "hidrotec_name";
        public const string Municipios = "hidrotec_municipios";
        public const string Explotaciones = "hidrotec_explotaciones";
        public const string IdInstalacion = "hidrotec_idinstalacion";

        public const string Servidor = "hidrotec_servidor";
        public const string Puerto = "hidrotec_puerto";
        public const string NombreBD = "hidrotec_nombrebd";
        public const string InstanciaBD = "hidrotec_instanciabd";
    }
}