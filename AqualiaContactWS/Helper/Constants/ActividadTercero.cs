﻿namespace  AqualiaContactWS.Helper

{
    public class ActividadTercero
    {
        public const string LogicalName = "hidrotec_actividaddetercero";
        public const string Nombre = "hidrotec_name";
        public const string UrlSharepoint = "hidrotec_urlsharepoint";
        public const string TipoActividad = "hidrotec_tipoactividad";
        public const string Expediente = "hidrotec_expedienteid";
        public const string Documento = "hidrotec_tipodocumentoid";
        public const string Idioma = "hidrotec_idioma";
        public const string TipoDias = "hidrotec_tipodias";
        public const string Plazo = "hidrotec_plazo";
        public const string PlazoPorHoras = "hidrotec_plazoporhoras";
        public const string Contacto = "hidrotec_contactoid";
        public const string EstadoDocumento = "statuscode";
        public const string Bloqueante = "hidrotec_bloqueante";
        public const string Descripcion = "hidrotec_descripcion";

        public const string ActividadDocumento = "1";

        public class _TipoDias
        {
            public const int Laborables = 1;
            public const int Naturales = 2;
        }

        public class _EstadoDocumento
        {
            public const int PendienteEnviar = 1;
            public const int PendienteRecibir = 500000000;
        }
    }
}