﻿namespace AqualiaContactWS.Helper
{
    public class ConfiguracionIntegracion
    {
        public const string LogicalName = "hidrotec_configuracionintegracion";
        public const string Id = "hidrotec_configuracionintegracionid";

        public const string IdIntegracion = "hidrotec_idintegracion";
        public const string Nombre = "hidrotec_name";
        public const string URLPruebas = "hidrotec_urlpruebas";
        public const string URL = "hidrotec_url";
        public const string IntegracionActiva = "hidrotec_integracionactiva";
        public const string ReintentosPermitidos = "hidrotec_reintentospermitidos";
    }
}