﻿namespace AqualiaContactWS.Helper
{
    public class OrdenTrabajo
    {
        public const string LogicalName = "hidrotec_ordendetrabajo";
        public const string Id = "hidrotec_ordendetrabajoid";

        public const string Nombre = "hidrotec_name";
        public const string NumeroOT = "hidrotec_numerodeot";
        public const string EstadoActual = "hidrotec_estadoactual";
        public const string Expediente = "hidrotec_expedienteid";
        public const string MotivoCambioEstado = "hidrotec_motivocambiodeestado";
    }
}