﻿namespace WS.Usuario.Helper
{
    public class Resolucion
    {
        public const string LogicalName = "hidrotec_resolucion";
        public const string ResolucionId = "hidrotec_resolucionid";
        public const string Nombre = "hidrotec_name";

        public const string ResolucionAnulada = "Anulada";
    }
}