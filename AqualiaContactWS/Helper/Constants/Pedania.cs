﻿namespace AqualiaContactWS.Helper
{
    public class Pedania
    {
        public const string LogicalName = "hidrotec_pedania";
        public const string Id = "hidrotec_pedaniaid";

        public const string Nombre = "hidrotec_name";
        public const string IdPedania = "hidrotec_idpedania";
    }
}