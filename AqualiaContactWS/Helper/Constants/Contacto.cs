﻿namespace AqualiaContactWS.Helper
{
    public class Contacto
    {
        public const string LogicalName = "contact";
        public const string Id = "contactid";

        public const string NombreCompleto = "fullname";
        public const string Nombre = "hidrotec_nombre";
        public const string PrimerApellido = "hidrotec_primerapellido";
        public const string SegundoApellido = "hidrotec_segundoapellido";
        public const string TipoPersona = "hidrotec_tipodepersona";
        public const string TipoDocumento = "hidrotec_tipodedocumentoid";
        public const string PaisPasaporte = "hidrotec_paisid_documento";
        public const string NumeroDocumento = "hidrotec_numerodocumento";
        public const string Identificado = "hidrotec_identificado";
        public const string DireccionCompuesta = "hidrotec_direccioncompuesta";

        public const string Pais = "hidrotec_paisid";
        public const string Provincia = "hidrotec_provinciaid";
        public const string ProvinciaTexto = "hidrotec_provincia";
        public const string Municipio = "hidrotec_municipioid";
        public const string MunicipioTexto = "hidrotec_municipio";
        public const string Via = "hidrotec_viaid";
        public const string ViaTexto = "hidrotec_via";
        public const string Numero = "hidrotec_numero";
        public const string CodigoPostal = "hidrotec_codigopostal";
        public const string Edificio = "hidrotec_edificio";
        public const string Bloque = "hidrotec_bloque";
        public const string Piso = "hidrotec_piso";
        public const string Puerta = "hidrotec_puerta";
        public const string Escalera = "hidrotec_escalera";
        public const string Otros = "hidrotec_otros";
        public const string Pedania = "hidrotec_pedaniaid";
        public const string Barrio = "hidrotec_barrioid";

        public const string Sincronizado = "hidrotec_sincronizado";
        public const string EnviarDatos = "hidrotec_enviardatos";
        public const string UltimaSincronizacion = "hidrotec_ultimasincronizacion";
        public const string OrigenModificacion = "hidrotec_origenmodificacion";
        public const string CanalModificacion = "hidrotec_canalmodificacion";

        public class _OrigenModificacion
        {
            public const int CRM = 1;
            public const int ETL = 2;
        }

        public class _TipoPersona
        {
            public const int Fisica = 1;
            public const int Juridica = 2;
        }

        public class _TipoDocumento
        {
            public const int NIFPersonaFisica = 1;
            public const int NIFPersonaJuridica = 2;
            public const int TarjetaResidentes = 3;
            public const int OrganismosPublicos = 4;
            public const int Pasaporte = 5;
            public const int NIE = 6;
        }
    }
}