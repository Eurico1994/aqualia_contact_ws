﻿namespace AqualiaContactWS.Helper
{
    public class Contrato
    {
        public const string LogicalName = "hidrotec_contrato";
        public const string Id = "hidrotec_contratoid";

        public const string NumeroContrato = "hidrotec_name";
        public const string Titular = "hidrotec_titularid";
        public const string Instalacion = "hidrotec_instalacionid";
        public const string IdContratoDiversa = "hidrotec_idcontratodiversa";
        public const string Idioma = "hidrotec_idiomacontrato";
    }
}