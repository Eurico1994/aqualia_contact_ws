﻿namespace AqualiaContactWS.Helper
{
    public class Provincia
    {
        public const string LogicalName = "hidrotec_provincia";
        public const string Id = "hidrotec_provinciaid";

        public const string Nombre = "hidrotec_name";
        public const string IdProvincia = "hidrotec_idprovincia";
    }
}