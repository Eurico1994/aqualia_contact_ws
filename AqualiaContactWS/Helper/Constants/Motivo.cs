﻿namespace WS.Usuario.Helper
{
    public class Motivo
    {
        public const string LogicalName = "hidrotec_motivo";
        public const string MotivoId = "hidrotec_motivoid";
        public const string Nombre = "hidrotec_name";
    }
}