﻿namespace AqualiaContactWS.Helper
{
    public class Via
    {
        public const string LogicalName = "hidrotec_via";
        public const string TipoVia = "hidrotec_tipoviaid";
        public const string CodigoVia = "hidrotec_idvia";
        public const string DescripcionVia = "hidrotec_name";
    }
}