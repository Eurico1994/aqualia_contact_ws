﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AqualiaContactWS.Helper
{
    public class UsuarioOV
    {
        public const string LogicalName = "hidrotec_ov_usuario";
        public const string Id = "hidrotec_ov_usuario";

        public const string Nombre = "hidrotec_name";
        public const string Email = "hidrotec_email";
        public const string ContactoPrincipal = "hidrotec_contactoprincipalid";
        public const string Clave = "hidrotec_password";
    }
}