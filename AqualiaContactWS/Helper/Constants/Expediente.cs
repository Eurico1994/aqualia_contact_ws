﻿namespace AqualiaContactWS.Helper
{
    public class Expediente
    {
        public const string LogicalName = "incident";

        public const string IdExpediente = "incidentid";
        public const string UltimoEstadoFactura = "hidrotec_ultimoestadofacturas";
        public const string UltimaFacturaActualizada = "hidrotec_ultimafacturaactualizadaid";
        public const string Relacion = "hidrotec_relacionid";
        public const string Tipo = "hidrotec_tipoid";
        public const string Subtipo = "hidrotec_subtipoid";
        public const string Nombre = "title";
        public const string UltimoEstadoOT = "hidrotec_ultimoestadoots";
        public const string UltimaOrdenTrabajoActualizada = "hidrotec_ultimaotactualizadaid";
        public const string UltimoEstadoPresupuesto = "hidrotec_ultimoestadopresupuestos";
        public const string UltimoPresupuestoctualizado = "hidrotec_ultimopresupuestoactualizadoid";
        public const string ProcesoDeNegocio = "hidrotec_procesodenegocioid";
        public const string ParametrizacionExplotacion = "hidrotec_parametrizacionexplotacionid";
        public const string EstadoExpediente = "hidrotec_estadoexpedienteid";
        public const string TipoDias = "hidrotec_tipodias";
        public const string DiasPlazo = "hidrotec_diasplazo";
        public const string MunicipioPrincipal = "hidrotec_municipioprincipalid";
        public const string PeriodoGeneral = "hidrotec_periodogeneralid";
        public const string PeriodoDilacionTotal = "hidrotec_periododilaciontotalid";
        public const string DuracionDiasNaturales = "hidrotec_duraciondiasnaturales";
        public const string DuracionDiasLaborables = "hidrotec_duraciondiaslaborables";
        public const string Contrato = "hidrotec_contratoid";

        public const string Contacto = "customerid";
        public const string ConfiguracionOrganizativa = "hidrotec_configuracionorganizativaid";
        public const string Resolucion = "hidrotec_resolucionid";
        public const string DescripcionResolucion = "hidrotec_descripcionresolucion";
        public const string DescripcionMotivo = "hidrotec_descripcionmotivo";
        public const string FechaCierre = "hidrotec_fechadecierre";
        public const string CanalEntrada = "hidrotec_canaldeentrada";
        public const string CanalResolucion = "hidrotec_canalnotificacionresolucion";
        public const string ComunicadaResolucion = "hidrotec_comunicadaresolucion";
        public const string Explotacion = "hidrotec_explotacionid";
        public const string CreacionAutomatica = "hidrotec_creacionautomatica";
        public const string IdiomaContacto = "hidrotec_idiomacontrato";
        public const string Propietario = "ownerid";

        public class _TipoDias
        {
            public const int Laborables = 1;
            public const int Naturales = 2;
        }
    }
}