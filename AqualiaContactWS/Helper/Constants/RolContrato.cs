﻿namespace AqualiaContactWS.Helper
{
    public class RolContrato

    {
        public const string LogicalName = "hidrotec_roldecontrato";
        public const string Id = "hidrotec_roldecontratoid";

        public const string Nombre = "hidrotec_name";
        public const string Rol = "hidrotec_rolid";
        public const string Contacto = "hidrotec_contactoid";
        public const string Contrato = "hidrotec_contratoid";
        public const string TelefonoFijo = "hidrotec_telefonofijo";
        public const string TelefonoMovil = "hidrotec_telefonomovil";
        public const string Fax = "hidrotec_fax";
        public const string Email = "hidrotec_correoelectronico";

        //Datos correspondencia
        public const string Pais = "hidrotec_paisid";

        public const string Provincia = "hidrotec_provinciaid";
        public const string ProvinciaTexto = "hidrotec_provincia";
        public const string Municipio = "hidrotec_municipioid";
        public const string MunicipioTexto = "hidrotec_municipio";
        public const string Via = "hidrotec_viaid";
        public const string ViaTexto = "hidrotec_via";
        public const string Numero = "hidrotec_numero";
        public const string CodigoPostal = "hidrotec_codigopostal";
        public const string Edificio = "hidrotec_edificio";
        public const string Bloque = "hidrotec_bloque";
        public const string Piso = "hidrotec_piso";
        public const string Puerta = "hidrotec_puerta";
        public const string Escalera = "hidrotec_escalera";
        public const string Otros = "hidrotec_otros";
        public const string Pedania = "hidrotec_pedaniaid";
        public const string Barrio = "hidrotec_barrioid";
        public const string DireccionCompuesta = "hidrotec_direccioncompuesta";

        public const string PaisIBAN = "hidrotec_paisibanid";
        public const string IBAN = "hidrotec_iban";
        public const string DigitoControlIBAN = "hidrotec_digitocontroliban";
        public const string CodigoBanco = "hidrotec_codigobanco";
        public const string CodigoSucursal = "hidrotec_codigosucursal";
        public const string DigitoControlCCC = "hidrotec_digitocontrolccc";
        public const string NumCuenta = "hidrotec_numerodecuenta";
        public const string BajaDomiciliacion = "hidrotec_bajadomiciliacion";
        public const string FechaLimite = "hidrotec_fechalimite";
        public const string ImporteMaximo = "hidrotec_importemaximo";
        public const string Banco = "hidrotec_bancoid";

        public const string Sincronizado = "hidrotec_sincronizado";
        public const string EnviarDatos = "hidrotec_enviardatos";
        public const string UltimaSincronizacion = "hidrotec_ultimasincronizacion";
        public const string OrigenModificacion = "hidrotec_origenmodificacion";
        public const string CanalModificacion = "hidrotec_canalmodificacion";

        public class _OrigenModificacion
        {
            public const int CRM = 1;
            public const int ETL = 2;
        }
    }
}