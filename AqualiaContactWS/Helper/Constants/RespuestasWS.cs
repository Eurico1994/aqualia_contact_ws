﻿using AqualiaContactWS.Model.NEO_AltaAveria;
using AqualiaContactWS.Model.NEO_DescargarFacturaElectronica;
using AqualiaContactWS.Model.NEO_DocumentacionNecesaria;
using AqualiaContactWS.Model.NEO_EntidadBancaria;
using AqualiaContactWS.Model.NEO_EnviarIncidencias;
using AqualiaContactWS.Model.NEO_EnviarReclamacion;
using AqualiaContactWS.Model.NEO_LecturaContador;
using AqualiaContactWS.Model.NEO_ListadoDocumentosPago;
using AqualiaContactWS.Model.NEO_ListadoPaises;
using AqualiaContactWS.Model.NEO_ListadoPreguntasRespuestas;
using AqualiaContactWS.Model.NEO_ListadoSubmotivosReclamacion;
using AqualiaContactWS.Model.NEO_SolicitarOperacionContador;
using Helper.Model;
using System;

namespace AqualiaContactWS.Helper
{
    public class RespuestasWS
    {
        public ResponseSimple GetError(string Codigo, string Idioma, string Mensaje)
        {

            string TextoRespuesta = "";
            if(Idioma=="")
            {
                Idioma = "ES-ES";

            }
            
            System.Threading.Thread.CurrentThread.CurrentUICulture =
                   new System.Globalization.CultureInfo(Idioma.ToUpper());
            switch (Mensaje)
            {
                case "SinConexion":

                    TextoRespuesta = Resources.Mensajes.SinConexion;
                    break;
                case "SinParametros":

                    TextoRespuesta = Resources.Mensajes.SinParametros;
                    break;
                case "Idioma":

                    TextoRespuesta = Resources.Mensajes.Idioma;
                    break;
                case "CanaEntrada":

                    TextoRespuesta = Resources.Mensajes.CanalEntrada;
                    break;
                case "Pais":

                    TextoRespuesta = Resources.Mensajes.Pais;
                    break;
                case "Detalle":

                    TextoRespuesta = Resources.Mensajes.Detalle;
                    break;
                case "Direccion":

                    TextoRespuesta = Resources.Mensajes.Direccion;
                    break;
                case "DireccionSinCodificar":

                    TextoRespuesta = Resources.Mensajes.DireccionSinCodificar;
                    break;
                case "DatosAveria":

                    TextoRespuesta = Resources.Mensajes.DatosAveria;
                    break;

                case "Usuario":

                    TextoRespuesta = Resources.Mensajes.Usuario;
                    break;
                case "UsuarioBuscado":

                    TextoRespuesta = Resources.Mensajes.UsuarioBuscado;
                    break;
                case "IdiomaNoCorrecto":

                    TextoRespuesta = Resources.Mensajes.IdiomaNoCorrecto;
                    break;
                case "CodPregunta":

                    TextoRespuesta = Resources.Mensajes.CodPregunta;
                    break;
                case "ResPregunta":

                    TextoRespuesta = Resources.Mensajes.ResPregunta;
                    break;
                case "CodTipoDocumento":
                    TextoRespuesta = Resources.Mensajes.CodTipoDocumento;
                    break;
                case "NumDocumento":
                    TextoRespuesta = Resources.Mensajes.NumDocumento;
                    break;
                case "Correo":
                    TextoRespuesta = Resources.Mensajes.Correo;
                    break;
                case "Contrato":
                    TextoRespuesta = Resources.Mensajes.Contrato;
                    break;
                case "UsuarioExiste":
                    TextoRespuesta = Resources.Mensajes.UsuarioExiste;
                    break;
                case "NoPreguntaSeguridad":
                    TextoRespuesta = Resources.Mensajes.NoPreguntaSeguridad;
                    break;
                case "NoContratoContacto":
                    TextoRespuesta = Resources.Mensajes.NoContratoContacto;
                    break;
                case "Clave":
                    TextoRespuesta = Resources.Mensajes.Clave;
                    break;
                case "NoUsuarioClave":
                    TextoRespuesta = Resources.Mensajes.NoUsuarioClave;
                    break;
                case "NoUsuarioRespuesta":
                    TextoRespuesta = Resources.Mensajes.NoUsuarioRespuesta;
                    break;
                case "NuevaClave":
                    TextoRespuesta = Resources.Mensajes.NuevaClave;
                    break;
                case "TiposIdentificacionBuscados":
                    TextoRespuesta = Resources.Mensajes.TiposIdentificacionBuscados;
                    break;

                case "NoMotivosDeBaja":
                    TextoRespuesta = Resources.Mensajes.NoMotivosDeBaja;
                    break;
                case "CodigoMotivoBajaNoEncontrado":
                    TextoRespuesta = Resources.Mensajes.CodigoMotivoBajaNoEncontrado;
                    break;
                case "NoHayContratosAsociadosContacto":
                    TextoRespuesta = Resources.Mensajes.NoHayContratosAsociadosContacto;
                    break;
                case "Identificadorcontrato":
                    TextoRespuesta = Resources.Mensajes.Identificadorcontrato;
                    break;
                case "Numerocontrato":
                    TextoRespuesta = Resources.Mensajes.Numerocontrato;
                    break;
                case "Codigocac":
                    TextoRespuesta = Resources.Mensajes.Codigocac;
                    break;
                case "Codigocontrato":
                    TextoRespuesta = Resources.Mensajes.Codigocontrato;
                    break;
                case "Codigoinstalacion":
                    TextoRespuesta = Resources.Mensajes.Codigoinstalacion;
                    break;
                case "Motivocambio":
                    TextoRespuesta = Resources.Mensajes.Motivocambio;
                    break;
                case "Telefonocontacto":
                    TextoRespuesta = Resources.Mensajes.Telefonocontacto;
                    break;
                case "ContratoBuscado":
                    TextoRespuesta = Resources.Mensajes.ContratoBuscado;
                    break;
                case "Codigocontador":
                    TextoRespuesta = Resources.Mensajes.Codigocontador;
                    break;
                case "Fechalectura":
                    TextoRespuesta = Resources.Mensajes.Fechalectura;
                    break;
                case "Imagen":
                    TextoRespuesta = Resources.Mensajes.Imagen;
                    break;
                case "Valorlectura":
                    TextoRespuesta = Resources.Mensajes.Valorlectura;
                    break;
                case "ContactoNoEncontrado":
                    TextoRespuesta = Resources.Mensajes.ContactoNoEncontrado;
                    break;
                case "CodigoOperacion":
                    TextoRespuesta = Resources.Mensajes.CodigoOperacion;
                    break;
                case "Motivosolicitud":
                    TextoRespuesta = Resources.Mensajes.Motivosolicitud;
                    break;
                case "ErrorComunicaciones":
                    TextoRespuesta = Resources.Mensajes.ErrorComunicaciones;
                    break;
                case "Codigomunicipio":
                    TextoRespuesta = Resources.Mensajes.Codigomunicipio;
                    break;
                case "Fechahasta":
                    TextoRespuesta = Resources.Mensajes.Fechahasta;
                    break;
                case "Fechadesde":
                    TextoRespuesta = Resources.Mensajes.Fechadesde;
                    break;
                case "Tienedeuda":
                    TextoRespuesta = Resources.Mensajes.Tienedeuda;
                    break;
                case "Numerodocumentopago":
                    TextoRespuesta = Resources.Mensajes.Numerodocumentopago;
                    break;
                case "NoDetalleDocumentoPago":
                    TextoRespuesta = Resources.Mensajes.NoDetalleDocumentoPago;
                    break;



            }

            return new ResponseSimple
            {

               
                codigorespuesta = Codigo,
                textorespuesta = TextoRespuesta
            };
        }


        public ResponseSimple GetRespuesta(string Codigo, string Idioma, string Mensaje)
        {

            string TextoRespuesta = "";
            if (Idioma == "")
            {
                Idioma = "ES-ES";

            }

            System.Threading.Thread.CurrentThread.CurrentUICulture =
                   new System.Globalization.CultureInfo(Idioma.ToUpper());
            switch (Mensaje)
            {
                case "UsuarioExistente":

                    TextoRespuesta = Resources.Mensajes.SinConexion;
                    break;
                case "OK":

                    TextoRespuesta = Resources.Mensajes.SinConexion;
                    break;
                case "AutenticacionOK":

                    TextoRespuesta = Resources.Mensajes.AutenticacionOK;
                    break;
                    


            }

            return new ResponseSimple
            {


                codigorespuesta = Codigo,
                textorespuesta = TextoRespuesta
            };
        }
        public ResponseSimple Exception(Exception e, string codigo)
        {
            return new ResponseSimple
            {
                codigorespuesta = codigo,
                textorespuesta = "Error:" + e.Message + " || " + e.StackTrace
            };
        }
        public ResponseSimple SinConexion()
        {
            return new ResponseSimple
            {

                codigorespuesta = "500",
                textorespuesta = "Error. No se ha podido conectar a la organización."
            };
        }


        public ResponseSimple SinParametros()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9999",
                textorespuesta = "Error al comprobar la solicitud. No se han introducido parámetros de entrada."
            };
        }

        public ResponseSimple Pais()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9998",
                textorespuesta = "Error. El pais introducido en la llamada está vacío."
            };
        }

        public ResponseSimple Usuario()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9997",
                textorespuesta = "Error. El usuario introducido en la llamada está vacío."
            };
        }

        public ResponseSimple UsuarioBuscado()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9996",
                textorespuesta = "Error. El usuario introducido no existe."
            };
        }

        public ResponseSimple CanalEntrada()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9995",
                textorespuesta = "Error. El canal de entrada esta vacío."
            };
        }

        public ResponseSimple Idioma()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9994",
                textorespuesta = "Error. El idioma esta vacío."
            };
        }

        public ResponseSimple UsuarioExistente()
        {
            return new ResponseSimple
            {
                codigorespuesta = "00001",
                textorespuesta = "El usuario existe"
            };
        }

        public ResponseSimple Exception(Exception e)
        {
            return new ResponseSimple
            {
                codigorespuesta = "9998",
                textorespuesta = "Error. Se ha producido un error. Detalles " + e.Message + " || " + e.StackTrace
            };
        }



        public ResponseSimple CodPregunta()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9992",
                textorespuesta = "Error. El codigo de la pregunta esta vacío."
            };
        }

        public ResponseSimple ResPregunta()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9991",
                textorespuesta = "Error. La respuesta de la pregunta esta vacía."
            };
        }

        public ResponseSimple CodTipoDocumento()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9990",
                textorespuesta = "Error. El codigo del tipo de documento esta vacío."
            };
        }

        public ResponseSimple TiposIdentificacionBuscados()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9989",
                textorespuesta = "Error. No se encontró lista de tipo de identificación."
            };
        }

        public ResponseSimple Correo()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9988",
                textorespuesta = "Error. El correo esta vacío."
            };
        }

        public ResponseSimple NumDocumento()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9987",
                textorespuesta = "Error. El número de documento esta vacío."
            };
        }

        public ResponseSimple Contrato()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9986",
                textorespuesta = "Error. El número de contrato esta vacío."
            };
        }

        public ResponseSimple CodigoMotivoBaja()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9985",
                textorespuesta = "Error. El código de motivo de la baja esta vacío."
            };
        }

        public ResponseSimple NuevaClave()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9984",
                textorespuesta = "Error. La clave esta vacía."
            };
        }

        public ResponseSimple Clave()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9983",
                textorespuesta = "Error. La clave esta vacía."
            };
        }

        public ResponseSimple ok()
        {
            return new ResponseSimple
            {
                codigorespuesta = "OK",
                textorespuesta = "Todo los parametros OK."
            };
        }

        public ResponseSimple Identificadorcontrato()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9982",
                textorespuesta = "Error. Es necesario proporcionar el identificardor del contrato."
            };
        }

        public ResponseSimple Codigocac()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9981",
                textorespuesta = "Error. Es necesario proporcionar el Código del CAC."
            };
        }

        public ResponseSimple Codigocontrato()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9981",
                textorespuesta = "Error. Es necesario proporcionar el Código del contrato."
            };
        }

        public ResponseSimple Codigoinstalacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9981",
                textorespuesta = "Error. Es necesario proporcionar el Código de la instalación."
            };
        }

        public ResponseSimple Numerocontrato()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9981",
                textorespuesta = "Error. Es necesario proporcionar el número del contrato."
            };
        }

        public ResponseSimple Motivocambio()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9980",
                textorespuesta = "Error. Es necesario proporcionar el id del motivo de cambio."
            };
        }

        public ResponseSimple Telefonocontacto()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9979",
                textorespuesta = "Error. Es necesario proporcionar el número del teléfono del contacto."
            };
        }

        internal ResponseSimple Fechalectura()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9978",
                textorespuesta = "Error. Es necesario proporcionar la fecha de la lectura del contador."
            };
        }

        internal ResponseSimple Codigocontador()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9977",
                textorespuesta = "Error. Es necesario proporcionar el código del contador."
            };
        }

        internal ResponseSimple Imagen()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9976",
                textorespuesta = "Error. Es necesario proporcionar la imagen en Base64."
            };
        }

        internal ResponseSimple Valorlectura()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9975",
                textorespuesta = "Error. Es necesario proporcionar el valor de la lectura."
            };
        }

        internal ResponseSimple Motivosolicitud()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9974",
                textorespuesta = "Error. Es necesario proporcionar el motivo de la solicitud."
            };
        }

        internal ResponseSimple Identificadornodo()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9973",
                textorespuesta = "Error. Es necesario proporcionar el identificador del nodo."
            };
        }

        internal ResponseSimple Detalle()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9972",
                textorespuesta = "Error. Es necesario proporcionar una descripcion del detalle de la avería."
            };
        }

        internal ResponseSimple Direccionsincodificar()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9971",
                textorespuesta = "Error. Es necesario proporcionar la dirección sin codificar."
            };
        }

        internal ResponseSimple Codigomunicipio()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9970",
                textorespuesta = "Error. Es necesario proporcionar el codigo de municipio."
            };
        }

        internal ResponseSimple Detalleincidencia()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9969",
                textorespuesta = "Error. Es necesario proporcionar un detalle de la incidencia."
            };
        }

        internal ResponseSimple Codigotipoincidencia()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9968",
                textorespuesta = "Error. Es necesario proporcionar el codigo del tipo de incidencia."
            };
        }

        internal ResponseSimple Codigotipocliente()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9967",
                textorespuesta = "Error. Es necesario proporcionar el codigo del tipo de cliente."
            };
        }

        internal ResponseSimple Numerocuenta()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9966",
                textorespuesta = "Error. Es necesario proporcionar el número de cuenta."
            };
        }

        internal ResponseSimple Codigoentidad()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9965",
                textorespuesta = "Error. Es necesario proporcionar el codigo de la entidad."
            };
        }

        internal ResponseSimple Codigomotivoreclamacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9964",
                textorespuesta = "Error. Es necesario proporcionar el codigo del motivo de la reclamación."
            };
        }

        internal ResponseSimple Codigosubmotivoreclamacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9963",
                textorespuesta = "Error. Es necesario proporcionar el codigo del motivo de la subreclamación."
            };
        }

        internal ResponseSimple Detallereclamacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9962",
                textorespuesta = "Error. Es necesario proporcionar el detalle de la reclamación."
            };
        }

        internal ResponseSimple Fechahasta()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9961",
                textorespuesta = "Error. Es necesario proporcionar una fecha fin."
            };
        }

        internal ResponseSimple Fechadesde()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9960",
                textorespuesta = "Error. Es necesario proporcionar una fecha inicio."
            };
        }

        internal ResponseSimple Tienedeuda()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9959",
                textorespuesta = "Error. Es necesario informar si tiene deuda."
            };
        }

        internal ResponseSimple Numerodocumentopago()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9958",
                textorespuesta = "Error. Es necesario informar el número del documento de pago."
            };
        }

        internal ResponseSimple Codigofactura()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9957",
                textorespuesta = "Error. Es necesario informar el código de la factura."
            };
        }

        internal ResponseSimple DatosCobro()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9956",
                textorespuesta = "Error. Es necesario informar los datos de cobro."
            };
        }

        internal ResponseSimple Datoscorrespondencia()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9955",
                textorespuesta = "Error. Es necesario informar los datos de correspondencia."
            };
        }

        internal ResponseSimple Datossuministro()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9954",
                textorespuesta = "Error. Es necesario informar los datos de suministro."
            };
        }

        internal ResponseSimple Datostitular()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9953",
                textorespuesta = "Error. Es necesario informar los datos del titular."
            };
        }

        internal ResponseSimple ContratoBuscado()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9952",
                textorespuesta = "Error. El número de contrato introducido no existe."
            };
        }

        internal ResponseSimple CodigoOperacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9951",
                textorespuesta = "Error. El código de Operación no existe."
            };
        }

      
        internal ResponseSimple DatosAveria()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9950",
                textorespuesta = "Error. Datos averia no informados."
            };
        }
        internal ResponseSimple DatosContacto()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9949",
                textorespuesta = "Error. Datos contacto no informados."
            };
        }

        internal ResponseSimple Direccion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9948",
                textorespuesta = "Error. Datos dirección no informados."
            };
        }

        internal ResponseSimple Geolocalizacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9947",
                textorespuesta = "Error. Datos geolocalización no informados."
            };
        }
        internal ResponseSimple CorreoElectronicoContacto()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9946",
                textorespuesta = "Error. Correo no informado correctamente en los datos de Contacto."
            };
        }

        internal ResponseSimple ContactoNoEncontrado()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9945",
                textorespuesta = "Error. No se pudo encontrar el contacto ni con el usuario ni correo electrónico"
            };
        }

        internal ResponseSimple RelacionTipoSubtipo()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9944",
                textorespuesta = "Error. No se pudo encontrar la relación el tipo o el subtipo."
            };
        }
        internal ResponseSimple NoHayTipoIncidencia()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9943",
                textorespuesta = "Error. No existen tipos de incidencia"
            };
        }
        internal ResponseSimple NoHayContratosAsociadosContacto()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9942",
                textorespuesta = "Error. No hay contratos asociados al contacto."
            };
        }

        internal ResponseSimple NoMotivoCambioSolicitudTarifa()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9941",
                textorespuesta = "Error. El motivo de cambio informado no existe o no está asociado a la relación Solicitud Cambio Tarifa."
            };
        }

        internal ResponseSimple Notipocliente()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9940",
                textorespuesta = "Error. No se han encontrado tipos de cliente"
            };
        }
        internal ResponseSimple NodocumentacionNecesaria()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9939",
                textorespuesta = "Error. No hay documentos necesarios."
            };
        }

        internal ResponseSimple DatosMunicipio()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9938",
                textorespuesta = "Error. No hay datos asociados al municipio proporcionado."
            };
        }

        internal ResponseSimple EntidadBancariaNoEncontrada()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9937",
                textorespuesta = "Error. No se ha encontrado la entidad bancaria."
            };
        }
        internal ResponseSimple PaisNoEncontrado()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9936",
                textorespuesta = "Error. No se ha encontrado el pais."
            };
        }

        internal ResponseSimple BancoNoEncontrado()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9935",
                textorespuesta = "Error. No se ha encontrado el banco."
            };
        }

        internal ResponseSimple IdiomaNoCorrecto()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9934",
                textorespuesta = "Error. El idioma proporcionado no es correcto."
            };
        }

       
        internal ResponseSimple NoMotivosDeBaja()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9933",
                textorespuesta = "Error. No hay motivos de baja."
            };
        }

        internal ResponseSimple NoUSuarioRespuesta()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9932",
                textorespuesta = "Error. El usuario o la pregunta de seguridad del usuario, no se encuentran dadas de alta."
            };
        }
        internal ResponseSimple NoUSuarioClave()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9931",
                textorespuesta = "Error. El usuario o la clave usuario no son correctos."
            };
        }
        internal ResponseSimple NoPreguntasSeguridad()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9930",
                textorespuesta = "Error. El usuario no tiene preguntas de seguridad asociadas."
            };
        }

        internal ResponseSimple NoContratoContacto()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9929",
                textorespuesta = "Error. No existe contrato o contacto asociado al número de documento."
            };
        }

        internal ResponseSimple UsuarioExiste()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9928",
                textorespuesta = "Error. Ya existe un usuario, con ese número de documento asociado al contrato."
            };
        }

        internal ResponseSimple NoPreguntaSeguridad()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9927",
                textorespuesta = "Error. La pregunta de seguridad no existe."
            };
        }
        internal ResponseSimple NoMotivosReclamacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9926",
                textorespuesta = "Error. No se han encontrado motivos de reclamación."
            };
        }

        internal ResponseSimple ReclamacionesNoEncontradas()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9925",
                textorespuesta = "Error. No se han encontrado reclamaciones, revise los filtros proporcionados."
            };
        }
        internal ResponseSimple SolicitudesNoEncontradas()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9924",
                textorespuesta = "Error. No se han encontrado solicitudes, revise los filtros proporcionados."
            };
        }
        internal ResponseSimple MotivoSubmotivoReclamacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9923",
                textorespuesta = "Error. No se ha encontrado la combinación de motivo y submotivo de reclamación."
            };
        }

        internal ResponseSimple ContratoBuscadoReclamacion()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9922",
                textorespuesta = "Error. El número de contrato introducido no existe o no tiene datos asociados."
            };
        }

        internal ResponseSimple ContactoSolicitanteNoExiste()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9921",
                textorespuesta = "Error. El contacto solicitante no existe"
            };
        }


        //

        internal ResponseSimple TipoDocumentoTitular()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9920",
                textorespuesta = "Error. El tipo de documento del titular no se ha encontrado."
            };
        }

        internal ResponseSimple PaisTitular()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9919",
                textorespuesta = "Error. El pais del titular no se ha encontrado."
            };
        }

        internal ResponseSimple TipoDocumentoContactoCobro()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9918",
                textorespuesta = "Error. El tipo de documento del contacto de cobro no se ha encontrado."
            };
        }

        internal ResponseSimple PaisContactoCobro()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9917",
                textorespuesta = "Error. El pais del del contacto de cobro no se ha encontrado."
            };
        }
        internal ResponseSimple TipoDocumentoContactoCorrespondencia()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9916",
                textorespuesta = "Error. El tipo de documento del contacto de correspondencia no se ha encontrado."
            };
        }

        internal ResponseSimple PaisContactoCorrespondencia()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9915",
                textorespuesta = "Error. El pais del del contacto de correspondencia no se ha encontrado."
            };
        }

        internal ResponseSimple NoDetalleDocumentoPafo()
        {
            return new ResponseSimple
            {
                codigorespuesta = "9914",
                textorespuesta = "Error. No hay detalle del documento de pago proporcionado."
            };
        }


    }
}
