﻿namespace AqualiaContactWS.Helper
{
    public class Municipio
    {
        public const string LogicalName = "hidrotec_municipio";
        public const string Id = "hidrotec_municipioid";

        public const string Nombre = "hidrotec_name";
        public const string IdMunicipio = "hidrotec_idmunicipio";
    }
}