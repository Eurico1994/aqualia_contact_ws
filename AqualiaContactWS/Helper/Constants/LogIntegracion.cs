﻿namespace AqualiaContactWS.Helper
{
    public class LogIntegracion
    {
        public const string LogicalName = "hidrotec_logerrorintegracion";
        public const string Id = "hidrotec_logerrorintegracionid";

        public const string Estado = "hidrotec_estado";
        public const string TipoIntegracion = "hidrotec_tipointegracionid";
        public const string NombreEntidad = "hidrotec_nombreentidad";
        public const string GuidRegistro = "hidrotec_guidregistro";
        public const string ParamEntrada = "hidrotec_parametrosdeentrada";
        public const string RespuestaServicio = "hidrotec_respuestadelservicio";
        public const string SistemaOrigen = "hidrotec_sistemaorigen";
        public const string SistemaDestino = "hidrotec_sistemadestino";
        public const string TipoError = "hidrotec_tipoerror";
        public const string LogErrorAnterior = "hidrotec_logerroranteriorid";
        public const string ReintentosRestantes = "hidrotec_reintentosrestantes";
        public const string Expediente = "hidrotec_expediente";

        public const string SistemaGesred = "GESRED";
        public const string SistemaDiversa = "DIVERSA";
        public const string SistemaCRM = "CRM";

        public class _Estado
        {
            public const int ErrorSinTratar = 0;
            public const int ErrorTratado = 1;
            public const int Correcto = 2;
            public const int Descartado = 3;
        }

        public class _TipoError
        {
            public const int Funcional = 1;
            public const int Tecnico = 2;
        }
    }
}