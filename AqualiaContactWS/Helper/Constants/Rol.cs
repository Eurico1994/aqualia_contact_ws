﻿namespace AqualiaContactWS.Helper
{
    public class Rol
    {
        public const string LogicalName = "hidrotec_maestroderoles";
        public const string Id = "hidrotec_maestroderolesid";

        public const string IdRol = "hidrotec_idrol";
        public const string Nombre = "hidrotec_name";
    }
}