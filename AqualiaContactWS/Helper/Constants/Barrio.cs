﻿namespace AqualiaContactWS.Helper
{
    public class Barrio
    {
        public const string LogicalName = "hidrotec_barrio";
        public const string Id = "hidrotec_barrioid";

        public const string Nombre = "hidrotec_name";
        public const string IdBarrio = "hidrotec_idbarrio";
    }
}