﻿namespace AqualiaContactWS.Helper
{
    public class Documento
    {
        public const string LogicalName = "hidrotec_documento";
        public const string Id = "hidrotec_documentoid";
        public const string Nombre = "hidrotec_name";

        public const string IdDocumento = "hidrotec_iddocumento";
        public const string EstadoInicial = "hidrotec_estadoinicial";

        public class _EstadoInicial
        {
            public const int PendienteRecibir = 0;
            public const int PendienteEnviarContacto = 1;
        }
    }
}