﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Helper.Model
{
    public class ResponseSimple
    {
        public string codigorespuesta { get; set; }
        public string textorespuesta { get; set; }
    }
}