﻿using AqualiaContactWS.Model.NEO_EnviarReclamacion;
using AqualiaContactWS.Model.NEO_ListadoMotivosReclamacion;
using AqualiaContactWS.Model.NEO_ListadoReclamaciones;
using AqualiaContactWS.Model.NEO_ListadoSolicitudes;
using AqualiaContactWS.Model.NEO_ListadoSubmotivosReclamacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReclamacion" in both code and config file together.
    [ServiceContract]
    public interface IReclamacion
    {
        [OperationContract]//ok
        NEO_ListadoMotivosReclamacionResponse NEO_ListadoMotivosReclamacion(NEO_ListadoMotivosReclamacionRequest request);

        [OperationContract]//ok
        NEO_ListadoSubmotivosReclamacionResponse NEO_ListadoSubmotivosReclamacion(NEO_ListadoSubmotivosReclamacionRequest request);

        [OperationContract]//ok
        NEO_EnviarReclamacionResponse NEO_EnviarReclamacion(NEO_EnviarReclamacionRequest request);

        [OperationContract]//ok
        NEO_ListadoReclamacionesResponse NEO_ListadoReclamaciones(NEO_ListadoReclamacionesRequest request);

        [OperationContract]//ok
        NEO_ListadoSolicitudesResponse NEO_ListadoSolicitudes(NEO_ListadoSolicitudesRequest request);
    }
}
