﻿using AqualiaContactWS.Model.NEO_AltaAveria;
using AqualiaContactWS.Model.NEO_ListadoPreguntasRespuestas;
using AqualiaContactWS.Model.NEO_RecuperarDatosContacto;
using AqualiaContactWS.Model.NEO_RecuperarDireccion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AqualiaContactWS
{
    // NOTE: You can use the "rtrtrt" command on the "Refactor" menu to change the interface name "IAverias" in both code and config file together.
    [ServiceContract]
    public interface IAverias
    {
        [OperationContract]//ok
        NEO_ListadoPreguntasRespuestasResponse NEO_ListadoPreguntasRespuestas(NEO_ListadoPreguntasRespuestasRequest request);

        [OperationContract]//ok
        NEO_AltaAveriaResponse NEO_AltaAveria(NEO_AltaAveriaRequest request);

        [OperationContract]//ok
        NEO_RecuperarDireccionResponse NEO_RecuperarDireccion(NEO_RecuperarDireccionRequest request);

        [OperationContract]//ok
        NEO_RecuperarDatosContactoResponse NEO_RecuperarDatosContacto(NEO_RecuperarDatosContactoRequest request);
    }
}
