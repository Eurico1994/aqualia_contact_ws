﻿using AqualiaContactWS.Model.NEO_ActivarFacturaElectronica;
using AqualiaContactWS.Model.NEO_LecturaContador;
using AqualiaContactWS.Model.NEO_ListadoContadoresContrato;
using AqualiaContactWS.Model.NEO_ListadoOperacionesContador;
using AqualiaContactWS.Model.NEO_SolicitarCambioTarifa;
using AqualiaContactWS.Model.NEO_SolicitarOperacionContador;
using System.ServiceModel;
using WS.Contratos.Model.NEO_ListadoContratos;
using WS.Contratos.Model.NEO_ListadoDetalleContrato;
using WS.Contratos.Model.NEO_ObtenerDatosContrato;
using WS.Contratos.Model.NEO_VincularContratosMultiusuario;

namespace WS.Contratos
{
    [ServiceContract]
    public interface IContratos
    {
        [OperationContract]//ok
        NEO_ListadoContratosResponse NEO_ListadoContratos(NEO_ListadoContratosRequest request);

        [OperationContract]//ok
        NEO_VincularContratosMultiusuarioResponse NEO_VincularContratosMultiusuario(NEO_VincularContratosMultiusuarioRequest request);

        [OperationContract]//ok
        NEO_ListadoDetalleContratoResponse NEO_ListadoDetalleContrato(NEO_ListadoDetalleContratoRequest request);

        [OperationContract]//ok
        NEO_ObtenerDatosContratoResponse NEO_ObtenerDatosContrato(NEO_ObtenerDatosContratoRequest request);

        [OperationContract]//ok
        NEO_SolicitarCambioTarifaResponse NEO_SolicitarCambioTarifa(NEO_SolicitarCambioTarifaRequest request);

        [OperationContract]
        NEO_ListadoContadoresContratoResponse NEO_ListadoContadoresContrato(NEO_ListadoContadoresContratoRequest request);

        [OperationContract]//ok
        NEO_LecturaContadorResponse NEO_LecturaContador(NEO_LecturaContadorRequest request);

        [OperationContract]//ok
        NEO_ListadoOperacionesContadorResponse NEO_ListadoOperacionesContador(NEO_ListadoOperacionesContadorRequest request);

        [OperationContract]//ok
        NEO_SolicitarOperacionContadorResponse NEO_SolicitarOperacionContador(NEO_SolicitarOperacionContadorRequest request);

        [OperationContract]//ok
        NEO_ActivarFacturaElectronicaResponse NEO_ActivarFacturaElectronica(NEO_ActivarFacturaElectronicaRequest request);
    }
}

