﻿using AqualiaContactWS.Model.NEO_EnviarIncidencias;
using AqualiaContactWS.Model.NEO_ListadoTipoIncidencias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AqualiaContactWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IConfiguracion" in both code and config file together.
    [ServiceContract]
    public interface IConfiguracion
    {
        [OperationContract]//ok
        NEO_ListadoTipoIncidenciasResponse NEO_ListadoTipoIncidencias(NEO_ListadoTipoIncidenciasRequest request);
        [OperationContract]//ok
        NEO_EnviarIncidenciasResponse NEO_EnviarIncidencias(NEO_EnviarIncidenciasRequest request);
    }
}
